<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('permalink_helper');
		$this->load->model('webadmin_model');
		$this->load->model('sekolah_model');
		$this->load->model('rkas_model');
		$this->load->library('upload');
		//$this->load->library('pdf');
	}


	function set_password(){
		$q = $this->db->get('sekolah');
		if($q){
			foreach ($q->result() as $row) {
				$data['password'] = md5($row->npsn);
				$this->db->where('npsn',$row->npsn);
				$this->db->update('sekolah',$data);
			}
		}
	}

	public function ajax(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Dashboard';
		$data['jhsmall'] = 'Operator Sekolah';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'sekolah/ajax';
		$this->load->view('sekolah/layout',$data);
	}

	public function data_belanja($id,$belanja){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['program'] = $this->sekolah_model->data_program_per_id($id,$belanja);
		if($belanja == 1){
			$belanja_id = $belanja;
		}else{
			$belanja_id = '2,3';
		}
		$data['belanja_id'] = $belanja_id;
		$this->load->view('sekolah/data_belanja_2019',$data);
	}

	public function add_tes()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['nama'] = str_replace(".","",$this->input->post('nama')); 
		$sql = $this->db->insert('ajax',$data);
		if($sql){
			echo json_encode($sql);
		}
		
	}


	//PDF

	public function cetak(){
	    ob_start();
	    require_once('./application/third_party/html2pdf/html2pdf.class.php');
	    $data['siswa'] = "hello world";
	    $this->load->view('sekolah/cetak_rkas_pdf');
	    $html = ob_get_contents();
	    

	    $pdf = new HTML2PDF('L','Legal','en');
	    $pdf->WriteHTML($html);
	    ob_end_clean();
	    $pdf->Output('Data Siswa.pdf', 'D');

	}


	public function pdf() {	
		$pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        //$pdf->WriteHTML($html);
        // mencetak string 
        $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUSAN NEEGRI 2 LANGSA',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'DAFTAR SISWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'NIM',1,0);
        $pdf->Cell(85,6,'NAMA MAHASISWA',1,0);
        $pdf->Cell(27,6,'NO HP',1,0);
        $pdf->Cell(25,6,'TANGGAL LHR',1,1);
        $pdf->SetFont('Arial','',10);
        // $mahasiswa = $this->db->get('mahasiswa')->result();
        // foreach ($mahasiswa as $row){
        //     $pdf->Cell(20,6,$row->nim,1,0);
        //     $pdf->Cell(85,6,$row->nama_lengkap,1,0);
        //     $pdf->Cell(27,6,$row->no_hp,1,0);
        //     $pdf->Cell(25,6,$row->tanggal_lahir,1,1); 
        // }
        $pdf->Output();
		
	}

	public function generate($html, $filename='', $stream=TRUE, $paper = 'A4', $orientation = "portrait")
	 	{
	 		// require_once(FCPATH.'application/third_party/dompdf/dompdf_config.inc.php');
	 		// require_once("./application/third_party/dompdf/include/autoload.inc.php");
			//use Dompdf\Dompdf;
			require_once("./vendor/dompdf/dompdf/autoload.inc.php");
			//use Dompdf\Dompdf;

		    $dompdf = new DOMPDF();
		    $dompdf->load_html($html);
		    $dompdf->set_paper($paper, $orientation);
		    $dompdf->render();
		    if ($stream) {
		        $dompdf->stream($filename.".pdf", array("Attachment" => 0));
		    } else {
		        return $dompdf->output();
		    }
	  	}


	function pdfrkas()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$tahun = $this->session->userdata('token_tahun');
		$regulasi = $this->sekolah_model->get_regulasi_login($tahun);
		$acak = rand(10000,100000);
		ob_start();
	    require_once('./application/third_party/html2pdf/html2pdf.class.php');
		$data['siswa'] = "hello world";
		if($regulasi == 3){
			$this->load->view('sekolah/cetak_rkas_pdf_3');
		}else{
			$this->load->view('sekolah/cetak_rkas_pdf');
		}
	    
	    $html = ob_get_contents();
	    $pdf = new HTML2PDF('L','Legal','en');
	    $pdf->WriteHTML($html);
	    ob_end_clean();
	    $pdf->Output($acak.'_'.$tahun.'.pdf', 'D');
		
	}
	//END PDF

	function json(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$regulasi_id = $this->session->userdata('token_regulasi');
		$kode = $this->input->post('id');
		$pecah = explode("-", $kode);
		$kd = $pecah[1];
		/*$this->db->where('program_id',$kode);
		$sql2 = $this->db->get('sub_program')->result();
		foreach ($sql2 as $row2) {
		}*/

		$sql = $this->db->query("select * from komponen_pembiayaan where komponen_kode in($kd) and regulasi_id='$regulasi_id'")->result();

		$array = array();
		foreach ($sql as $row) {
			array_push($array, $row);
		}
		echo json_encode($array);
	}

	public function index()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Dashboard';
		$data['jhsmall'] = 'Operator Sekolah';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'sekolah/home';
		$this->load->view('sekolah/layout',$data);			
	}
	
	//aset
	public function laporan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Data KIB-B';
		$data['jhsmall'] = 'Aset';
		$data['bc'] = "<li><a href='#'>Aset</a></li>";
		$data['conten'] = 'sekolah/laporan_realisai';
		$this->load->view('sekolah/layout',$data);			
	}
	public function kibb()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$data['totalaset'] = $this->sekolah_model->totalkibb($sid);
		$data['jh1'] = 'Data KIB-B';
		$data['jhsmall'] = 'Aset';
		$data['bc'] = "<li><a href='#'>Aset</a></li>";
		$data['conten'] = 'sekolah/kib_b';
		$this->load->view('sekolah/layout',$data);			
	}
	
	public function kibbtes()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Data KIB-B';
		$data['jhsmall'] = 'Aset';
		$data['bc'] = "<li><a href='#'>Aset</a></li>";
		$data['conten'] = 'sekolah/kib_b';
		$this->load->view('sekolah/layout',$data);			
	}
	
	public function datakibb()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$page = $this->input->post("page",true);
		$rows = $this->input->post("rows",true);
		$kodebarang = $this->input->post("kodebarang",true);
		$namabarang = $this->input->post("namabarang",true);
		$offset = ($page-1)*$rows;
		if($kodebarang)
			$this->db->like('kode_barang',$kodebarang);
		if($namabarang)
			$this->db->like('nama_barang',$namabarang);

		$this->db->where('sekolah_id',$sid);
		$this->db->select("count(*) as jum");
		$result["total"] = $this->db->get("kib_b")->row()->jum;

		if($kodebarang)
			$this->db->like('kode_barang',$kodebarang);
		if($namabarang)
			$this->db->like('nama_barang',$namabarang);

		$this->db->limit($rows,$offset);

		$this->db->select("*,sumber_dana.nama_sumber_dana, kib_b.id as id_new, format(harga_perolehan,0) as hargaperolehan_new, format(harga_estimasi,0) as hargaestimasi_new, format(harga_perolehan,0) as jumlah_perolehan ,format(harga_estimasi,0) as jumlah_estimasi, format((jumlah_barang * harga_perolehan),0) as total_harga");
		//+(jumlah_barang * harga_estimasi) //(jumlah_barang * //(jumlah_barang * 
		$this->db->join('sekolah','sekolah.sekolah_id=kib_b.sekolah_id','left');
		$this->db->join('sumber_dana','kib_b.asal_usul=sumber_dana.dana_id');
		$this->db->where('kib_b.sekolah_id',$sid);
		$this->db->order_by('kib_b.id','desc');
		$result["rows"] = $this->db->get("kib_b")->result();
		echo json_encode($result);
	}

	public function aksi_kibb(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$id = $this->input->post('id');
		$pecah = explode(',', $id);
		$data['tombol'] = $pecah[0];
		$data['dataid'] = $pecah[1];
		$data['sumberdana'] = $this->sekolah_model->sumberdana();
		if($pecah[1] != 0){
			if($pecah[0] == 2){
				$data['datakibb'] = $this->sekolah_model->tampilid('kib_b',$pecah[1]);
			}else{
				$data['datakibb'] = $this->sekolah_model->tampilid('kib_b',$pecah[1]);
			}
				
		}

		$this->load->view('sekolah/form_kib_b',$data);
	}

	public function add_kibb(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['sekolah_id'] = $this->session->userdata('token_sekolah_id');
		$data['kode_barang'] = $this->input->post('kodebarang');
		$data['no_register'] = $this->input->post('noregister');
		$data['nama_barang'] = $this->input->post('namabarang');
		$data['kondisi'] = $this->input->post('kondisi');
		$data['jumlah_barang'] = $this->input->post('jumlah');
		$data['satuan'] = $this->input->post('satuan');
		$data['ukuran_cc'] = $this->input->post('ukuran');
		$data['merk_type'] = $this->input->post('merk');
		$data['bahan'] = $this->input->post('bahan');
		$data['pabrik'] = $this->input->post('pabrik');
		$data['rangka'] = $this->input->post('rangka');
		$data['mesin'] = $this->input->post('mesin');
		$data['no_polisi'] = $this->input->post('nopolisi');
		$data['bpkb'] = $this->input->post('bpkb');
		$data['tahun_pembelian'] = $this->input->post('tahun');
		$data['asal_usul'] = $this->input->post('asal');
		$data['harga_perolehan'] = $this->input->post('harga');
		// $data['harga_estimasi'] = $this->input->post('hargaestimasi');
		$data['ket'] = $this->input->post('ket');

		$sql = $this->sekolah_model->simpan('kib_b',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			    <strong>Success!</strong> Data berhasil disimpan.
			  </div>");
			redirect(base_url('sekolah/kibb'));
		}
	}

	public function edit_kibb($id){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$idkibb = $this->input->post('idkibb');
		$data['sekolah_id'] = $this->session->userdata('token_sekolah_id');
		$data['kode_barang'] = $this->input->post('kodebarang');
		$data['no_register'] = $this->input->post('noregister');
		$data['nama_barang'] = $this->input->post('namabarang');
		$data['kondisi'] = $this->input->post('kondisi');
		$data['jumlah_barang'] = $this->input->post('jumlah');
		$data['satuan'] = $this->input->post('satuan');
		$data['ukuran_cc'] = $this->input->post('ukuran');
		$data['merk_type'] = $this->input->post('merk');
		$data['bahan'] = $this->input->post('bahan');
		$data['pabrik'] = $this->input->post('pabrik');
		$data['rangka'] = $this->input->post('rangka');
		$data['mesin'] = $this->input->post('mesin');
		$data['no_polisi'] = $this->input->post('nopolisi');
		$data['bpkb'] = $this->input->post('bpkb');
		$data['tahun_pembelian'] = $this->input->post('tahun');
		$data['asal_usul'] = $this->input->post('asal');
		$data['harga_perolehan'] = $this->input->post('harga');
		//$data['harga_estimasi'] = $this->input->post('hargaestimasi');
		$data['ket'] = $this->input->post('ket');
		$sql = $this->sekolah_model->edit('kib_b',$data,$idkibb);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			    <strong>Success!</strong> Data berhasil diubah.
			  </div>");
			redirect(base_url('sekolah/kibb'));
		}
	}

	public function hapus_kibb($id){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sql = $this->sekolah_model->hapus('kib_b',$id);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			    <strong>Success!</strong> Data berhasil dihapus.
			  </div>");
			redirect(base_url('sekolah/kibb'));
		}
	}


	public function kibe(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$data['totalaset'] = $this->sekolah_model->totalkibe($sid);
		$data['jh1'] = 'Data KIB-E';
		$data['jhsmall'] = 'Aset';
		$data['bc'] = "<li><a href='#'>Aset</a></li>";
		$data['conten'] = 'sekolah/kib_e';
		$this->load->view('sekolah/layout',$data);
	}
	
	public function kibetes(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Data KIB-E';
		$data['jhsmall'] = 'Aset';
		$data['bc'] = "<li><a href='#'>Aset</a></li>";
		$data['conten'] = 'sekolah/kib_e';
		$this->load->view('sekolah/layout',$data);
	}

	public function datakibe()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$page = $this->input->post("page",true);
		$rows = $this->input->post("rows",true);
		$kodebarang = $this->input->post("kodebarang",true);
		$namabarang = $this->input->post("namabarang",true);
		$offset = ($page-1)*$rows;
		if($kodebarang)
			$this->db->like('kode_barang',$kodebarang);
		if($namabarang)
			$this->db->like('nama_barang',$namabarang);

		$this->db->where('sekolah_id',$sid);
		$this->db->select("count(*) as jum");
		$result["total"] = $this->db->get("kib_e")->row()->jum;

		if($kodebarang)
			$this->db->like('kode_barang',$kodebarang);
		if($namabarang)
			$this->db->like('nama_barang',$namabarang);

		$this->db->limit($rows,$offset);
		$this->db->select("*,sumber_dana.nama_sumber_dana, kib_e.id as id_new, format(harga_perolehan,0) as harga_perolehan_new, format(harga_estimasi,0) as harga_estimasi_new, format((jumlah * harga_perolehan),0) as total_harga");
		//+(jumlah * harga_estimasi)
		$this->db->join('sekolah','sekolah.sekolah_id=kib_e.sekolah_id','left');
		$this->db->join('sumber_dana','kib_e.asal_usul=sumber_dana.dana_id');
		$this->db->where('kib_e.sekolah_id',$sid);
		$this->db->order_by('kib_e.id','desc');
		$result["rows"] = $this->db->get("kib_e")->result();
		echo json_encode($result);
	}

	public function aksi_kibe(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$id = $this->input->post('id');
		$pecah = explode(',', $id);
		$data['tombol'] = $pecah[0];
		$data['dataid'] = $pecah[1];
		$data['sumberdana'] = $this->sekolah_model->sumberdana();
		if($pecah[1] != 0){
			if($pecah[0] == 2){
				$data['datakibe'] = $this->sekolah_model->tampilid('kib_e',$pecah[1]);
			}else{
				$data['datakibe'] = $this->sekolah_model->tampilid('kib_e',$pecah[1]);	
			}
				
		}
		
		$this->load->view('sekolah/form_kib_e',$data);
	}

	public function add_kibe(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['sekolah_id'] = $this->session->userdata('token_sekolah_id');
		$data['kode_barang'] = $this->input->post('kodebarang');
		$data['no_register'] = $this->input->post('noregister');
		$data['nama_barang'] = $this->input->post('namabarang');
		$data['kondisi'] = $this->input->post('kondisi');
		$data['judul'] = $this->input->post('judul');
		$data['jumlah'] = $this->input->post('jumlah');
		$data['spesifikasi'] = $this->input->post('spesifikasi');
		$data['asal_daerah'] = $this->input->post('asaldaerah');
		$data['pencipta'] = $this->input->post('pencipta');
		$data['bahan'] = $this->input->post('bahan');
		$data['tahun_pembelian'] = $this->input->post('tahuncetak');
		$data['asal_usul'] = $this->input->post('asal');
		$data['harga_perolehan'] = $this->input->post('hargaperolehan');
// 		$data['harga_estimasi'] = $this->input->post('hargaestimasi');
		$data['ket'] = $this->input->post('ket');

		$sql = $this->sekolah_model->simpan('kib_e',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			    <strong>Success!</strong> Data berhasil disimpan.
			  </div>");
			redirect(base_url('sekolah/kibe'));
		}
	}

	public function edit_kibe($id){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$idkibe = $this->input->post('idkibe');
		$data['sekolah_id'] = $this->session->userdata('token_sekolah_id');
		$data['kode_barang'] = $this->input->post('kodebarang');
		$data['no_register'] = $this->input->post('noregister');
		$data['nama_barang'] = $this->input->post('namabarang');
		$data['kondisi'] = $this->input->post('kondisi');
		$data['judul'] = $this->input->post('judul');
		$data['jumlah'] = $this->input->post('jumlah');
		$data['spesifikasi'] = $this->input->post('spesifikasi');
		$data['asal_daerah'] = $this->input->post('asaldaerah');
		$data['pencipta'] = $this->input->post('pencipta');
		$data['bahan'] = $this->input->post('bahan');
		$data['tahun_pembelian'] = $this->input->post('tahuncetak');
		$data['asal_usul'] = $this->input->post('asal');
		$data['harga_perolehan'] = $this->input->post('hargaperolehan');
// 		$data['harga_estimasi'] = $this->input->post('hargaestimasi');
		$data['ket'] = $this->input->post('ket');
		$sql = $this->sekolah_model->edit('kib_e',$data,$idkibe);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			    <strong>Success!</strong> Data berhasil diubah.
			  </div>");
			redirect(base_url('sekolah/kibe'));
		}
	}

	public function hapus_kibe($id){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sql = $this->sekolah_model->hapus('kib_e',$id);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			    <strong>Success!</strong> Data berhasil dihapus.
			  </div>");
			redirect(base_url('sekolah/kibe'));
		}
	}

	//end aset

	public function cetakrkas()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$tahun = $this->session->userdata('token_tahun');
		$regulasi = $this->sekolah_model->get_regulasi_login($tahun);
		$data['jh1'] = 'Cetak RKA Sekolah';
		$data['jhsmall'] = 'Cetak';
		$data['bc'] = "<li><a href='#'>Layout</a></li>";
		if($regulasi == 3){
			$data['conten'] = 'sekolah/cetak_rkas_3';
		}else{
			$data['conten'] = 'sekolah/cetak_rkas';
		}
		$this->load->view('sekolah/layout',$data);			
	}

	public function excelrkas()
	{	
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$tahun = $this->session->userdata('token_tahun');
		$regulasi = $this->sekolah_model->get_regulasi_login($tahun);
		$acak = rand(100000,100000000);
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$acak.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$data['jh1'] = 'Cetak RKA Sekolah';
		$data['jhsmall'] = 'Cetak';
		$data['bc'] = "<li><a href='#'>Layout</a></li>";
		//$data['conten'] = 'sekolah/cetak_rkas';
		if($regulasi == 3){
			$this->load->view('sekolah/cetak_rkas_excel_3',$data);
		}else{
			$this->load->view('sekolah/cetak_rkas_excel',$data);
		}
	}

	function login(){
		if(!empty($this->session->userdata('token_login_sekolah'))){
			redirect(base_url('sekolah'));
		}else{
			$this->load->view('sekolah/login');
		}
	}
	function logout(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$this->session->sess_destroy();
		redirect(base_url('sekolah/login'));
	}
	function getlogin(){
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$tahun = $this->input->post('tahun');
		$this->sekolah_model->m_get_login($u,$p,$tahun);
	}


	//RKA SEKOLAH
	public function ubahpassword(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$this->db->where('sekolah_id',$key);
		$sql = $this->db->get('sekolah');
		if($sql){
			$data['ubahp'] = $sql->result();
		}
		$this->load->view('sekolah/ubah_password',$data);
	}

	public function proses_ubah_password(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$plama = md5($this->input->post('plama'));
		$key = $this->session->userdata('token_sekolah_id');

		$cek = $this->db->query("SELECT * FROM sekolah where password='$plama' and sekolah_id='$key' LIMIT 1");
		if($cek->num_rows() > 0){
			$data['password'] = md5($this->input->post('pbaruulang'));
			$this->db->where('sekolah_id',$key);
			$sql = $this->db->update('sekolah',$data);
			if($sql){
				//$sess = $this->session->userdata('token_login_sekolah');
				$dataa['sekolah_id'] = $key;
				$this->db->insert('password_log',$dataa);

				$this->session->sess_destroy();
				redirect(base_url('sekolah'));
			}
		}else{
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				<strong>Pemberitahuan!</strong> Data Gagal Disimpan, Password yang Lama Tidak Cocok.
				</div>");
			redirect(base_url('sekolah'));		
		}
	}
	public function bosnas(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Entry RKAS BOSNAS';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'sekolah/bosnas';
		$this->load->view('sekolah/layout',$data);
	}


	public function barangjasa_lama(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['cp'] = $this->sekolah_model->cekprogram();
		$data['jh1'] = 'RKAS Belanja Barang dan Jasa';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['belanja_id'] = '1';
		$data['conten'] = 'sekolah/belanja_barangjasa';
		$this->load->view('sekolah/layout',$data);
	}

	public function barangjasa(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['cp'] = $this->sekolah_model->cekprogram();
		$data['jh1'] = 'RKAS Belanja Barang dan Jasa';
		$data['jhsmall'] = 'RKAS Bosnas';
		$data['belanja_id'] = '1';
		//$data['total_per_standar'] = $this->sekolah_model->get_total_per_standar();
		$data['standar_nasional'] = $this->sekolah_model->get_standar_nasional();
		$data['conten'] = 'sekolah/belanja_barangjasa_2019';
		$this->load->view('sekolah/layout',$data);
	}

	public function rincianbelanja($id=''){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Rincian Belanja Barang dan Jasa';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'sekolah/rincian_belanja';

		$this->db->where('standar_id',$id);
		$sql = $this->db->get('sub_standar');
		if($sql){
			$data['sub_standar'] = $sql->result();
		}

		$this->load->view('sekolah/layout',$data);
	}

	public function tambahrincian()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$ambilid = $this->input->post('id');
		$pecah = explode(",", $ambilid);
		$data['save_method'] = $pecah[0];

		$data['key'] = $pecah[1]; //$this->input->post('id');
		$data['kode'] = $this->input->post('kode');
		$key = $pecah[1]; //$this->input->post('id');
		$data['sub_program'] = $this->webadmin_model->sub_kegiatan($key);

		if($pecah[0] == "er"){
			$this->db->where('rincian_id',$key);
			$this->db->where('soft_delete',0);
			$sql = $this->db->get('rincian_belanja');
			if($sql){
				$data['rincian'] = $sql->result();
			}	
		}elseif ($pecah[0] == "trh") {
			# code...
			$this->db->where('rincian_id',$key);
			$this->db->where('soft_delete',0);
			$sql = $this->db->get('rincian_belanja');
			if($sql){
				$data['head'] = $sql->result();
			}
		}elseif($pecah[0] == "eh"){
			# code...
			$this->db->where('rincian_id',$key);
			$this->db->where('soft_delete',0);
			$sql = $this->db->get('rincian_belanja');
			if($sql){
				$data['rincian'] = $sql->result();
			}
		}elseif($pecah[0] == "erh"){
			# code...
			$this->db->where('header_id',$key);
			$this->db->where('soft_delete',0);
			$sql = $this->db->get('rincian_header');
			if($sql){
				$data['rincian'] = $sql->result();
			}
		}


		$this->load->view('sekolah/tambah_rincian',$data);
		
	}

	public function editrincian()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['key'] = $this->input->post('id3');
		$data['kode'] = $this->input->post('kode');
		$key = $this->input->post('id3');
		$this->db->where('rincian_id',$key);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('rincian_belanja');
		if($sql){
			$data['rincian'] = $sql->result();
		}

		$this->load->view('sekolah/edit_rincian',$data);
		
	}

	public function editheader()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['key'] = $this->input->post('id3');
		$data['kode'] = $this->input->post('kode');
		$key = $this->input->post('id3');
		$this->db->where('rincian_id',$key);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('rincian_belanja');
		if($sql){
			$data['rincian'] = $sql->result();
		}

		$this->load->view('sekolah/edit_header',$data);
		
	}

	public function proses_edit_rincian(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key= $this->input->post('id');
		$kode = $this->input->post('kode');
		if($kode == 1){$link="barangjasa";}else{$link="modal";}
		$data['detail_rincian'] = $this->input->post('nama');
		$data['volume'] = str_replace(".", "", $this->input->post('volume'));
		$data['satuan'] = $this->input->post('satuan');
		$data['harga_satuan'] = str_replace(".", "", $this->input->post('hargasatuan'));
		$data['tw1'] = str_replace(".", "", $this->input->post('tw1'));
		$data['tw2'] = str_replace(".", "", $this->input->post('tw2'));	
		$data['tw3'] = str_replace(".", "", $this->input->post('tw3'));
		$data['tw4'] = str_replace(".", "", $this->input->post('tw4'));

		$this->db->where('rincian_id',$key);
		$sql = $this->db->update('rincian_belanja',$data);
		if($sql){
			// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			// 	<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			// 	<strong>Pemberitahuan!</strong> Data Berhasil Diubah.
			// 	</div>");
			// redirect(base_url('sekolah/'.$link));
			echo json_encode($sql);
		}
	}

	public function proses_edit_header(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key= $this->input->post('id');
		$kode = $this->input->post('kode');
		if($kode == 1){$link="barangjasa";}else{$link="modal";}
		$data['detail_rincian'] = $this->input->post('nama');

		$this->db->where('rincian_id',$key);
		$sql = $this->db->update('rincian_belanja',$data);
		if($sql){
			// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			// 	<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			// 	<strong>Pemberitahuan!</strong> Data Berhasil Diubah.
			// 	</div>");
			// redirect(base_url('sekolah/'.$link));
			echo json_encode($sql);
		}
	}

	public function proses_edit_rincian_header(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key= $this->input->post('id');
		$kode = $this->input->post('kode');
		if($kode == 1){$link="barangjasa";}else{$link="modal";}
		$data['header_rincian'] = $this->input->post('nama');
		$data['volume_hr'] = str_replace(".", "", $this->input->post('volume'));
		$data['satuan_hr'] = $this->input->post('satuan');
		$data['harga_satuan_hr'] = str_replace(".", "", $this->input->post('hargasatuan'));
		$data['htw1'] = str_replace(".", "", $this->input->post('tw1'));
		$data['htw2'] = str_replace(".", "", $this->input->post('tw2'));
		$data['htw3'] = str_replace(".", "", $this->input->post('tw3'));
		$data['htw4'] = str_replace(".", "", $this->input->post('tw4'));

		$this->db->where('header_id',$key);
		$sql = $this->db->update('rincian_header',$data);
		if($sql){
			// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			// 	<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			// 	<strong>Pemberitahuan!</strong> Data Berhasil Diubah.
			// 	</div>");
			// redirect(base_url('sekolah/'.$link));
			echo json_encode($sql);
		}
	}

	public function editrincianheader()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['key'] = $this->input->post('id3');
		$data['kode'] = $this->input->post('kode');
		$key = $this->input->post('id3');
		$this->db->where('header_id',$key);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('rincian_header');
		if($sql){
			$data['rincian'] = $sql->result();
		}

		$this->load->view('sekolah/edit_rincian_header',$data);
		
	}

	public function tambahrincianheader()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['key'] = $this->input->post('id2');
		$data['kode'] = $this->input->post('kode');
		$key = $this->input->post('id2');
		//$data['sub_program'] = $this->sekolah_model->sub_kegiatan($key);

		$this->db->where('rincian_id',$key);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('rincian_belanja');
		if($sql){
			$data['head'] = $sql->result();
		}
		$this->load->view('sekolah/tambah_rincian_header',$data);
		
	}

	public function add_rincian()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sr = $this->input->post('sr');
		$kode = $this->input->post('kode');


		//belanja ID 3
		if($kode == 2){
			$kdprogram = $this->input->post('program');
			$programarray = array(100,143,144,381,424,425);
			if(in_array($kdprogram, $programarray)){
				$belanjaid = 3;
			}else{
				$belanjaid = 2;
			}	
		}else{
			$belanjaid = 1;
		}
		
		if($kode == 1){$link="barangjasa";}else{$link="modal";}
		if($sr == 1){
			//header
			$key = $this->input->post('id');
			$data['program_id'] = $this->input->post('program');
			$data['detail_rincian'] = $this->input->post('nama');
			$data['status_rincian'] = $this->input->post('sr');
			$data['komponen_kode'] = $this->input->post('skb');
			$data['volume'] = 0;
			$data['satuan'] = "-";
			$data['harga_satuan'] = 0;
			$data['tw1'] = 0;
			$data['tw2'] = 0;
			$data['tw3'] = 0;
			$data['tw4'] = 0;
			$data['sekolah_id'] = $this->session->userdata('token_sekolah_id');
			$data['tahun'] = $this->session->userdata('token_tahun');
			$data['belanja_id'] = $belanjaid; //$this->input->post('kode');


			

			$sql = $this->db->insert('rincian_belanja',$data);
			if($sql){
				// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				// 	    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				// 	    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				// 	  </div>");
				//redirect(base_url('sekolah/'.$link));
				echo json_encode($sql);
			}
		}else{
			//detail
			$key = $this->input->post('id');
			$data['program_id'] = $this->input->post('program');
			$data['detail_rincian'] = $this->input->post('nama');
			$data['status_rincian'] = $this->input->post('sr');
			$data['komponen_kode'] = $this->input->post('skb');
			$data['volume'] = str_replace(".", "", $this->input->post('volume'));
			$data['satuan'] = $this->input->post('satuan');
			$data['harga_satuan'] = str_replace(".", "", $this->input->post('hargasatuan'));
			$data['tw1'] = str_replace(".", "", $this->input->post('tw1'));
			$data['tw2'] = str_replace(".", "", $this->input->post('tw2'));
			$data['tw3'] = str_replace(".", "", $this->input->post('tw3'));
			$data['tw4'] = str_replace(".", "", $this->input->post('tw4'));
			$data['sekolah_id'] = $this->session->userdata('token_sekolah_id');
			$data['tahun'] = $this->session->userdata('token_tahun');
			$data['belanja_id'] = $belanjaid; //$this->input->post('kode');
			

			$sql = $this->db->insert('rincian_belanja',$data);
			if($sql){
				// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				// 	    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				// 	    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				// 	  </div>");
				//redirect(base_url('sekolah/'.$link));
				echo json_encode($sql);
			}
		}
			
	}

	public function add_rincian_header()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$kode = $this->input->post('kode');
		if($kode == 1){$link="barangjasa";}else{$link="modal";}
		$key = $this->input->post('id');
		$data['rincian_id'] = $key;
		$data['header_rincian'] = $this->input->post('nama');
		$data['volume_hr'] = str_replace(".", "", $this->input->post('volume'));
		$data['satuan_hr'] = $this->input->post('satuan');
		$data['harga_satuan_hr'] = str_replace(".", "", $this->input->post('hargasatuan'));
		$data['htw1'] = str_replace(".", "", $this->input->post('tw1'));
		$data['htw2'] = str_replace(".", "", $this->input->post('tw2'));
		$data['htw3'] = str_replace(".", "", $this->input->post('tw3'));
		$data['htw4'] = str_replace(".", "", $this->input->post('tw4'));
			

		$sql = $this->db->insert('rincian_header',$data);
		if($sql){
			// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			// 	    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			// 	    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
			// 	  </div>");
			// redirect(base_url('sekolah/'.$link));
			echo json_encode($sql);
		}
	}

	public function modal(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['cp'] = $this->sekolah_model->cekprogram();
		$data['jh1'] = 'RKAS Belanja Modal';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['belanja_id'] = '2'; //2,3
		//$data['belanja_id2'] = 3;
		$data['conten'] = 'sekolah/belanja_modal_2019';
		$data['standar_nasional'] = $this->sekolah_model->get_standar_nasional();
		$this->load->view('sekolah/layout',$data);
	}


	public function hapusrincian(){
		//$id,$kode
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		//$kode = $this->input->post('kode');
		//if($kode == 1){$link="barangjasa";}else{$link="modal";}
		$id = $this->input->post('id');
		$this->db->where('rincian_id',$id);
		$sql = $this->db->delete('rincian_belanja');
		if($sql){
			// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			// 	    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			// 	    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
			// 	  </div>");
			// redirect(base_url('sekolah/'.$link));
			echo json_encode($sql);
		}
	}
	public function hapusrincianheader(){
		//$id,$kode
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		//$kode = $this->input->post('kode');
		//if($kode == 1){$link="barangjasa";}else{$link="modal";}
		$id = $this->input->post('id');
		$this->db->where('header_id',$id);
		$sql = $this->db->delete('rincian_header');
		if($sql){
			// $this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
			// 	    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
			// 	    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
			// 	  </div>");
			// redirect(base_url('sekolah/'.$link));
			echo json_encode($sql);
		}
	}

	public function cek_belanja_bos($sekolahid,$tahun){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$getdata = $this->sekolah_model->total_belanja_bos($sekolahid,$tahun);
		$dt_totalbos = str_replace(array(',','.'),'',$getdata['total_bos']);
		$dt_totalbelanja = str_replace(array(',','.'),'',$getdata['total_belanja']);
		$data['total_bos'] = $dt_totalbos;
		$data['total_belanja'] = $dt_totalbelanja;
		$data['tahun'] = $this->session->userdata('token_tahun');
		if($dt_totalbelanja >= $dt_totalbos){
			//disable
			$data['tombol'] = "<a href='#' class='btn btn-success disabled'><i class='fa fa-plus-circle'></i> Rincian</a> <span id='load'></span><p><div class='alert alert-danger'><strong>Maaf!</strong> Anda tidak bisa menambahkan rincian belanja lagi, karena total belanja sudah melebihi pendapatan atau total belanja sudah sama dengan pendapatan.</div></p>";
		}else{
			//enable
			$data['tombol'] = false;

		}
		echo json_encode($data);
	}
	

	//RKA SEKOLAH

	public function pejabat()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$data['jh1'] = 'Pejabat Penandatangan';
		$data['jhsmall'] = 'Data Pejabat Penandatangan';
		$data['bc'] = "<li><a href='#'>Pejabat Penandatangan</a></li>";
		$data['conten'] = 'sekolah/pejabat';

		//$this->db->where('soft_delete','0');
		//$this->db->select(',* s.soft_delete as ket');
		// $this->db->join('kec','kec.kd_kec=s.kd_kec');
		// $this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$this->db->where('sekolah_id',$sid);
		$sql = $this->db->get('ttd_pejabat');
		$data['pejabat'] = $sql->result();

		$this->load->view('sekolah/layout',$data);		
	}

	public function aksi_pejabat()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			//$data['kec'] = $this->webadmin_model->kecamatan();
			$this->load->view('sekolah/add_pejabat');
		}else{
			
			$this->db->where('ttd_id',$pecah[1]);
			$sql = $this->db->get('ttd_pejabat');
			if($sql->num_rows() > 0){
				$data['pejabat'] = $sql->result();
			}

			$this->load->view('sekolah/edit_pejabat',$data);
		}
		
	}

	public function add_pejabat()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$data['nama_komite_sekolah'] = $this->input->post('ketuayayasan');
		$data['nip_komite_sekolah'] = $this->input->post('nipketua');
		$data['nama_kepsek'] = $this->input->post('kepsek');
		$data['nip_kepsek'] = $this->input->post('nipkepsek');
		$data['nama_bendahara_bos'] = $this->input->post('namabendahara');
		$data['nip_bendahara_bos'] = $this->input->post('nipbendahara');
		$data['periode_aktif'] = $this->input->post('tglaktif');
		$data['sekolah_id'] = $this->session->userdata('token_sekolah_id');

		
		$dataa['soft_delete'] = 1;
		$this->db->where('sekolah_id',$sid);
		$update = $this->db->update('ttd_pejabat',$dataa);

		$sql = $this->db->insert('ttd_pejabat',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('sekolah/pejabat'));
		}		
	
	}

	public function proses_edit_pejabat()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$idpejabat = $this->input->post('idpejabat');
		$sid = $this->session->userdata('token_sekolah_id');

		$data['nama_komite_sekolah'] = $this->input->post('ketuayayasan');
		$data['nip_komite_sekolah'] = $this->input->post('nipketua');
		$data['nama_kepsek'] = $this->input->post('kepsek');
		$data['nip_kepsek'] = $this->input->post('nipkepsek');
		$data['nama_bendahara_bos'] = $this->input->post('namabendahara');
		$data['nip_bendahara_bos'] = $this->input->post('nipbendahara');
		$data['periode_aktif'] = $this->input->post('tglaktif');

		if($this->input->post('status') == 0){
			$dataa['soft_delete'] = 1;
			$data['soft_delete'] = 0;
			$this->db->where('sekolah_id',$sid);
			$update = $this->db->update('ttd_pejabat',$dataa);
		}else{
			$data['soft_delete'] = 1;
		}
		
		$this->db->where('sekolah_id',$sid);
		$this->db->where('ttd_id',$idpejabat);
		$sql = $this->db->update('ttd_pejabat',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
				  </div>");
			redirect(base_url('sekolah/pejabat'));
		}
		
	}

	public function hapus_pejabat()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->uri->segment(3);
		$sid = $this->session->userdata('token_sekolah_id');
		$this->db->where('sekolah_id',$sid);
		$this->db->where('ttd_id',$key);

		$sql = $this->db->delete('ttd_pejabat');
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('sekolah/pejabat'));
		}
	}



	//prestasi
	public function jsonn(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$this->load->library('datatables');
		//$this->datatables->where('p.sekolah_id',$key);
		$this->datatables->select("left(p.tgl_mulai,4) as tahun, kl.nama_katlomba, p.jenis_lomba,p.tingkat_lomba,p.peringkat,p.nama_siswa,p.tempat_lomba,p.penyelenggara,p.tgl_mulai,p.tgl_akhir,p.prestasi_id, l.nama_lomba as namalomba, p.nama_lomba as uraian");
		$this->datatables->join('kategori_lomba kl', 'p.katlomba_id = kl.katlomba_id');
		$this->datatables->join('lomba l', 'kl.lomba_id = l.lomba_id');
		$this->datatables->from("prestasi p");
		$this->datatables->where("p.sekolah_id",$key);

		//$this->datatables->select("id_berita,judul,nama_kategori,berita.username,hari,tanggal");
        //$this->datatables->from("prestasi");
        //$this->datatables->join('kategori', 'berita.id_kategori = kategori.id_kategori');
        return print_r($this->datatables->generate());
	}

	public function prestasi(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Prestasi Sekolah';
		$data['jhsmall'] = 'Prestasi Sekolah';
		$data['conten'] = 'sekolah/prestasi';
		$this->load->view('sekolah/layout',$data);
	}

	public function tambahprestasi(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$tokensekolah = $this->session->userdata('token_sekolah_id');
		$data['lomba'] = $this->webadmin_model->lomba();
		if($key == 0){
			$this->load->view('sekolah/tambah_prestasi',$data);
		}else{
			$this->db->where('sekolah_id',$tokensekolah);
			$this->db->where('prestasi_id',$key);
			$sql = $this->db->get('prestasi');
			if($sql){
				$data['prestasi'] = $sql->result();
			}
			$this->load->view('sekolah/edit_prestasi',$data);
		}
		

	}

	public function add_prestasi()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$data['nama_lomba'] = $this->input->post('nama');
		$data['jenis_lomba'] = $this->input->post('jl');
		$data['nama_siswa'] = $this->input->post('namasiswa');
		$data['peringkat'] = $this->input->post('peringkat');
		$data['tingkat_lomba'] = $this->input->post('tingkat');
		$data['tempat_lomba'] = $this->input->post('tempat');
		$data['penyelenggara'] = $this->input->post('penyelenggara');
		$data['sertifikat'] = $this->input->post('sertifikat');
		$data['tgl_mulai'] = $this->input->post('tglmulai');
		$data['tgl_akhir'] = $this->input->post('tglakhir');
		$data['sekolah_id'] = $key;


		$sql = $this->db->insert('prestasi',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('sekolah/prestasi'));
		}	
	}

	public function upload(){
		$this->load->view('sekolah/upload');	
	}

	function upload_image(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
        $config['upload_path'] = './assets/prestasi/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
        $this->upload->initialize($config);
        if(!empty($_FILES['sertifikat']['tmp_name'])){
 
            if ($this->upload->do_upload('sertifikat')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/prestasi/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './assets/prestasi/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];
                //$judul=$this->input->post('xjudul');

                
				$data['nama_lomba'] = $this->input->post('nama');
				$data['jenis_lomba'] = $this->input->post('jl');
				$data['nama_siswa'] = $this->input->post('namasiswa');
				$data['peringkat'] = $this->input->post('peringkat');
				$data['tingkat_lomba'] = $this->input->post('tingkat');
				$data['tempat_lomba'] = $this->input->post('tempat');
				$data['penyelenggara'] = $this->input->post('penyelenggara');
				$data['sertifikat'] = $gambar;
				$data['tgl_mulai'] = $this->input->post('tglmulai');
				$data['tgl_akhir'] = $this->input->post('tglakhir');
				$data['sekolah_id'] = $key;
				$data['katlomba_id'] = $this->input->post('lomba');

				$sql = $this->db->insert('prestasi',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/prestasi'));
				}

            }
                      
        }else{
            	//echo "Image yang diupload kosong";
            	$data['nama_lomba'] = $this->input->post('nama');
				$data['jenis_lomba'] = $this->input->post('jl');
				$data['nama_siswa'] = $this->input->post('namasiswa');
				$data['peringkat'] = $this->input->post('peringkat');
				$data['tingkat_lomba'] = $this->input->post('tingkat');
				$data['tempat_lomba'] = $this->input->post('tempat');
				$data['penyelenggara'] = $this->input->post('penyelenggara');
				//$data['sertifikat'] = $gambar;
				$data['tgl_mulai'] = $this->input->post('tglmulai');
				$data['tgl_akhir'] = $this->input->post('tglakhir');
				$data['sekolah_id'] = $key;
				$data['katlomba_id'] = $this->input->post('lomba');

				$sql = $this->db->insert('prestasi',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/prestasi'));
				}
        }
                 
    }

    function edit_upload_image($id){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
        $config['upload_path'] = './assets/prestasi/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
        $this->upload->initialize($config);
        if(!empty($_FILES['sertifikat']['tmp_name'])){
 
            if ($this->upload->do_upload('sertifikat')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/prestasi/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './assets/prestasi/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];
                //$judul=$this->input->post('xjudul');

                
				$data['nama_lomba'] = $this->input->post('nama');
				$data['jenis_lomba'] = $this->input->post('jl');
				$data['nama_siswa'] = $this->input->post('namasiswa');
				$data['peringkat'] = $this->input->post('peringkat');
				$data['tingkat_lomba'] = $this->input->post('tingkat');
				$data['tempat_lomba'] = $this->input->post('tempat');
				$data['penyelenggara'] = $this->input->post('penyelenggara');
				$data['sertifikat'] = $gambar;
				$data['tgl_mulai'] = $this->input->post('tglmulai');
				$data['tgl_akhir'] = $this->input->post('tglakhir');
				$data['katlomba_id'] = $this->input->post('lomba');
				//$data['sekolah_id'] = $key;

				$this->db->where('prestasi_id',$id);
				$sql = $this->db->update('prestasi',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/prestasi'));
				}

            }
                      
        }else{
            	//echo "Image yang diupload kosong";
            	$data['nama_lomba'] = $this->input->post('nama');
				$data['jenis_lomba'] = $this->input->post('jl');
				$data['nama_siswa'] = $this->input->post('namasiswa');
				$data['peringkat'] = $this->input->post('peringkat');
				$data['tingkat_lomba'] = $this->input->post('tingkat');
				$data['tempat_lomba'] = $this->input->post('tempat');
				$data['penyelenggara'] = $this->input->post('penyelenggara');
				//$data['sertifikat'] = $gambar;
				$data['tgl_mulai'] = $this->input->post('tglmulai');
				$data['tgl_akhir'] = $this->input->post('tglakhir');
				$data['katlomba_id'] = $this->input->post('lomba');
				//$data['sekolah_id'] = $key;

				$this->db->where('prestasi_id',$id);
				$sql = $this->db->update('prestasi',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/prestasi'));
				}
        }
                 
    }

    function hapus_prestasi($id){
    	$this->model_sequrity_rkas->get_sequrity_sekolah();
    	$key = $this->session->userdata('token_sekolah_id');

    	$this->db->where('prestasi_id',$id);
    	$this->db->where('sekolah_id',$key);
    	$sql = $this->db->delete('prestasi');
    	if($sql){
    		$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
						  </div>");
					redirect(base_url('sekolah/prestasi'));	
    	}

    		
    }


    //SARPRAS

    public function ruangkelas(){
    	$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Sarana dan Prasarana Sekolah';
		$data['jhsmall'] = 'Sarpras';
		$data['conten'] = 'sekolah/sarpras';
		$this->load->view('sekolah/layout',$data);

    }

    public function lihatfoto(){
    	$this->model_sequrity_rkas->get_sequrity_sekolah();
		// $data['jh1'] = 'Sarana dan Prasarana Sekolah';
		// $data['jhsmall'] = 'Sarpras';
		// $data['conten'] = 'sekolah/sarpras';
		$id = $this->input->post('id');
		$pecah = explode(",", $id);

		if($pecah[2] == "kelas"){
			$this->db->where('fotokelas_id',$pecah[0]);
			//$this->db->where('soft_delete',0);
			$sql = $this->db->get('foto_kelas');
			if($sql){
				$data['foto'] = $sql->result();
				$field = $pecah[1];
				$p = explode("_", $field);
				$data['field'] = $p[1];
				$data['lokasi'] = 'foto_kelas';
				$this->load->view('sekolah/lihat_foto',$data);		
			}
		}else{
			$this->db->where('fotolabor_id',$pecah[0]);
			//$this->db->where('soft_delete',0);
			$sql = $this->db->get('foto_labor');
			if($sql){
				$data['foto'] = $sql->result();
				$field = $pecah[1];
				$p = explode("_", $field);
				$data['field'] = $p[1];
				$data['lokasi'] = 'foto_labor';
				$this->load->view('sekolah/lihat_foto',$data);	
			}
		}
		


		//$this->load->view('sekolah/layout',$data);

    }

    public function add_sarpras_kelas(){
    	$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$data['sekolah_id'] = $key;
		$data['jumlah_kelas'] = $this->input->post('kelas');
		$data['jumlah_baik'] = $this->input->post('baik');
		$data['rusak_ringan'] = $this->input->post('ringan');
		$data['rusak_sedang'] = $this->input->post('sedang');
		$data['rusak_berat'] = $this->input->post('berat');
		$data['tahun'] = $this->session->userdata('token_tahun');

		$sql = $this->db->insert('sarpras_kelas',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/ruangkelas'));
		}

    }

    public function tambahsarpras(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$data['idfoto'] = $key;
		$tokensekolah = $this->session->userdata('token_sekolah_id');
		if($key == 0){
			$this->load->view('sekolah/tambah_sarpras',$data);
		}else{
			$this->db->where('sekolah_id',$tokensekolah);
			$this->db->where('prestasi_id',$key);
			$sql = $this->db->get('prestasi');
			if($sql){
				$data['prestasi'] = $sql->result();
			}
			$this->load->view('sekolah/edit_prestasi',$data);
		}
		

	}

	function upload_image_sarpras(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');

		$id = $this->input->post('idfoto');
		$pecah = explode(",", $id);
		$pecah1 = $pecah[0];
		$pecah2 = $pecah[1];

		
        $config['upload_path'] = './assets/foto_kelas/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
        $this->upload->initialize($config);

		$this->db->where('sekolah_id',$key);
		$this->db->where('tahun_foto',$tahun);
		$this->db->where('kategori_foto',$pecah2);
		$cek = $this->db->get('foto_kelas');

	if($cek->num_rows() > 0){
		//ada update

		if(!empty($_FILES['rk']['tmp_name'])){
 
            if ($this->upload->do_upload('rk')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/foto_kelas/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './assets/foto_kelas/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];

				$data[$pecah1] = $gambar;

				$this->db->where('sekolah_id',$key);
				$this->db->where('kategori_foto',$pecah2);
				$this->db->where('tahun_foto',$tahun);
				$sql = $this->db->update('foto_kelas',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/ruangkelas'));
				}

            }
                      
        }else{

					redirect(base_url('sekolah/ruangkelas'));
        }

	}else{
		//tambah baru
		if(!empty($_FILES['rk']['tmp_name'])){
 
            if ($this->upload->do_upload('rk')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/foto_kelas/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './assets/foto_kelas/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];

                $data['kategori_foto'] = $pecah2;
                $data['tahun_foto'] = $tahun;
				$data[$pecah1] = $gambar;
				$data['sekolah_id'] = $key;
				
				$sql = $this->db->insert('foto_kelas',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/ruangkelas'));
				}

            }
                      
        }else{
            	
					redirect(base_url('sekolah/ruangkelas'));

        }

	}  
                 
    }


    //sarpras labor
    public function ruanglabor(){
    	$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Sarana dan Prasarana Sekolah';
		$data['jhsmall'] = 'Sarpras';
		$data['conten'] = 'sekolah/labor';
		$this->load->view('sekolah/layout',$data);

    }

    public function tambahsarpraslabor(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$data['idfoto'] = $key;
		$tokensekolah = $this->session->userdata('token_sekolah_id');
		if($key == 0){
			$this->load->view('sekolah/tambah_sarpras_labor',$data);
		}else{
			$this->db->where('sekolah_id',$tokensekolah);
			$this->db->where('prestasi_id',$key);
			$sql = $this->db->get('prestasi');
			if($sql){
				$data['prestasi'] = $sql->result();
			}
			$this->load->view('sekolah/edit_prestasi',$data);
		}
		

	}

    public function add_sarpras_labor(){
    	$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$lab = $this->input->post('jenislab');
		$tahun = $this->session->userdata('token_tahun');
		$data['sekolah_id'] = $key;
		$data['labor_id'] = $this->input->post('jenislab');
		$data['jumlah_labor'] = $this->input->post('labor');
		$data['jumlah_baik'] = $this->input->post('baik');
		$data['rusak_ringan'] = $this->input->post('ringan');
		$data['rusak_sedang'] = $this->input->post('sedang');
		$data['rusak_berat'] = $this->input->post('berat');
		$data['tahun'] = $this->session->userdata('token_tahun');

		$this->db->where('sekolah_id',$key);
		$this->db->where('labor_id',$lab);
		$this->db->where('tahun',$tahun);
		$cek = $this->db->get('sarpras_labor');
		if($cek->num_rows() > 0){
			//data sudah ada
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
							    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
							    <strong>Pemberitahuan!</strong> Data Sudah Ada pada Database.
							  </div>");
						redirect(base_url('sekolah/ruanglabor'));
		}else{
			//proses simpan
			$sql = $this->db->insert('sarpras_labor',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
							    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
							    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
							  </div>");
						redirect(base_url('sekolah/ruanglabor'));
			}
		}

    }

	function upload_image_sarpras_labor(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');

		$id = $this->input->post('idfoto');
		$pecah = explode(",", $id);
		$pecah1 = $pecah[0];
		$pecah2 = $pecah[1];
		$pecah3 = $pecah[2];

		
        $config['upload_path'] = './assets/foto_labor/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
        $this->upload->initialize($config);

		$this->db->where('sarpraslabor_id',$pecah3);
		//$this->db->where('tahun_foto',$tahun);
		$this->db->where('kategori_foto',$pecah2);
		$cek = $this->db->get('foto_labor');

	if($cek->num_rows() > 0){
		//ada update

		if(!empty($_FILES['rk']['tmp_name'])){
 
            if ($this->upload->do_upload('rk')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/foto_labor/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './assets/foto_labor/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];

				$data[$pecah1] = $gambar;

				$this->db->where('sarpraslabor_id',$pecah3);
				$this->db->where('kategori_foto',$pecah2);
				//$this->db->where('tahun_foto',$tahun);
				$sql = $this->db->update('foto_labor',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/ruanglabor'));
				}

            }
                      
        }else{

					redirect(base_url('sekolah/ruanglabor'));
        }

	}else{
		//tambah baru
		if(!empty($_FILES['rk']['tmp_name'])){
 
            if ($this->upload->do_upload('rk')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/foto_labor/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= './assets/foto_labor/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
 
                $gambar=$gbr['file_name'];

                $data['kategori_foto'] = $pecah2;
                //$data['tahun_foto'] = $tahun;
				$data[$pecah1] = $gambar;
				$data['sarpraslabor_id'] = $pecah3;
				
				$sql = $this->db->insert('foto_labor',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
						  </div>");
					redirect(base_url('sekolah/ruanglabor'));
				}

            }
                      
        }else{
            	
					redirect(base_url('sekolah/ruanglabor'));

        }

	}  
                 
    }

    public function jumlahpd(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data['jh1'] = 'Jumlah Peserta Didik';
		$data['jhsmall'] = 'Penerima BOS';
		$data['conten'] = 'sekolah/jumlah_pd';
		$this->load->view('sekolah/layout',$data);
	}

	//GURU MAPEL
	public function gurutambahan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$data['jh1'] = 'Guru Tambahan';
		$data['jhsmall'] = 'Data Guru Tambahan';
		$data['bc'] = "<li><a href='#'>Guru Tambahan</a></li>";
		$data['conten'] = 'sekolah/guru_tambahan';
		
		$this->db->where('rs.soft_delete',0);
		$this->db->where('rs.sekolah_id',$sekolahid);
		$this->db->join('sekolah s','s.sekolah_id=rs.sekolah_id');
		$this->db->join('gtk g','g.gtk_id=rs.gtk_id');
		$sql = $this->db->get('riwayat_sekolah rs');
		$data['guru'] = $sql->result();

		$this->load->view('sekolah/layout',$data);		
	}

	public function mapelgurutambahan($id="")
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$data['jh1'] = 'Mapel Guru Tambahan';
		$data['jhsmall'] = 'Data Mapel Guru Tambahan';
		$data['bc'] = "<li><a href='#'>Mapel Guru Tambahan</a></li>";
		$data['conten'] = 'sekolah/mapel_guru_tambahan';

		$this->db->where('soft_delete',0);
		//$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('gtk_id',$id);
		$sql2 = $this->db->get('gtk');
		$data['guru'] = $sql2->result();

		$this->load->view('sekolah/layout',$data);
	}

	public function aksi_mapelgurutambahan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['mapel'] = $this->sekolah_model->mapel();
			$data['gtkid'] = $pecah[1];
			$data['link'] = $pecah[2];
			$this->load->view('sekolah/add_mapelguru',$data);
		}elseif ($pecah[0] == 3) {
			$data['sekolah'] = $this->sekolah_model->sekolah();
			$data['gtkid'] = $pecah[1];
			$this->load->view('sekolah/add_sekolah_tambahan',$data);	
		}else{
			$this->db->where('riwayatmapel_id',$pecah[1]);
			$sql = $this->db->get('riwayat_mapel');
			if($sql->num_rows() > 0){
				$data['mapel'] = $this->sekolah_model->mapel();
				$data['riwayat'] = $sql->result();
			}

			$this->load->view('sekolah/edit_mapelguru',$data);
		}
		
	}

	public function mapelguru($id="")
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$data['jh1'] = 'Mapel Guru';
		$data['jhsmall'] = 'Data Mapel Guru';
		$data['bc'] = "<li><a href='#'>Mapel Guru</a></li>";
		$data['conten'] = 'sekolah/mapel_guru';

		$this->db->where('soft_delete',0);
		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('gtk_id',$id);
		$sql2 = $this->db->get('gtk');
		$data['guru'] = $sql2->result();

		$this->load->view('sekolah/layout',$data);	
	}

	public function aksi_mapelguru()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['mapel'] = $this->sekolah_model->mapel();
			$data['gtkid'] = $pecah[1];
			$data['link'] = $pecah[2];
			$this->load->view('sekolah/add_mapelguru',$data);
		}elseif ($pecah[0] == 3) {
			$data['sekolah'] = $this->sekolah_model->sekolah();
			$data['gtkid'] = $pecah[1];
			$this->load->view('sekolah/add_sekolah_tambahan',$data);	
		}else{
			$this->db->where('riwayatmapel_id',$pecah[1]);
			$sql = $this->db->get('riwayat_mapel');
			if($sql->num_rows() > 0){
				$data['link'] = $pecah[2];
				$data['mapel'] = $this->sekolah_model->mapel();
				$data['riwayat'] = $sql->result();
			}

			$this->load->view('sekolah/edit_mapelguru',$data);
		}
		
	}

	public function aksi_sekolah_tambahan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$this->db->where('riwayatsekolah_id',$key);
		$sql = $this->db->get('riwayat_sekolah');
		if($sql->num_rows() > 0){
			$data['sekolah'] = $this->sekolah_model->sekolah();
			$data['riwayat'] = $sql->result();
		}
		$this->load->view('sekolah/edit_sekolah_tambahan',$data);

		/*$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['mapel'] = $this->sekolah_model->mapel();
			$data['gtkid'] = $pecah[1];
			$this->load->view('sekolah/add_mapelguru',$data);
		}elseif ($pecah[0] == 3) {
			$data['sekolah'] = $this->sekolah_model->sekolah();
			$data['gtkid'] = $pecah[1];
			$this->load->view('sekolah/add_sekolah_tambahan',$data);	
		}else{
			$this->db->where('riwayatmapel_id',$pecah[1]);
			$sql = $this->db->get('riwayat_mapel');
			if($sql->num_rows() > 0){
				$data['mapel'] = $this->sekolah_model->mapel();
				$data['riwayat'] = $sql->result();
			}

			$this->load->view('sekolah/edit_mapelguru',$data);
		} */
		
	}

	public function add_sekolah_tambahan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$gtkid = $this->input->post('gtkid');
		$sekolah = $this->input->post('sekolah');

		$this->db->where('soft_delete',0);
		$this->db->where('gtk_id',$gtkid);
		$this->db->where('sekolah_id',$sekolah);
		$cek = $this->db->get('riwayat_sekolah');
		if($cek->num_rows() >0){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Sekolah Tambahan Guru Sudah diinputkan pada database.
					  </div>");
				redirect(base_url('sekolah/mapelguru/'.$gtkid));
		}else{
			$data['sekolah_id'] = $sekolah;
			//$data['kd_mapel'] = $this->input->post('mapel');
			$data['gtk_id'] = $this->input->post('gtkid');
			$sql = $this->db->insert('riwayat_sekolah',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('sekolah/mapelguru/'.$gtkid));
			}	
		}

			
	}

	public function proses_edit_sekolah_tambahan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$gtkid = $this->input->post('gtkid');
		$sekolah = $this->input->post('sekolah');
		$sekolahid = $this->input->post('sekolahid');
		$riwayatid = $this->input->post('riwayatid');

		if($sekolahid==$sekolah){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('sekolah/mapelguru/'.$gtkid));
		}else{
			$this->db->where('soft_delete',0);
			$this->db->where('gtk_id',$gtkid);
			$this->db->where('sekolah_id',$sekolah);
			$cek = $this->db->get('riwayat_sekolah');
			if($cek->num_rows() >0){
				$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Sekolah Tambahan Guru Sudah diinputkan pada database.
						  </div>");
					redirect(base_url('sekolah/mapelguru/'.$gtkid));
			}else{
				//$data['sekolah_id'] = $sekolahid;
				$data['sekolah_id'] = $this->input->post('sekolah');
				//$data['gtk_id'] = $this->input->post('gtkid');
				$this->db->where('riwayatsekolah_id',$riwayatid);
				$sql = $this->db->update('riwayat_sekolah',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
						  </div>");
					redirect(base_url('sekolah/mapelguru/'.$gtkid));
				}	
			}

		}

			
	}

	public function hapus_sekolah_tambahan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$key = $this->uri->segment(3);

		$this->db->where('riwayatsekolah_id',$key);
		$sql2 = $this->db->get('riwayat_sekolah');
		foreach ($sql2->result() as $row) {
			$gtkid = $row->gtk_id;
		}

		$data['soft_delete'] = 1;
		$this->db->where('gtk_id',$gtkid);
		$this->db->where('riwayatsekolah_id',$key);
		$sql = $this->db->update('riwayat_sekolah',$data);

		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('sekolah/mapelguru/'.$gtkid));
		}
	}

	public function add_mapelguru()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$gtkid = $this->input->post('gtkid');
		$mapel = $this->input->post('mapel');
		$link = $this->input->post('link');

		$this->db->where('soft_delete',0);
		$this->db->where('gtk_id',$gtkid);
		$this->db->where('kd_mapel',$mapel);
		$this->db->where('sekolah_id',$sekolahid);
		$cek = $this->db->get('riwayat_mapel');
		if($cek->num_rows() >0){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Mapel Sudah diinputkan pada database.
					  </div>");
				redirect(base_url('sekolah/'.$link.'/'.$gtkid));
		}else{
			$data['sekolah_id'] = $sekolahid;
			$data['kd_mapel'] = $this->input->post('mapel');
			$data['gtk_id'] = $this->input->post('gtkid');
			$sql = $this->db->insert('riwayat_mapel',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('sekolah/'.$link.'/'.$gtkid));
			}	
		}

			
	}

	public function proses_edit_mapelguru()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$gtkid = $this->input->post('gtkid');
		$mapel = $this->input->post('mapel');
		$kdmapel = $this->input->post('kdmapel');
		$riwayatid = $this->input->post('riwayat');
		$link = $this->input->post('link');

		if($mapel==$kdmapel){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('sekolah/'.$link.'/'.$gtkid));
		}else{
			$this->db->where('soft_delete',0);
			$this->db->where('gtk_id',$gtkid);
			$this->db->where('kd_mapel',$mapel);
			$this->db->where('sekolah_id',$sekolahid);
			$cek = $this->db->get('riwayat_mapel');
			if($cek->num_rows() >0){
				$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Mapel Sudah diinputkan pada database.
						  </div>");
					redirect(base_url('sekolah/'.$link.'/'.$gtkid));
			}else{
				//$data['sekolah_id'] = $sekolahid;
				$data['kd_mapel'] = $this->input->post('mapel');
				//$data['gtk_id'] = $this->input->post('gtkid');
				$this->db->where('riwayatmapel_id',$riwayatid);
				$sql = $this->db->update('riwayat_mapel',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
						    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
						    <strong>Pemberitahuan!</strong> Data Berhasil Diuba.
						  </div>");
					redirect(base_url('sekolah/'.$link.'/'.$gtkid));
				}	
			}

		}

			
	}

	public function hapus_mapelguru()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('riwayatmapel_id',$key);
		$sql = $this->db->update('riwayat_mapel',$data);

		$this->db->where('riwayatmapel_id',$key);
		$sql2 = $this->db->get('riwayat_mapel');
		foreach ($sql2->result() as $row) {
			$gtkid = $row->gtk_id;
		}
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('sekolah/mapelguru/'.$gtkid));
		}
	}

	public function hapus_mapelgurutambahan()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('riwayatmapel_id',$key);
		$sql = $this->db->update('riwayat_mapel',$data);

		$this->db->where('riwayatmapel_id',$key);
		$sql2 = $this->db->get('riwayat_mapel');
		foreach ($sql2->result() as $row) {
			$gtkid = $row->gtk_id;
		}
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('sekolah/mapelgurutambahan/'.$gtkid));
		}
	}

	public function gurumapel()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->session->userdata('token_sekolah_id');
		$data['jh1'] = 'Guru Mapel';
		$data['jhsmall'] = 'Data Guru Mapel';
		$data['bc'] = "<li><a href='#'>Guru Mapel</a></li>";
		$data['conten'] = 'sekolah/gurumapel';
		
		$this->db->where('soft_delete',0);
		$this->db->where('status_gtk','PNS');
		$this->db->where('sekolah_id',$key);
		$sql = $this->db->get('gtk');
		$data['pns'] = $sql->result();

		$this->db->where('soft_delete',0);
		$this->db->where('status_gtk','NON PNS');
		$this->db->where('sekolah_id',$key);
		$sql2 = $this->db->get('gtk');
		$data['nonpns'] = $sql2->result();

		$this->load->view('sekolah/layout',$data);		
	}

	public function aksi_gurumapel()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['pangkat'] = $this->sekolah_model->pangkat();
			$data['mapel'] = $this->sekolah_model->mapel();
			$data['sekolah'] = $this->sekolah_model->sekolah();
			$data['agama'] = $this->sekolah_model->agama();
			$data['pddk'] = $this->sekolah_model->pendidikan();
			$this->load->view('sekolah/add_gurumapel',$data);
		}else{
			$this->db->where('gtk_id',$pecah[1]);
			$sql = $this->db->get('gtk');
			if($sql->num_rows() > 0){
				$data['pangkat'] = $this->sekolah_model->pangkat();
				$data['mapel'] = $this->sekolah_model->mapel();
				$data['sekolah'] = $this->sekolah_model->sekolah();
				$data['agama'] = $this->sekolah_model->agama();
				$data['pddk'] = $this->sekolah_model->pendidikan();
				$data['gtk'] = $sql->result();
			}

			$this->load->view('sekolah/edit_gurumapel',$data);
		}
		
	}

	public function add_gurumapel()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$stgtk = $this->input->post('statusguru');
		if($stgtk == "PNS"){
			$data['nip'] = $this->input->post('nip');	
			$data['kd_pangkat'] = $this->input->post('pangkat');
		}
		$data['nuptk'] = $this->input->post('nuptk');
		$data['nama_lengkap'] = $this->input->post('nama');
		$data['gelar_depan'] = $this->input->post('gelardepan');
		$data['gelar_belakang'] = $this->input->post('gelarbelakang');
		$data['tempat_lahir'] = $this->input->post('tempatlahir');
		$data['tgl_lahir'] = $this->input->post('tgllahir');
		$data['jk'] = $this->input->post('jk');
		$data['agama_id'] = $this->input->post('agama');
		$data['pdd_terakhir_id'] = $this->input->post('pddk');
		$data['jurusan'] = $this->input->post('jurusan');
		$data['no_hp'] = $this->input->post('nohp');
		$data['email'] = $this->input->post('email');
		$data['status_gtk'] = $this->input->post('statusguru');
		$data['sekolah_id'] = $sekolahid;

		$sql = $this->db->insert('gtk',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('sekolah/gurumapel'));
		}	
	}

	public function proses_edit_gurumapel()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$stgtk = $this->input->post('status');
		$gtkid = $this->input->post('gtkid');

		if($stgtk == "PNS"){
			$data['nip'] = $this->input->post('nip');	
			$data['kd_pangkat'] = $this->input->post('pangkat');
		}
		$data['nuptk'] = $this->input->post('nuptk');
		$data['nama_lengkap'] = $this->input->post('nama');
		$data['gelar_depan'] = $this->input->post('gelardepan');
		$data['gelar_belakang'] = $this->input->post('gelarbelakang');
		$data['tempat_lahir'] = $this->input->post('tempatlahir');
		$data['tgl_lahir'] = $this->input->post('tgllahir');
		$data['jk'] = $this->input->post('jk');
		$data['agama_id'] = $this->input->post('agama');
		$data['pdd_terakhir_id'] = $this->input->post('pddk');
		$data['jurusan'] = $this->input->post('jurusan');
		$data['no_hp'] = $this->input->post('nohp');
		$data['email'] = $this->input->post('email');
		$data['sekolah_id'] = $sekolahid;

		$this->db->where('gtk_id',$gtkid);
		$sql = $this->db->update('gtk',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
				  </div>");
			redirect(base_url('sekolah/gurumapel'));
		}	
	}

	public function hapus_gurumapel()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('gtk_id',$key);
		$sql = $this->db->update('gtk',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('sekolah/gurumapel'));
		}
	}
	
	
	// TODO::TAMSIL PEGAWAI
	public function tamsil(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$periode = 1;
		$data['periode'] = $periode;
		$tahun = date('Y');
		$data['jh1'] = 'Tamsil Pegawai';
		$data['jhsmall'] = 'Data Tamsil Pegawai';
		$data['bc'] = "<li><a href='#'>Tamsil Pegawai</a></li>";
		// $data['conten'] = 'sekolah/tamsil_pegawai';
		$data['conten'] = 'sekolah/tamsil_tutup';

		$this->db->where('tahun',$tahun);
		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('periode',$periode);
		$cek = $this->db->get('tamsil_file');
		if($cek->num_rows() > 0){
			$row = $cek->row_array();
			$data['alert_tamsil'] = 1;
			$data['file_tamsil'] = $row['file_tamsil'];
		}else{
			$data['alert_tamsil'] = 0;
			$data['file_tamsil'] = "";
		}

		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('tahun',$tahun);
		$this->db->where('periode',$periode);
		$this->db->where('deleted_at',NULL);
		$cek_tamsil = $this->db->get('tamsil');
		if($cek_tamsil->num_rows() > 0){
			//ada
			$data['info_upload'] = 1;
		}else{
			//tidak ada
			$data['info_upload'] = 0;
		}


		$tahun = date('Y');
		$this->db->join('gtk','gtk.gtk_id=tamsil.gtk_id');
		$this->db->join('pangkat','pangkat.kd_pangkat=gtk.kd_pangkat');
		$this->db->join('pdd_terakhir','pdd_terakhir.pdd_terakhir_id=gtk.pdd_terakhir_id');
		$this->db->where('tamsil.sekolah_id',$sekolahid);
		$this->db->where('tamsil.tahun',$tahun);
		$this->db->where('tamsil.periode',$periode);
		$this->db->where('tamsil.deleted_at',NULL);
		$sql = $this->db->get('tamsil');
		if($sql){
			$data['pns'] = $sql->result();
		}

		$this->load->view('sekolah/layout',$data);
	}

	public function aksi_tamsil()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			//tambah data
			$this->db->where('sekolah_id',$sid);
			$this->db->where('soft_delete',0);
			$this->db->where('status_gtk','PNS');
			$sql = $this->db->get('gtk');
			if($sql){
				$data['gurupns'] = $sql->result();
			}
			
			$this->load->view('sekolah/tamsil_add',$data);
		}else{
			//edit data
			$tamsilid = $pecah[1];
			$this->db->where('tamsil_id',$tamsilid);
			$this->db->join('gtk','gtk.gtk_id=tamsil.gtk_id');
			$sql = $this->db->get('tamsil');
			if($sql){
				$data['tamsil'] = $sql->result();
			}

			$this->load->view('sekolah/tamsil_edit',$data);
		}
		
	}

	public function add_tamsil(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sid = $this->session->userdata('token_sekolah_id');
		$tahun = date('Y');
		$periode = 1;
		$gtkid = $this->input->post('namaguru');
		$data['sekolah_id'] = $sid;
		$data['gtk_id'] = $this->input->post('namaguru');
		$data['tahun'] = $tahun;
		$data['periode'] = $periode;
		$data['jumlah_jam_mengajar'] = $this->input->post('jam');
		$data['mapel'] = $this->input->post('mapel');
		$data['nama_bank'] = 'Bank Riau Kepri';
		$data['no_rekening'] = $this->input->post('norek');
		$data['nama_rekening'] = $this->input->post('namarek');
		$data['keterangan'] = $this->input->post('keterangan');

		$this->db->where('gtk_id',$gtkid);
		$this->db->where('periode',$periode);
		$this->db->where('tahun',$tahun);
		$this->db->where('sekolah_id',$sid);
		$this->db->where('deleted_at',NULL);
		$cek = $this->db->get('tamsil');
		if($cek->num_rows() > 0){
			//alert data sudah ada
			$status = array('status' => 1);
			echo json_encode($status);
		}else{
			//simpan
			$sql = $this->db->insert('tamsil',$data);
			if($sql){
				echo json_encode($sql);
			}
		}
	}

	public function proses_edit_tamsil(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$tamsilid = $this->input->post('tamsilid');
		$data['jumlah_jam_mengajar'] = $this->input->post('jam');
		$data['mapel'] = $this->input->post('mapel');
		$data['nama_bank'] = 'Bank Riau Kepri';
		$data['no_rekening'] = $this->input->post('norek');
		$data['nama_rekening'] = $this->input->post('namarek');
		$data['keterangan'] = $this->input->post('keterangan');

		$this->db->where('tamsil_id',$tamsilid);
		$sql = $this->db->update('tamsil',$data);
		if($sql){
			echo json_encode($sql);
		}
	}

	public function hapus_tamsil($id)
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$data['deleted_at'] = date('Y-m-d H:i:s');
		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('tamsil_id',$id);
		$sql = $this->db->update('tamsil',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('sekolah/tamsil'));
		}
	}

	public function upload_file_tamsil(){
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$periode = 1;
		$tanggal = date('Y-m-d');
		$tahun = date('Y');
		$acak = rand(100,10000000);
		
		$nmfile = $_FILES["lampiran"]["name"];
		$lokasi = $_FILES["lampiran"]["tmp_name"];
		$type = $_FILES["lampiran"]["type"];
		$namareal = str_replace(array(" ", ","),'-',$nmfile);

		$data['sekolah_id'] = $sekolahid;
		$data['tahun'] = $tahun;
		$data['periode'] = $periode;

		$this->db->where('tahun',$tahun);
		$this->db->where('sekolah_id',$sekolahid);
		$this->db->where('periode',$periode);
		$cek = $this->db->get('tamsil_file');
		if($cek->num_rows() > 0){
			//jika ada maka di update
			$row = $cek->row_array();
			$target = "assets/filetamsil/".$row['file_tamsil'];			
			unlink($target);
			if(move_uploaded_file($lokasi,"./assets/filetamsil/".$acak.'-'.$tanggal.'-'.strtolower($namareal))){
				$data['file_tamsil'] = $acak.'-'.$tanggal.'-'.strtolower($namareal);
				$this->db->where('tamsil_file_id',$row['tamsil_file_id']);
				$sql = $this->db->update('tamsil_file',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
								<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
								<strong>Sukses!</strong> Data Berhasil Disimpan.
							</div>");
					redirect(base_url('sekolah/tamsil'));
				}
			}
		}else{
			//jika tidak data disimpan
			if(move_uploaded_file($lokasi,"./assets/filetamsil/".$acak.'-'.$tanggal.'-'.strtolower($namareal))){
				$data['file_tamsil'] = $acak.'-'.$tanggal.'-'.strtolower($namareal);
				$sql = $this->db->insert('tamsil_file',$data);
				if($sql){
					$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
								<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
								<strong>Sukses!</strong> Data Berhasil Disimpan.
							</div>");
					redirect(base_url('sekolah/tamsil'));
				}
			}

		}
			
	}

	public function downloadtamsil($sid,$tahun,$periode)
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		// $sekolahid = $this->session->userdata('token_sekolah_id');
		// $tahun = date('Y');
		// $periode = 1;
		$acak = rand(100000,100000000);
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$acak.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->db->join('gtk','gtk.gtk_id=tamsil.gtk_id');
		$this->db->join('pangkat','pangkat.kd_pangkat=gtk.kd_pangkat');
		$this->db->join('pdd_terakhir','pdd_terakhir.pdd_terakhir_id=gtk.pdd_terakhir_id');
		$this->db->where('tamsil.sekolah_id',$sid);
		$this->db->where('tamsil.tahun',$tahun);
		$this->db->where('tamsil.periode',$periode);
		$this->db->where('tamsil.deleted_at',NULL);
		$sql = $this->db->get('tamsil');
		if($sql){
			$this->db->join('kec','kec.kd_kec=sekolah.kd_kec');
			$this->db->join('kab','kab.kd_kab=kec.kd_kab');
			$this->db->where('sekolah_id',$sid);
			$getsekolah = $this->db->get('sekolah')->row_array();
			$data['nama_sekolah'] = $getsekolah['nama_sp'];
			$data['nama_kab'] = $getsekolah['nama_kab'];
			$data['tamsil'] = $sql->result();
		}

		$this->load->view('sekolah/tamsil_download_pegawai',$data);

	}
	//ENDING TAMSIL

	// BKU 2019

	public function bku()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$data['tahun'] = $this->session->userdata('token_tahun');
		$data['jh1'] = 'Input BKU';
		$data['jhsmall'] = 'Buku Kas Umum';
		$data['bc'] = "<li><a href='#'>Buku Kas Umum</a></li>";
		$data['conten'] = 'sekolah/bku/index';
		$data['js'] = 'sekolah/bku/js';
		$data['bulan'] = $this->rkas_model->get_bulan_all();
		$this->load->view('sekolah/layout',$data);
	}

	public function bku_show()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$bulan = $this->input->post('bulan');

		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['bulan_id'] = $bulan;
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
			}
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,NULL);
			$this->load->view('sekolah/bku/bku_show',$data);	
		}else{
			redirect(base_url('sekolah'));
		}
		
	}

	public function bku_show_ajax($id)
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$bulan = $id;
		$data['bulan'] = $this->rkas_model->nama_bulan($id);
		$data['bulan_id'] = $id;
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
			}
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,NULL);
			$this->load->view('sekolah/bku/bku_show',$data);
		}else{
			redirect(base_url('sekolah'));
		}
	}

	public function form_bku()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$aksi = $this->input->post('id');
		$pecah = explode(',',$aksi);
		$data['aksi'] = $pecah[0];
		$data['bukti'] = $this->rkas_model->get_bukti();
		$data['daftar_pajak'] = $this->rkas_model->get_daftar_pajak($pecah[2]);
		if($pecah[0] == 0){
			//insert
			$data['bulan_id'] = $pecah[2];
			$data['pajak'] = $this->rkas_model->get_pajak();
			$data['rekening'] = $this->rkas_model->get_rekening();
		}else{
			//update
			$bku_id = $pecah[2];
			$data['bulan_id'] = $pecah[1];
			$data['data_bku'] = $this->rkas_model->get_bku_id($bku_id);
			$belanja_id = $data['data_bku']['belanja_id'];
			$data['program'] = $this->rkas_model->get_program($belanja_id);
			$data['pajak'] = $this->rkas_model->get_pajak();
		}
		$this->load->view('sekolah/bku/form_bku', $data);
	}

	public function bku_ajax()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$id = $this->input->post('id');
		$data['bulan_id'] = $this->input->post('bulan_id');
		$data['program'] = $this->rkas_model->get_program($id);
		$this->load->view('sekolah/bku/form_bku_respon',$data);
	}

	public function aksi_simpan_bku()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$transaksi = $this->input->post('jumlah_transaksi');
		$jenis_transaksi = $this->input->post('jenis_transaksi');
		$bukti = $this->input->post('bukti');
		$data['tanggal_transaksi'] = $this->input->post('tanggal_transaksi');
		if($jenis_transaksi == 'pajak'){
			$data['pajak_id'] = $this->input->post('pajak');
		}else{
			$data['pajak_id'] = 0;
		}

		if($bukti == 1 or $bukti == 2){
			$data['rincian_id'] = 0;
			$status_pajak = NULL;
		}else{
			if($bukti == 3){
				if($jenis_transaksi == 'pajak'){
					$data['rincian_id'] = $this->input->post('program');
				}else{
					$data['rincian_id'] = 0;
				}
				$status_pajak = NULL;
			}else{
				if($jenis_transaksi == 'pajak'){
					$daftar_pajak = $this->input->post('daftar_pajak');
					$ambil = explode(',',$daftar_pajak);
					$data['rincian_id'] = $ambil[1];
					$data['pajak_id'] = $ambil[0];
					$status_pajak = '1,'.$ambil[2];
				}else if($jenis_transaksi == 'bungabank'){
					$data['rincian_id'] = 0;
					$data['jenis_transaksi'] = 1;
					$status_pajak = NULL;
				}else{
					$data['rincian_id'] = $this->input->post('program');
					$status_pajak = NULL;
				}	
			}
			
		}
		
		$data['bukti_id'] = $this->input->post('bukti');
		$data['no_urut_bukti'] = $this->rkas_model->get_number($bukti);
		$data['uraian_bku'] = $this->input->post('uraian');
		$data['jumlah_transaksi'] = $this->rkas_model->destroy_titik($transaksi);
		$data['bulan_id'] = $this->input->post('bulan_id');
		$data['tahun'] = $tahun;
		$data['sekolah_id'] = $sekolahid;

		$this->rkas_model->simpan_data('bku',$data,$status_pajak);

	}

	public function aksi_edit_bku()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$bku_id = $this->input->post('bku_id');
		$bulan_id = $this->input->post('bulan_id');
		$transaksi = $this->input->post('jumlah_transaksi');
		$jenis_transaksi = $this->input->post('jenis_transaksi');
		$bukti = $this->input->post('bukti');
		$pajak_id = $this->input->post('pajak_id');
		$jenis_id = $this->input->post('jenis_id');

		if($bukti == 1 or $bukti == 2){
			$data['rincian_id'] = 0;
			$data['pajak_id'] = 0;
		}else{
			if($bukti == 3){
				//KM
				if($pajak_id != 0){
					$data['rincian_id'] = $this->input->post('program');
					$data['pajak_id'] = $this->input->post('pajak');	
				}
			}else{
				//KK
				if($pajak_id == 0){
					if($jenis_id == 0){
						$data['rincian_id'] = $this->input->post('program');
						$data['pajak_id'] = 0;
					}
				}
			}
			
		}
		
		$data['uraian_bku'] = $this->input->post('uraian');
		$data['jumlah_transaksi'] = $this->rkas_model->destroy_titik($transaksi);
		$this->rkas_model->ubah_data('bku',$bku_id,$data,$bulan_id);
	}

	public function hapus_bku()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$id = $this->input->post('id');
		if(isset($id)){
			$bulan = $this->input->post('bulan');
			$this->rkas_model->hapus_bku('bku',$id,$bulan);
		}else{
			redirect(base_url('sekolah'));
		}
		
	}


	public function databku()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$data['tahun'] = $this->session->userdata('token_tahun');
		$data['jh1'] = 'Data BKU';
		$data['jhsmall'] = 'Data Buku Kas Umum';
		$data['bc'] = "<li><a href='#'>Buku Kas Umum</a></li>";
		$data['conten'] = 'sekolah/databku/index';
		// $data['saldo_awal'] = 0;
		$data['bulan'] = $this->rkas_model->get_bulan_all();
		$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,NULL,NULL,NULL);
		$this->load->view('sekolah/layout',$data);
	}

	public function data_bku_show()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$bulan = $this->input->post('bulan');

		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
			}
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,NULL);
			$data['saldo_tunai'] = $this->rkas_model->get_saldo_tunai($bulan,$tahun,$sekolahid);
			$this->load->view('sekolah/databku/bku_show',$data);	
		}else{
			redirect(base_url('sekolah'));
		}
		
	}

	public function cetaklaporan($bulan,$key)
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$acak = rand(100000,100000000);
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$acak."_laporan_".$key.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($key == 'bku'){
				if($bulan == 1){
					$data['saldo_awal'] = 0;
				}else{
					$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
				}
				$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,NULL);
				$data['saldo_tunai'] = $this->rkas_model->get_saldo_tunai($bulan,$tahun,$sekolahid);
				$this->load->view('sekolah/databku/cetak_bku',$data);
			}else if($key == 'bpk'){
				if($bulan == 1){
					$data['saldo_awal'] = 0;
				}else{
					$data['saldo_awal'] = $this->rkas_model->saldo_bpk($bulan,$tahun,$sekolahid);
				}
				$bukti = array('KM','KK');
				$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,$bukti,NULL);
				$this->load->view('sekolah/bpk/cetak_bpk',$data);	
			}else if($key == 'bpb'){
				if($bulan == 1){
					$data['saldo_awal'] = 0;
				}else{
					$data['saldo_awal'] = $this->rkas_model->saldo_bpb($bulan,$tahun,$sekolahid);
				}
				$bukti = array('BM','BK');
				$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,$bukti,NULL);
				$this->load->view('sekolah/bpb/cetak_bpb',$data);	
			}else if($key == 'bpp'){
				if($bulan == 1){
					$data['saldo_awal'] = 0;
				}else{
					$data['saldo_awal'] = $this->rkas_model->saldo_pajak($bulan,$tahun,$sekolahid);
				}
				$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,'pajak');
				$this->load->view('sekolah/bpp/cetak_bpp',$data);
			}else{
				echo "<h1>Oops.. <br>Terjadi Kesalahan</h1>";
			}
				
		}else{
			redirect(base_url('sekolah'));
		}	
	}

	public function data_belanja_rkas()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$rincian_id = $this->input->post('rincian_id');
		$bulan_id = $this->input->post('bulan_id');
		$data = $this->rkas_model->get_total_belanja_program($rincian_id,$bulan_id);
		echo json_encode($data);		
	}

	public function get_data_pajak()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$id = $this->input->post('id');
		$data = $this->rkas_model->get_data_pajak($id);
	}

	public function bpk()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$data['tahun'] = $this->session->userdata('token_tahun');
		$data['jh1'] = 'Data BPK';
		$data['jhsmall'] = 'Data Buku Pembantu Kas';
		$data['bc'] = "<li><a href='#'>Buku Pembantu Kas</a></li>";
		$data['conten'] = 'sekolah/bpk/index';
		$data['bulan'] = $this->rkas_model->get_bulan_all();
		$this->load->view('sekolah/layout',$data);
	}

	public function data_bpk_show()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$bulan = $this->input->post('bulan');
		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo_bpk($bulan,$tahun,$sekolahid);
			}
			$bukti = array('KM','KK');
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,$bukti,NULL);
			$this->load->view('sekolah/bpk/bpk_show',$data);	
		}else{
			redirect(base_url('sekolah'));
		}
		
	}

	public function bpb()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$data['tahun'] = $this->session->userdata('token_tahun');
		$data['jh1'] = 'Data BPB';
		$data['jhsmall'] = 'Data Buku Pembantu Bank';
		$data['bc'] = "<li><a href='#'>Buku Pembantu Bank</a></li>";
		$data['conten'] = 'sekolah/bpb/index';
		$data['bulan'] = $this->rkas_model->get_bulan_all();
		$this->load->view('sekolah/layout',$data);
	}

	public function data_bpb_show()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$bulan = $this->input->post('bulan');
		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo_bpb($bulan,$tahun,$sekolahid);
			}
			$bukti = array('BM','BK');
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,$bukti,NULL);
			$this->load->view('sekolah/bpb/bpb_show',$data);	
		}else{
			redirect(base_url('sekolah'));
		}
		
	}

	public function bpp()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$data['tahun'] = $this->session->userdata('token_tahun');
		$data['jh1'] = 'Data BPP';
		$data['jhsmall'] = 'Data Buku Pembantu Pajak';
		$data['bc'] = "<li><a href='#'>Buku Pembantu Pajak</a></li>";
		$data['conten'] = 'sekolah/bpp/index';
		$data['bulan'] = $this->rkas_model->get_bulan_all();
		$this->load->view('sekolah/layout',$data);
	}

	public function data_bpp_show()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$bulan = $this->input->post('bulan');
		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo_pajak($bulan,$tahun,$sekolahid);
			}
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,'pajak');
			$this->load->view('sekolah/bpp/bpp_show',$data);	
		}else{
			redirect(base_url('sekolah'));
		}
		
	}

	public function aksi_simpan_saldo()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$saldo = $this->input->post('saldo_tunai');
		$saldo_bulan = $this->input->post('saldo_bulan');
		$saldo_number = $this->rkas_model->destroy_titik($saldo);
		$saldo_bulan_number = $this->rkas_model->destroy_titik($saldo_bulan);
		if(isset($saldo)){
			$data['bulan_id'] = $this->input->post('bulan_id');
			$data['jumlah_saldo'] = $saldo_number;
			$this->rkas_model->simpan_saldo($data,$saldo_bulan_number);
		}else{
			redirect(base_url('sekolah'));
		}
	}

	public function realisasi()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$data['regulasi'] = $this->sekolah_model->get_regulasi_login($tahun);
		$data['tahun'] = $this->session->userdata('token_tahun');
		$data['jh1'] = 'Laporan Realisasi Dana BOS';
		$data['jhsmall'] = 'Laporan Realisasi Dana BOS';
		$data['bc'] = "<li><a href='#'>Laporan Realisasi Dana BOS</a></li>";
		$data['conten'] = 'sekolah/realisasi/index';
		$data['js'] = 'sekolah/realisasi/js';
		$this->load->view('sekolah/layout',$data);
	}

	public function realisasi_show()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$tw = $this->input->post('tw');
		$data['tahun'] = $tahun;
		$data['sekolahid'] = $sekolahid;
		$data['jasa'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid);
		$data['mesin'] = $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid);
		$data['aset'] = $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid);
		$data['total_belanja_jasa'] = $this->rkas_model->get_total_jenis_belanja(1,$tahun,$sekolahid);
		$data['total_belanja_mesin'] = $this->rkas_model->get_total_jenis_belanja(2,$tahun,$sekolahid);
		$data['total_belanja_aset'] = $this->rkas_model->get_total_jenis_belanja(3,$tahun,$sekolahid);
		$data['total_realisasi_jasa'] = $this->rkas_model->get_total_realisasi(1,$tw,$tahun,$sekolahid);
		$data['total_realisasi_mesin'] = $this->rkas_model->get_total_realisasi(2,$tw,$tahun,$sekolahid);
		$data['total_realisasi_aset'] = $this->rkas_model->get_total_realisasi(3,$tw,$tahun,$sekolahid);
		$data['komponen_biaya'] = $this->rkas_model->get_komponen_biaya($sekolahid,$tahun);
		$data['standar_nasional'] = $this->rkas_model->get_standar_nasional();
		$data['tw'] = $tw;
		$data['total_komponen'] = $this->rkas_model->get_realisasi_komponen_biaya($tw,$tahun,$sekolahid);
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		$data['get_total_saldo_tunai'] = $this->rkas_model->get_total_saldo_tunai_tw($tw,$tahun,$sekolahid);
		
		if($tw == 1){
			$data['jasa_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['mesin_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['aset_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_jasa_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_mesin_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_aset_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
		}else{
			$twafter = $tw-1;
			$data['jasa_before'] = $this->rkas_model->get_realisasi(1,$twafter,$tahun,$sekolahid);
			$data['mesin_before'] = $this->rkas_model->get_realisasi(2,$twafter,$tahun,$sekolahid);
			$data['aset_before'] = $this->rkas_model->get_realisasi(3,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_jasa_before'] = $this->rkas_model->get_total_realisasi_before(1,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_mesin_before'] = $this->rkas_model->get_total_realisasi_before(2,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_aset_before'] = $this->rkas_model->get_total_realisasi_before(3,$twafter,$tahun,$sekolahid);
		}

		if($tw == 1){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = 0;
			$data['sptjm_tw3'] = 0;
			$data['sptjm_tw4'] = 0;
		}else if($tw == 2){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = 0;
			$data['sptjm_tw4'] = 0;
		}else if($tw == 3){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-2,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw4'] = 0;
		}else{
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-3,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-3,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-3,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-2,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw4'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
		}
		
		if(isset($tw)){
			$this->load->view('sekolah/realisasi/realisasi_show',$data);
		}else{
			redirect(base_url('sekolah'));
		}	
	}

	public function cetakrealisasi($tw,$key)
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$acak = rand(100000,100000000);
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$acak."_laporan_".$key.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$sekolahid = $this->session->userdata('token_sekolah_id');
		$tahun = $this->session->userdata('token_tahun');
		$romawi = array(1 => array('R' => 'I', 'P' => 'Januari - Maret'), 2 => array('R' => 'II', 'P' => 'April - Juni'), 3 => array('R' => 'III', 'P' => 'Juli - September'), 4 => array('R' => 'IV', 'P' => 'Oktober - Desember'));
		$data['tahun'] = $tahun;
		$data['sekolahid'] = $sekolahid;
		$data['jasa'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid);
		$data['mesin'] = $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid);
		$data['aset'] = $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid);
		$data['total_belanja_jasa'] = $this->rkas_model->get_total_jenis_belanja(1,$tahun,$sekolahid);
		$data['total_belanja_mesin'] = $this->rkas_model->get_total_jenis_belanja(2,$tahun,$sekolahid);
		$data['total_belanja_aset'] = $this->rkas_model->get_total_jenis_belanja(3,$tahun,$sekolahid);
		$data['total_realisasi_jasa'] = $this->rkas_model->get_total_realisasi(1,$tw,$tahun,$sekolahid);
		$data['total_realisasi_mesin'] = $this->rkas_model->get_total_realisasi(2,$tw,$tahun,$sekolahid);
		$data['total_realisasi_aset'] = $this->rkas_model->get_total_realisasi(3,$tw,$tahun,$sekolahid);
		$data['komponen_biaya'] = $this->rkas_model->get_komponen_biaya($sekolahid,$tahun);
		$data['standar_nasional'] = $this->rkas_model->get_standar_nasional();
		$data['tw'] = $tw;
		$data['tw_romawi'] = $romawi[$tw]['R'];
		$data['periode'] = $romawi[$tw]['P'];
		$data['tahun'] = $tahun;
		$data['total_komponen'] = $this->rkas_model->get_realisasi_komponen_biaya($tw,$tahun,$sekolahid);
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		$data['get_total_saldo_tunai'] = $this->rkas_model->get_total_saldo_tunai_tw($tw,$tahun,$sekolahid);
		if($tw == 1){
			$twbefore = 0;
		}elseif($tw == 2){
			$twbefore = 1;
		}elseif($tw == 3){
			$twbefore = 2;
		}elseif($tw == 4){
			$twbefore = 3;
		}
		$data['get_total_saldo_tunai_before'] = $this->rkas_model->get_total_saldo_tunai_tw($twbefore,$tahun,$sekolahid);
		
		if($tw == 1){
			$data['jasa_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['mesin_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['aset_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_jasa_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_mesin_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_aset_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
		}else{
			$twafter = $tw-1;
			$data['jasa_before'] = $this->rkas_model->get_realisasi(1,$twafter,$tahun,$sekolahid);
			$data['mesin_before'] = $this->rkas_model->get_realisasi(2,$twafter,$tahun,$sekolahid);
			$data['aset_before'] = $this->rkas_model->get_realisasi(3,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_jasa_before'] = $this->rkas_model->get_total_realisasi_before(1,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_mesin_before'] = $this->rkas_model->get_total_realisasi_before(2,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_aset_before'] = $this->rkas_model->get_total_realisasi_before(3,$twafter,$tahun,$sekolahid);
		}

		if($tw == 1){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = 0;
			$data['sptjm_tw3'] = 0;
			$data['sptjm_tw4'] = 0;
		}else if($tw == 2){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = 0;
			$data['sptjm_tw4'] = 0;
		}else if($tw == 3){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-2,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw4'] = 0;
		}else{
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-3,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-3,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-3,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-2,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw4'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
		}
		
		if(isset($tw)){
			if($key == 'realisasi'){
				$this->load->view('sekolah/realisasi/cetak_realisasi',$data);
			}else if($key == 'penggunaan'){
				$this->load->view('sekolah/realisasi/cetak_penggunaan',$data);
			}else if($key == 'sptjm'){
				$this->load->view('sekolah/realisasi/cetak_sptjm',$data);
			}else{
				echo "<h1>Oopss...<br>Terjadi kesalahan.</h1>";
			}
			
		}else{
			redirect(base_url('sekolah'));
		}
	}






	public function tester()
	{
		$tw = 2;
		$twafter = $tw-1;
		$total_realisasi_jasa_before = $this->rkas_model->get_total_realisasi_before(1,$twafter,$tahun,$sekolahid);
		$total_realisasi_mesin_before = $this->rkas_model->get_total_realisasi_before(2,$twafter,$tahun,$sekolahid);
		$total_realisasi_aset_before = $this->rkas_model->get_total_realisasi_before(3,$twafter,$tahun,$sekolahid);
		$total_realisasi_jasa = $this->rkas_model->get_total_realisasi(1,$tw,$tahun,$sekolahid);
		$total_realisasi_mesin = $this->rkas_model->get_total_realisasi(2,$tw,$tahun,$sekolahid);
		$total_realisasi_aset = $this->rkas_model->get_total_realisasi(3,$tw,$tahun,$sekolahid);

		$respon = array(
			'total_realisasi_jasa_before' => $total_realisasi_jasa_before['total_realisasi'],
			'total_realisasi_mesin_before' => $total_realisasi_mesin_before['total_realisasi'],
			'total_realisasi_aset_before' => $total_realisasi_aset_before['total_realisasi'],
			'total_realisasi_jasa' => $total_realisasi_jasa['total_realisasi'],
			'total_realisasi_mesin' => $total_realisasi_mesin['total_realisasi'],
			'total_realisasi_aset' => $total_realisasi_aset['total_realisasi']
		);
		$respon = $this->rkas_model->get_total_saldo_tunai_tw(1,$tahun,$sekolahid);
		echo json_encode($respon);
	}

	// END BKU 2019


	// START RKAS 2020
	public function rkas2020()
	{
		$this->model_sequrity_rkas->get_sequrity_sekolah();
		$data = $this->rkas_model->get_data_bos(1,2019);
		var_dump($data);
		// $data['jh1'] = 'Cetak RKA Sekolah';
		// $data['jhsmall'] = 'Cetak';
		// $data['bc'] = "<li><a href='#'>Layout</a></li>";
		// $data['conten'] = 'sekolah/rkas/index';
		// $this->load->view('sekolah/layout',$data);		
	}

	// END RKAS 2020

}
