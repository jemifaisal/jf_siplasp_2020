<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webadmin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('permalink_helper');
		$this->load->model('webadmin_model');
		$this->load->model('sekolah_model');
		$this->load->model('rkas_model');
	}

	public function copy_program(){
		$this->db->where('regulasi_id',1);
		$sql = $this->db->get('sub_program');
		if($sql){
			// $data = $sql->result();
			foreach($sql->result() as $rows){
				$data['program_kode'] = $rows->program_kode;
				$data['nama_program'] = $rows->nama_program;
				$data['substandar_id'] = $rows->substandar_id;
				$data['komponen_kode'] = $rows->komponen_kode;
 				$data['regulasi_id'] = 2;
				//$this->db->insert('sub_program',$data);
			}
		}
	}
	
	public function programexcel(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$this->db->join('sub_standar ss','sp.substandar_id=ss.substandar_id');
		$this->db->join('standar_nasional sn','=ss.standar_id=sn.standar_id');
		$this->db->where('sp.soft_delete',0);
		$this->db->where('sn.soft_delete',0);
		$this->db->where('ss.soft_delete',0);
		$this->db->order_by('sp.substandar_id','asc');
		$this->db->order_by('sp.program_id','asc');
		$sql = $this->db->get('sub_program sp');
		$data['program'] = $sql->result();
		$this->load->view('webadmin/program_excel',$data);
		//var_dump($sql);	
	}

	public function index()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Dashboard';
		$data['jhsmall'] = 'Administrator Area';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'webadmin/home';
		$this->load->view('webadmin/layout',$data);			
	}
	
	//aset
	public function asetkabupaten($tahun,$kabupaten){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['tahun'] = $tahun;
		$data['kabupaten'] = $kabupaten;
		$this->db->where('kd_kab',$kabupaten);
		$query = $this->db->get('kab');
		if($query){
			foreach ($query->result() as $key) {
				$data['nama_kabupaten'] = $key->nama_kab;
				$kab = $key->nama_kab;
			}	
		}
        
		$this->db->join('kec','kec.kd_kec=sekolah.kd_kec');
		$this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$this->db->join('kib_e','kib_e.sekolah_id=sekolah.sekolah_id');
		$this->db->join('sumber_dana','sumber_dana.dana_id=kib_e.asal_usul');

		$this->db->where('sekolah.status_sekolah','Negeri');
		$this->db->where('kab.kd_kab',$kabupaten);
		$this->db->where('kib_e.asal_usul',2);
		$this->db->where('kib_e.tahun_pembelian',$tahun);
		$this->db->order_by('sekolah.nama_sp','ASC');
		$sql = $this->db->get('sekolah');
		// var_dump($sql->result());
		if($sql){
			$data['aset'] = $sql->result();
		}
		$acak = rand(100000,100000000);
		header("Content-type: application/octet-stream"); //octet-stream vnd-ms-excel
		header("Content-Disposition: attachment; filename=".$acak."_".$kab.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$this->load->view('webadmin/downloadaset_kabupaten',$data);
	}
	
	public function aset()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Aset Sekolah';
		$data['jhsmall'] = 'Sekolah';
		$data['kib'] = $this->webadmin_model->lihatdata();
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'webadmin/lihat';
		$this->load->view('webadmin/layout',$data);
	}
	
	public function downloadaset($tahun,$sekolah)
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		//$data['conten'] = 'webadmin/lihat';
		$data['tahun'] = $tahun;
		$data['sekolah'] = $sekolah;
		$data['nama_sekolah'] = $this->webadmin_model->ambil_sekolah($sekolah);
		$nm_sp = $this->webadmin_model->ambil_sekolah($sekolah);
		$nama_sekolah = str_replace(' ', '_', $nm_sp);
		$acak = rand(100000,100000000);
		header("Content-type: application/vnd-ms-excel"); //octet-stream
		header("Content-Disposition: attachment; filename=".$acak."_".$nama_sekolah.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		$data['kib_b_bosnas'] = $this->webadmin_model->detail_aset_tahun('kib_b',$tahun,$sekolah,1);
		$data['total_kib_b_bosnas'] = $this->webadmin_model->total_aset_sekolah_tahun($tahun,$sekolah,'kib_b','BOSNAS');

		$data['kib_b_bosda'] = $this->webadmin_model->detail_aset_tahun('kib_b',$tahun,$sekolah,2);
		$data['total_kib_b_bosda'] = $this->webadmin_model->total_aset_sekolah_tahun($tahun,$sekolah,'kib_b','BOSDA');

		$data['kib_e_bosnas'] = $this->webadmin_model->detail_aset_tahun('kib_e',$tahun,$sekolah,1);
		$data['total_kib_e_bosnas'] = $this->webadmin_model->total_aset_sekolah_tahun($tahun,$sekolah,'kib_e','BOSNAS');

		$data['kib_e_bosda'] = $this->webadmin_model->detail_aset_tahun('kib_e',$tahun,$sekolah,2);
		$data['total_kib_e_bosda'] = $this->webadmin_model->total_aset_sekolah_tahun($tahun,$sekolah,'kib_e','BOSDA');

		$this->load->view('webadmin/downloadaset',$data);
	}
	// end aset

	public function login_log(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		echo "<h2>History Login Per Hari</h2>";
		$today = date('Y-m-d');
		$this->db->join('sekolah s','s.npsn=ll.username');
		$this->db->where('left(ll.datetime,10)',$today);
		$this->db->order_by('ll.log_id','desc');
		$sql = $this->db->get('login_log ll');	
		if($sql){
			// var_dump(count($sql));
			// echo count($sql->result())."<hr>";
			$no =0;
			foreach ($sql->result() as $row) {
                $no++;
				echo $no. " => ".$row->npsn." => ". $row->nama_sp." => ".$row->datetime."<hr>";
			}
		}
	}
	
	public function password_log(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		echo "<h2>History Ubah Password</h2>";
		$today = date('Y-m-d');
		$this->db->join('sekolah s','s.sekolah_id=ll.sekolah_id');
		//$this->db->where('left(ll.datetime,10)',$today);
		$this->db->order_by('ll.password_id','desc');
		$sql = $this->db->get('password_log ll');	
		if($sql){
			// var_dump(count($sql));
			// echo count($sql->result())."<hr>";
			$no =0;
			foreach ($sql->result() as $row) {
                $no++;
				echo $no. " => ".$row->npsn." => ". $row->nama_sp." => ".$row->datetime."<hr>";
			}
		}
	}

	public function rkasekolah($id='',$tahun='')
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$regulasi = $this->sekolah_model->get_regulasi_login($tahun);
		$data['sekolahid'] = $id;
		$data['tahunsekolah'] = $tahun;
		$data['jh1'] = 'Cetak RKA Sekolah';
		$data['jhsmall'] = 'Cetak';
		$data['bc'] = "<li><a href='#'>Layout</a></li>";
		if($regulasi == 3){
			$data['conten'] = 'webadmin/cetak_rkas_3';
		}else{
			$data['conten'] = 'webadmin/cetak_rkas';
		}
		
		$this->load->view('webadmin/layout',$data);	
	}

	public function excelrkas($id='',$tahun='')
	{	
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['sekolahid'] = $id;
		$data['tahunsekolah'] = $tahun;
		$acak = rand(100000,100000000);
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$acak.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$data['jh1'] = 'Cetak RKA Sekolah';
		$data['jhsmall'] = 'Cetak';
		$data['bc'] = "<li><a href='#'>Layout</a></li>";
		//$data['conten'] = 'sekolah/cetak_rkas';
		$this->load->view('webadmin/cetak_rkas_excel',$data);		
	}

	function pdfrkas($id='',$tahun='')
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['sekolahid'] = $id;
		$data['tahunsekolah'] = $tahun;
		$acak = rand(10000,100000);
		ob_start();
	    require_once('./application/third_party/html2pdf/html2pdf.class.php');
	    $data['siswa'] = "hello world";
	    $this->load->view('webadmin/cetak_rkas_pdf',$data);
	    $html = ob_get_contents();
	    

	    $pdf = new HTML2PDF('L','Legal','en');
	    $pdf->WriteHTML($html);
	    ob_end_clean();
	    $pdf->Output($acak.'_'.$tahun.'.pdf', 'D');
		
	}

	//SARPRAS
	public function sarpras(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Sarpras Sekolah';
		$data['jhsmall'] = 'Data Sarpras Sekolah';
		$data['bc'] = "<li><a href='#'>Sarpras Sekolah</a></li>";
		$data['conten'] = 'webadmin/sarpras_sekolah';

		//$this->db->where('soft_delete','0');
		//$this->db->select(',* s.soft_delete as ket');
		//$this->db->join('kec','kec.kd_kec=s.kd_kec');
		//$this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$this->db->select('*, sum(sl.jumlah_labor) as j_labor, s.sekolah_id as sekolahid, sk.jumlah_baik as k_baik,sk.rusak_ringan as k_rr,sk.rusak_sedang as k_rs,sk.rusak_berat as k_rb,
			sum(sl.jumlah_baik) as l_baik,sum(sl.rusak_ringan) as l_rr,sum(sl.rusak_sedang) as l_rs,sum(sl.rusak_berat) as l_rb');
		$this->db->join('sarpras_kelas sk','sk.sekolah_id=s.sekolah_id','left');
		$this->db->join('sarpras_labor sl','sl.sekolah_id=s.sekolah_id','left');
		//$this->db->join('sarpras_labor sl','sl.sekolah_id=s.sekolah_id','left');
		$this->db->group_by('s.sekolah_id');
		$sql = $this->db->get('sekolah s');
		$data['sarpras'] = $sql->result();

		$this->load->view('webadmin/layout',$data);
	}

	public function detailsarpras($id=''){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Detail Sarpras Sekolah';
		$data['jhsmall'] = 'Data Detail Sarpras Sekolah';
		$data['bc'] = "<li><a href='#'>Detail Sarpras Sekolah</a></li>";
		$data['conten'] = 'webadmin/detail_sarpras_kelas';

		$this->db->where('sekolah_id',$id);
		$this->db->where('status',0);
		$sql = $this->db->get('sekolah');
		if($sql){
			$data['sekolah'] = $sql->result();
			$data['sekolah_id'] = $id;
		}
		$this->load->view('webadmin/layout',$data);
			
	}


	//RKA SEKOLAH
	function login(){
		if(!empty($this->session->userdata('token_login_webadmin'))){
			redirect(base_url('webadmin'));
		}else{
			$this->session->sess_destroy();
			$this->load->view('webadmin/login');
		}
	}
	function logout(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$this->session->sess_destroy();
		redirect(base_url('webadmin/login'));
	}
	function getlogin(){
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->webadmin_model->m_get_login($u,$p);
	}

	public function penerimabos(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Data Penerima Dana BOS';
		$data['jhsmall'] = 'Sekolah';
		$data['get_tahun'] = $this->webadmin_model->get_tahun();
		$data['get_kab'] = $this->webadmin_model->kabupaten();
		$data['conten'] = 'webadmin/penerima_bos_2019';
		$this->load->view('webadmin/layout',$data);

	}

	public function edit_sisa_uang(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$key = $this->input->post('id');
		$this->db->where('bos_id',$key);
		$this->db->join('sekolah s','pb.npsn = s.npsn');
		$sql = $this->db->get('penerima_bos pb');
		if($sql){
			$data['bos'] = $sql->result();
		}
		$this->load->view('webadmin/sisa_uang',$data);

	}

	public function add_sisa_uang(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$bosid = $this->input->post('bosid');
		$data['sisa_uang'] = str_replace(".", "", $this->input->post('sisauang'));
		$data['triwulan'] = $this->input->post('triwulan');

		$this->db->where('bos_id',$bosid);
		$sql = $this->db->update('penerima_bos',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/penerimabos'));	
		}
			
	}

	public function json_penerimabos(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$key = $this->session->userdata('token_sekolah_id');
		$this->load->library('datatables');
		//$this->datatables->where('p.sekolah_id',$key);
		$this->datatables->select("s.npsn,s.jenjang, s.status_sekolah,pb.tahun,s.nama_sp,kb.nama_kab,k.nama_kec");
		$this->datatables->join('penerima_bos pb', 'pb.npsn = s.npsn','left');
		$this->datatables->join('kec k', 's.kd_kec = k.kd_kec','left');
		$this->datatables->join('kab kb', 'k.kd_kab = kb.kd_kab','left');
		$this->datatables->from("sekolah s");
		//$this->datatables->where("s.sekolah_id",$key);

		//$this->datatables->select("id_berita,judul,nama_kategori,berita.username,hari,tanggal");
        //$this->datatables->from("prestasi");
        //$this->datatables->join('kategori', 'berita.id_kategori = kategori.id_kategori');
        return print_r($this->datatables->generate());
	}


	public function prestasi(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Data Prestasi';
		$data['jhsmall'] = 'Sekolah';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'webadmin/prestasi';
		$this->load->view('webadmin/layout',$data);

	}

	public function json_prestasi(){
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$this->load->library('datatables');
		$this->datatables->select("s.nama_sp,left(p.tgl_mulai,4) as tahun, kl.nama_katlomba, p.jenis_lomba,p.tingkat_lomba,p.peringkat,p.nama_siswa,p.tempat_lomba,p.penyelenggara,p.tgl_mulai,p.tgl_akhir,p.prestasi_id, l.nama_lomba as namalomba, p.nama_lomba as uraian");
		$this->datatables->join('kategori_lomba kl', 'p.katlomba_id = kl.katlomba_id');
		$this->datatables->join('lomba l', 'kl.lomba_id = l.lomba_id');
		$this->datatables->join('sekolah s', 'p.sekolah_id = s.sekolah_id');
		$this->datatables->from("prestasi p");
        return print_r($this->datatables->generate());
	}


	public function bosnas(){
		$data['jh1'] = 'Entry RKAS BOSNAS';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'webadmin/bosnas';
		$this->load->view('webadmin/layout',$data);
	}


	public function barangjasa(){
		$data['jh1'] = 'RKAS Belanja Barang dan Jasa';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'webadmin/belanja_barangjasa';

		$sql = $this->db->get('standar_nasional');
		if($sql){
			$data['standar_nasional'] = $sql->result();
		}

		$this->load->view('webadmin/layout',$data);
	}

	public function rincianbelanja($id=''){
		$data['jh1'] = 'Rincian Belanja Barang dan Jasa';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'webadmin/rincian_belanja';

		$this->db->where('standar_id',$id);
		$sql = $this->db->get('sub_standar');
		if($sql){
			$data['sub_standar'] = $sql->result();
		}

		$this->load->view('webadmin/layout',$data);
	}

	public function tambahrincian()
	{
		$data['key'] = $this->input->post('id');
		$key = $this->input->post('id');
		$data['sub_program'] = $this->webadmin_model->sub_kegiatan($key);
		$this->load->view('webadmin/tambah_rincian',$data);
		
	}

	public function editrincian()
	{
		$data['key'] = $this->input->post('id3');
		$key = $this->input->post('id3');
		$this->db->where('rincian_id',$key);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('rincian_belanja');
		if($sql){
			$data['rincian'] = $sql->result();
		}

		$this->load->view('webadmin/edit_rincian',$data);
		
	}

	public function editrincianheader()
	{
		$data['key'] = $this->input->post('id3');
		$key = $this->input->post('id3');
		$this->db->where('rincian_id',$key);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('rincian_header');
		if($sql){
			$data['rincian'] = $sql->result();
		}

		$this->load->view('webadmin/edit_rincian_header',$data);
		
	}

	public function tambahrincianheader()
	{
		$data['key'] = $this->input->post('id2');
		$key = $this->input->post('id2');
		//$data['sub_program'] = $this->webadmin_model->sub_kegiatan($key);

		$this->db->where('rincian_id',$key);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('rincian_belanja');
		if($sql){
			$data['head'] = $sql->result();
		}
		$this->load->view('webadmin/tambah_rincian_header',$data);
		
	}

	public function add_rincian()
	{
		$sr = $this->input->post('sr');
		if($sr == 1){
			//header
			$key = $this->input->post('id');
			$data['program_id'] = $this->input->post('program');
			$data['detail_rincian'] = $this->input->post('nama');
			$data['status_rincian'] = $this->input->post('sr');
			$data['tw1'] = 0;
			$data['tw2'] = 0;
			$data['tw3'] = 0;
			$data['tw4'] = 0;
			

			$sql = $this->db->insert('rincian_belanja',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/coba'));
			}
		}else{
			//detail
			$key = $this->input->post('id');
			$data['program_id'] = $this->input->post('program');
			$data['detail_rincian'] = $this->input->post('nama');
			$data['status_rincian'] = $this->input->post('sr');
			$data['tw1'] = $this->input->post('tw1');
			$data['tw2'] = $this->input->post('tw2');
			$data['tw3'] = $this->input->post('tw3');
			$data['tw4'] = $this->input->post('tw4');
			

			$sql = $this->db->insert('rincian_belanja',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/coba'));
			}
		}
			
	}

	public function add_rincian_header()
	{
		$key = $this->input->post('id');
		$data['rincian_id'] = $key;
		$data['header_rincian'] = $this->input->post('nama');
		$data['htw1'] = $this->input->post('tw1');
		$data['htw2'] = $this->input->post('tw2');
		$data['htw3'] = $this->input->post('tw3');
		$data['htw4'] = $this->input->post('tw4');
			

		$sql = $this->db->insert('rincian_header',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('webadmin/coba'));
		}
	}

	public function modal(){
		$data['jh1'] = 'RKAS Belanja Modal';
		$data['jhsmall'] = 'RKAS Bosnas';
		//$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$data['conten'] = 'webadmin/belanja_modal';
		$this->load->view('webadmin/layout',$data);
	}

	public function coba(){
		$data['jh1'] = 'RKAS Belanja Modal';
		$data['jhsmall'] = 'RKAS Bosnas';
		$data['conten'] = 'webadmin/coba2';
		$this->load->view('webadmin/layout',$data);
	}


	public function hapusrincian($id){
		$this->db->where('rincian_id',$id);
		$sql = $this->db->delete('rincian_belanja');
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/coba'));	
		}
	}

	public function hapusrincianheader($id){
		$this->db->where('header_id',$id);
		$sql = $this->db->delete('rincian_header');
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/coba'));	
		}
	}


	//RKA SEKOLAH
	public function komponen()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Komponen Pembiayaan';
		$data['jhsmall'] = 'Data Komponen Pembiayaan';
		$data['bc'] = "<li><a href='#'>Komponen Pembiayaan</a></li>";
		$data['conten'] = 'webadmin/komponen_pembiayaan';
		$sql = $this->db->get('komponen_pembiayaan');
		$data['komponen'] = $sql->result();

		$this->load->view('webadmin/layout',$data);		
	}

	public function aksi_komponen()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$this->load->view('webadmin/add_komponen');
		}else{
			$this->db->where('komponen_id',$pecah[1]);
			$sql = $this->db->get('komponen_pembiayaan');
			if($sql->num_rows() > 0){
				$data['komponen'] = $sql->result();
			}

			$this->load->view('webadmin/edit_komponen',$data);
		}
		
	}

	public function add_komponen()
	{
		$sk = $this->input->post('kodekomponen');
		$this->db->where('komponen_kode',$sk);
		$cek = $this->db->get('komponen_pembiayaan');
		if($cek->num_rows() > 0){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Kode Komponen Pembiayaan Sudah Ada Pada Database.
					  </div>");
				redirect(base_url('webadmin/komponen'));
		}else{
			$data['komponen_kode'] = $this->input->post('kodekomponen');
			$data['nama_komponen'] = $this->input->post('namakomponen');
			$sql = $this->db->insert('komponen_pembiayaan',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/komponen'));
			}	
		}

			
	}

	public function proses_edit_komponen()
	{
		$sk = $this->input->post('kodekomponen');
		$standarid = $this->input->post('komponenid');
		$this->db->where('komponen_kode',$sk);
		$cek = $this->db->get('komponen_pembiayaan');
		if($cek->num_rows() == 1){
			$data['komponen_kode'] = $this->input->post('kodekomponen');
			$data['nama_komponen'] = $this->input->post('namakomponen');
			$data['soft_delete'] = $this->input->post('status');
			$this->db->where('komponen_id',$standarid);
			$sql = $this->db->update('komponen_pembiayaan',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/komponen'));
			}
		}elseif ($cek->num_rows() < 1) {
			# code...
			$data['komponen_kode'] = $this->input->post('kodekomponen');
			$data['nama_komponen'] = $this->input->post('namakomponen');
			//$this->db->where('standar_id',$standarid);
			$sql = $this->db->insert('komponen_pembiayaan',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/komponen'));
			}
		}else{
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Kode Komponen Pembiayaan Sudah Ada Pada Database.
					  </div>");
				redirect(base_url('webadmin/komponen'));
				
		}
	}

	public function hapus_komponen()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('komponen_id',$key);
		$sql = $this->db->update('komponen_pembiayaan',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/komponen'));
		}
	}

	public function program()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Program Kegiatan';
		$data['jhsmall'] = 'Data Program Kegiatan';
		$data['bc'] = "<li><a href='#'>Program Kegiatan</a></li>";
		$data['conten'] = 'webadmin/program';
		$this->db->select('*, p.soft_delete as soft_del');
		$this->db->join('sub_standar ss','ss.substandar_id=p.substandar_id');
		$this->db->order_by('p.substandar_id','ASC');
		$sql = $this->db->get('sub_program p');
		$data['program'] = $sql->result();

		$this->load->view('webadmin/layout',$data);	
	}

	public function aksi_program()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['substandar'] = $this->webadmin_model->substandar();
			$data['biaya'] = $this->webadmin_model->biaya();
			$this->load->view('webadmin/add_program',$data);
		}else{
			$this->db->where('program_id',$pecah[1]);
			$sql = $this->db->get('sub_program');
			$data['substandar'] = $this->webadmin_model->substandar();
			$data['biaya'] = $this->webadmin_model->biaya();
			if($sql->num_rows() > 0){
				$data['program'] = $sql->result();
			}

			$this->load->view('webadmin/edit_program',$data);
		}
		
	}

	public function add_program()
	{
		$sk = $this->input->post('substandar');
		$this->db->where('substandar_id',$sk);
		$cek = $this->db->get('sub_standar');
		if($cek){
			foreach ($cek->result() as $row) {
				$standar_kode = $row->substandar_kode;
			}
			
			$data['substandar_id'] = $this->input->post('substandar');
			$data['program_kode'] = $standar_kode.".".$this->input->post('kodesub');
			$data['nama_program'] = $this->input->post('namasub');
			$data['komponen_kode'] = implode(',',$this->input->post('biaya'));
			$sql = $this->db->insert('sub_program',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/program'));
			}	
		}

			
	}

	public function proses_edit_program()
	{
		$subid = $this->input->post('programid');
		$sk = $this->input->post('substandar');
		$this->db->where('substandar_id',$sk);
		$cek = $this->db->get('sub_standar');
		if($cek){
			foreach ($cek->result() as $row) {
				$standar_kode = $row->substandar_kode;
			}

			$a = $this->input->post('kodesub');
			$pecah = explode(".", $a);
			$data['substandar_id'] = $this->input->post('substandar');
			$data['program_kode'] = $standar_kode.".".$pecah[2];
			$data['nama_program'] = $this->input->post('namasub');
			$data['komponen_kode'] = implode(',',$this->input->post('biaya'));
			$data['soft_delete'] = $this->input->post('status');

			$this->db->where('program_id',$subid);
			$sql = $this->db->update('sub_program',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/program'));
			}	
		}	
		
	}

	public function hapus_program()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('program_id',$key);
		$sql = $this->db->update('sub_program',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/program'));
		}
	}

	public function substandar()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Sub Standar Nasional';
		$data['jhsmall'] = 'Data Sub Standar Nasional';
		$data['bc'] = "<li><a href='#'>Sub Standar Nasional</a></li>";
		$data['conten'] = 'webadmin/substandar_nasional';
		$this->db->select('*, ss.soft_delete as soft_del');
		$this->db->join('standar_nasional sn','ss.standar_id=sn.standar_id');
		$this->db->order_by('sn.standar_id','ASC');
		$sql = $this->db->get('sub_standar ss');
		$data['substandar'] = $sql->result();

		$this->load->view('webadmin/layout',$data);	
	}

	public function aksi_substandar()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['standar'] = $this->webadmin_model->standar();
			$this->load->view('webadmin/add_substandar',$data);
		}else{
			$this->db->where('substandar_id',$pecah[1]);
			$sql = $this->db->get('sub_standar');
			$data['standar'] = $this->webadmin_model->standar();
			if($sql->num_rows() > 0){
				$data['substandar'] = $sql->result();
			}

			$this->load->view('webadmin/edit_substandar',$data);
		}
		
	}

	public function add_substandar()
	{
		$sk = $this->input->post('standar');
		$this->db->where('standar_id',$sk);
		$cek = $this->db->get('standar_nasional');
		if($cek){
			foreach ($cek->result() as $row) {
				$standar_kode = $row->standar_kode;
			}
			
			$data['standar_id'] = $this->input->post('standar');
			$data['substandar_kode'] = $standar_kode.".".$this->input->post('kodesub');
			$data['namasub_standar'] = $this->input->post('namasub');
			$data['soft_delete'] = 0;
			$sql = $this->db->insert('sub_standar',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/substandar'));
			}	
		}

			
	}

	public function proses_edit_substandar()
	{
		$subid = $this->input->post('subid');
		$sk = $this->input->post('standar');
		$this->db->where('standar_id',$sk);
		$cek = $this->db->get('standar_nasional');
		if($cek){
			foreach ($cek->result() as $row) {
				$standar_kode = $row->standar_kode;
			}

			$a = $this->input->post('kodesub');
			$pecah = explode(".", $a);
			$data['standar_id'] = $this->input->post('standar');
			$data['substandar_kode'] = $standar_kode.".".$pecah[1];
			$data['namasub_standar'] = $this->input->post('namasub');
			$data['soft_delete'] = $this->input->post('status');

			$this->db->where('substandar_id',$subid);
			$sql = $this->db->update('sub_standar',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/substandar'));
			}	
		}	
		
	}

	public function hapus_substandar()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('substandar_id',$key);
		$sql = $this->db->update('sub_standar',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/substandar'));
		}
	}

	public function standar()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Standar Nasional';
		$data['jhsmall'] = 'Data Standar Nasional';
		$data['bc'] = "<li><a href='#'>Standar Nasional</a></li>";
		$data['conten'] = 'webadmin/standar_nasional';
		$sql = $this->db->get('standar_nasional');
		$data['standar'] = $sql->result();

		$this->load->view('webadmin/layout',$data);		
	}

	public function aksi_standar()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$this->load->view('webadmin/add_standar');
		}else{
			$this->db->where('standar_id',$pecah[1]);
			$sql = $this->db->get('standar_nasional');
			if($sql->num_rows() > 0){
				$data['standar'] = $sql->result();
			}

			$this->load->view('webadmin/edit_standar',$data);
		}
		
	}

	public function add_standar()
	{
		$sk = $this->input->post('kodestandar');
		$this->db->where('standar_kode',$sk);
		$cek = $this->db->get('standar_nasional');
		if($cek->num_rows() > 0){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Kode Standar Nasional Sudah Ada Pada Database.
					  </div>");
				redirect(base_url('webadmin/standar'));
		}else{
			$data['standar_kode'] = $this->input->post('kodestandar');
			$data['nama_standar'] = $this->input->post('namastandar');
			$sql = $this->db->insert('standar_nasional',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/standar'));
			}	
		}

			
	}

	public function proses_edit_standar()
	{
		$sk = $this->input->post('kodestandar');
		$standarid = $this->input->post('standarid');
		$this->db->where('standar_kode',$sk);
		$cek = $this->db->get('standar_nasional');
		if($cek->num_rows() == 1){
			$data['standar_kode'] = $this->input->post('kodestandar');
			$data['nama_standar'] = $this->input->post('namastandar');
			$data['soft_delete'] = $this->input->post('status');
			$this->db->where('standar_id',$standarid);
			$sql = $this->db->update('standar_nasional',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/standar'));
			}
		}elseif ($cek->num_rows() < 1) {
			# code...
			$data['standar_kode'] = $this->input->post('kodestandar');
			$data['nama_standar'] = $this->input->post('namastandar');
			//$this->db->where('standar_id',$standarid);
			$sql = $this->db->insert('standar_nasional',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				redirect(base_url('webadmin/standar'));
			}
		}else{
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Kode Standar Nasional Sudah Ada Pada Database.
					  </div>");
				redirect(base_url('webadmin/standar'));
				
		}
	}

	public function hapus_standar()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('standar_id',$key);
		$sql = $this->db->update('standar_nasional',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/standar'));
		}
	}


	public function labor()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Labor';
		$data['jhsmall'] = 'Data Labor';
		$data['bc'] = "<li><a href='#'>Labor</a></li>";
		$data['conten'] = 'webadmin/labor';
		$sql = $this->db->get('labor');
		$data['labor'] = $sql->result();

		$this->load->view('webadmin/layout',$data);		
	}

	public function aksi_labor()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$this->load->view('webadmin/add_labor');
		}else{
			$this->db->where('labor_id',$pecah[1]);
			$sql = $this->db->get('labor');
			if($sql->num_rows() > 0){
				$data['labor'] = $sql->result();
			}

			$this->load->view('webadmin/edit_labor',$data);
		}
		
	}

	public function add_labor()
	{
		$data['nama_labor'] = $this->input->post('namalabor');
		$sql = $this->db->insert('labor',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('webadmin/labor'));
		}	
	}

	public function proses_edit_labor()
	{
		$key = $this->input->post('idlabor');
		$data['nama_labor'] = $this->input->post('namalabor');
		$data['soft_delete'] = $this->input->post('status');
		$this->db->where('labor_id',$key);
		$sql = $this->db->update('labor',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
				  </div>");
			redirect(base_url('webadmin/labor'));
		}
	}

	public function hapus_labor()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('labor_id',$key);
		$sql = $this->db->update('labor',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/labor'));
		}
	}

	public function kategorilomba()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Kategori Lomba';
		$data['jhsmall'] = 'Data Kategori Lomba';
		$data['bc'] = "<li><a href='#'>Kategori Lomba</a></li>";
		$data['conten'] = 'webadmin/kategori_lomba';

		$this->db->select('*, kl.soft_delete as soft_del');
		$this->db->join('lomba l','l.lomba_id=kl.lomba_id');
		$sql = $this->db->get('kategori_lomba kl');
		$data['katlomba'] = $sql->result();

		$this->load->view('webadmin/layout',$data);	
	}

	public function aksi_kategorilomba()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['lomba'] = $this->webadmin_model->datalomba();
			$this->load->view('webadmin/add_kategorilomba',$data);
		}else{

			$this->db->where('katlomba_id',$pecah[1]);
			$sql = $this->db->get('kategori_lomba');
			if($sql->num_rows() > 0){
				$data['katlomba'] = $sql->result();
			}
			$data['lomba'] = $this->webadmin_model->datalomba();
			$this->load->view('webadmin/edit_kategorilomba',$data);
		}
		
	}

	public function add_kategorilomba()
	{
		$data['lomba_id'] = $this->input->post('lomba');
		$data['nama_katlomba'] = $this->input->post('namakatlomba');
		$sql = $this->db->insert('kategori_lomba',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('webadmin/kategorilomba'));
		}	
	}

	public function proses_edit_kategorilomba()
	{
		$key = $this->input->post('idkatlomba');
		$data['lomba_id'] = $this->input->post('lomba');
		$data['nama_katlomba'] = $this->input->post('namakatlomba');
		$data['soft_delete'] = $this->input->post('status');
		$this->db->where('katlomba_id',$key);
		$sql = $this->db->update('kategori_lomba',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
				  </div>");
			redirect(base_url('webadmin/kategorilomba'));
		}
	}

	public function hapus_kategorilomba()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('katlomba_id',$key);
		$sql = $this->db->update('kategori_lomba',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/kategorilomba'));
		}
	}

	public function lomba()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Lomba';
		$data['jhsmall'] = 'Data Lomba';
		$data['bc'] = "<li><a href='#'>Lomba</a></li>";
		$data['conten'] = 'webadmin/lomba';
		$sql = $this->db->get('lomba');
		$data['lomba'] = $sql->result();

		$this->load->view('webadmin/layout',$data);		
	}

	public function aksi_lomba()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$this->load->view('webadmin/add_lomba');
		}else{
			$this->db->where('lomba_id',$pecah[1]);
			$sql = $this->db->get('lomba');
			if($sql->num_rows() > 0){
				$data['lomba'] = $sql->result();
			}

			$this->load->view('webadmin/edit_lomba',$data);
		}
		
	}

	public function add_lomba()
	{
		$data['nama_lomba'] = $this->input->post('namalomba');
		$sql = $this->db->insert('lomba',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('webadmin/lomba'));
		}	
	}

	public function proses_edit_lomba()
	{
		$key = $this->input->post('idlomba');
		$data['nama_lomba'] = $this->input->post('namalomba');
		$data['soft_delete'] = $this->input->post('status');
		$this->db->where('lomba_id',$key);
		$sql = $this->db->update('lomba',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
				  </div>");
			redirect(base_url('webadmin/lomba'));
		}
	}

	public function hapus_lomba()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('lomba_id',$key);
		$sql = $this->db->update('lomba',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/lomba'));
		}
	}


	public function sekolah()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Sekolah';
		$data['jhsmall'] = 'Data Sekolah';
		$data['bc'] = "<li><a href='#'>Sekolah</a></li>";
		$data['conten'] = 'webadmin/sekolah';

		//$this->db->where('soft_delete','0');
		//$this->db->select(',* s.soft_delete as ket');
		$this->db->join('kec','kec.kd_kec=s.kd_kec');
		$this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$sql = $this->db->get('sekolah s');
		$data['sekolah'] = $sql->result();

		$this->load->view('webadmin/layout',$data);
	}

	public function aksi_sekolah()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['kec'] = $this->webadmin_model->kecamatan();
			$this->load->view('webadmin/add_sekolah',$data);
		}else{
			$data['kec'] = $this->webadmin_model->kecamatan();
			$this->db->where('sekolah_id',$pecah[1]);
			$sql = $this->db->get('sekolah');
			if($sql->num_rows() > 0){
				$data['sekolah'] = $sql->result();
			}

			$this->load->view('webadmin/edit_sekolah',$data);
		}
		
	}

	public function add_sekolah()
	{
		$npsn = $this->input->post('npsn');
		$this->db->where('npsn',$npsn);
		$cek = $this->db->get('sekolah');
		$level = $this->session->userdata('token_level_admin');
		if($cek->num_rows() > 0){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> NPSN yang anda masukkan sudah ada pada database.
					  </div>");
				redirect(base_url('webadmin/sekolah'));	
		}else{
			$data['npsn'] = $this->input->post('npsn');
			$data['nama_sp'] = $this->input->post('namasp');
			$data['jenjang'] = $this->input->post('jenjang');
			$data['status_sekolah'] = $this->input->post('statussekolah');
			$data['akreditasi'] = $this->input->post('akreditasi');
			$data['kd_kec'] = $this->input->post('kec');
			$data['username'] = $this->input->post('npsn');
			$data['password'] = md5($this->input->post('npsn'));
			$sql = $this->db->insert('sekolah',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				if($level == 'admin'){
					redirect(base_url('webadmin/sekolah'));
				}else{
					redirect(base_url('supervisor/sekolah'));
				}
			}	
		}	
	}

	public function proses_edit_sekolah()
	{
		$npsn = $this->input->post('npsn');
		$paswd = $this->input->post('paswd');
		$sid = $this->input->post('sid');
		$level = $this->session->userdata('token_level_admin');
		$this->db->where('npsn',$npsn);
		$cek = $this->db->get('sekolah');
		if($cek->num_rows() == 1){
			$data['npsn'] = $this->input->post('npsn');
			$data['nama_sp'] = $this->input->post('namasp');
			$data['jenjang'] = $this->input->post('jenjang');
			$data['status_sekolah'] = $this->input->post('statussekolah');
			$data['akreditasi'] = $this->input->post('akreditasi');
			$data['kd_kec'] = $this->input->post('kec');
			$data['status'] = $this->input->post('status');
			if(!empty($paswd)){
				$data['password'] = md5($this->input->post('paswd'));
			}
			$this->db->where('sekolah_id',$sid);
			$sql = $this->db->update('sekolah',$data);
			if($sql){
				$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
					  </div>");
				if($level == 'admin'){
					redirect(base_url('webadmin/sekolah'));
				}else{
					redirect(base_url('supervisor/sekolah'));
				}
			}
		}else{
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
					    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
					    <strong>Pemberitahuan!</strong> NPSN yang anda masukkan sudah ada pada database.
					  </div>");
				if($level == 'admin'){
					redirect(base_url('webadmin/sekolah'));
				}else{
					redirect(base_url('supervisor/sekolah'));
				}
		}	
	}

	public function hapus_sekolah()
	{
		$key = $this->uri->segment(3);
		$data['status'] = 1;
		$level = $this->session->userdata('token_level_admin');
		$this->db->where('sekolah_id',$key);
		$sql = $this->db->update('sekolah',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			if($level == 'admin'){
				redirect(base_url('webadmin/sekolah'));
			}else{
				redirect(base_url('supervisor/sekolah'));
			}
		}
	}


	public function kecamatan()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Kecamatan';
		$data['jhsmall'] = 'Data Kecamatan';
		$data['bc'] = "<li><a href='#'>Kecamatan</a></li>";
		$data['conten'] = 'webadmin/kecamatan';

		//$this->db->where('soft_delete','0');
		$this->db->select('kab.nama_kab,kec.kd_kec,kec.soft_delete,kec.nama_kec');
		$this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$sql = $this->db->get('kec');
		$data['kec'] = $sql->result();

		$this->load->view('webadmin/layout',$data);		
	}

	public function edit_kecamatan()
	{
		$key = $this->input->post('id');
		$pecah = explode(",", $key);
		if($pecah[0] == 1){
			$data['kab'] = $this->webadmin_model->kabupaten();
			$this->load->view('webadmin/add_kecamatan',$data);
		}else{
			$data['kab'] = $this->webadmin_model->kabupaten();
			$this->db->where('kd_kec',$pecah[1]);
			$sql = $this->db->get('kec');
			if($sql->num_rows() > 0){
				$data['kec'] = $sql->result();
			}

			$this->load->view('webadmin/edit_kecamatan',$data);	
		}
		
	}

	public function add_kecamatan()
	{
		$data['kd_kab'] = $this->input->post('kab');
		$data['nama_kec'] = $this->input->post('namakec');
		$sql = $this->db->insert('kec',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('webadmin/kecamatan'));
		}	
	}

	public function proses_edit_kecamatan()
	{
		$key = $this->input->post('idkec');
		$data['kd_kab'] = $this->input->post('kab');
		$data['nama_kec'] = $this->input->post('namakec');
		$data['soft_delete'] = $this->input->post('status');
		$this->db->where('kd_kec',$key);
		$sql = $this->db->update('kec',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
				  </div>");
			redirect(base_url('webadmin/kecamatan'));
		}
	}

	public function hapus_kecamatan()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('kd_kec',$key);
		$sql = $this->db->update('kec',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/kecamatan'));
		}
	}

	public function kabupaten()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Kabupaten';
		$data['jhsmall'] = 'Data Kabupaten';
		$data['bc'] = "<li><a href='#'>Kabupaten</a></li>";
		$data['conten'] = 'webadmin/kabupaten';

		//$this->db->where('soft_delete','0');
		$sql = $this->db->get('kab');
		$data['kab'] = $sql->result();

		$this->load->view('webadmin/layout',$data);		
	}

	public function add_kabupaten()
	{
		$data['nama_kab'] = $this->input->post('nama');
		$sql = $this->db->insert('kab',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-success alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Disimpan.
				  </div>");
			redirect(base_url('webadmin/kabupaten'));
		}	
	}

	public function edit_kabupaten()
	{
		$key = $this->input->post('id');
		$this->db->where('kd_kab',$key);
		$sql = $this->db->get('kab');
		if($sql->num_rows() > 0){
			$data['kab'] = $sql->result();
		}

		$this->load->view('webadmin/edit_kabupaten',$data);
	}

	public function proses_edit_kabupaten()
	{
		$key = $this->input->post('id');
		$data['nama_kab'] = $this->input->post('nama');
		$data['soft_delete'] = $this->input->post('status');
		$this->db->where('kd_kab',$key);
		$sql = $this->db->update('kab',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-warning alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Diubah.
				  </div>");
			redirect(base_url('webadmin/kabupaten'));
		}
	}

	public function hapus_kabupaten()
	{
		$key = $this->uri->segment(3);
		$data['soft_delete'] = 1;
		$this->db->where('kd_kab',$key);
		$sql = $this->db->update('kab',$data);
		if($sql){
			$this->session->set_flashdata('info',"<div class='alert alert-danger alert-dismissable'>
				    <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				    <strong>Pemberitahuan!</strong> Data Berhasil Dihapus.
				  </div>");
			redirect(base_url('webadmin/kabupaten'));
		}
	}

	// Start Laporan BOS
	public function laporan()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Laporan BOS';
		$data['jhsmall'] = 'Laporan';
		$data['get_tahun'] = $this->webadmin_model->get_tahun();
		$data['get_kab'] = $this->webadmin_model->kabupaten();
		$data['conten'] = 'webadmin/laporan/laporan_bos';
		$this->load->view('webadmin/layout',$data);	
	}

	public function laporandetail($id,$tahun)
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Detail Laporan BOS';
		$data['jhsmall'] = 'Detail Laporan';
		$data['tahun'] = $tahun;
		$data['sekolah_id'] = $id;
		$data['bulan'] = $this->rkas_model->get_bulan_all();
		$data['profil'] = $this->rkas_model->get_profil_sekolah($id);
		$data['conten'] = 'webadmin/laporan/laporan_detail_bos';
		$this->load->view('webadmin/layout',$data);	
	}

	public function bku_show()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$sekolahid = $this->input->post('sekolahid');
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');

		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
			}
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,NULL);
			$data['saldo_tunai'] = $this->rkas_model->get_saldo_tunai($bulan,$tahun,$sekolahid);
			$this->load->view('webadmin/laporan/laporan_bku_show',$data);
		}else{
			redirect(base_url('webadmin'));
		}
	}

	public function bpk_show()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$sekolahid = $this->input->post('sekolahid');
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
			}
			$bukti = array('KM','KK');
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,$bukti,NULL);
			$this->load->view('webadmin/laporan/laporan_bpk_show',$data);
		}else{
			redirect(base_url('webadmin'));
		}
	}

	public function bpb_show()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$sekolahid = $this->input->post('sekolahid');
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
			}
			$bukti = array('BM','BK');
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,$bukti,NULL);
			$this->load->view('webadmin/laporan/laporan_bpb_show',$data);
		}else{
			redirect(base_url('webadmin'));
		}
	}

	public function bpp_show()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$sekolahid = $this->input->post('sekolahid');
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$data['bulan'] = $this->rkas_model->nama_bulan($bulan);
		$data['tahun'] = $tahun;
		$data['bulan_id'] = $bulan;
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		if(isset($bulan)){
			if($bulan == 1){
				$data['saldo_awal'] = 0;
			}else{
				$data['saldo_awal'] = $this->rkas_model->saldo($bulan,$tahun,$sekolahid);
			}
			$data['bku'] = $this->rkas_model->data_bku($tahun,$sekolahid,$bulan,NULL,'pajak');
			$this->load->view('webadmin/laporan/laporan_bpp_show',$data);	
		}else{
			redirect(base_url('webadmin'));
		}
	}

	public function realisasi_show($key)
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$sekolahid = $this->input->post('sekolahid');
		$tahun = $this->input->post('tahun');
		$tw = $this->input->post('tw');
		$data['tahun'] = $tahun;
		$data['sekolahid'] = $sekolahid;
		$data['jasa'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid);
		$data['mesin'] = $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid);
		$data['aset'] = $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid);
		$data['total_belanja_jasa'] = $this->rkas_model->get_total_jenis_belanja(1,$tahun,$sekolahid);
		$data['total_belanja_mesin'] = $this->rkas_model->get_total_jenis_belanja(2,$tahun,$sekolahid);
		$data['total_belanja_aset'] = $this->rkas_model->get_total_jenis_belanja(3,$tahun,$sekolahid);
		$data['total_realisasi_jasa'] = $this->rkas_model->get_total_realisasi(1,$tw,$tahun,$sekolahid);
		$data['total_realisasi_mesin'] = $this->rkas_model->get_total_realisasi(2,$tw,$tahun,$sekolahid);
		$data['total_realisasi_aset'] = $this->rkas_model->get_total_realisasi(3,$tw,$tahun,$sekolahid);
		$data['komponen_biaya'] = $this->rkas_model->get_komponen_biaya($sekolahid,$tahun);
		$data['standar_nasional'] = $this->rkas_model->get_standar_nasional();
		$data['tw'] = $tw;
		$data['total_komponen'] = $this->rkas_model->get_realisasi_komponen_biaya($tw,$tahun,$sekolahid);
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		$data['get_total_saldo_tunai'] = $this->rkas_model->get_total_saldo_tunai_tw($tw,$tahun,$sekolahid);
		
		if($tw == 1){
			$data['jasa_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['mesin_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['aset_before'] = $this->rkas_model->get_realisasi(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_jasa_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_mesin_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
			$data['total_realisasi_aset_before'] = $this->rkas_model->get_total_realisasi_before(NULL,$tw,$tahun,$sekolahid);
		}else{
			$twafter = $tw-1;
			$data['jasa_before'] = $this->rkas_model->get_realisasi(1,$twafter,$tahun,$sekolahid);
			$data['mesin_before'] = $this->rkas_model->get_realisasi(2,$twafter,$tahun,$sekolahid);
			$data['aset_before'] = $this->rkas_model->get_realisasi(3,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_jasa_before'] = $this->rkas_model->get_total_realisasi_before(1,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_mesin_before'] = $this->rkas_model->get_total_realisasi_before(2,$twafter,$tahun,$sekolahid);
			$data['total_realisasi_aset_before'] = $this->rkas_model->get_total_realisasi_before(3,$twafter,$tahun,$sekolahid);
		}

		if($tw == 1){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = 0;
			$data['sptjm_tw3'] = 0;
			$data['sptjm_tw4'] = 0;
		}else if($tw == 2){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = 0;
			$data['sptjm_tw4'] = 0;
		}else if($tw == 3){
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-2,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw4'] = 0;
		}else{
			$data['sptjm_tw1'] = $this->rkas_model->get_realisasi(1,$tw-3,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-3,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-3,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw2'] = $this->rkas_model->get_realisasi(1,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-2,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-2,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw3'] = $this->rkas_model->get_realisasi(1,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw-1,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw-1,$tahun,$sekolahid)['total_tw'];
			$data['sptjm_tw4'] = $this->rkas_model->get_realisasi(1,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(2,$tw,$tahun,$sekolahid)['total_tw'] + $this->rkas_model->get_realisasi(3,$tw,$tahun,$sekolahid)['total_tw'];
		}
		
		if(isset($tw)){
			if($key == 'realisasi'){
				$this->load->view('webadmin/laporan/laporan_realisasi_show',$data);
			}elseif($key == 'penggunaan'){
				$this->load->view('webadmin/laporan/laporan_penggunaan_show',$data);	
			}elseif($key == 'sptjm'){
				$this->load->view('webadmin/laporan/laporan_sptjm_show',$data);
			}else{
				echo "<h1>Oopss...<br>Terjadi kesalahan.</h1>";
			}
		}else{
			redirect(base_url('webadmin'));
		}
	}

	public function integrasi()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$data['jh1'] = 'Integrasi Penggunaan Dana BOS';
		$data['jhsmall'] = 'Integrasi';
		$data['get_tahun'] = $this->webadmin_model->get_tahun();
		$data['get_kab'] = $this->webadmin_model->kabupaten();
		$data['conten'] = 'webadmin/integrasi/index';
		$this->load->view('webadmin/layout',$data);	
	}

	public function form_integrasi()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$id = $this->input->post('id');
		$pecah = explode(',',$id);
		$tahun = $pecah[1];
		$sekolahid = $pecah[0];
		$tw = $pecah[2];
		$data['profil'] = $this->rkas_model->get_profil_sekolah($sekolahid);
		$data['tahun'] = $tahun;
		$data['sekolahid'] = $sekolahid;
		$data['tw'] = $tw;
		$data['komponen_biaya'] = $this->rkas_model->get_komponen_biaya($sekolahid,$tahun);
		$data['total_komponen'] = $this->rkas_model->get_realisasi_komponen_biaya($tw,$tahun,$sekolahid);
		$data['integrasi'] = $this->rkas_model->get_integrasi_tw($tw,$tahun,$sekolahid);
		if(isset($tw)){
			$this->load->view('webadmin/integrasi/form_integrasi',$data);
		}else{
			redirect(base_url('webadmin'));
		}
	}

	public function proses_integrasi()
	{
		$this->model_sequrity_rkas->get_sequrity_webadmin();
		$tw = $this->input->post('tw');		
		$tahun = $this->input->post('tahun');		
		$sekolahid = $this->input->post('sekolahid');
		$sql= $this->rkas_model->api_insert_penggunaan($tw,$tahun,$sekolahid);
		$pesan = json_decode($sql,true);
		if($pesan['success'] == 1){
			$query = $this->rkas_model->update_history_integrasi($tw,$tahun,$sekolahid);
			if($query){
				$respon = array('status' => true, 'msg' => 'data berhasil dikirim dan diupdate.');
				//echo json_encode($respon);
			}else{
				$respon = array('status' => false, 'msg' => 'data gagal dikirim dan diupdate..');
				//echo json_encode($respon);
			}
		}else{
			$respon = array('status' => false, 'msg' => 'data gagal dikirim.');
			//echo json_encode($respon);	
		}
		echo json_encode($respon);
	}

	public function tester()
	{
		// $hasil = $this->rkas_model->insert_integrasi(1,2019,174);
		// echo $hasil['02'];
		// var_dump($this->rkas_model->get_profil_sekolah(174)['id_sekolah']);
		// $query = $this->rkas_model->update_history_integrasi(1,2019,174);
		// var_dump($query);
		$cek = $this->rkas_model->get_integrasi_tw(3,2019,174);
		var_dump($cek);
	}
	

	// End Laporan BOS
}
