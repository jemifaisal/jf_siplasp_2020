<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('permalink_helper');
		$this->load->model('webadmin_model');
	}

	public function index()
	{
		$data['content'] = 'c_home';
		$this->load->view('home_v', $data);
	}

}
