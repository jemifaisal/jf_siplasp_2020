<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Integrasi extends CI_Controller {

	function __construct(){
		//header('Access-Control-Allow-Origin: *');
    	// header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
	}

	public function index()
	{
		//$this->load->view('sekolah/bku/integrasi');
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://bos.ruangpanji.com/index.php/api/services_penggunaan/laporan_pencairan_dan_penggunaan_by_sekolah_id?tahun=2020&triwulan=1&sekolah_id=62B8E1AA-8140-49E8-9AA8-C3DD628189EE",
			// CURLOPT_URL => "https://bos.kemdikbud.go.id/api/services_penggunaan/laporan_penggunaan",
			// CURLOPT_URL => "https://bos.kemdikbud.go.id/index.php/api/services_penggunaan/laporan_pencairan_dan_penggunaan_by_sekolah_id?tahun=2019&triwulan=1&sekolah_id=62B8E1AA-8140-49E8-9AA8-C3DD628189EE",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"api-key: f51b276a09a2c862e70acf88f953c0db91b62529"
			),
		));

		$response = curl_exec($curl);
		
		$err = curl_error($curl);
		
		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}

}
