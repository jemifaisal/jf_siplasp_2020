<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rkas extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->db->select('tahun');
		$this->db->group_by('tahun');
		$this->db->order_by('tahun','desc');
		$sqltahun = $this->db->get('rincian_belanja');
		if($sqltahun){
			$data['data_tahun'] = $sqltahun->result();
		}

		$ignore = array(123,12345);
		$this->db->where('status',0);
		$this->db->where_not_in('npsn',$ignore);
		$this->db->order_by('nama_sp','asc');
		$sqlsekolah = $this->db->get('sekolah');
		if($sqlsekolah){
			$data['data_sekolah'] = $sqlsekolah->result();
		}
		// $this->db->where('npsn',$npsn);
		// $sql = $this->db->get('sekolah');
		// if($sql){
		// 	$rows = $sql->row_array();
		// }
		$data['get_tahun'] = 2019;
		$data['get_id'] = 1;
		$data['content'] = 'cetak_rkas_public';
		$this->load->view('home_v', $data);
	}

	public function show()
	{

		$tahun = $this->input->post('tahun');
		$sekolah = $this->input->post('sekolah');
		if($tahun == NULL and $sekolah == NULL){
			redirect(base_url(''));
		}else{
			$this->db->where('npsn',$sekolah);
			$sql = $this->db->get('sekolah');
			if($sql){
				$rows = $sql->row_array();
			}
			$data['get_tahun'] = $tahun;
			$data['get_id'] = $rows['sekolah_id'];
			$this->load->view('show_rkas', $data);
		}
	}

}
