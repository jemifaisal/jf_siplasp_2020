<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webadmin_model extends CI_Model {

	public function m_get_login($u,$p)
	{
		$pwd = md5($p);
		$this->db->where('username',$u);
		$this->db->where('password',$pwd);
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('users');
		$token = rand(1000000,10000000);
		if($sql->num_rows() > 0){
			foreach ($sql->result() as $row) {
				if($row->level_user == "supervisor"){
					$token_login_webadmin = $this->session->userdata('token_login_webadmin');
					unset($token_login_webadmin);
					
					$superpisor = $token."5up3rvis012";
					$sess_  = array('token_nama_admin' => $row->nama_user,
								'token_level_admin' => $row->level_user,
								'token_username' => $row->username,
								'token_login_supervisor' => $superpisor);

					$this->session->set_userdata($sess_);
					//redirect(base_url('supervisor'));
					$msg = "<div class='alert alert-success'>
					<strong><i class='fa fa-spinner fa-spin' style='font-size:20px'></i> Success!</strong> Berhasil login, silahkan tunggu...
					</div>";
					$respon = array('status' => true, 'msg' => $msg, 'level' => 1);
					echo json_encode($respon);

				}else{
					$admin = $token."73m!@###";
					$sess_  = array('token_nama_admin' => $row->nama_user,
								'token_level_admin' => $row->level_user,
								'token_username' => $row->username,
								'token_login_webadmin' => $admin);

					$this->session->set_userdata($sess_);
					//redirect(base_url('webadmin'));
					$msg = "<div class='alert alert-success'>
					<strong><i class='fa fa-spinner fa-spin' style='font-size:20px'></i> Success!</strong> Berhasil login, silahkan tunggu...
					</div>";
					$respon = array('status' => true, 'msg' => $msg, 'level' => 0);
					echo json_encode($respon);
				}
				// $sess_  = array('token_nama_admin' => $row->nama_user,
				// 				'token_level_admin' => $row->level_user,
				// 				'token_username' => $row->username,
				// 				'token_login_supervisor' => $superpisor,
				// 				'token_login_webadmin'=> $admin);

				
			}

		}else{
			$msg = "<div class='alert alert-danger alert-dismissable'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				<strong>Gagal Login!</strong> Username atau Password Anda Salah.!
				</div>";
			//redirect(base_url('sekolah/login'));
			$respon = array('status' => false, 'msg' => $msg);
			echo json_encode($respon);
		}
	}

	public function pendidikan()
	{
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('pendidikan');
		if($sql){
			return $sql->result();
		}
	}

	public function kabupaten()
	{
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('kab');
		if($sql){
			return $sql->result();
		}
	}

	public function kecamatan()
	{
		$this->db->select('kec.kd_kec,kec.nama_kec,kec.soft_delete,kec.kd_kab,kab.nama_kab');
		$this->db->join('kab','kec.kd_kab=kab.kd_kab');
		$this->db->where('kec.soft_delete',0);
		$sql = $this->db->get('kec');
		if($sql){
			return $sql->result();
		}
	}

	public function sub_kegiatan($id)
	{
		$regulasi_id = $this->session->userdata('token_regulasi');
		$this->db->where('soft_delete',0);
		$this->db->where('regulasi_id',$regulasi_id);
		$this->db->where('substandar_id',$id);
		$sql = $this->db->get('sub_program');
		if($sql){
			return $sql->result();
		}
	}

	public function lomba()
	{
		$sql = $this->db->query("SELECT * FROM kategori_lomba kl join lomba l on kl.lomba_id=l.lomba_id where kl.soft_delete='0'");
		if($sql){
			return $sql->result();
		}
	}

	public function datalomba()
	{
		$sql = $this->db->query("SELECT * FROM lomba where soft_delete='0'");
		if($sql){
			return $sql->result();
		}
	}

	public function standar()
	{
		$sql = $this->db->query("SELECT * FROM standar_nasional where soft_delete='0'");
		if($sql){
			return $sql->result();
		}
	}

	public function substandar()
	{
		$sql = $this->db->query("SELECT * FROM sub_standar ss join standar_nasional sn on ss.standar_id=sn.standar_id where ss.soft_delete='0'");
		if($sql){
			return $sql->result();
		}
	}

	public function biaya()
	{
		$sql = $this->db->query("SELECT * FROM komponen_pembiayaan where soft_delete='0'");
		if($sql){
			return $sql->result();
		}
	}
	
	public function lihatdata()
	{
		$sql = $this->db->query("select * from kib_b");
		if($sql){
			return $sql->result();
		}
	}

	public function aset_perjenis($kib,$kab)
	{
		if($kib == 'kib_b'){
			$barang = 'jumlah_barang';
		}else{
			$barang = 'jumlah';
		}
		$tahun = $this->input->get('tahun');
		$sql = $this->db->query("select sekolah.sekolah_id, kab.kd_kab, kab.nama_kab, format(sum($barang * harga_perolehan),0) as total  from $kib left join sekolah on sekolah.sekolah_id=$kib.sekolah_id left join kec on kec.kd_kec=sekolah.kd_kec left join kab on kab.kd_kab=kec.kd_kab where kec.kd_kab='$kab' and $kib.tahun_pembelian='$tahun'");
		if($sql){
			foreach ($sql->result() as $key) {
				return $key->total;
			}
		}
	}

	public function aset_perkab($kib,$kab)
	{
		if($kib == 'kib_b'){
			$barang = 'jumlah_barang';
		}else{
			$barang = 'jumlah';
		}
		$url = $_SERVER['REQUEST_URI'];// apache_getenv("REQUEST_URI");
		$tahun = $this->input->get('tahun');

		$sql = $this->db->query("select sekolah.sekolah_id, kab.kd_kab, kab.nama_kab, format(sum($barang * harga_perolehan),0) as total  from $kib left join sekolah on sekolah.sekolah_id=$kib.sekolah_id left join kec on kec.kd_kec=sekolah.kd_kec left join kab on kab.kd_kab=kec.kd_kab where kec.kd_kab='$kab' and $kib.tahun_pembelian='$tahun'");
		if($sql){
			foreach ($sql->result() as $key) {
				$kibb = $this->aset_perjenis('kib_b',$key->kd_kab);
				$kibe = $this->aset_perjenis('kib_e',$key->kd_kab);
				$a = str_replace(',', '', $kibb);
				$b = str_replace(',', '', $kibe);
				$c = number_format($a+$b,0);
				echo "<tr>
					<td>$key->nama_kab</td>
					<td>".$this->aset_perjenis('kib_b',$key->kd_kab)."</td>
					<td>".$this->aset_perjenis('kib_e',$key->kd_kab)."</td>
					<td>".$c."</td>
					<td><a href='$url&kab=$key->kd_kab' class='btn btn-success btn-xs'>Detail</a></td>
				</tr>"; 	
			}
		}
	}

	public function sekolah($kab)
	{
		$this->db->join('kec','kec.kd_kec=sekolah.kd_kec');
		$this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$this->db->where('kec.kd_kab',$kab);
		$sql = $this->db->get('sekolah');
		$url = $_SERVER['REQUEST_URI']; //apache_getenv("REQUEST_URI");
		if($sql){
			//return $sql->result();
			echo "<thead>
			<tr>
				<th rowspan='2'>Kabupaten / Kota</th>
				<th rowspan='2'>Nama Sekolah</th>
				<th rowspan='2'>Jenjang</th>
				<th rowspan='2'>Status</th>
				<th colspan='2'>KIB-B</th>
				<th colspan='2'>KIB-E</th>
				<th rowspan='2'>Action</th>
			</tr>
			<tr>
				<th>BOSNAS</th>
				<th>BOSDA</th>
				<th>BOSNAS</th>
				<th>BOSDA</th>	
			</tr>
		</thead>";
			foreach ($sql->result() as $key) {
				echo "<tr>
					<td>$key->nama_kab</td>
					<td>$key->nama_sp</td>
					<td>$key->jenjang</td>
					<td>$key->status_sekolah</td>
					<td>".$this->webadmin_model->aset_persekolah('kib_b',$key->sekolah_id,'BOSNAS')."</td>
					<td>".$this->webadmin_model->aset_persekolah('kib_b',$key->sekolah_id,'BOSDA')."</td>
					<td>".$this->webadmin_model->aset_persekolah('kib_e',$key->sekolah_id,'BOSNAS')."</td>
					<td>".$this->webadmin_model->aset_persekolah('kib_e',$key->sekolah_id,'BOSDA')."</td>";


				// $aset = $this->aset_persekolah('kib_b',$key->sekolah_id);
				
				// if(count($aset) == 2){
				// 	foreach ($aset as $asets) {
				// 		echo "<td>$asets->total</td>";
				// 	}
						
				// }else if(count($aset) == 1){
				// 	foreach ($aset as $asets) {
				// 		if($asets->nama_sumber_dana == 'BOSNAS'){
				// 			echo "<td>$asets->total</td><td><span class='btn btn-danger btn-xs'>Belum</span></td>";	
				// 		}else{
				// 			echo "<td><span class='btn btn-danger btn-xs'>Belum</span></td><td>$asets->total</td>";
				// 		}
				// 	}
					
				// }else if(count($aset) == 0){
				// 	echo "<td><span class='btn btn-danger btn-xs'>Belum</span></td><td><span class='btn btn-danger btn-xs'>Belum</span></td>";
				// }
				echo "<td><a href='".$url."&sekolah=$key->sekolah_id' class='btn btn-success btn-xs'>Detail</a></td>";	
				echo "</tr>";
			}
		}
	}

	public function aset_persekolah($kib,$sekolah,$dana)
	{
		if($kib == 'kib_b'){
			$barang = 'jumlah_barang';
		}else{
			$barang = 'jumlah';
		}

		$tahun = $this->input->get('tahun');

		$sql = $this->db->query("SELECT sekolah.nama_sp, $kib.tahun_pembelian,sumber_dana.nama_sumber_dana, format(sum($barang*harga_perolehan),0) as total from $kib LEFT JOIN sekolah ON sekolah.sekolah_id=$kib.sekolah_id left join sumber_dana on sumber_dana.dana_id=$kib.asal_usul WHERE sekolah.sekolah_id='$sekolah' and $kib.tahun_pembelian='$tahun' and nama_sumber_dana='$dana' GROUP by asal_usul,tahun_pembelian order by $kib.asal_usul asc");
		if($sql->num_rows() > 0){
			//return $sql->result();
			foreach ($sql->result() as $key) {
				// if($key->total == ""){
				// 	$data = $key->total;
				// }else{
				// 	$data = "<span class='btn btn-danger btn-xs'>Belum</span>";
				// }
				return $key->total;
			} 
		}else{
			return "<span class='btn btn-danger btn-xs'>Belum</span>";
		}
	}

	public function detail_aset($kib,$sekolah)
	{
		$tahun = $this->input->get('tahun');
		if($kib == 'kib_b'){
			$barang = 'jumlah_barang';
		}else{
			$barang = 'jumlah';
		}

		$sql = $this->db->query("SELECT *, format(harga_perolehan,0) as harga_satuan FROM $kib left join sumber_dana on sumber_dana.dana_id=$kib.asal_usul where tahun_pembelian='$tahun' and sekolah_id='$sekolah'");
		if($sql){
			return $sql->result();
		}
	}

	public function header_aset($id)
	{
		$this->db->where('sekolah_id',$id);
		$this->db->join('kec','kec.kd_kec=sekolah.kd_kec');
		$this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$sql = $this->db->get('sekolah');
		if($sql){
			foreach ($sql->result() as $key) {
				
			}
		}
		$data = "<h2>Detail Aset Per Sekolah</h2>
			<table>
				<tr>
					<td>Nama Sekolah</td>
					<td>: </td>
					<th>$key->nama_sp</th>
				</tr>
				<tr>
					<td>Status Sekolah</td>
					<td>: </td>
					<th>$key->status_sekolah</th>
				</tr>
				<tr>
					<td>Kabupaten</td>
					<td>:</td>
					<th>$key->nama_kab</th>
				</tr>
				<tr>
					<td>Detail Aset KIB-B</td>
					<td>:</td>
					<td>
						<span class='btn btn-default'> 
							<span class='btn btn-warning btn-xs'>BOSNAS</span>
							<span class='btn btn-success btn-xs'>Rp. ".$this->total_aset_sekolah($key->sekolah_id,'kib_b','BOSNAS')."</span>
						</span>
						<span class='btn btn-default'> 
							<span class='btn btn-danger btn-xs'>BOSDA</span>
							<span class='btn btn-success btn-xs'>Rp. ".$this->total_aset_sekolah($key->sekolah_id,'kib_b','BOSDA')."</span>
						</span>
					</td>
				</tr>
				<tr>
					<td>Detail Aset KIB-E</td>
					<td>:</td>
					<td>
						<span class='btn btn-default'> 
							<span class='btn btn-warning btn-xs'>BOSNAS</span>
							<span class='btn btn-success btn-xs'>Rp. ".$this->total_aset_sekolah($key->sekolah_id,'kib_e','BOSNAS')."</span>
						</span>
						<span class='btn btn-default'> 
							<span class='btn btn-danger btn-xs'>BOSDA</span>
							<span class='btn btn-success btn-xs'>Rp. ".$this->total_aset_sekolah($key->sekolah_id,'kib_e','BOSDA')."</span>
						</span>
					</td>
				</tr>
			</table>
		";
		return $data;
	}
	
	public function header_aset_tamsil($id)
	{
		$this->db->where('sekolah_id',$id);
		$this->db->join('kec','kec.kd_kec=sekolah.kd_kec');
		$this->db->join('kab','kab.kd_kab=kec.kd_kab');
		$sql = $this->db->get('sekolah');
		if($sql){
			foreach ($sql->result() as $key) {
				
			}
		}
		$data = "<h3>Data TAMSIL Pegawai Per Sekolah</h3>
			<table>
				<tr>
					<td>Nama Sekolah</td>
					<td>: </td>
					<th>$key->nama_sp</th>
				</tr>
				<tr>
					<td>Status Sekolah</td>
					<td>: </td>
					<th>$key->status_sekolah</th>
				</tr>
				<tr>
					<td>Kabupaten</td>
					<td>:</td>
					<th>$key->nama_kab</th>
				</tr>
			</table>
		";
		return $data;
	}

	public function total_aset_sekolah($sekolah,$kib,$dana)
	{
		if($kib == 'kib_b'){
			$barang = 'jumlah_barang';
		}else{
			$barang = 'jumlah';
		}
		$tahun = $this->input->get('tahun');
		$sql = $this->db->query("select format(sum($barang*harga_perolehan),0) as total from $kib left join sumber_dana on sumber_dana.dana_id=$kib.asal_usul where tahun_pembelian='$tahun' and nama_sumber_dana='$dana' and sekolah_id='$sekolah'");
		if($sql){
			foreach ($sql->result() as $key) {
				return $key->total;
			}
		}
	}
	
	public function total_aset_sekolah_tahun($tahun,$sekolah,$kib,$dana)
	{
		if($kib == 'kib_b'){
			$barang = 'jumlah_barang';
		}else{
			$barang = 'jumlah';
		}
		$sql = $this->db->query("select format(sum($barang*harga_perolehan),0) as total from $kib left join sumber_dana on sumber_dana.dana_id=$kib.asal_usul where tahun_pembelian='$tahun' and nama_sumber_dana='$dana' and sekolah_id='$sekolah'");
		if($sql){
			foreach ($sql->result() as $key) {
				return $key->total;
			}
		}
	}

	public function detail_aset_tahun($kib,$tahun,$sekolah,$dana)
	{
		if($kib == 'kib_b'){
			$barang = 'jumlah_barang';
		}else{
			$barang = 'jumlah';
		}

		$sql = $this->db->query("SELECT * FROM $kib left join sumber_dana on sumber_dana.dana_id=$kib.asal_usul where tahun_pembelian='$tahun' and sekolah_id='$sekolah' and $kib.asal_usul='$dana' order by $kib.nama_barang");
		if($sql){
			return $sql->result();
		}
	}

	public function ambil_sekolah($id)
	{
		$this->db->where('sekolah_id',$id);
		$sql = $this->db->get('sekolah');
		if($sql){
			foreach ($sql->result() as $key) {
				return $key->nama_sp;
			}
		}
	}

	//tgl 28-02-2019
	public function get_tahun(){
		$sql = $this->db->query("SELECT tahun from penerima_bos group by tahun");
		if($sql){
			return $sql->result();
		}
	}

	public function get_jumlah_sekolah_penerima_bos($id,$jenjang,$status,$tahun){
		if($jenjang == 'SLB'){
			$jenjang_real = "'SLB','SDLB','SMPLB','SMALB'";
		}else{
			$jenjang_real = "'$jenjang'";
		}
		$sql = $this->db->query("SELECT COUNT(penerima_bos.bos_id) as total from penerima_bos
			join sekolah on sekolah.npsn=penerima_bos.npsn
			join kec on kec.kd_kec=sekolah.kd_kec
			join kab on kab.kd_kab=kec.kd_kab
			where kab.kd_kab='$id' and sekolah.jenjang IN($jenjang_real) and sekolah.status_sekolah='$status'
			and penerima_bos.tahun='$tahun' and penerima_bos.soft_delete='0' and sekolah.status='0'
			and kab.soft_delete='0' and kec.soft_delete='0'
		")->row_array();
		if($sql){
			return $sql['total'];
		}
	}

	public function penerima_bos_per_kab($id,$tahun){
		$data['bc'] = "<li><a href='#'>Layout</a></li>";
		$this->db->where('pb.soft_delete',0);
		$this->db->where('s.status',0);
		$this->db->where('k.soft_delete',0);
		$this->db->where('kb.soft_delete',0);
		$this->db->where('pb.tahun',$tahun);
		$this->db->where('kb.kd_kab',$id);
		$this->db->select("*, s.sekolah_id, s.npsn,s.jenjang, s.status_sekolah,pb.tahun,s.nama_sp,kb.nama_kab,k.nama_kec,
			(pb.kelas_i+pb.kelas_ii+pb.kelas_iii+pb.kelas_iv+pb.kelas_v+pb.kelas_vi+pb.kelas_vii+pb.kelas_viii+pb.kelas_ix+pb.kelas_x+pb.kelas_xi+pb.kelas_xii) as jumlah_siswa,
			(pb.kelas_i+pb.kelas_ii+pb.kelas_iii+pb.kelas_iv+pb.kelas_v+pb.kelas_vi+pb.kelas_vii+pb.kelas_viii+pb.kelas_ix+pb.kelas_x+pb.kelas_xi+pb.kelas_xii) * dasar_dana as total_bos

		");
		$this->db->join('penerima_bos pb', 'pb.npsn = s.npsn');
		$this->db->join('kec k', 's.kd_kec = k.kd_kec','left');
		$this->db->join('kab kb', 'k.kd_kab = kb.kd_kab','left');
		$this->db->join('dasar_dana_bos','dasar_dana_bos.jenjang_sekolah=s.jenjang and dasar_dana_bos.tahun=pb.tahun');
		$this->db->order_by('s.nama_sp','asc');
		$sql = $this->db->get("sekolah s");
		if($sql){
			return $sql->result();
		}
	}

	public function total_bos_sekolah($id,$tahun){
		$sql = $this->db->query("SELECT penerima_bos.triwulan, format(penerima_bos.sisa_uang,0) as sisa_uang, sekolah.jenjang, format(kelas_i+kelas_ii+kelas_iii+kelas_iv+kelas_v+kelas_vi+kelas_vii+kelas_viii+kelas_ix+kelas_x+kelas_xi+kelas_xii,0) as jumlah_pd,
			format(((kelas_i+kelas_ii+kelas_iii+kelas_iv+kelas_v+kelas_vi+kelas_vii+kelas_viii+kelas_ix+kelas_x+kelas_xi+kelas_xii)*dasar_dana_bos.dasar_dana),0) as total FROM penerima_bos 
			join sekolah on sekolah.npsn=penerima_bos.npsn
			join dasar_dana_bos on dasar_dana_bos.jenjang_sekolah=sekolah.jenjang and dasar_dana_bos.tahun=penerima_bos.tahun
			where sekolah.sekolah_id='$id' and penerima_bos.tahun='$tahun'")->row_array();
		if($sql){
			if($sql['jenjang'] == "SLB" or $sql['jenjang'] == "SDLB"){
				if($sql['jumlah_pd'] < 60 ){
					$total = number_format('120000000',0);
				}else{
					$total = $sql['total'];
				}
			}else{
				$total = $sql['total'];
			}
			$arr = array(',','.');
			$tw1 = number_format(20 * str_replace($arr,'',$total) / 100,0);
			$tw2 = number_format(40 * str_replace($arr,'',$total) / 100,0);
			$tw3 = number_format(20 * str_replace($arr,'',$total) / 100,0);
			$tw4 = number_format(20 * str_replace($arr,'',$total) / 100,0);

			$data = array('jumlah_pd' => $sql['jumlah_pd'], 'total' => $total, 'sisa_uang' => $sql['sisa_uang'], 'triwulan' => $sql['triwulan'], 'tw1' => $tw1, 'tw2' => $tw2, 'tw3' => $tw3, 'tw4' => $tw4);
			return $data;
		}
	}

	public function total_belanja_bos($id,$tahun){
		$tahun = $tahun; // $this->session->userdata('token_tahun');
		$sekolah = $id; //$this->session->userdata('token_sekolah_id');
		$sql = $this->db->query("SELECT SUM(rincian_belanja.volume * rincian_belanja.harga_satuan) as total from rincian_belanja left join sub_program on rincian_belanja.program_id=sub_program.program_id left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id 
			where rincian_belanja.soft_delete='0' and sub_standar.soft_delete='0' and sub_program.soft_delete='0' 
			and standar_nasional.soft_delete='0'
			and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah'")->row_array();
		if($sql){
			$query = $this->db->query("
					SELECT SUM(rincian_header.volume_hr * rincian_header.harga_satuan_hr) as total from rincian_belanja 
						left join rincian_header on rincian_belanja.rincian_id=rincian_header.rincian_id
						left join sub_program on rincian_belanja.program_id=sub_program.program_id 
						left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id 
						left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id

						where rincian_belanja.status_rincian='1' and rincian_belanja.soft_delete='0'
						and sub_standar.soft_delete='0' and sub_program.soft_delete='0' and standar_nasional.soft_delete='0' and rincian_header.soft_delete='0'
						and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah'")->row_array();
			if($query){
				$total_rincian_header = $query['total'];
				$total_rincian = $sql['total'];
				$total_bos_sekolah = $this->total_bos_sekolah($sekolah,$tahun);

				$total_belanja = number_format($total_rincian + $total_rincian_header,0);
				$a = str_replace(array(',','.'),'',$total_bos_sekolah['total']);
				$b = str_replace(array(',','.'),'',$total_belanja);
				$c = intval($a)- intval($b);

				$sisa_lebih = number_format($c);
				$data = array('total_bos' => $total_bos_sekolah['total'] ,'total_belanja' => $total_belanja, 'sisa_lebih' => $sisa_lebih);
				return $data;
			}
			
		}	
	}
	//end 28-02-2019

	
}
