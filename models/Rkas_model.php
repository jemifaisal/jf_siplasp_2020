<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rkas_model extends CI_Model {
    public function get_bulan_all()
    {
        $sql = $this->db->get('bulan');
        if($sql){
            return $sql->result();
        }
    }

    public function get_profil_sekolah($id)
    {
        $this->db->select('*, ttd_pejabat.nama_kepsek as kepsek');
        $this->db->where('sekolah.sekolah_id',$id);
        $this->db->where('sekolah.status',0);
        $this->db->join('kec','kec.kd_kec=sekolah.kd_kec');
        $this->db->join('kab','kab.kd_kab=kec.kd_kab');
        $this->db->join('ttd_pejabat','ttd_pejabat.sekolah_id=sekolah.sekolah_id');
        $sql = $this->db->get('sekolah');
        if($sql){
            return $sql->row_array();
        }
    }

    public function data_bku($tahun,$sekolah,$bulan,$bukti,$pajak)
    {
        $this->db->select('*,rincian_belanja.komponen_kode as kode_komponen, format(bku.jumlah_transaksi,0) as jumlah_transaksi_format');
        $this->db->where('bku.deleted_at', null);
        $this->db->where('bku.tahun', $tahun);
        $this->db->where('bku.sekolah_id', $sekolah);
        if($bulan != NULL){
            $this->db->where('bku.bulan_id', $bulan);
        }
        if($bukti != NULL){
            $this->db->where_in('bukti.kode_bukti',$bukti);
        }
        if($pajak != NULL){
            $this->db->where('pajak_id !=',0);
        }
        $this->db->join('rincian_belanja','rincian_belanja.rincian_id=bku.rincian_id','LEFT');
        $this->db->join('jenis_belanja','jenis_belanja.belanja_id=rincian_belanja.belanja_id','LEFT');
        $this->db->join('sub_program','sub_program.program_id=rincian_belanja.program_id','LEFT');
        $this->db->join('bukti','bukti.bukti_id=bku.bukti_id','LEFT');
        $this->db->order_by('bku.tanggal_transaksi, bku.created_at','asc');
        // $this->db->order_by('bku.tanggal_transaksi,bku.no_urut_bukti','asc');
        $sql = $this->db->get('bku');
        if($sql){
            return $sql->result();
        }
    }

    public function saldo($bulan,$tahun,$sekolahid)
    {
        $debit = $this->saldo_kelompok($bulan,$tahun,$sekolahid,'D');
        $kredit = $this->saldo_kelompok($bulan,$tahun,$sekolahid,'K');
        return $debit - $kredit;
    }

    public function saldo_pajak($bulan,$tahun,$sekolahid)
    {
        $debit = $this->saldo_kelompok_pajak($bulan,$tahun,$sekolahid,'D');
        $kredit = $this->saldo_kelompok_pajak($bulan,$tahun,$sekolahid,'K');
        return $debit - $kredit;
    }

    public function saldo_kelompok_pajak($bulan,$tahun,$sekolah,$kode)
    {
        // $sekolah = $this->session->userdata('token_sekolah_id');
		// $tahun = $this->session->userdata('token_tahun');
        $this->db->select("SUM(jumlah_transaksi) as jumlah_transaksi");
        $this->db->join('bukti','bukti.bukti_id=bku.bukti_id','LEFT');
        $this->db->where('pajak_id !=',0);
        $this->db->where('tahun',$tahun);
        $this->db->where('sekolah_id',$sekolah);
        $this->db->where('bulan_id <',$bulan);
        $this->db->where('kelompok',$kode);
        $this->db->where('bku.deleted_at',NULL);
        $sql = $this->db->get('bku');
        if($sql){
            $rows = $sql->row_array();
            return $rows['jumlah_transaksi'];
        }
    }

    public function saldo_bpk($bulan,$tahun,$sekolahid)
    {
        $debit = $this->saldo_kelompok_bpk($bulan,$tahun,$sekolahid,'D');
        $kredit = $this->saldo_kelompok_bpk($bulan,$tahun,$sekolahid,'K');
        return $debit - $kredit;
    }

    public function saldo_kelompok_bpk($bulan,$tahun,$sekolah,$kode)
    {
        // $sekolah = $this->session->userdata('token_sekolah_id');
        // $tahun = $this->session->userdata('token_tahun');
        // kode = KM,KK
        $bukti = array('KM','KK');
        $this->db->select("SUM(jumlah_transaksi) as jumlah_transaksi");
        $this->db->join('bukti','bukti.bukti_id=bku.bukti_id','LEFT');
        $this->db->where_in('bukti.kode_bukti',$bukti);
        // $this->db->where('pajak_id !=',0);
        $this->db->where('tahun',$tahun);
        $this->db->where('sekolah_id',$sekolah);
        $this->db->where('bulan_id <',$bulan);
        $this->db->where('kelompok',$kode);
        $this->db->where('bku.deleted_at',NULL);
        $sql = $this->db->get('bku');
        if($sql){
            $rows = $sql->row_array();
            return $rows['jumlah_transaksi'];
        }
    }

    public function saldo_bpb($bulan,$tahun,$sekolahid)
    {
        $debit = $this->saldo_kelompok_bpb($bulan,$tahun,$sekolahid,'D');
        $kredit = $this->saldo_kelompok_bpb($bulan,$tahun,$sekolahid,'K');
        return $debit - $kredit;
    }

    public function saldo_kelompok_bpb($bulan,$tahun,$sekolah,$kode)
    {
        // $sekolah = $this->session->userdata('token_sekolah_id');
        // $tahun = $this->session->userdata('token_tahun');
        // kode = BM,BK
        $bukti = array('BM','BK');
        $this->db->select("SUM(jumlah_transaksi) as jumlah_transaksi");
        $this->db->join('bukti','bukti.bukti_id=bku.bukti_id','LEFT');
        $this->db->where_in('bukti.kode_bukti',$bukti);
        // $this->db->where('pajak_id !=',0);
        $this->db->where('tahun',$tahun);
        $this->db->where('sekolah_id',$sekolah);
        $this->db->where('bulan_id <',$bulan);
        $this->db->where('kelompok',$kode);
        $this->db->where('bku.deleted_at',NULL);
        $sql = $this->db->get('bku');
        if($sql){
            $rows = $sql->row_array();
            return $rows['jumlah_transaksi'];
        }
    }

    public function saldo_kelompok($bulan,$tahun,$sekolah,$kode)
    {
        // $sekolah = $this->session->userdata('token_sekolah_id');
		// $tahun = $this->session->userdata('token_tahun');
        $this->db->select("SUM(jumlah_transaksi) as jumlah_transaksi");
        $this->db->join('bukti','bukti.bukti_id=bku.bukti_id','LEFT');
        $this->db->where('tahun',$tahun);
        $this->db->where('sekolah_id',$sekolah);
        $this->db->where('bulan_id <',$bulan);
        $this->db->where('kelompok',$kode);
        $this->db->where('bku.deleted_at',NULL);
        $sql = $this->db->get('bku');
        if($sql){
            $rows = $sql->row_array();
            return $rows['jumlah_transaksi'];
        }
    }

    public function nama_bulan($id)
    {
        $this->db->where('bulan_id',$id);
        $sql = $this->db->get('bulan');
        if($sql){
            $rows = $sql->row_array();
            return $rows['nama_bulan'];
        }
    }

    public function get_rekening()
    {
        $this->db->where('soft_delete',0);
        $sql = $this->db->get('jenis_belanja');
        if($sql){
            return $sql->result();
        }
    }

    public function get_program($id)
    {
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $tahun = $this->session->userdata('token_tahun');
        $this->db->select('*,rincian_belanja.komponen_kode as kode_komponen');
        $this->db->where('rincian_belanja.program_id !=',0);
        $this->db->where('rincian_belanja.tahun',$tahun);
        $this->db->where('rincian_belanja.sekolah_id',$sekolahid);
        $this->db->where('rincian_belanja.belanja_id',$id);
        $this->db->where('rincian_belanja.soft_delete',0);
        $this->db->join('sub_program','sub_program.program_id=rincian_belanja.program_id');
        $this->db->order_by('rincian_belanja.program_id','asc');
        $this->db->group_by('rincian_belanja.program_id,rincian_belanja.komponen_kode');
        $sql = $this->db->get('rincian_belanja');
        if($sql){
            return $sql->result();
        }

    }

    public function get_total_belanja_program($rincian_id,$bulan)
    {
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $tahun = $this->session->userdata('token_tahun');
        $sql = $this->db->query("SELECT SUM(tw1) AS total_tw1, SUM(tw2) AS total_tw2, SUM(tw3) AS total_tw3, SUM(tw4) AS total_tw4,
                SUM(htw1) as total_htw1, SUM(htw2) as total_htw2, SUM(htw3) as total_htw3, SUM(htw4) as total_htw4 
                FROM rincian_belanja 
                LEFT JOIN rincian_header ON rincian_belanja.rincian_id=rincian_header.rincian_id and rincian_header.soft_delete=0
                WHERE program_id=(SELECT program_id FROM rincian_belanja WHERE rincian_id='$rincian_id')
                AND komponen_kode=(SELECT komponen_kode FROM rincian_belanja WHERE rincian_id='$rincian_id')
                AND sekolah_id='$sekolahid' AND tahun='$tahun' and rincian_belanja.soft_delete='0'");
        if($sql){
            $rows = $sql->row_array();
            $total_tw1 = $rows['total_tw1'] + $rows['total_htw1'];
            $total_tw2 = $rows['total_tw2'] + $rows['total_htw2'];
            $total_tw3 = $rows['total_tw3'] + $rows['total_htw3'];
            $total_tw4 = $rows['total_tw4'] + $rows['total_htw4'];

            $total_bku = $this->get_total_bku_per_rincian($rincian_id,$bulan);

            if(in_array($bulan,array('1','2','3'))){
                $respon = array('total_tw' => $total_tw1, 'total_tw_format' =>number_format($total_tw1,0), 'total_bku' => $total_bku, 'total_bku_format' => number_format($total_bku,0), 'ket' => 'TW I');
            }elseif(in_array($bulan, array('4','5','6'))){
                $respon = array('total_tw' => $total_tw2, 'total_tw_format' =>number_format($total_tw2,0), 'total_bku' => $total_bku, 'total_bku_format' => number_format($total_bku,0), 'ket' => 'TW II');
            }elseif(in_array($bulan, array('7','8','9'))){
                $respon = array('total_tw' => $total_tw3, 'total_tw_format' =>number_format($total_tw3,0), 'total_bku' => $total_bku, 'total_bku_format' => number_format($total_bku,0), 'ket' => 'TW III');
            }else{
                $respon = array('total_tw' => $total_tw4, 'total_tw_format' =>number_format($total_tw4,0), 'total_bku' => $total_bku, 'total_bku_format' => number_format($total_bku,0), 'ket' => 'TW IV');
            }
            return $respon;
        }
    }

    public function get_total_bku_per_rincian($rincian_id,$bulan)
    {
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $tahun = $this->session->userdata('token_tahun');
        if(in_array($bulan,array('1','2','3'))){
            $bulan_id = '1,2,3';
        }elseif(in_array($bulan, array('4','5','6'))){
            $bulan_id = '4,5,6';
        }elseif(in_array($bulan, array('7','8','9'))){
            $bulan_id = '7,8,9';
        }else{
            $bulan_id = '10,11,12';
        }

        $sql = $this->db->query("SELECT SUM(jumlah_transaksi) as total_bku FROM bku 
                where rincian_id='$rincian_id' and bulan_id in($bulan_id) 
                and deleted_at is NULL and pajak_id = 0 and bukti_id=4
                and sekolah_id='$sekolahid' and tahun='$tahun'");
        if($sql){
            $hasil = $sql->row_array();
            return $hasil['total_bku'];
        }   
    }

    public function get_bukti()
    {
        $this->db->where('deleted_at',NULL);
        $sql = $this->db->get('bukti');
        if($sql){
            return $sql->result();
        }
    }

    public function get_pajak()
    {
        $this->db->where('deleted_at',NULL);
        $sql = $this->db->get('pajak');
        if($sql){
            return $sql->result();
        }
    }

    public function get_daftar_pajak($bulan)
    {
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $tahun = $this->session->userdata('token_tahun');
        $this->db->where('sekolah_id',$sekolahid);
        $this->db->where('tahun',$tahun);
        // $this->db->where('bulan_id',$bulan);
        $this->db->where('pajak_id !=',0);
        $this->db->where('status_pajak',0);
        $this->db->where('bukti_id',3);
        $this->db->where('deleted_at',NULL);
        $this->db->order_by('tanggal_transaksi,bku_id','asc');
        $sql = $this->db->get('bku');
        if($sql){
            return $sql->result();
        }

    }

    public function get_data_pajak($id)
    {
        $this->db->where('bku_id',$id);
        $sql = $this->db->get('bku');
        if($sql){
            $rows = $sql->row_array();
            $respon = array('status' => true, 'uraian' => $rows['uraian_bku'], 'jumlah_transaksi' => $rows['jumlah_transaksi']);
            echo json_encode($respon);    
        }
    }

    public function destroy_titik($data)
    {
        $nilai = str_replace('.','',$data);
        return $nilai;
    }

    public function get_number($bukti)
    {
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $tahun = $this->session->userdata('token_tahun');
        $this->db->where('sekolah_id',$sekolahid);
        $this->db->where('tahun',$tahun);
        $this->db->where('bukti_id',$bukti);
        $this->db->where('deleted_at',NULL);
        $this->db->order_by('no_urut_bukti','desc');
        $sql = $this->db->get('bku',1);
        if($sql->num_rows() > 0){
            $rows = $sql->row_array();
            $tem_no = $rows['no_urut_bukti'];
            $filter = strval($tem_no);
            $no_urut = $filter+1;
            return str_pad($no_urut,3,'0',STR_PAD_LEFT);
        }else{
            return str_pad(1,3,'0',STR_PAD_LEFT);
        }
    }

    public function simpan_data($tabel,$data,$status_pajak)
    {
        $bulan = $data['bulan_id'];
        $sql = $this->db->insert($tabel,$data);
        if($sql){
            $pecah = explode(',',$status_pajak);
            if($pecah[0] == 1){
                $data['id_bku'] = $pecah[1];
                $isi['status_pajak'] = 1;
                $this->db->where('bku_id',$pecah[1]);
                $sqlupdate = $this->db->update($tabel,$isi);
                if($sqlupdate){
                    $respon = array('status' => $sqlupdate, 'bulan' => $bulan);
                    echo json_encode($respon);
                }
            }else{
                $respon = array('status' => $sql, 'bulan' => $bulan);
                echo json_encode($respon);   
            }
        }else{
            $respon = array('status' => false, 'bulan' => $bulan);
            echo json_encode($respon);
        }
    }

    public function ubah_data($tabel,$id,$data,$bulan)
    {
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $tahun = $this->session->userdata('token_tahun');
        $this->db->where('bku_id',$id);
        $this->db->where('tahun',$tahun);
        $this->db->where('sekolah_id',$sekolahid);
        $sql = $this->db->update($tabel,$data);
        if($sql){
            $respon = array('status' => $sql, 'bulan' => $bulan);
            echo json_encode($respon);
        }else{
            $respon = array('status' => false, 'bulan' => $bulan);
            echo json_encode($respon);
        }
    }

    public function hapus_bku($tabel,$id,$bulan)
    {
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $tahun = $this->session->userdata('token_tahun');
        $this->db->where('bku_id',$id);
        $cek = $this->db->get('bku');
        if($cek){
            $rows = $cek->row_array();
            if($rows['id_bku'] != NULL){
                //UPDATE
                $data['status_pajak'] = 0;
                $this->db->where('bku_id',$rows['id_bku']);
                $update = $this->db->update('bku',$data);
                if($update){
                    $this->db->where('bku_id',$id);
                    $this->db->where('sekolah_id',$sekolahid);
                    $this->db->where('tahun',$tahun);
                    $sql = $this->db->delete($tabel);
                    if($sql){
                        $respon = array('status' => $sql, 'bulan' => $bulan);
                        echo json_encode($respon);
                    }else{
                        $respon = array('status' => false, 'bulan' => $bulan);
                        echo json_encode($respon);
                    }
                }
                
            }else{
                //hapus only
                $this->db->where('bku_id',$id);
                $this->db->where('sekolah_id',$sekolahid);
                $this->db->where('tahun',$tahun);
                $sql = $this->db->delete($tabel);
                if($sql){
                    $respon = array('status' => $sql, 'bulan' => $bulan);
                    echo json_encode($respon);
                }else{
                    $respon = array('status' => false, 'bulan' => $bulan);
                    echo json_encode($respon);
                }
            }
        }
    }

    public function get_bku_id($id)
    {
        $this->db->where('bku.bku_id',$id);
        $this->db->join('bukti','bukti.bukti_id=bku.bukti_id','left');
        $this->db->join('pajak','pajak.pajak_id=bku.pajak_id','left');
        $this->db->join('rincian_belanja','rincian_belanja.rincian_id=bku.rincian_id','left');
        $this->db->join('jenis_belanja','jenis_belanja.belanja_id=rincian_belanja.belanja_id','left');
        $sql = $this->db->get('bku');
        if($sql){
            return $sql->row_array();
        }
    }

    public function simpan_saldo($data,$saldo)
    {
        $tahun = $this->session->userdata('token_tahun');
        $sekolahid = $this->session->userdata('token_sekolah_id');
        $bulan = $data['bulan_id'];
        $data['sekolah_id'] = $sekolahid;
        $data['tahun'] = $tahun;

        $this->db->where('sekolah_id',$sekolahid);
        $this->db->where('tahun',$tahun);
        $this->db->where('bulan_id',$bulan);
        $cek = $this->db->get('saldo_tunai');

        $saldo_tunai = $saldo - $data['jumlah_saldo'];
        if($cek->num_rows() > 0){
            //update
            $rows = $cek->row_array();
            $this->db->where('saldo_id',$rows['saldo_id']);
            $isi['jumlah_saldo'] = $data['jumlah_saldo'];
            $sql = $this->db->update('saldo_tunai',$isi);
            if($sql){
                $respon = array('status' => $sql, 'bulan' => $bulan, 'saldo_bank' => number_format($saldo_tunai,0));
                echo json_encode($respon);
            }else{
                $respon = array('status' => false, 'bulan' => $bulan);
                echo json_encode($respon);
            }
        }else{
            //simpan
            $sql = $this->db->insert('saldo_tunai',$data);
            if($sql){
                $respon = array('status' => $sql, 'bulan' => $bulan, 'saldo_bank' => number_format($saldo_tunai,0));
                echo json_encode($respon);
            }else{
                $respon = array('status' => false, 'bulan' => $bulan);
                echo json_encode($respon);
            }
        }

        
    }

    public function get_saldo_tunai($bulan,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        $this->db->where('sekolah_id',$sekolahid);
        $this->db->where('tahun',$tahun);
        $this->db->where('bulan_id',$bulan);
        $sql = $this->db->get('saldo_tunai');
        if($sql){
            return $sql->row_array();
        }
    }

    public function get_total_saldo_tunai_tw($tw,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        $regulasi = $this->sekolah_model->get_regulasi_login($tahun);
        if($regulasi == 3){
            if($tw == 1){
                // $bulan = '1,2,3';
                $bulan = '3';
            }elseif($tw == 2){
                // $bulan = '4,5,6,7,8';
                $bulan = '8';
            }elseif($tw == 3){
                // $bulan = '9,10,11,12';
                $bulan = '12';
            }else{
                $bulan = '00';   
            }
        }else{
            if($tw == 1){
                $bulan = '1,2,3';
            }elseif($tw == 2){
                $bulan = '4,5,6';
            }elseif($tw == 3){
                $bulan = '7,8,9';
            }elseif($tw == 4){
                $bulan = '10,11,12';
            }else{
                $bulan = '00';
            }    
        }
        
        $sql = $this->db->query("SELECT SUM(jumlah_saldo) as total_saldo from saldo_tunai where sekolah_id='$sekolahid' and tahun='$tahun' and bulan_id IN($bulan)");
        if($sql){
            return $sql->row_array();
        }
    }

    public function get_realisasi($belanja_id,$tw,$tahun,$sekolahid)
    {
        $penerimaan = $this->get_penerimaan($tahun,$sekolahid);
        $total_tw = $this->get_rkas_per_tw($belanja_id,$tw,$tahun,$sekolahid);
        return array('penerimaan' => $penerimaan['total_penerimaan'], 'total_tw' => $total_tw['total_tw'], 'belanja_id' => $belanja_id, 'tw' => $tw);
    }

    public function get_penerimaan($tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        $sql = $this->db->query("SELECT (SUM(kelas_i+kelas_ii+kelas_iii+kelas_iv+kelas_v+kelas_vi+kelas_vii+kelas_viii+kelas_ix+kelas_x+kelas_xi+kelas_xii)*dasar_dana) AS total_penerimaan
                FROM sekolah
                JOIN penerima_bos on sekolah.npsn=penerima_bos.npsn and penerima_bos.tahun='$tahun'
                JOIN dasar_dana_bos on sekolah.jenjang=dasar_dana_bos.jenjang_sekolah and dasar_dana_bos.tahun='$tahun'
                where sekolah.sekolah_id = '$sekolahid'");
        if($sql){
            return $sql->row_array();
        }
    }

    public function get_rkas_per_tw($belanja_id,$tw,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        $sql = $this->db->query("SELECT (SUM(tw$tw) + SUM(IF(rincian_belanja.status_rincian=1,rincian_header.htw$tw,0))) as total_tw
            from rincian_belanja 
            left join rincian_header on rincian_belanja.rincian_id=rincian_header.rincian_id
            where rincian_belanja.soft_delete=0 and rincian_belanja.belanja_id in('$belanja_id')
            and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolahid'");
        if($sql){
            return $sql->row_array();
        }
    }

    public function get_total_jenis_belanja($belanja_id,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        // $sql = $this->db->query("SELECT (SUM(tw1) + SUM(htw1) + SUM(tw2) + SUM(htw2) + SUM(tw3) + SUM(htw3) + SUM(tw4) + SUM(htw4)) as total
        //     from rincian_belanja 
        //     left join rincian_header on rincian_belanja.rincian_id=rincian_header.rincian_id
        //     where rincian_belanja.soft_delete=0 and rincian_belanja.belanja_id in('$belanja_id')
        //     and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolahid'");
        $sql = $this->db->query("SELECT SUM(tw1+tw2+tw3+tw4) + SUM(IF(rincian_belanja.status_rincian=1,rincian_header.htw1+rincian_header.htw2+rincian_header.htw3+rincian_header.htw4,0)) as total 
                                FROM rincian_belanja LEFT join rincian_header on rincian_header.rincian_id=rincian_belanja.rincian_id 
                                WHERE rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolahid' and rincian_belanja.belanja_id='$belanja_id' AND rincian_belanja.soft_delete=0");
        if($sql){
            $rows = $sql->row_array();
            $respon = array('total' => $rows['total'], 'belanja_id' => $belanja_id);
            return $respon;
        }       
    }

    public function get_total_realisasi($belanja_id,$tw,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        $regulasi = $this->sekolah_model->get_regulasi_login($tahun);
        if($regulasi == 3){
            if($tw == 1){
                $twin = '1,2,3';
            }elseif($tw == 2){
                $twin = '4,5,6,7,8';
            }else{
                $twin = '9,10,11,12';
            }
        }else{
            if($tw == 1){
                $twin = '1,2,3';
            }elseif($tw == 2){
                $twin = '4,5,6';
            }elseif($tw == 3){
                $twin = '7,8,9';
            }else{
                $twin = '10,11,12';
            }
        }
        
        $sql = $this->db->query("SELECT SUM(bku.jumlah_transaksi) as total_realisasi
                from bku
                join rincian_belanja on rincian_belanja.rincian_id=bku.rincian_id
                where bku.sekolah_id='$sekolahid' and bku.tahun='$tahun' and bku.pajak_id=0 
                and bku.jenis_transaksi=0 and bku.deleted_at is null and bku.bulan_id in($twin) 
                and bku.bukti_id=4 and rincian_belanja.belanja_id='$belanja_id'");
        if($sql){
            return $sql->row_array();
        }   
    }

    public function get_total_realisasi_before($belanja_id,$tw,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        $regulasi = $this->sekolah_model->get_regulasi_login($tahun);
        if($regulasi == 3){
            if($tw == 1){
                $twin = '1,2,3';
            }elseif($tw == 2){
                $twin = '1,2,3,4,5,6,7,8';
            }else{
                $twin = '1,2,3,4,5,6,7,8,9,10,11,12';
            }
        }else{
            if($tw == 1){
                $twin = '1,2,3';
            }elseif($tw == 2){
                $twin = '1,2,3,4,5,6';
            }elseif($tw == 3){
                $twin = '1,2,3,4,5,6,7,8,9';
            }else{
                $twin = '1,2,3,4,5,6,7,8,9,10,11,12';
            }
        }
        
        $sql = $this->db->query("SELECT SUM(bku.jumlah_transaksi) as total_realisasi
                from bku
                join rincian_belanja on rincian_belanja.rincian_id=bku.rincian_id
                where bku.sekolah_id='$sekolahid' and bku.tahun='$tahun' and bku.pajak_id=0 
                and bku.jenis_transaksi=0 and bku.deleted_at is null and bku.bulan_id in($twin) 
                and bku.bukti_id=4 and rincian_belanja.belanja_id='$belanja_id'");
        if($sql){
            return $sql->row_array();
        }   
    }

    public function get_komponen_biaya($id,$tahun)
    {
        $regulasi = $this->get_regulasi_id($tahun);
        $reg_id = $regulasi['regulasi_id'];
        if($regulasi['jumlah_tw'] == 3){
            $sql = $this->db->query("SELECT * FROM komponen_pembiayaan where soft_delete=0 and regulasi_id='$reg_id' order by komponen_id asc");
        }else{
            $rows = $this->get_profil_sekolah($id);
            if($rows['jenjang'] == 'SMA'){
                $limit = 10;    
            }else {
                $limit = 12;            
            }
            $sql = $this->db->query("SELECT * FROM komponen_pembiayaan where soft_delete=0 and regulasi_id='$reg_id' order by komponen_id asc limit $limit");
        }
        
        if($sql){
            return $sql->result();
        }
    }

    public function get_standar_nasional()
    {
        $this->db->where('soft_delete',0);
        $this->db->order_by('standar_id','asc');
        $sql = $this->db->get('standar_nasional');
        if($sql){
            return $sql->result();
        }
    }

    public function get_realisasi_komponen_biaya($tw,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        $sn = $this->get_standar_nasional();
        $kp = $this->get_komponen_biaya($sekolahid,$tahun);
        foreach($kp as $kps){
            $respon[$kps->komponen_kode] = $this->get_total_per_komponen($tw,$kps->komponen_kode,$tahun,$sekolahid);
        }
        
        return $respon;
    }

    public function get_total_per_standar_komponen($tw,$standar,$komponen,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        if($tw == 1){
            $twin = '1,2,3';
        }elseif($tw == 2){
            $twin = '4,5,6';
        }elseif($tw == 3){
            $twin = '7,8,9';
        }else{
            $twin = '10,11,12';
        }
        // standar_nasional.standar_id, standar_nasional.nama_standar, rincian_belanja.komponen_kode, 
        $sql = $this->db->query("SELECT SUM(bku.jumlah_transaksi) as total_realisasi
                from bku
                join rincian_belanja on rincian_belanja.rincian_id=bku.rincian_id
                left join sub_program on sub_program.program_id=rincian_belanja.program_id
                left join sub_standar on sub_standar.substandar_id=sub_program.substandar_id
                left join standar_nasional on standar_nasional.standar_id=sub_standar.standar_id
                where bku.sekolah_id='$sekolahid' and bku.tahun='$tahun' and bku.pajak_id=0 
                and bku.jenis_transaksi=0 and bku.deleted_at is null and bku.bulan_id in($twin) 
                and bku.bukti_id=4
                and standar_nasional.standar_id='$standar' and rincian_belanja.komponen_kode='$komponen'");
        if($sql){
            return $sql->row_array()['total_realisasi'];
        }
    }

    public function get_total_per_komponen($tw,$komponen,$tahun,$sekolahid)
    {
        // $tahun = $this->session->userdata('token_tahun');
        // $sekolahid = $this->session->userdata('token_sekolah_id');
        if($tw == 1){
            $twin = '1,2,3';
        }elseif($tw == 2){
            $twin = '4,5,6';
        }elseif($tw == 3){
            $twin = '7,8,9';
        }else{
            $twin = '10,11,12';
        }
        // standar_nasional.standar_id, standar_nasional.nama_standar, rincian_belanja.komponen_kode, 
        $sql = $this->db->query("SELECT rincian_belanja.komponen_kode, SUM(jumlah_transaksi) as total
                FROM bku
                left join rincian_belanja on rincian_belanja.rincian_id=bku.rincian_id
                WHERE bku.pajak_id = 0
                and bku.bukti_id =4
                and bku.jenis_transaksi=0
                and bku.deleted_at is null
                and bku.sekolah_id ='$sekolahid'
                and bku.tahun ='$tahun'
                and bulan_id in($twin)
                and rincian_belanja.komponen_kode='$komponen'");
        if($sql){
            return $sql->row_array()['total'];
        }
    }

    public function insert_integrasi($tw,$tahun,$sekolahid)
    {
        $kp = $this->get_komponen_biaya($sekolahid,$tahun);
        foreach($kp as $kps){
            $respon[$kps->komponen_kode] = $this->get_total_per_komponen($tw,$kps->komponen_kode,$tahun,$sekolahid);
        }
        
        return $respon;
    }

    public function api_insert_penggunaan($tw,$tahun,$sekolahid)
    {
        $sekolah = $this->get_profil_sekolah($sekolahid);
        $penggunaan = $this->insert_integrasi($tw,$tahun,$sekolahid);
        $jasa = $this->get_realisasi(1,$tw,$tahun,$sekolahid);
		$mesin = $this->get_realisasi(2,$tw,$tahun,$sekolahid);
		$aset = $this->get_realisasi(3,$tw,$tahun,$sekolahid);
        $penerimaan = $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bos.ruangpanji.com/api/services_penggunaan/laporan_penggunaan",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('sekolah_id' => $sekolah['id_sekolah'],'tahun' => $tahun,'triwulan' => $tw,
                'penerimaan_dana' => $penerimaan,
                'komponen_1' => $penggunaan['01'],
                'komponen_1_a_1' => '0',
                'komponen_1_a_2' => '0',
                'komponen_1_a_3' => '',
                'komponen_1_b_1' => '0',
                'komponen_1_b_2' => '0',
                'komponen_1_b_3' => '',
                'komponen_1_c_1' => '0',
                'komponen_1_c_2' => '0',
                'komponen_1_c_3' => '',
                'komponen_1_d_1' => '0',
                'komponen_1_d_2' => '0',
                'komponen_1_d_3' => '',
                'komponen_1_e_1' => '0',
                'komponen_2' => $penggunaan['02'],
                'komponen_3' => $penggunaan['03'],
                'komponen_4' => $penggunaan['04'],
                'komponen_5' => $penggunaan['05'],
                'komponen_6' => $penggunaan['06'],
                'komponen_7' => $penggunaan['07'],
                'komponen_8' => $penggunaan['08'],
                'komponen_9' => $penggunaan['09'],
                'komponen_10' => $penggunaan['10']),
            CURLOPT_HTTPHEADER => array(
                "api-key: f51b276a09a2c862e70acf88f953c0db91b62529"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function update_history_integrasi($tw,$tahun,$sekolahid)
    {
        $this->db->where('sekolah_id',$sekolahid);
        $this->db->where('tahun',$tahun);
        $this->db->where('triwulan',$tw);
        $sql = $this->db->get('riwayat_integrasi');
        if($sql->num_rows() > 0){
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('sekolah_id',$sekolahid);
            $this->db->where('tahun',$tahun);
            $this->db->where('triwulan',$tw);
            $query = $this->db->update('riwayat_integrasi',$data);
            if($query){
                $respon = array('status' => true, 'msg' => 'data berhasil di update');
            }else{
                $respon = array('status' => false, 'msg' => 'data gagal di update');
            }
        }else{
            $data['sekolah_id'] = $sekolahid;
            $data['tahun'] = $tahun;
            $data['triwulan'] = $tw;
            $query = $this->db->insert('riwayat_integrasi',$data);
            if($query){
                $respon = array('status' => true, 'msg' => 'data berhasil di simpan');
            }else{
                $respon = array('status' => false, 'msg' => 'data gagal di simpan');
            }
        }

        return $respon;
    }

    public function get_integrasi_tw($tw,$tahun,$sekolahid)
    {
        $this->db->where('triwulan',$tw);
        $this->db->where('tahun',$tahun);
        $this->db->where('sekolah_id',$sekolahid);
        $sql = $this->db->get('riwayat_integrasi');
        if($sql){
            return $sql->row_array();
        }
    }



    //RKAS 2020
    public function get_data_bos($sekolahid,$tahun)
    {
        $sql = $this->db->query("SELECT SUM(penerima_bos.kelas_i+penerima_bos.kelas_ii+penerima_bos.kelas_iii+penerima_bos.kelas_iv+penerima_bos.kelas_v+penerima_bos.kelas_vi+penerima_bos.kelas_vii+penerima_bos.kelas_viii+penerima_bos.kelas_ix+penerima_bos.kelas_x+penerima_bos.kelas_xi+penerima_bos.kelas_xii) AS jumlah_siswa, 
            dasar_dana_bos.dasar_dana, 
            sum(penerima_bos.kelas_i+penerima_bos.kelas_ii+penerima_bos.kelas_iii+penerima_bos.kelas_iv+penerima_bos.kelas_v+penerima_bos.kelas_vi+penerima_bos.kelas_vii+penerima_bos.kelas_viii+penerima_bos.kelas_ix+penerima_bos.kelas_x+penerima_bos.kelas_xi+penerima_bos.kelas_xii) * dasar_dana_bos.dasar_dana as total_dana_bos 
            FROM penerima_bos 
            JOIN sekolah on sekolah.npsn=penerima_bos.npsn 
            JOIN dasar_dana_bos on dasar_dana_bos.tahun=penerima_bos.tahun and sekolah.jenjang=dasar_dana_bos.jenjang_sekolah 
            WHERE sekolah.sekolah_id = '$sekolahid' and sekolah.status=0 AND penerima_bos.tahun='$tahun'");
        if($sql){
            return $sql->row_array();
        }
    }

    public function get_regulasi_id($tahun)
	{
		$sql = $this->db->query("SELECT regulasi.regulasi_id,regulasi.jumlah_tw FROM rincian_belanja 
				join sub_program on sub_program.program_id=rincian_belanja.program_id
				join regulasi on regulasi.regulasi_id=sub_program.regulasi_id
				where rincian_belanja.tahun='$tahun' and rincian_belanja.soft_delete='0' limit 1");
		if($sql){
			$hasil = $sql->row_array()['jumlah_tw'];
			if($hasil != NULL){
				$respon = $sql->row_array();
			}else {
				$this->db->where('status_regulasi',1);
				$q = $this->db->get('regulasi');
				if($q){
					$rows = $q->row_array();
					$respon = $q->row_array();
				}

			}
		}
		return $respon;
	}
    //END RKAS 2020
}
