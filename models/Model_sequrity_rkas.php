<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_sequrity_rkas extends CI_Model {

	public function get_sequrity_sekolah()
	{
		$token_login_sekolah = $this->session->userdata('token_login_sekolah');
		if(empty($token_login_sekolah)){
			//$this->session->sess_destroy();
			unset($token_login_sekolah);
			redirect(base_url('sekolah/login'));
		}
		// redirect(base_url('perbaikan'));
	}

	public function get_sequrity_webadmin()
	{
		$token_login_webadmin = $this->session->userdata('token_login_webadmin');
		if(empty($token_login_webadmin)){
			//$this->session->sess_destroy();
			//unset($token_login_webadmin);
			redirect(base_url('webadmin/login'));
		}	
	}

	public function get_sequrity_supervisor()
	{
		$token_login_supervisor = $this->session->userdata('token_login_supervisor');
		if(empty($token_login_supervisor)){
			//$this->session->sess_destroy();
			$token_login_webadmin = $this->session->userdata('token_login_webadmin');
			unset($token_login_webadmin);
			redirect(base_url('webadmin/login'));
		}	
	}

}
