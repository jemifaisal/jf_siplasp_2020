<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah_model extends CI_Model {

	public function m_get_login($u,$p,$tahun)
	{
		$pwd = md5($p);
		$this->db->select('username, password, nama_sp, sekolah_id');
		$this->db->where('username',$u);
		$this->db->where('password',$pwd);
		$this->db->where('status',0);
		$sql = $this->db->get('sekolah');
		$token = rand(1000000,10000000);
		if($sql->num_rows() > 0){
			//simpan jam login
			$data['username'] = $u;
			$query = $this->db->insert('login_log',$data);
			$regulasi_id = $this->get_regulasi();
			foreach ($sql->result() as $row) {
				$sess_  = array('token_nama_sekolah' => $row->nama_sp,
								'token_sekolah_id' => $row->sekolah_id,
								'token_tahun' => $tahun,
								'token_regulasi' => $regulasi_id,
								'token_login_sekolah'=>$token."73m!@###");

				$this->session->set_userdata($sess_);
				//redirect(base_url('sekolah'));
				$msg = "<div class='alert alert-success'>
				<strong><i class='fa fa-spinner fa-spin' style='font-size:20px'></i> Success!</strong> Berhasil login, silahkan tunggu...
				</div>";
				$respon = array('status' => true, 'msg' => $msg);
				echo json_encode($respon);
			}

		}else{
			//$this->session->set_flashdata('info',
			$msg = "<div class='alert alert-danger alert-dismissable'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				<strong>Gagal Login!</strong> Username atau Password Anda Salah.!
				</div>";
			//redirect(base_url('sekolah/login'));
			$respon = array('status' => false, 'msg' => $msg);
			echo json_encode($respon);
		}
	}

	//16 feb 2019
	public function get_standar_nasional($id=null){
		if($id == null){
			$this->db->where('soft_delete',0);
			$this->db->order_by('standar_kode','asc');
			$sql = $this->db->get('standar_nasional');
			if($sql){
				return $sql->result();
			}
		}else{
			$this->db->where('standar_nasional.soft_delete',0);
			$this->db->where('standar_nasional.standar_id',$id);
			$this->db->order_by('standar_nasional.standar_kode','asc');
			$sql = $this->db->get('standar_nasional');
			if($sql){
				return $sql->row_array();
			}			
		}	
	}

	public function get_sub_standar($id,$subid=null){
		if($subid == null){
			$this->db->where('sub_standar.soft_delete',0);
			$this->db->where('standar_nasional.soft_delete',0);
			$this->db->where('standar_nasional.standar_id',$id);
			$this->db->join('standar_nasional','sub_standar.standar_id=standar_nasional.standar_id');
			$sql = $this->db->get('sub_standar');
			if($sql){
				return $sql->result();
			}	
		}else{
			$this->db->where('sub_standar.soft_delete',0);
			$this->db->where('standar_nasional.soft_delete',0);
			$this->db->where('standar_nasional.standar_id',$id);
			$this->db->where('sub_standar.substandar_id',$subid);
			$this->db->join('standar_nasional','sub_standar.standar_id=standar_nasional.standar_id');
			$sql = $this->db->get('sub_standar');
			if($sql){
				return $sql->row_array();
			}
		}
		
	}

	public function get_total_per_standar($id,$belanja,$subid=null){
		//BELUM BERDASARKAN BELANJA ID
		$tahun = $this->session->userdata('token_tahun');
		$sekolah = $this->session->userdata('token_sekolah_id');
		if($belanja == 1){
			$belanja_id = $belanja;
		}else{
			$belanja_id = '2,3';
		}
		if($subid == null){
			$sql = $this->db->query("SELECT SUM(rincian_belanja.volume * rincian_belanja.harga_satuan) as total from rincian_belanja left join sub_program on rincian_belanja.program_id=sub_program.program_id left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id where rincian_belanja.soft_delete='0' and sub_standar.soft_delete='0' and sub_program.soft_delete='0' and standar_nasional.soft_delete='0' and standar_nasional.standar_id='$id' and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah' and rincian_belanja.belanja_id in($belanja_id)")->row_array();
			if($sql){
				$query = $this->db->query("
						SELECT SUM(rincian_header.volume_hr * rincian_header.harga_satuan_hr) as total from rincian_belanja 
							left join rincian_header on rincian_belanja.rincian_id=rincian_header.rincian_id
							left join sub_program on rincian_belanja.program_id=sub_program.program_id 
							left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id 
							left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id

							where rincian_belanja.status_rincian='1' and rincian_belanja.soft_delete='0'
							and sub_standar.soft_delete='0' and sub_program.soft_delete='0' and standar_nasional.soft_delete='0' and rincian_header.soft_delete='0'
							and standar_nasional.standar_id='$id' and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah' and rincian_belanja.belanja_id in($belanja_id)
					")->row_array();
				if($query){
					$total_rincian_header = $query['total'];
					$total_rincian = $sql['total'];

					return number_format($total_rincian + $total_rincian_header,0);
				}
				
			}
		}else{
			$sql = $this->db->query("SELECT SUM(rincian_belanja.volume * rincian_belanja.harga_satuan) as total from rincian_belanja left join sub_program on rincian_belanja.program_id=sub_program.program_id left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id where rincian_belanja.soft_delete='0' and sub_standar.soft_delete='0' and sub_program.soft_delete='0' and standar_nasional.soft_delete='0' and standar_nasional.standar_id='$id' and sub_standar.substandar_id='$subid' and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah' and rincian_belanja.belanja_id in($belanja_id)")->row_array();
			if($sql){
				$query = $this->db->query("
						SELECT SUM(rincian_header.volume_hr * rincian_header.harga_satuan_hr) as total from rincian_belanja 
							left join rincian_header on rincian_belanja.rincian_id=rincian_header.rincian_id
							left join sub_program on rincian_belanja.program_id=sub_program.program_id 
							left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id 
							left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id

							where rincian_belanja.status_rincian='1' and rincian_belanja.soft_delete='0'
							and sub_standar.soft_delete='0' and sub_program.soft_delete='0' and standar_nasional.soft_delete='0' and rincian_header.soft_delete='0'
							and standar_nasional.standar_id='$id' and sub_standar.substandar_id='$subid' and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah' and rincian_belanja.belanja_id in($belanja_id)
					")->row_array();
				if($query){
					$total_rincian_header = $query['total'];
					$total_rincian = $sql['total'];

					return number_format($total_rincian + $total_rincian_header,0);
				}
				
			}	
		}
		

	}

	public function data_program_per_id($id,$belanja){
		$tahun = $this->session->userdata('token_tahun');
		$sekolah = $this->session->userdata('token_sekolah_id');
		if($belanja == 1){
			$belanja_id = array($belanja);
		}else{
			$belanja_id = array(2,3);
		}
		$this->db->select('rincian_belanja.komponen_kode as komponenkode, sub_program.program_kode, sub_program.nama_program, sub_program.program_id');
		$this->db->join('sub_program','sub_program.program_id=rincian_belanja.program_id');
		$this->db->join('sub_standar','sub_standar.substandar_id=sub_program.substandar_id');
		$this->db->where('rincian_belanja.sekolah_id',$sekolah);
		$this->db->where('rincian_belanja.tahun',$tahun);
		$this->db->where('sub_standar.substandar_id',$id);
		$this->db->where_in('rincian_belanja.belanja_id',$belanja_id);
		$this->db->where('rincian_belanja.soft_delete',0);
		$this->db->where('sub_program.soft_delete',0);
		$this->db->where('sub_standar.soft_delete',0);
		$this->db->group_by('rincian_belanja.program_id, rincian_belanja.komponen_kode');
		$sql = $this->db->get('rincian_belanja');
		if($sql){
			return $sql->result();
		}
	}

	public function detail_program_per_id($id,$belanja,$kode){
		$tahun = $this->session->userdata('token_tahun');
		$sekolah = $this->session->userdata('token_sekolah_id');
		if($belanja == 1){
			$belanja_id = array($belanja);
		}else{
			$belanja_id = array(2,3);
		}
		$this->db->where('rincian_belanja.program_id',$id);
		$this->db->where('rincian_belanja.sekolah_id',$sekolah);
		$this->db->where('rincian_belanja.tahun',$tahun);
		$this->db->where('rincian_belanja.tahun',$tahun);
		$this->db->where('rincian_belanja.komponen_kode',$kode);
		$this->db->where_in('rincian_belanja.belanja_id',$belanja_id);
		$this->db->order_by('rincian_belanja.status_rincian','asc');
		$sql = $this->db->get('rincian_belanja');
		if($sql){
			return $sql->result();
		}
	}

	public function detail_program_header_per_id($id){
		$this->db->where('rincian_header.rincian_id',$id);
		$this->db->where('rincian_header.soft_delete',0);
		$sql = $this->db->get('rincian_header');
		if($sql){
			return $sql->result();
		}
	}

	public function total_program_per_id($id,$belanja,$kode){
		$tahun = $this->session->userdata('token_tahun');
		$sekolah = $this->session->userdata('token_sekolah_id');
		if($belanja == 1){
			$belanja_id = $belanja;
		}else{
			$belanja_id = '2,3';
		}
		$sql = $this->db->query("SELECT rincian_belanja.rincian_id, SUM(rincian_belanja.volume * rincian_belanja.harga_satuan) as total from rincian_belanja where rincian_belanja.program_id='$id' and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah' and rincian_belanja.belanja_id in($belanja_id) and rincian_belanja.soft_delete='0' and rincian_belanja.komponen_kode='$kode'")->row_array();
		if($sql){
			$rincian = $sql['rincian_id'];
			$query = $this->db->query("SELECT SUM(rincian_header.volume_hr * rincian_header.harga_satuan_hr) as total from rincian_belanja
				left join rincian_header on rincian_header.rincian_id=rincian_belanja.rincian_id
				where rincian_belanja.program_id='$id' and rincian_belanja.belanja_id in($belanja_id) 
				and rincian_belanja.status_rincian='1' 
				and rincian_belanja.komponen_kode='$kode'
				and rincian_belanja.sekolah_id='$sekolah' and rincian_belanja.tahun='$tahun'
				and rincian_belanja.soft_delete='0' and rincian_header.soft_delete='0'")->row_array();
			if($query){
				return $sql['total'] + $query['total'];
			}	
		}
	}

	public function total_per_header($id,$tw,$aksi){
		if($aksi == 1){
			$sql = $this->db->query("SELECT SUM($tw) as total from rincian_header where soft_delete='0' and rincian_id='$id'")->row_array();
			if($sql){
				return $sql['total'];
			}
		}else{
			$sql = $this->db->query("SELECT SUM(volume_hr * harga_satuan_hr) as total from rincian_header where soft_delete='0' and rincian_id='$id'")->row_array();
			if($sql){
				return $sql['total'];
			}	
		}
			
	}

	public function total_perogram_per_tw($id,$tw,$belanja,$kode){
		$tahun = $this->session->userdata('token_tahun');
		$sekolah = $this->session->userdata('token_sekolah_id');
		if($belanja == 1){
			$belanja_id = $belanja;
		}else{
			$belanja_id = '2,3';
		}
		$sql = $this->db->query("SELECT rincian_belanja.rincian_id, SUM($tw) as total from rincian_belanja where soft_delete='0' and program_id='$id' and sekolah_id='$sekolah' and tahun='$tahun' and rincian_belanja.belanja_id in($belanja_id) and rincian_belanja.komponen_kode='$kode'")->row_array();
		if($sql){
			if($tw == 'tw1'){
				$twh = 'htw1';
			}elseif($tw == 'tw2'){
				$twh = 'htw2';
			}elseif($tw == 'tw3'){
				$twh = 'htw3';
			}else{
				$twh = 'htw4';
			}
			$query = $this->db->query("SELECT SUM(rincian_header.$twh) as total from rincian_belanja
				left join rincian_header on rincian_header.rincian_id=rincian_belanja.rincian_id
				where rincian_belanja.program_id='$id' and rincian_belanja.belanja_id in($belanja_id) and rincian_belanja.status_rincian='1' 
				and rincian_belanja.sekolah_id='$sekolah' and rincian_belanja.tahun='$tahun'
				and rincian_belanja.komponen_kode='$kode'
				and rincian_belanja.soft_delete='0' and rincian_header.soft_delete='0'")->row_array();
			
			return $sql['total'] + $query['total'];
		}	
	}

	public function total_bos_sekolah($id,$tahun){
		$sql = $this->db->query("SELECT penerima_bos.triwulan, format(penerima_bos.sisa_uang,0) as sisa_uang, sekolah.jenjang, format(kelas_i+kelas_ii+kelas_iii+kelas_iv+kelas_v+kelas_vi+kelas_vii+kelas_viii+kelas_ix+kelas_x+kelas_xi+kelas_xii,0) as jumlah_pd,
			format(((kelas_i+kelas_ii+kelas_iii+kelas_iv+kelas_v+kelas_vi+kelas_vii+kelas_viii+kelas_ix+kelas_x+kelas_xi+kelas_xii)*dasar_dana_bos.dasar_dana),0) as total FROM penerima_bos 
			join sekolah on sekolah.npsn=penerima_bos.npsn
			join dasar_dana_bos on dasar_dana_bos.jenjang_sekolah=sekolah.jenjang and dasar_dana_bos.tahun=penerima_bos.tahun
			where sekolah.sekolah_id='$id' and penerima_bos.tahun='$tahun'")->row_array();
		if($sql){
			if($sql['jenjang'] == "SLB" or $sql['jenjang'] == "SDLB"){
				if($sql['jumlah_pd'] < 60 ){
					$total = number_format('120000000',0);
				}else{
					$total = $sql['total'];
				}
			}else{
				$total = $sql['total'];
			}

			$regulasi = $this->sekolah_model->get_regulasi_login($tahun);
			if($regulasi == 3){
				$arr = array(',','.');
				$tw1 = number_format(30 * str_replace($arr,'',$total) / 100,0);
				$tw2 = number_format(40 * str_replace($arr,'',$total) / 100,0);
				$tw3 = number_format(30 * str_replace($arr,'',$total) / 100,0);
				$tw4 = number_format(0,0);
			}else{
				$arr = array(',','.');
				$tw1 = number_format(20 * str_replace($arr,'',$total) / 100,0);
				$tw2 = number_format(40 * str_replace($arr,'',$total) / 100,0);
				$tw3 = number_format(20 * str_replace($arr,'',$total) / 100,0);
				$tw4 = number_format(20 * str_replace($arr,'',$total) / 100,0);
			}

			$data = array('jumlah_pd' => $sql['jumlah_pd'], 'total' => $total, 'sisa_uang' => $sql['sisa_uang'], 'triwulan' => $sql['triwulan'], 'tw1' => $tw1, 'tw2' => $tw2, 'tw3' => $tw3, 'tw4' => $tw4);
			return $data;
		}
	}

	public function total_belanja_bos($id,$tahun){
		$tahun = $this->session->userdata('token_tahun');
		$sekolah = $this->session->userdata('token_sekolah_id');
		$sql = $this->db->query("SELECT SUM(rincian_belanja.volume * rincian_belanja.harga_satuan) as total from rincian_belanja left join sub_program on rincian_belanja.program_id=sub_program.program_id left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id 
			where rincian_belanja.soft_delete='0' and sub_standar.soft_delete='0' and sub_program.soft_delete='0' 
			and standar_nasional.soft_delete='0'
			and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah'")->row_array();
		if($sql){
			$query = $this->db->query("
					SELECT SUM(rincian_header.volume_hr * rincian_header.harga_satuan_hr) as total from rincian_belanja 
						left join rincian_header on rincian_belanja.rincian_id=rincian_header.rincian_id
						left join sub_program on rincian_belanja.program_id=sub_program.program_id 
						left join sub_standar on sub_program.substandar_id=sub_standar.substandar_id 
						left join standar_nasional on sub_standar.standar_id=standar_nasional.standar_id

						where rincian_belanja.status_rincian='1' and rincian_belanja.soft_delete='0'
						and sub_standar.soft_delete='0' and sub_program.soft_delete='0' and standar_nasional.soft_delete='0' and rincian_header.soft_delete='0'
						and rincian_belanja.tahun='$tahun' and rincian_belanja.sekolah_id='$sekolah'")->row_array();
			if($query){
				$total_rincian_header = $query['total'];
				$total_rincian = $sql['total'];
				$total_bos_sekolah = $this->total_bos_sekolah($sekolah,$tahun);

				$total_belanja = number_format($total_rincian + $total_rincian_header,0);
				$a = str_replace(array(',','.'),'',$total_bos_sekolah['total']);
				$b = str_replace(array(',','.'),'',$total_belanja);
				$c = intval($a)- intval($b);

				$sisa_lebih = number_format($c);
				$data = array('total_bos' => $total_bos_sekolah['total'] ,'total_belanja' => $total_belanja, 'sisa_lebih' => $sisa_lebih);
				return $data;
			}
			
		}	
	}


	//end 16 feb 2019

	public function pangkat()
	{
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('pangkat');
		if($sql){
			return $sql->result();
		}
	}
	public function mapel()
	{
		$this->db->where('soft_delete',0);
		$this->db->order_by('no_urut','asc');
		$sql = $this->db->get('mapel');
		if($sql){
			return $sql->result();
		}
	}
	public function sekolah()
	{
		$this->db->select('sekolah_id,nama_sp');
		$this->db->where('status',0);
		$sql = $this->db->get('sekolah');
		if($sql){
			return $sql->result();
		}
	}

	public function agama()
	{
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('agama');
		if($sql){
			return $sql->result();
		}
	}

	public function pendidikan()
	{
		$this->db->where('soft_delete',0);
		$sql = $this->db->get('pdd_terakhir');
		if($sql){
			return $sql->result();
		}
	}

	public function cekprogram()
	{
		$regulasi_id = $this->session->userdata('token_regulasi');
		$this->db->where('soft_delete',0);
		$this->db->where('regulasi_id',$regulasi_id);
		$sql = $this->db->get('sub_program');
		if($sql){
			return $sql->result();
		}
	}
	
	//aset
	public function simpan($table,$row){
		$sql = $this->db->insert($table,$row);
		return $sql;
	}

	public function edit($table,$row,$id){
		$this->db->where('id',$id);
		$sql = $this->db->update($table,$row);
		return $sql;
	}

	public function hapus($table,$id){
		$this->db->where('id',$id);
		$sql = $this->db->delete($table);
		return $sql;
	}
	public function tampilid($table,$id){
		$this->db->where('id',$id);
		$sql = $this->db->get($table);
		return $sql->result();
	}
	public function sumberdana()
	{
		$this->db->where('soft_delete',0);
		$this->db->order_by('dana_id','asc');
		$sql = $this->db->get('sumber_dana');
		if($sql){
			return $sql->result();
		}
	}
	public function totalkibb($id)
	{
		
		$sql = $this->db->query("SELECT tahun_pembelian, sumber_dana.nama_sumber_dana, asal_usul, sum(jumlah_barang*harga_perolehan) as total FROM kib_b join sumber_dana on kib_b.asal_usul=sumber_dana.dana_id WHERE sekolah_id='$id' GROUP by asal_usul,tahun_pembelian");
		if($sql){
			return $sql->result();
		}
		
	}

	public function totalkibe($id)
	{
		
		$sql = $this->db->query("SELECT tahun_pembelian, sumber_dana.nama_sumber_dana, asal_usul, sum(jumlah*harga_perolehan) as total FROM kib_e join sumber_dana on kib_e.asal_usul=sumber_dana.dana_id WHERE sekolah_id='$id' GROUP by asal_usul,tahun_pembelian");
		if($sql){
			return $sql->result();
		}
		
	}
	//end aset

	//terbaru 2020
	public function get_regulasi()
	{
		$this->db->where('status_regulasi',1);
		$sql = $this->db->get('regulasi');
		if($sql){
			$rows = $sql->row_array();
			return $rows['regulasi_id'];
		}	
	}

	public function get_regulasi_login($tahun)
	{
		$sql = $this->db->query("SELECT regulasi.jumlah_tw FROM rincian_belanja 
				join sub_program on sub_program.program_id=rincian_belanja.program_id
				join regulasi on regulasi.regulasi_id=sub_program.regulasi_id
				where rincian_belanja.tahun='$tahun' and rincian_belanja.soft_delete='0' limit 1");
		if($sql){
			$hasil = $sql->row_array()['jumlah_tw'];
			if($hasil != NULL){
				$respon = $sql->row_array()['jumlah_tw'];
			}else {
				$this->db->where('status_regulasi',1);
				$q = $this->db->get('regulasi');
				if($q){
					$rows = $q->row_array();
					$respon = $q->row_array()['jumlah_tw'];
				}

			}
		}
		return $respon;
	}
	//end 2020

}
