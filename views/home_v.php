<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="jemifaisal">

    <meta content="Sistem Pelayanan Satuan Pendidikan Provinsi Riau" name="description"/>
    <meta content="<?php echo base_url(); ?>" property="og:url"/>
    <meta content="Sistem Pelayanan Satuan Pendidikan Provinsi Riau" property="og:title"/>
    <meta content="Sistem Pelayanan Satuan Pendidikan Provinsi Riau" property="og:description"/>
    <meta name="keywords" content="Sistem Pelayanan Satuan Pendidikan Provinsi Riau" itemprop="keywords" />
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/img/favicon.png'); ?>">
    <title>SIPLASP - Sistem Pelayanan Satuan Pendidikan Provinsi Riau</title>
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/select2/select2.min.css">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .list li{list-style-type: square;margin-bottom: -10px;}
    </style>
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation" style="padding:8px;">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="<?php echo base_url('assets/img/logo-riau.png'); ?>" width="32px" style="float:left; margin-right:15px;">
                <a class="navbar-brand topnav" href="<?php echo base_url(); ?>"><h3 style="padding:0px;margin:0px;"> SIPLASP</h3></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="font-size:17px;">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url(); ?>">[ Home ]</a>
                    </li>
                    <li>
                        <a href="#kontak">[ Kontak ]</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

      <?php $this->load->view($content); ?>
    
      <div class="content-section-b" id="kontak">
        <div class="container" >
            <div class="row text-center">
              <div class="col-lg-8 mx-auto text-center" style="border:0px solid; float:none; margin:0px auto;">
                <h2 class="section-heading">Kontak</h2>
                <hr class="my-4">
                <p class="mb-5">Silahkan hubungi kami jika ada yang mau ditanyakan</p>
              
                
              </div>
            </div>
            <div class="row">
                  <div class="col-lg-6 ml-auto text-center">
                    <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                    <p>(0761) 22552 - 21553</p>
                  </div>
                  <div class="col-lg-6 mr-auto text-center">
                    <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                    <p>
                      <a href="mailto:dinaspendidikan@riau.go.id">dinaspendidikan@riau.go.id</a>
                    </p>
                  </div>
            </div>
          </div>

    </div>

    <!-- Footer -->
    <footer style="background-color:#2C3E50;">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="<?php echo base_url(); ?>">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#kontak">Kontak</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; Dinas Pendidikan Provinsi Riau. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url(); ?>assets/webadmin/plugins/select2/select2.full.min.js"></script>

</body>

</html>
