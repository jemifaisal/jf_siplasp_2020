                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_kategorilomba'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Lomba</label>
                        <select name="lomba" class="form-control">
                          <?php
                            foreach ($lomba as $key) {
                              echo "<option value='$key->lomba_id'>$key->nama_lomba</option>";
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kategori Lomba</label>
                        <input type="text" name="namakatlomba" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori Lomba">
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>