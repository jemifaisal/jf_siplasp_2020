                <?php
                  foreach ($substandar as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_substandar'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Standar Nasional</label>
                        <select name="standar" class="form-control">
                          <?php
                            foreach ($standar as $key) {
                              if($row->standar_id == $key->standar_id){
                                echo "<option value='$key->standar_id' selected='selected'>$key->standar_kode - $key->nama_standar</option>";
                              }else{
                                echo "<option value='$key->standar_id'>$key->standar_kode - $key->nama_standar</option>";  
                              }
                              
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Kode Sub Standar Nasional</label>
                        <input type="text" name="kodesub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Sub Standar Nasional" value="<?php echo $row->substandar_kode; ?>" readonly="readonly">
                        <input type="hidden" name="subid" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Sub Standar Nasional" value="<?php echo $row->substandar_id; ?>" readonly="readonly">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sub Standar Nasional</label>
                        <input type="text" name="namasub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Sub Standar Nasional" value="<?php echo $row->namasub_standar; ?>">
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($row->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>