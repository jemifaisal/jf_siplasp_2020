<style type="text/css">
  .tabel{text-align: left;font-size: 11px;}
  .tabel th{text-align: center;}
  .tabel td{padding: 5px;}
  .kanan{text-align: right;}
  .tengah{text-align: center;}
  table{border-collapse: collapse;}

</style>
<?php
  $tahun = $tahunsekolah;//$this->session->userdata('token_tahun');
  $sekolahid = $sekolahid;//$this->session->userdata('token_sekolah_id');
  function rpbold($a){
    $h = "<b>".number_format($a,0,',','.')."</b>";
    return $h;
  }
  function rp($a){
    $h = number_format($a,0,',','.');
    return $h;
  }

  $sql12 = $this->db->query("SELECT * FROM sekolah s left join penerima_bos pb on s.npsn=pb.npsn where s.sekolah_id='$sekolahid' and pb.tahun='$tahun' LIMIT 1");
  if($sql12->num_rows() >0){
    foreach ($sql12->result() as $row12) {
      if($row12->jenjang == "SMA"){
        $cek_dasar = $this->db->query("SELECT * FROM dasar_dana_bos where tahun='$tahun' and jenjang_sekolah='$row12->jenjang'")->row_array();
        $tot = ($row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii) * $cek_dasar['dasar_dana'];
        $total = number_format($tot,0,',','.');
        $jumlahpd = $row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii;
        $tw1 = 30 * $tot / 100;
        $tw2 = 40 * $tot / 100;
        $tw3 = 30 * $tot / 100;
        $tw4 = 0;
        $sisauang = $row12->sisa_uang;
        $triwulan = $row12->triwulan;
      }elseif($row12->jenjang == "SMK"){
        $cek_dasar = $this->db->query("SELECT * FROM dasar_dana_bos where tahun='$tahun' and jenjang_sekolah='$row12->jenjang'")->row_array();
        $tot = ($row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii) * $cek_dasar['dasar_dana'];
        $total = number_format($tot,0,',','.');
        $jumlahpd = $row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii;
        $tw1 = 30 * $tot / 100;
        $tw2 = 40 * $tot / 100;
        $tw3 = 30 * $tot / 100;
        $tw4 = 0;
        $sisauang = $row12->sisa_uang;
        $triwulan = $row12->triwulan;
        
      }else if($row12->jenjang == "SDLB"){
        $cek_dasar = $this->db->query("SELECT * FROM dasar_dana_bos where tahun='$tahun' and jenjang_sekolah='$row12->jenjang'")->row_array();
        $siswa = ($row12->kelas_i + $row12->kelas_ii + $row12->kelas_iii + $row12->kelas_iv + $row12->kelas_v + $row12->kelas_vi + $row12->kelas_vii + $row12->kelas_viii + $row12->kelas_ix + $row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii);
        if($siswa < 60){
          $tot = 120000000;
        }else{
        $cek_dasar = $this->db->query("SELECT * FROM dasar_dana_bos where tahun='$tahun' and jenjang_sekolah='$row12->jenjang'")->row_array();
          $tot = $siswa * $cek_dasar['dasar_dana'];
        }
        $total = number_format($tot,0,',','.');
        $jumlahpd = $row12->kelas_i + $row12->kelas_ii + $row12->kelas_iii + $row12->kelas_iv + $row12->kelas_v + $row12->kelas_vi + $row12->kelas_vii + $row12->kelas_viii + $row12->kelas_ix + $row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii;
        $tw1 = 30 * $tot / 100;
        $tw2 = 40 * $tot / 100;
        $tw3 = 30 * $tot / 100;
        $tw4 = 0;
        $sisauang = $row12->sisa_uang;
        $triwulan = $row12->triwulan;
      }else if($row12->jenjang == "SMPLB"){
        $cek_dasar = $this->db->query("SELECT * FROM dasar_dana_bos where tahun='$tahun' and jenjang_sekolah='$row12->jenjang'")->row_array();
        $siswa = ($row12->kelas_i + $row12->kelas_ii + $row12->kelas_iii + $row12->kelas_iv + $row12->kelas_v + $row12->kelas_vi + $row12->kelas_vii + $row12->kelas_viii + $row12->kelas_ix + $row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii);
        if($siswa < 60){
          $tot = 120000000;
        }else{
          $tot = $siswa * $cek_dasar['dasar_dana'];
        }
        $total = number_format($tot,0,',','.');
        $jumlahpd = $row12->kelas_i + $row12->kelas_ii + $row12->kelas_iii + $row12->kelas_iv + $row12->kelas_v + $row12->kelas_vi + $row12->kelas_vii + $row12->kelas_viii + $row12->kelas_ix + $row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii;
        $tw1 = 30 * $tot / 100;
        $tw2 = 40 * $tot / 100;
        $tw3 = 30 * $tot / 100;
        $tw4 = 0;
        $sisauang = $row12->sisa_uang;
        $triwulan = $row12->triwulan;

      }else{
        $cek_dasar = $this->db->query("SELECT * FROM dasar_dana_bos where tahun='$tahun' and jenjang_sekolah='SLB'")->row_array();
        $sd = ($row12->kelas_i + $row12->kelas_ii + $row12->kelas_iii + $row12->kelas_iv + $row12->kelas_v + $row12->kelas_vi) ;
        $smp = ($row12->kelas_vii + $row12->kelas_viii + $row12->kelas_ix) ;
        $sma = ($row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii) ;
        $siswa = ($sd + $smp + $sma);
        if($siswa < 60){
          $tot = 120000000;
        }else{
          $tot = $siswa * $cek_dasar['dasar_dana'];
        }
        $total = number_format($tot,0,',','.');
        $jumlahpd = $row12->kelas_i + $row12->kelas_ii + $row12->kelas_iii + $row12->kelas_iv + $row12->kelas_v + $row12->kelas_vi + $row12->kelas_vii + $row12->kelas_viii + $row12->kelas_ix + $row12->kelas_x + $row12->kelas_xi + $row12->kelas_xii;
        $tw1 = 30 * $tot / 100;
        $tw2 = 40 * $tot / 100;
        $tw3 = 30 * $tot / 100;
        $tw4 = 0;
        $sisauang = $row12->sisa_uang;
        $triwulan = $row12->triwulan;
      }     
    }
  }else{
    $jumlahpd = "0"; $total="0";
    $tw1 = 0;$tw2 = 0;$tw3 =0;$tw4 = 0;$tot=0;
    $sisauang = 0;
    $triwulan = 0;
  }

  if($triwulan ==0){
    $stw1 = 0; $stw2 = 0; $stw3 = 0; $stw4 = 0;
  }else{
    if($triwulan ==1){
      $stw1 = $sisauang; $stw2 = 0; $stw3 = 0; $stw4 = 0;
    }elseif($triwulan ==2){
      $stw1 = 0; $stw2 = $sisauang; $stw3 = 0; $stw4 = 0;
    }elseif($triwulan ==3){
      $stw1 = 0; $stw2 = 0; $stw3 = $sisauang; $stw4 = 0;
    }elseif($triwulan ==4){
      $stw1 = 0; $stw2 = 0; $stw3 = 0; $stw4 = $sisauang;
    }
  }

  $sql11 = $this->db->query("SELECT * FROM sekolah s join kec on s.kd_kec=kec.kd_kec join kab on kec.kd_kab=kab.kd_kab where s.sekolah_id='$sekolahid'")->result();
  foreach ($sql11 as $row11) {
    
  }

  $sql = $this->db->query("select * from jenis_belanja")->result();
  foreach ($sql as $row) {
    
  }
?>

<div class="box box-primary">
  <div class="box-header with-border">
  <H4>Detail RKA Sekolah</H4><h4>
  <a href="<?php echo base_url('webadmin/excelrkas/'.$sekolahid.'/'.$tahunsekolah); ?>"><i class="fa fa-file-excel-o btn btn-success" aria-hidden="true"> Excel</i></a>
  <a href="<?php echo base_url('webadmin/pdfrkas/'.$sekolahid.'/'.$tahunsekolah); ?>"><i class="fa fa-file-pdf-o btn btn-warning" aria-hidden="true"> PDF</i></a>
  </h4>
<table class="tabel" border="0" width="100%">
<tr>
  <td colspan="11" align="center">
    <b>RENCANA KEGIATAN DAN ANGGARAN SEKOLAH (RKAS)<BR>
    TAHUN <?php echo $tahun; ?></b>

  </td>
</tr>
<tr>
  <td colspan="3" width="20%">Nama Sekolah</td>
  <td colspan="8">: <?php echo $row11->nama_sp; ?></td>
</tr>
<tr>
  <td colspan="3">Desa / Kelurahan</td>
  <td colspan="8">: <?php echo $row11->desa; ?></td>
</tr>
<tr>
  <td colspan="3">Kabupaten / Kota</td>
  <td colspan="8">: <?php echo $row11->nama_kab; ?></td>
</tr>
<tr>
  <td colspan="3">Provinsi</td>
  <td colspan="8">: Riau</td>
</tr>
<tr>
  <td colspan="3">Sumber Dana</td>
  <td colspan="8">: BOS Pusat</td>
</tr>
<tr>
  <td colspan="3">Jumlah Siswa</td>
  <td colspan="8">: <?php echo $jumlahpd; ?> Orang</td>
</tr>
</table>

<table border="1" class="tabel" width="100%">
<tr>
  <th rowspan="2">NO</th>
  <th rowspan="2" align="center">KODE REKENING</th>
  <th rowspan="2">KODE PROGRAM DAN<br> KEGIATAN</th>
  <th rowspan="2">JENIS PENGGUNAAN /<br> PEMBELANJAAN</th>
  <th rowspan="2">VOLUME</th>
  <th rowspan="2">SATUAN</th>
  <th rowspan="2">HARGA SATUAN</th>
  <th rowspan="2">JUMLAH</th>
  <th colspan="3">TAHAP</th>
</tr>
<tr>
  <th>I</th>
  <th>II</th>
  <th>III</th>
  <!-- <th>IV</th> -->
</tr>
<tr>
  <td class="tengah">1</td>
  <td class="tengah">2</td>
  <td class="tengah">3</td>
  <td class="tengah">4</td>
  <td class="tengah">5</td>
  <td class="tengah">6</td>
  <td class="tengah">7</td>
  <td class="tengah">8</td>
  <td class="tengah">9</td>
  <td class="tengah">10</td>
  <td class="tengah">11</td>
  <!-- <td class="tengah">12</td> -->
</tr>
<tr style="background-color:#f5e2b0;">
  <td></td>
  <td></td>
  <td></td>
  <td><b>PENDAPATAN</b></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"><?php echo rpbold($tot); ?></td>
  <td class="kanan"><?php echo rpbold($tw1); ?></td>
  <td class="kanan"><?php echo rpbold($tw2); ?></td>
  <td class="kanan"><?php echo rpbold($tw3); ?></td>
  <!-- <td class="kanan"><?php echo rpbold($tw4); ?></td> -->
</tr>
<tr style="background-color:#f5e2F7;">
  <td></td>
  <td></td>
  <td></td>
  <td><b>SISA UANG TAHUN SEBELUMNYA</b></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"><?php echo rpbold($sisauang); ?></td>
  <td class="kanan"><?php echo rpbold($stw1); ?></td>
  <td class="kanan"><?php echo rpbold($stw2); ?></td>
  <td class="kanan"><?php echo rpbold($stw3); ?></td>
  <!-- <td class="kanan"><?php echo rpbold($stw4); ?></td> -->
</tr>
<tr style="background-color:#fFA2F7;">
  <td></td>
  <td></td>
  <td></td>
  <td><b>TOTAL PENDAPATAN</b></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"><?php echo rpbold($tot + $sisauang); ?></td>
  <td class="kanan"><?php echo rpbold($tw1 + $stw1); ?></td>
  <td class="kanan"><?php echo rpbold($tw2 + $stw2); ?></td>
  <td class="kanan"><?php echo rpbold($tw3 + $stw3); ?></td>
  <!-- <td class="kanan"><?php echo rpbold($tw4 + $stw4); ?></td> -->
</tr>
<?php
  $sql12 = $this->db->query("select sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join rincian_header rh on rb.rincian_id=rh.rincian_id where rb.soft_delete='0' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql12 as $row12) {
                        $ttw1 = $row12->total+$row12->total2;
                        $ttw2 = $row12->total3+$row12->total4;
                        $ttw3 = $row12->total5+$row12->total6;
                        $ttw4 = $row12->total7+$row12->total8;
                      }

                      $sball = $ttw1+$ttw2+$ttw3+$ttw4; 

//total barang dan jasa
  $sql20 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id left join sub_standar ss on sp.substandar_id=ss.substandar_id left join standar_nasional sn on ss.standar_id=sn.standar_id where rb.belanja_id in(1) and rb.soft_delete='0' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql20 as $row20) {
                        $bjsatu = $row20->total+$row20->total2;
                        $bjdua = $row20->total3+$row20->total4;
                        $bjtiga = $row20->total5+$row20->total6;
                        $bjempat = $row20->total7+$row20->total8;
                      }

                      $bjsubtotal = $bjsatu+$bjdua+$bjtiga+$bjempat;

//total belanja modal
  $sql21 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id left join sub_standar ss on sp.substandar_id=ss.substandar_id left join standar_nasional sn on ss.standar_id=sn.standar_id where rb.belanja_id in(2,3) and rb.soft_delete='0' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql21 as $row21) {
                        $msatu = $row21->total+$row21->total2;
                        $mdua = $row21->total3+$row21->total4;
                        $mtiga = $row21->total5+$row21->total6;
                        $mempat = $row21->total7+$row21->total8;
                      }

                      $msubtotal = $msatu+$mdua+$mtiga+$mempat;
?>
<tr style="background-color:#b6f5b0;">
  <td></td>
  <td></td>
  <td></td>
  <td><b>SUB TOTAL (Belanja Barang Jasa + Belanja Modal)</b></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"><?php echo rpbold($sball); ?></td>
  <td class="kanan"><?php echo rpbold($ttw1); ?></td>
  <td class="kanan"><?php echo rpbold($ttw2); ?></td>
  <td class="kanan"><?php echo rpbold($ttw3); ?></td>
  <!-- <td class="kanan"><?php echo rpbold($ttw4); ?></td> -->
</tr>

<tr style="background-color:#f2fc82;">
  <td></td>
  <td></td>
  <td></td>
  <td><b>TOTAL BELANJA BARANG DAN JASA</b></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"><?php echo rpbold($bjsubtotal); ?></td>
  <td class="kanan"><?php echo rpbold($bjsatu); ?></td>
  <td class="kanan"><?php echo rpbold($bjdua); ?></td>
  <td class="kanan"><?php echo rpbold($bjtiga); ?></td>
  <!-- <td class="kanan"><?php echo rpbold($bjempat); ?></td> -->
</tr>

<tr style="background-color:#acb1f8;">
  <td></td>
  <td></td>
  <td></td>
  <td><b>TOTAL BELANJA MODAL (Peralatan dan Mesin + Aset Tetap Lainnya)</b></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"></td>
  <td class="kanan"><?php echo rpbold($msubtotal); ?></td>
  <td class="kanan"><?php echo rpbold($msatu); ?></td>
  <td class="kanan"><?php echo rpbold($mdua); ?></td>
  <td class="kanan"><?php echo rpbold($mtiga); ?></td>
  <!-- <td class="kanan"><?php echo rpbold($mempat); ?></td> -->
</tr>

<?php
  foreach($sql as $row){
    $sql10 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id left join sub_standar ss on sp.substandar_id=ss.substandar_id left join standar_nasional sn on ss.standar_id=sn.standar_id where rb.belanja_id='$row->belanja_id' and rb.soft_delete='0' and rb.belanja_id='$row->belanja_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql10 as $row10) {
                        $satu3 = $row10->total+$row10->total2;
                        $dua3 = $row10->total3+$row10->total4;
                        $tiga3 = $row10->total5+$row10->total6;
                        $empat3 = $row10->total7+$row10->total8;
                      }

                      $subtotal6 = $satu3+$dua3+$tiga3+$empat3;
    echo "<tr>
      <td></td>
      <td><b>$row->rekening_belanja</b></td>
      <td></td>
      <td><b>$row->nama_rekening</b></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>";

    echo "<tr>
      <td></td>
      <td><b>$row->subrekening_belanja</b></td>
      <td></td>
      <td><b>$row->nama_subrekening</b></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>";

    echo "<tr>
      <td></td>
      <td><b>$row->subrekening_belanja.01</b></td>
      <td></td>
      <td><b>$row->nama_subrekening ".$row11->nama_sp."</b></td>
      <td></td>
      <td></td>
      <td></td>
      <td class='kanan'>".rpbold($subtotal6)."</td>
      <td class='kanan'>".rpbold($satu3)."</td>
      <td class='kanan'>".rpbold($dua3)."</td>
      <td class='kanan'>".rpbold($tiga3)."</td>

    </tr>";
    //standar nasional
    $sql1 = $this->db->query("select * from rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id join sub_standar ss on sp.substandar_id=ss.substandar_id join  standar_nasional sn on ss.standar_id=sn.standar_id where rb.belanja_id='$row->belanja_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid' group by sn.standar_id")->result();
    foreach ($sql1 as $row1) {
      $sql9 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id left join sub_standar ss on sp.substandar_id=ss.substandar_id left join standar_nasional sn on ss.standar_id=sn.standar_id where sn.standar_id='$row1->standar_id' and rb.soft_delete='0' and rb.belanja_id='$row->belanja_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql9 as $row9) {
                        $satu2 = $row9->total+$row9->total2;
                        $dua2 = $row9->total3+$row9->total4;
                        $tiga2 = $row9->total5+$row9->total6;
                        $empat2 = $row9->total7+$row9->total8;
                      }

                      $subtotal5 = $satu2+$dua2+$tiga2+$empat2;
      echo "<tr>
        <td></td>
        <td></td>
        <td><b>$row1->standar_kode</b></td>
        <td><b>$row1->nama_standar</b></td>
        <td></td>
        <td></td>
        <td></td>
        <td class='kanan'>".rpbold($subtotal5)."</td>
        <td class='kanan'>".rpbold($satu2)."</td>
        <td class='kanan'>".rpbold($dua2)."</td>
        <td class='kanan'>".rpbold($tiga2)."</td>

      </tr>";

      //sub standar
      $sql2 = $this->db->query("select * from rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id join sub_standar ss on sp.substandar_id=ss.substandar_id where ss.standar_id='$row1->standar_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid' and rb.belanja_id='$row->belanja_id' group by ss.substandar_id")->result();
      foreach ($sql2 as $row2) {
        $sql8 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id left join sub_standar ss on sp.substandar_id=ss.substandar_id where ss.substandar_id='$row2->substandar_id' and rb.soft_delete='0' and rb.belanja_id='$row->belanja_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql8 as $row8) {
                        $satu1 = $row8->total+$row8->total2;
                        $dua1 = $row8->total3+$row8->total4;
                        $tiga1 = $row8->total5+$row8->total6;
                        $empat1 = $row8->total7+$row8->total8;
                      }

                      $subtotal4 = $satu1+$dua1+$tiga1+$empat1;

        echo "<tr>
          <td></td>
          <td></td>
          <td><b>$row2->substandar_kode</b></td>
          <td><b>$row2->namasub_standar</b></td>
          <td></td>
          <td></td>
          <td></td>
          <td class='kanan'>".rpbold($subtotal4)."</td>
          <td class='kanan'>".rpbold($satu1)."</td>
          <td class='kanan'>".rpbold($dua1)."</td>
          <td class='kanan'>".rpbold($tiga1)."</td>

        </tr>";

        //sub program
        $sql3 = $this->db->query("select *, rb.komponen_kode as kom_kode from rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id where sp.substandar_id='$row2->substandar_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid' and rb.belanja_id='$row->belanja_id' group by rb.program_id,rb.komponen_kode")->result();

        //SELECT *, rb.komponen_kode as kom_kode FROM rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id WHERE sp.substandar_id='$row2->substandar_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid' and rb.belanja_id='$belanja_id'  AND rb.soft_delete='0' group by rb.program_id,rb.komponen_kode")->result()
        


        foreach ($sql3 as $row3) {
          $sql7 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id where rb.program_id='$row3->program_id' and rb.komponen_kode='$row3->kom_kode' and rb.soft_delete='0' and rb.belanja_id='$row->belanja_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql7 as $row7) {
                        $satu = $row7->total+$row7->total2;
                        $dua = $row7->total3+$row7->total4;
                        $tiga = $row7->total5+$row7->total6;
                        $empat = $row7->total7+$row7->total8;
                      }

                      $subtotal1 = $satu+$dua+$tiga+$empat;
          echo "<tr>
            <td></td>
            <td></td>
            <td><b>$row3->program_kode.$row3->kom_kode</b></td>
            <td><b>$row3->nama_program</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td class='kanan'>".rpbold($subtotal1)."</td>
            <td class='kanan'>".rpbold($satu)."</td>
            <td class='kanan'>".rpbold($dua)."</td>
            <td class='kanan'>".rpbold($tiga)."</td>

          </tr>";
            //rincian belanja
            $sql4 = $this->db->query("select * from rincian_belanja where program_id='$row3->program_id' and komponen_kode='$row3->kom_kode' and belanja_id='$row->belanja_id' and tahun='$tahun' and sekolah_id='$sekolahid'")->result();
            foreach ($sql4 as $row4) {
              if($row4->status_rincian ==1){
                $jr = "<b>".$row4->detail_rincian."</b>";
                $twi = rpbold($row4->tw1);$twii = rpbold($row4->tw2);$twiii = rpbold($row4->tw3);$twiv = rpbold($row4->tw4);


                $sql6 = $this->db->query("SELECT *, SUM(htw1) as sumhtw1,SUM(htw2) as sumhtw2,SUM(htw3) as sumhtw3,SUM(htw4) as sumhtw4 FROM rincian_header where rincian_id='$row4->rincian_id'  AND soft_delete='0'")->result();
                          foreach ($sql6 as $row6) {
                            $isitw1 = "<b>".rpbold($row6->sumhtw1)."</b>"; $isitw2="<b>".rpbold($row6->sumhtw2)."</b>"; $isitw3="<b>".rpbold($row6->sumhtw3)."</b>"; $isitw4="<b>".rpbold($row6->sumhtw4)."</b>";
                          }

                          $subtotal2 = rpbold($row6->sumhtw1 + $row6->sumhtw2 + $row6->sumhtw3 +$row6->sumhtw4);

                          $vol = ""; $sat =""; $hargasat="";

              }else{
                $jr = "- ".$row4->detail_rincian;
                $twi = rp($row4->tw1);$twii = rp($row4->tw2);$twiii = rp($row4->tw3);$twiv = rp($row4->tw4);
                $isitw1 = rp($row4->tw1); $isitw2=rp($row4->tw2); $isitw3=rp($row4->tw3); $isitw4=rp($row4->tw4);

                $subtotal2 = rp($row4->tw1 + $row4->tw2 + $row4->tw3 +$row4->tw4);
                $vol = $row4->volume; $sat =$row4->satuan; $hargasat=rp($row4->harga_satuan);
              }
              echo "<tr>
                <td></td>
                <td></td>
                <td></td>
                <td>$jr</td>
                <td class='tengah'>$vol</td>
                <td class='tengah'>$sat</td>
                <td class='kanan'>$hargasat</td>
                <td class='kanan'>$subtotal2</td>
                <td class='kanan'>$isitw1</td>
                <td class='kanan'>$isitw2</td>
                <td class='kanan'>$isitw3</td>

              </tr>";

              //rincian_header
              if($row4->status_rincian ==1){
                $sql5 = $this->db->query("SELECT * FROM rincian_header where rincian_id='$row4->rincian_id'  AND soft_delete='0'")->result();
                foreach ($sql5 as $row5) {
                  $subtotal3 = rp($row5->htw1 +$row5->htw2 +$row5->htw3 +$row5->htw4);
                  echo "<tr>
                  <td></td>
                    <td></td>
                    <td></td>
                    <td>-- $row5->header_rincian</td>
                    <td class='tengah'>$row5->volume_hr</td>
                    <td class='tengah'>$row5->satuan_hr</td>
                    <td class='kanan'>".rp($row5->harga_satuan_hr)."</td>
                    <td class='kanan'>$subtotal3</td>
                    <td class='kanan'>".rp($row5->htw1)."</td>
                    <td class='kanan'>".rp($row5->htw2)."</td>
                    <td class='kanan'>".rp($row5->htw3)."</td>

                  </tr>";  
                }

              }


            }
        } 

      }

    }
    
  }
?>
<?php
  $this->db->where('sekolah_id',$sekolahid);
  $this->db->where('soft_delete',0);
  $ambil = $this->db->get('ttd_pejabat');
  if($ambil->num_rows() > 0){
    foreach ($ambil->result() as $ttd) {
      $komite_nama = $ttd->nama_komite_sekolah; $komite_nip = $ttd->nip_komite_sekolah;
      $kepsek_nama = $ttd->nama_kepsek; $kepsek_nip = $ttd->nip_kepsek;
      $bendahara_nama = $ttd->nama_bendahara_bos; $bendahara_nip = $ttd->nip_bendahara_bos;
    }  
  }else{
      $komite_nama = "......................"; $komite_nip = "......................";
      $kepsek_nama = "......................"; $kepsek_nip = "......................";
      $bendahara_nama = "......................"; $bendahara_nip = "......................";
  }

  $bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
        );
  
?>
<tr>
    <td colspan="4">
      <BR>
      Mengetahui, <br>
      Ketua Komite Sekolah
      <BR>
      <BR>
      <BR>
      <BR>
      <BR>
      <BR>
      <B><?php echo $komite_nama; ?></B><BR>
      NIP. <?php echo $komite_nip;  ?>
    </td>
    <td colspan="3">
      <BR>
      Menyetujui, <br>
      Kepala <?php echo $row11->nama_sp; ?>
      <BR>
      <BR>
      <BR>
      <BR>
      <BR>
      <BR>
      <B><?php echo $kepsek_nama; ?></B><BR>
      NIP. <?php echo $kepsek_nip; ?>
    </td>
    <td colspan="4">
      <BR>
      Pekanbaru, <?php  echo date("d")." ". $bulan[date('m')]." ". date("Y"); ?><br>
      Bendahara Dana BOS
      <BR>
      <BR>
      <BR>
      <BR>
      <BR>
      <BR>
      <B><?php echo $bendahara_nama; ?></B><BR>
      NIP. <?php echo $bendahara_nip; ?>
    </td>
    
  </tr>
</table>
<BR>
</div>
</div>