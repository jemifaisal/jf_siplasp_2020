<?php
  $id = $this->input->get('id');
  $tahun = $this->input->get('tahun');
  $url = $_SERVER['REQUEST_URI'];
?>
<div class="box">

  <div class="box-header with-border">
  <?php if(isset($tahun)){ ?>
    <a href="#" onclick="back()" class="btn btn-warning"><i class="fa fa-chevron-left"></i></a>
  <?php } ?>
  <h3 class="box-title">Integrasi Penggunaan Dana BOS</h3>
  </div>
  <div class="box-body">
      <div class="box-body">
        <?php
          if(isset($tahun)){
            //tampilkan kabupaten berdasarkan tahun
            // echo "<p><input type='submit' class='btn btn-warning' value='&laquo; Kembali' onclick=\"back()\"></p>";
            if(isset($id)){
              $bos = $this->webadmin_model->penerima_bos_per_kab($id,$tahun);
              // echo "<h3 class='lead'>Tahudn: $tahun</h3>";
              echo "<div class='table-responsive'><table id='pres' class='table table-bordered table-striped table-responsive'>
                <thead>
                  <tr> 
                    <th colspan='4'>Status Sync</th>
                    <th rowspan='2'>NPSN</th>
                    <th rowspan='2'>Nama Sekolah</th>
                    <th rowspan='2'>Kabupaten</th>
                    <th rowspan='2'>Jenjang</th>
                    <th rowspan='2'>Jumlah Siswa</th>
                    <th rowspan='2'>Total BOS</th>
                  </tr>
                  <tr>
                    <td>TW 1</td>
                    <td>TW 2</td>
                    <td>TW 3</td>
                    <td>TW 4</td>
                  </tr>
                </thead>
                <tbody>";
                $no=0;
                foreach ($bos as $row) {
                  $no++;
                  $jumlah_siswa = $row->kelas_i+$row->kelas_ii+$row->kelas_iii+$row->kelas_iv+$row->kelas_v+$row->kelas_vi+$row->kelas_vii+$row->kelas_viii+$row->kelas_ix+$row->kelas_x+$row->kelas_xi+$row->kelas_xii;
                  $tw1 = $this->rkas_model->get_integrasi_tw(1,$row->tahun,$row->sekolah_id);
                  $tw2 = $this->rkas_model->get_integrasi_tw(2,$row->tahun,$row->sekolah_id);
                  $tw3 = $this->rkas_model->get_integrasi_tw(3,$row->tahun,$row->sekolah_id);
                  $tw4 = $this->rkas_model->get_integrasi_tw(4,$row->tahun,$row->sekolah_id);
                  if($tw1 === NULL){
                    $btntw1 = 'fa-times-circle text-danger';
                  }else{
                    $btntw1 = 'fa-check-circle text-success';
                  }

                  if($tw2 === NULL){
                    $btntw2 = 'fa-times-circle text-danger';
                  }else{
                    $btntw2 = 'fa-check-circle text-success';
                  }

                  if($tw3 === NULL){
                    $btntw3 = 'fa-times-circle text-danger';
                  }else{
                    $btntw3 = 'fa-check-circle text-success';
                  }

                  if($tw4 === NULL){
                    $btntw4 = 'fa-times-circle text-danger';
                  }else{
                    $btntw4 = 'fa-check-circle text-success';
                  }
                  echo "<tr>
                    <td align='center'>
                      <i class='fa $btntw1'></i>
                      <a href='#' id='add' data-id='$row->sekolah_id,$row->tahun,1' class='btn btn-primary'><i class='fa fa-refresh'></i></a>
                    </td>
                    <td align='center'>
                      <i class='fa $btntw2'></i>
                      <a href='#' id='add' data-id='$row->sekolah_id,$row->tahun,2' class='btn btn-primary'><i class='fa fa-refresh'></i></a>
                    </td>
                    <td align='center'>
                      <i class='fa $btntw3'></i>
                      <a href='#' id='add' data-id='$row->sekolah_id,$row->tahun,3' class='btn btn-primary'><i class='fa fa-refresh'></i></a>
                    </td>
                    <td align='center'>
                      <i class='fa $btntw4'></i>
                      <a href='#' id='add' data-id='$row->sekolah_id,$row->tahun,4' class='btn btn-primary'><i class='fa fa-refresh'></i></a>
                    </td>
                    <td><a href='".base_url('webadmin/laporandetail/'.$row->sekolah_id.'/'.$row->tahun)."'>$row->npsn</a></td>
                    <td>$row->nama_sp</td>
                    <td>$row->nama_kab</td>
                    <td>$row->jenjang - $row->status_sekolah</td>
                    <td>".number_format($row->jumlah_siswa,0)."</td>
                    <td>".number_format($row->total_bos,0)."</td>
                  </tr>";
                }
                
              echo "</tbody>
              </table></div>";


            }else{
              // echo "<h3 class='box-title'>Tahun: $tahun</h3>";
              echo "<table id='pres' class='table table-bordered table-striped'>
                    <thead>
                      <tr>
                        <th rowspan='2'>Nama Kabupaten</th>
                        <th colspan='6'>Jumlah Sekolah</th>
                      </tr>
                      <tr>
                        <td>SLB Negeri</td>
                        <td>SLB Swasta</td>
                        <td>SMA Negeri</td>
                        <td>SMA Swasta</td>
                        <td>SMK Negeri</td>
                        <td>SMK Swasta</td>
                      </tr>
              </thead><tbody>";
              foreach($get_kab as $get_kabs){
                echo "<tr>
                  <td><a href='$url&id=$get_kabs->kd_kab' class='btn btn-info btn-xs'><i class='fa fa-eye'></i> Detail</a> $get_kabs->nama_kab</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SLB','Negeri',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SLB','Swasta',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMA','Negeri',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMA','Swasta',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMK','Negeri',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMK','Swasta',$tahun)."</td>
                </tr>";
              }
              echo "</tbody></table>";
            }
            
          }else{
            //tampilkan pilihan tahun
            foreach($get_tahun as $get_tahuns){
              echo "<div class='col-md-4 col-sm-8 col-xs-12'>
                    <div class='info-box'>
                      <span class='info-box-icon bg-blue'><i class='fa fa-refresh'></i></span>
        
                      <div class='info-box-content'>
                        <span class='info-box-text'>Laporan BOS</span>
                        <span class='info-box-number'>$get_tahuns->tahun</span>
                        <span class='info-box-text'><a href='$url?tahun=$get_tahuns->tahun' class='btn btn-primary btn-xs'>Detail &raquo;</a></span>
                      </div>
                    </div>
              </div>";
            }

          }
        ?>
        
      </div>

  </div>

</div>

<div id="addsisauang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Form Integrasi Penggunaan Dana BOS</h4>
      </div>
      <div class="modal-body">
        <div class="box">
          
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  function back(){
		window.history.back();
	}     
  $(document).ready(function() {
    $("#pres").DataTable({
      responsive: true
    });    

    $(document).on('click','#add',function(e){
      e.preventDefault();
      $("#addsisauang").modal('show');
      $.post("<?php echo base_url('webadmin/form_integrasi'); ?>",
          {id:$(this).attr('data-id')},
          function(html){
              $(".modal-body").html(html);
          }   
      );
    });

  });
</script>