<p>
    <span class="lead">
        <span class="label label-primary">Tahun: <?php echo $tahun; ?></span>
        <span class="label label-primary">Triwulan: <?php echo $tw; ?></span>
    </span>
</p>
<p>
    <span class="lead">
        <span class="label label-info"><?php echo $profil['npsn']; ?></span>
        <span class="label label-info"><?php echo $profil['nama_sp']; ?></span>
        <span class="label label-info"><?php echo $profil['nama_kab']; ?></span>
    </span>
</p>
<p class="required">
    <?php
        echo "Sinkron pertama kali: <b>".$integrasi['created_at']."</b>, diupdate pada: <b>".$integrasi['updated_at']."</b>";
    ?>
</p>
<hr>
<form method="post" id="form_integrasi">
    <input type="hidden" name="tw" value="<?php echo $tw; ?>">
    <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
    <input type="hidden" name="sekolahid" value="<?php echo $sekolahid; ?>">
    <p><button type="submit" class="btn btn-success"><span id="btn_caption"><i class='fa fa-refresh'></i> Kirim</span></button></p>
    <div class="required"><i>*jika data sudah benar, silahkan klik tombol Kirim untuk Integrasi data</i></div>
    <div class="table-responsive">
        <table class="table table-bordered table-responsive table-hover">
            <tr>
                <th>Kode</th>
                <th>Nama Komponen</th>
                <th>Total</th>
            </tr>
            <?php
                $komponen_total = array();
                foreach($komponen_biaya as $komponen_biayas){
                    echo "<tr>
                        <td>$komponen_biayas->komponen_kode</td>
                        <td>$komponen_biayas->nama_komponen</td>
                        <td>".number_format($total_komponen[$komponen_biayas->komponen_kode],0)."</td>
                    </tr>";
                    $komponen_total[] = $total_komponen[$komponen_biayas->komponen_kode];
                }
            ?>
            <tr>
                <th colspan="2">Total</th>
                <th><?php echo number_format(array_sum($total_komponen),0); ?></th>
            </tr>
        </table>
    </div>
</form>

<script>
    function back(){
		window.history.back();
	}    

    $(document).ready(function() {
        $('#form_integrasi').on('submit', function(e){
            if(!e.isDefaultPrevented()){
                $("#btn_caption").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
                var inputData = new FormData($(this)[0]);
                var url = "<?php echo base_url('webadmin/proses_integrasi'); ?>";
                $.ajax({
                    url: url,
                    type: "post",
                    data: inputData,
                    dataType: "JSON",
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data){
                        $("#btn_caption").html("<i class='fa fa-refresh'></i> Proses");
                        if(data.status === true){
                            swal({
                                title: 'Sukses',
                                text: 'Data berhasil disimpan',
                                type: 'success'
                            }).then(function(){
                                location.reload();
                            }).catch(swal.noop);
                        }else{
                            swal({
                                title: 'Maaf...',
                                text: 'Terjadi kesalahan. Silahkan coba lagi..!',
                                type: 'error',
                                timer: 1500
                            }).catch(swal.noop);
                        }
                    },
                    error: function(){
                        swal({
                            title: 'Maaf...',
                            text: 'Terjadi kesalahan. Silahkan coba lagi..!',
                            type: 'error',
                            timer: 1500
                        }).catch(swal.noop);
                    }
                    
                });

                return false;
            }
        });
    });
</script>