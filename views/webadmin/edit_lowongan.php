                <?php foreach ($lowongan as $row) {
                  # code...
                } ?>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/webadmin/ckeditor/ckeditor.js"></script>
                <div class="box box-success">
                  <div class="box-header">
                    <h3 class="box-title">Ubah Data Lowongan Kerja</h3>
                  </div>
                  <form role="form" method="post" action="<?php echo base_url('webadmin/edit_lowongan/'.$row->lowongan_id); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Judul Lowongan Kerja</label>
                        <input type="text" name="judul" required="required" class="form-control" value="<?php echo $row->judul_lowongan; ?>" id="exampleInputEmail1" placeholder="Judul Lowongan">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Perusahaan</label>
                        <input type="text" name="namapt" required="required" class="form-control" value="<?php echo $row->nama_pt; ?>" id="exampleInputEmail1" placeholder="Nama Perusahaan">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Isi Lowongan</label>
                        <textarea class="form-control ckeditor" id="ckeditor" name="isi"><?php echo $row->isi_lowongan; ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kabupaten / Kota</label>
                        <select class="form-control select2" name="kabupaten[]" multiple="multiple" data-placeholder="Kabupaten / Kota" style="width: 100%;">
                          <?php
                            $k = $row->kd_kab;
                            $pecah = explode(',',$k);
                            foreach ($kabupaten as $keys) {
                              if(in_array($keys->kd_kab, $pecah)){$tempel= "selected='selected'";}else{$tempel='';}

                              echo "<option value='$keys->kd_kab' $tempel>".$keys->nama_kab."</option>";   
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Pendidikan yang Dibutuhkan</label>
                        <select class="form-control select2" name="pendidikan[]" multiple="multiple" data-placeholder="Pendidikan" style="width: 100%;">
                          <?php
                            $p = $row->pendidikan_id;
                            $pdd = explode(',',$p);
                            foreach ($pendidikan as $key) {
                              if(in_array($key->pendidikan_id, $pdd)){$tarok= "selected='selected'";}else{$tarok='';}
                              echo "<option value='$key->pendidikan_id' $tarok>$key->jenjang</option>";
                            }
                          ?>
                        </select>

                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Terbit</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="tglterbit" class="form-control pull-right" id="datepicker" value="<?php echo $row->tgl_terbit; ?>">
                        </div>
                      </div>
                      <div class="bootstrap-timepicker">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jam Terbit</label>
                        <div class="input-group">
                          <input type="text" class="form-control" name="jamterbit" id="timepicker" value="<?php echo $row->jam_terbit; ?>">

                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                        </div>
                      </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Akhir Lowongan</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="tglakhirlowongan" id="datepicker2" value="<?php echo $row->tgl_akhir_lowongan; ?>">
                        </div>
                      </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                  </form>
                </div>

                
