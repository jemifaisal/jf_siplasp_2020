                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_kecamatan'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kabupaten</label>
                        <select name="kab" class="form-control">
                          <?php
                            foreach ($kab as $row) {
                              echo "<option value='$row->kd_kab'>$row->nama_kab</option>";
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kecamatan</label>
                        <input type="text" name="namakec" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kecamatan">
                      </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>
