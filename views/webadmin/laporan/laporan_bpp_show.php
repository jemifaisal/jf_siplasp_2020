<table class="table table-bordered table-responsive">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Tanggal</th>
        <th rowspan="2">Kode Rekening</th>
        <th rowspan="2">No. Kode</th>
        <th rowspan="2">No. Bukti</th>
        <th rowspan="2">Uraian</th>
        <th colspan="4">Penerimaan (Debit)</th>
        <th rowspan="2">Pengeluaran<br> (Kredit)</th>
        <th rowspan="2">Saldo</th>
    </tr>
    <tr>
        <th>PPN</th>
        <th>PPh 21</th>
        <th>PPh 22</th>
        <th>PPh 23</th>
    </tr>
    <?php
        $no = 0;
        $saldo = 0;
        foreach($bku as $bkus){
            $no ++;
            if($bkus->program_kode == NULL){
                $dot = "";
            }else{
                $dot = ".";
            }
            echo "<tr>
                <td>$no</td>
                <td>".date("d M Y",strtotime($bkus->tanggal_transaksi))."</td>
                <td>$bkus->rekening_belanja</td>
                <td>$bkus->program_kode$dot$bkus->kode_komponen</td>
                <td>$bkus->kode_bukti $bkus->no_urut_bukti</td>
                <td>$bkus->uraian_bku</td>";
            if($bkus->kelompok == "D"){
                $param = '+';
                if($bkus->pajak_id == 1){
                    $ppn = $bkus->jumlah_transaksi_format;
                    $pph21 = "";
                    $pph22 = "";
                    $pph23 = "";
                }elseif($bkus->pajak_id == 2){
                    $ppn = "";
                    $pph21 = $bkus->jumlah_transaksi_format;
                    $pph22 = "";
                    $pph23 = "";
                }elseif($bkus->pajak_id == 3){
                    $ppn = "";
                    $pph21 = "";
                    $pph22 = $bkus->jumlah_transaksi_format;
                    $pph23 = "";
                }elseif($bkus->pajak_id == 4){
                    $ppn = "";
                    $pph21 = "";
                    $pph22 = "";
                    $pph23 = $bkus->jumlah_transaksi_format;
                }
                echo "<td class='success'>$ppn</td>
                    <td class='success'>$pph21</td>
                    <td class='success'>$pph22</td>
                    <td class='success'>$pph23</td>
                    <td class='warning'></td>";
                $saldo = $saldo + $bkus->jumlah_transaksi;
            }else{
                $param = '-';
                echo "<td class='success'></td>
                    <td class='success'></td>
                    <td class='success'></td>
                    <td class='success'></td>
                    <td class='warning'>$bkus->jumlah_transaksi_format</td>";
                $saldo = $saldo - $bkus->jumlah_transaksi;
            }

            
            echo "<td class='info'>".number_format($saldo,0)."</td></tr>";
        }
    ?>
</table>