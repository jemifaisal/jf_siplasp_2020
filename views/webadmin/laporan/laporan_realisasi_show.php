<table class="table table-bordered table-responsive">
    <tr>
        <th>Uraian</th>
        <th>Jumlah Anggaran (Rp.)</th>
        <th>Realisasi s/d <br>Triwulan Lalu (Rp.)</th>
        <th>Realisasi Triwulan ini (Rp.)</th>
        <th>Jumlah Realisasi s/d <br>Triwulan ini</th>
        <th>Selisih (Rp.)</th>
    </tr>
    <tr>
        <td>Penerimaan</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'],0); ?></span></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'] + $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw'] ,0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'] - ($jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'] + $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw']),0); ?></span></td>
    </tr>
    <tr>
        <td>Pengeluaran:</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'],0); ?></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right">
            <?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>a) Belanja Pegawai</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>b) Belanja Barang dan Jasa</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_jasa['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_jasa['total'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>c) Belanja Modal:</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_mesin['total'] + $total_belanja_aset['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format(($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'])) + ($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'])),0); ?></span></td>
    </tr>
    <tr>
        <td>1) Belanja Modal Peralatan dan Mesin</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_mesin['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>2) Belanja Modal Aset Tetap Lainnya</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_aset['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_jasa['total'] + $total_belanja_mesin['total'] + $total_belanja_aset['total'],0);  ?></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format(($total_belanja_jasa['total'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'])) + (($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'])) + ($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']))),0); ?></span></td>
    </tr>
</table>