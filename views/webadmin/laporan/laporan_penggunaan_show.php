<div class="table-responsive">
    <table class="table table-bordered table-responsive">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Program/Kegiatan</th>
            <?php
                foreach($komponen_biaya as $komponen_biayas){
                    echo "<th>$komponen_biayas->nama_komponen</th>";
                }
            ?>
            <th rowspan="2">Jumlah</th>
        </tr>
        <tr>
            <?php
                foreach($komponen_biaya as $komponen_biayas){
                    echo "<th>$komponen_biayas->komponen_kode</th>";
                }
            ?>
        </tr>
        <?php
            $no=0;
            $jumlah = array();
            foreach($standar_nasional as $standar_nasionals){
                $no++;
                echo "<tr>
                    <td>1.$no</td>
                    <td>$standar_nasionals->nama_standar</td>";
                    $total_per_standar = array();
                    $i=0;
                    foreach($komponen_biaya as $komponen_biayas){
                        $i++;        
                        echo "<td>".number_format($this->rkas_model->get_total_per_standar_komponen($tw,$standar_nasionals->standar_id,$komponen_biayas->komponen_kode,$tahun,$sekolahid),0)."</td>";
                        $total_per_standar[] = $this->rkas_model->get_total_per_standar_komponen($tw,$standar_nasionals->standar_id,$komponen_biayas->komponen_kode,$tahun,$sekolahid);
                    }  

                    
                echo "<td>".number_format(array_sum($total_per_standar),0)."</td>
                </tr>";   
            }
            
        ?>
        <tr>
            <th></th>
            <th>Total</th>
            <?php
                $komponen_total = array();
                foreach($komponen_biaya as $komponen_biayas){
                    echo "<th>".number_format($total_komponen[$komponen_biayas->komponen_kode],0)."</th>";
                    $komponen_total[] = $total_komponen[$komponen_biayas->komponen_kode];
                }
            ?>
            <th><?php echo number_format(array_sum($total_komponen),0); ?></th>
        </tr>
    </table>
</div>