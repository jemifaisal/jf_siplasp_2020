<div class="col-md-8" style="border: 0px solid #ccc;">
    <div style="border:0px solid;">
        <table border='1' width="600px">
            <tr>
                <td colspan="4">
                    <p align="center">
                    SURAT PERNYATAAN TANGGUNG JAWAB MUTLAK (SPTJM)<br>
                    SURAT PERNYATAAN TANGGUNG JAWAB <br>
                    Nomor :
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">A. Penerimaan Dana BOS (8)</td>
            </tr>
            <tr>
                <td width="50px">&nbsp;&nbsp;</td>
                <td>1. Triwulan I</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw1,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Triwulan II</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw2,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>3. Triwulan III</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw3,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>4. Triwulan IV</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw4,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Jumlah</td>
                <td></td>
                <td><span class="pull-left"><u><b>Rp</span><span class="pull-right"><u><?php echo number_format($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4,0); ?></b></u></span></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">B. Pengeluaran Dana BOS</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>1. Jenis Belanja Pegawai</td>
                <td>Rp</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Jenis Belanja Barang dan Jasa</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>3. Jenis Belanja Modal</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;a) Peralatan dan Mesin</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;b) Aset Tetap Lainnya</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Jumlah</td>
                <td></td>
                <td><span class="pull-left"><u><b>Rp</span><span class="pull-right"><u><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></u></b></span></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">C. Sisa Dana BOS (A-B)</td>
                <td>
                    <span class="pull-left"><b><u>Rp</u></b></span>
                    <span class="pull-right"><b><u>
                    <?php
                        $sisa_dana = ($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4) - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']);
                        echo number_format(($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4) - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']),0);   
                    ?>
                    </u></b></span>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>Terdiri Atas:</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>1. Sisa Kas Tunai</td>
                <td>
                    <span class="pull-left"><b>Rp</b></span>
                    <span class="pull-right"><b><?php echo number_format($get_total_saldo_tunai['total_saldo'],0); ?></b></span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Sisa di Bank</td>
                <td>
                    <span class="pull-left"><b>Rp</b></span>
                    <span class="pull-right"><b><?php echo number_format($sisa_dana - $get_total_saldo_tunai['total_saldo'],0); ?></b></span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
        </table>
    </div>
</div>