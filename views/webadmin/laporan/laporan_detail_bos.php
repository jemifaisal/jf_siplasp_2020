<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="#" class="btn btn-warning" onclick="back()"><i class="fa fa-chevron-left"></i></a> Detail Laporan Dana BOS</h3>
    </div>
    <div class="box-body">
        <p>
            <span class="lead">
                <span class="label label-danger"><?php echo $profil['npsn']; ?></span>
                <span class="label label-danger"><?php echo $profil['nama_sp']; ?></span>
                <span class="label label-danger"><?php echo $profil['nama_kab']; ?></span>
            </span>
        </p>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#bku"><span class="text-success">Buku Kas Umum</span></a></li>
            <li><a data-toggle="tab" href="#bpk"><span class="text-primary">Buku Pembantu Kas</span></a></li>
            <li><a data-toggle="tab" href="#bpb"><span class="text-warning">Buku Pembantu Bank</span></a></li>
            <li><a data-toggle="tab" href="#bpp"><span class="text-info">Buku Pembantu Pajak</span></a></li>
            <li><a data-toggle="tab" href="#realisasi"><span class="text-danger">Realisasi Dana BOS</span></a></li>
            <li><a data-toggle="tab" href="#penggunaan"><span class="text-dark">Penggunaan Dana BOS</span></a></li>
            <li><a data-toggle="tab" href="#sptjm"><span class="text-success">SPTJM</span></a></li>
        </ul>

        <div class="tab-content">
            <div id="bku" class="tab-pane fade in active">
                <h3 class="text-success">Buku Kas Umum</h3>
                <form class="form-inline" method="post" id="form_lihat">
                    <div class="form-group">
                        <label for="email">Tahun:</label>
                        <b class="form-control" ><?php echo $tahun; ?></b>
                        <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
                        <input type="hidden" readonly="readonly" name="sekolahid" id="sekolahid" class="form-control" value="<?php echo $sekolah_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Bulan:</label>
                        <select name="bulan" id="bulan" class="form-control">
                            <!-- <option value="Januari">Januari</option> -->
                            <?php
                            foreach($bulan as $bulans){
                                echo "<option value='$bulans->bulan_id'>$bulans->nama_bulan</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
                    <button type="submit" class="btn btn-warning"><span id="btn_caption"><i class='fa fa-send'></i> Proses</span></button>
                </form>
                <br>
                <div id="bku_show"></div>
            </div>


            <div id="bpk" class="tab-pane fade">
                <h3 class="text-primary">Buku Pembantu Kas</h3>
                <form class="form-inline" method="post" id="form_lihat_bpk">
                    <div class="form-group">
                        <label for="email">Tahun:</label>
                        <b class="form-control" ><?php echo $tahun; ?></b>
                        <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
                        <input type="hidden" readonly="readonly" name="sekolahid" id="sekolahid" class="form-control" value="<?php echo $sekolah_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Bulan:</label>
                        <select name="bulan" id="bulan" class="form-control">
                            <!-- <option value="Januari">Januari</option> -->
                            <?php
                            foreach($bulan as $bulans){
                                echo "<option value='$bulans->bulan_id'>$bulans->nama_bulan</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
                    <button type="submit" class="btn btn-warning"><span id="btn_caption_bpk"><i class='fa fa-send'></i> Proses</span></button>
                </form>
                <br>
                <div id="bpk_show"></div>
            </div>
            <div id="bpb" class="tab-pane fade">
                <h3 class="text-warning">Buku Pembantu Bank</h3>
                <form class="form-inline" method="post" id="form_lihat_bpb">
                    <div class="form-group">
                        <label for="email">Tahun:</label>
                        <b class="form-control" ><?php echo $tahun; ?></b>
                        <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
                        <input type="hidden" readonly="readonly" name="sekolahid" id="sekolahid" class="form-control" value="<?php echo $sekolah_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Bulan:</label>
                        <select name="bulan" id="bulan" class="form-control">
                            <!-- <option value="Januari">Januari</option> -->
                            <?php
                            foreach($bulan as $bulans){
                                echo "<option value='$bulans->bulan_id'>$bulans->nama_bulan</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
                    <button type="submit" class="btn btn-warning"><span id="btn_caption_bpb"><i class='fa fa-send'></i> Proses</span></button>
                </form>
                <br>
                <div id="bpb_show"></div>
            </div>
            <div id="bpp" class="tab-pane fade">
                <h3 class="text-info">Buku Pembantu Pajak</h3>
                <form class="form-inline" method="post" id="form_lihat_bpp">
                    <div class="form-group">
                        <label for="email">Tahun:</label>
                        <b class="form-control" ><?php echo $tahun; ?></b>
                        <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
                        <input type="hidden" readonly="readonly" name="sekolahid" id="sekolahid" class="form-control" value="<?php echo $sekolah_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Bulan:</label>
                        <select name="bulan" id="bulan" class="form-control">
                            <!-- <option value="Januari">Januari</option> -->
                            <?php
                            foreach($bulan as $bulans){
                                echo "<option value='$bulans->bulan_id'>$bulans->nama_bulan</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
                    <button type="submit" class="btn btn-warning"><span id="btn_caption_bpp"><i class='fa fa-send'></i> Proses</span></button>
                </form>
                <br>
                <div id="bpp_show"></div>
            </div>
            <div id="realisasi" class="tab-pane fade">
                <h3 class="text-danger">Realisasi Dana BOS</h3>
                <form class="form-inline" method="post" id="form_lihat_realisasi">
                    <div class="form-group">
                        <label for="email">Tahun:</label>
                        <b class="form-control" ><?php echo $tahun; ?></b>
                        <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
                        <input type="hidden" readonly="readonly" name="sekolahid" id="sekolahid" class="form-control" value="<?php echo $sekolah_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Triwulan:</label>
                        <select name="tw" id="tw" class="form-control">
                            <option value="1">Triwulan I</option>
                            <option value="2">Triwulan II</option>
                            <option value="3">Triwulan III</option>
                            <option value="4">Triwulan IV</option>
                        </select>
                    </div>
                    <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
                    <button type="submit" class="btn btn-warning"><span id="btn_caption_realisasi"><i class='fa fa-send'></i> Proses</span></button>
                </form>
                <br>
                <div id="realisasi_show"></div>
            </div>
            <div id="penggunaan" class="tab-pane fade">
                <h3 class="text-default">Penggunaan Dana BOS</h3>
                <form class="form-inline" method="post" id="form_lihat_penggunaan">
                    <div class="form-group">
                        <label for="email">Tahun:</label>
                        <b class="form-control" ><?php echo $tahun; ?></b>
                        <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
                        <input type="hidden" readonly="readonly" name="sekolahid" id="sekolahid" class="form-control" value="<?php echo $sekolah_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Triwulan:</label>
                        <select name="tw" id="tw" class="form-control">
                            <option value="1">Triwulan I</option>
                            <option value="2">Triwulan II</option>
                            <option value="3">Triwulan III</option>
                            <option value="4">Triwulan IV</option>
                        </select>
                    </div>
                    <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
                    <button type="submit" class="btn btn-warning"><span id="btn_caption_penggunaan"><i class='fa fa-send'></i> Proses</span></button>
                </form>
                <br>
                <div id="penggunaan_show"></div>
            </div>
            <div id="sptjm" class="tab-pane fade">
                <h3 class="text-success">SPTJM</h3>
                <form class="form-inline" method="post" id="form_lihat_sptjm">
                    <div class="form-group">
                        <label for="email">Tahun:</label>
                        <b class="form-control" ><?php echo $tahun; ?></b>
                        <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
                        <input type="hidden" readonly="readonly" name="sekolahid" id="sekolahid" class="form-control" value="<?php echo $sekolah_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Triwulan:</label>
                        <select name="tw" id="tw" class="form-control">
                            <option value="1">Triwulan I</option>
                            <option value="2">Triwulan II</option>
                            <option value="3">Triwulan III</option>
                            <option value="4">Triwulan IV</option>
                        </select>
                    </div>
                    <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
                    <button type="submit" class="btn btn-warning"><span id="btn_caption_sptjm"><i class='fa fa-send'></i> Proses</span></button>
                </form>
                <br>
                <div id="sptjm_show"></div>
            </div>
        </div>
    </div>
</div>

<script>
  function back(){
		window.history.back();
	}     
  $(document).ready(function() {
    $("#pres").DataTable({
      responsive: true
    });

    $('#form_lihat').on('submit', function(e){
      $("#bku_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('webadmin/bku_show'); ?>";
        $.ajax({
            url: url,
            type: "post",
            data: inputData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
                $("#bku_show").html(data);
                $("#btn_caption").html("<i class='fa fa-send'></i> Proses");
            }
        });
        
        return false;
      }
    });

    $('#form_lihat_bpk').on('submit', function(e){
      $("#bpk_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption_bpk").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('webadmin/bpk_show'); ?>";
        $.ajax({
            url: url,
            type: "post",
            data: inputData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
                $("#bpk_show").html(data);
                $("#btn_caption_bpk").html("<i class='fa fa-send'></i> Proses");
            }
        });
        
        return false;
      }
    });

    $('#form_lihat_bpb').on('submit', function(e){
      $("#bpb_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption_bpb").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('webadmin/bpb_show'); ?>";
        $.ajax({
            url: url,
            type: "post",
            data: inputData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
                $("#bpb_show").html(data);
                $("#btn_caption_bpb").html("<i class='fa fa-send'></i> Proses");
            }
        });
        
        return false;
      }
    }); 

    $('#form_lihat_bpp').on('submit', function(e){
      $("#bpp_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption_bpp").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('webadmin/bpp_show'); ?>";
        $.ajax({
            url: url,
            type: "post",
            data: inputData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
                $("#bpp_show").html(data);
                $("#btn_caption_bpp").html("<i class='fa fa-send'></i> Proses");
            }
        });
        
        return false;
      }
    }); 

    $('#form_lihat_realisasi').on('submit', function(e){
      $("#realisasi_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption_realisasi").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('webadmin/realisasi_show/realisasi'); ?>";
        $.ajax({
            url: url,
            type: "post",
            data: inputData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
                $("#realisasi_show").html(data);
                $("#btn_caption_realisasi").html("<i class='fa fa-send'></i> Proses");
            }
        });
        
        return false;
      }
    }); 

    $('#form_lihat_penggunaan').on('submit', function(e){
      $("#penggunaan_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption_penggunaan").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('webadmin/realisasi_show/penggunaan'); ?>";
        $.ajax({
            url: url,
            type: "post",
            data: inputData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
                $("#penggunaan_show").html(data);
                $("#btn_caption_penggunaan").html("<i class='fa fa-send'></i> Proses");
            }
        });
        
        return false;
      }
    }); 

    $('#form_lihat_sptjm').on('submit', function(e){
      $("#sptjm_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption_sptjm").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('webadmin/realisasi_show/sptjm'); ?>";
        $.ajax({
            url: url,
            type: "post",
            data: inputData,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data){
                $("#sptjm_show").html(data);
                $("#btn_caption_sptjm").html("<i class='fa fa-send'></i> Proses");
            }
        });
        
        return false;
      }
    }); 

  });
</script>