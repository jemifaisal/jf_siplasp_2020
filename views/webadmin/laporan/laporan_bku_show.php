<!-- <a href="<?php echo base_url('sekolah/cetaklaporan/'.$bulan_id.'/bku'); ?>" class="btn btn-success"><i class='fa fa-download'></i> Excel</a> -->
<table class="table table-bordered table-hover table-responsive">
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Kode Rekening</th>
        <th>No. Kode</th>
        <th>No. Bukti</th>
        <th>Uraian</th>
        <th>Penerimaan<br> (Debit)</th>
        <th>Pengeluaran<br> (Kredit)</th>
        <th>Saldo</th>
    </tr>
    <?php
        $no = 0;
        $saldo = $saldo_awal;
        if($bulan_id != 1){
            echo "<tr style='font-weight:bold;color:blue;' bgcolor='#ddd'>
                <td colspan='7'>SALDO BULAN LALU</td>
                <td></td>
                <td>".number_format($saldo,0)."</td>
            </tr>";
        }
        foreach($bku as $bkus){
            $no ++;
            if($bkus->program_kode == NULL){
                $dot = "";
            }else{
                $dot = ".";
            }
            echo "<tr>
                <td>$no</td>
                <td>".date("d M Y",strtotime($bkus->tanggal_transaksi))."</td>
                <td>$bkus->rekening_belanja</td>
                <td>$bkus->program_kode$dot$bkus->kode_komponen</td>
                <td>$bkus->kode_bukti $bkus->no_urut_bukti</td>
                <td>$bkus->uraian_bku</td>";
            if($bkus->kelompok == "D"){
                $param = '+';
                echo "<td class='success'>$bkus->jumlah_transaksi_format</td>
                    <td class='warning'></td>";
                $saldo = $saldo + $bkus->jumlah_transaksi;
            }else{
                $param = '-';
                echo "<td class='success'></td>
                    <td class='warning'>$bkus->jumlah_transaksi_format</td>";
                $saldo = $saldo - $bkus->jumlah_transaksi;
            }

            
            echo "<td class='info'>".number_format($saldo,0)."</td></tr>";
        }
    ?>
</table>
<br>
<table border='0' width='100%'>
    <tr>
        <td width='230'>Saldo BKU Bulan <?php echo $bulan; ?> Sebesar</td>
        <td width='2'>:</td>
        <td> <?php echo number_format($saldo,0); ?> </td>
    </tr>
    <tr>
        <td>Terdiri Dari</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Saldo Tunai</td>
        <td>:</td>
        <td><?php echo number_format($saldo_tunai['jumlah_saldo'],0); ?></td>
    </tr>
    <tr>
        <td>Saldo Bank</td>
        <td>: </td>
        <td><span id="txt_saldo"> <?php echo number_format($saldo - $saldo_tunai['jumlah_saldo'],0); ?></span></td>
    </tr>
</table>
<br>

<script>
    function convertToRupiah(objek) {
        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length; 
        j = 0; 
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i-1,1) + separator + c;
            } else {
                c = b.substr(i-1,1) + c;
            }
        }
        objek.value = c;
    }
</script>
