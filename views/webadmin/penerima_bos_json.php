<!-- Default box -->

<div class="box">

  <div class="box-header with-border">

    <h3 class="box-title">Data Prestasi Sekolah</h3>
  </div>
  <div class="box-body">
            <div class="box-body">

              <table id="pres" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tahun</th>
                  <th>NPSN</th>
                  <th>Nama Sekolah</th>
                  <th>Kabupaten</th>
                  <th>Kecamatan</th>
                  <th>Jenjang</th>
                  <th>Status Sekolah</th>
                  <th>Jumlah Siswa</th>
                  <th>Total BOS</th>
                  <th>Sisa Uang</th>
                  <th>Grand Total BOS</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                
              </table>
            </div>

  </div>

</div>

<script>
            var save_method; //for save method string
            var table;

            $(document).ready(function() {
                //datatables
                table = $('#pres').DataTable({ 
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [[0,"desc"]], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('webadmin/json_penerimabos/'); ?>",
                        "type": "POST"
                    },
                    //Set column definition initialisation properties.
                    "columns": [
                        {"data": "tahun"},
                        {"data": "npsn"},
                        {"data": "nama_sp"},
                        {"data": "nama_kab"},
                        {"data": "nama_kec"},
                        {"data": "jenjang"},
                        {"data": "status_sekolah"}
                        
                    ],

                });

            });
</script>