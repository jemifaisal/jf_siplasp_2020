                <?php foreach ($kab as $row) {
                  # code...
                } ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_kabupaten'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kabupaten</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $row->kd_kab; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kabupaten" value="<?php echo $row->nama_kab; ?>">
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($row->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                  </form>
                </div>
