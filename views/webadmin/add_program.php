                <?php
                  //$this->db->select('MAX(right(substandar_kode,2)) as kode_akhir');
                  $sql = $this->db->query("SELECT MAX(right(program_kode,3)) as kode_akhir FROM sub_program");
                  if($sql->num_rows() >0){
                    foreach ($sql->result() as $row) {
                      # code...
                      $a = $row->kode_akhir;
                      //$pecah = explode(".", $a);
                      $kode_mulai = $a + 1;
                    }  
                  }else{
                    $kode_mulai = "001";
                  }
                  
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_program'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Sub Standar Nasional</label>
                        <select name="substandar" class="form-control">
                          <?php
                            foreach ($substandar as $key) {
                              echo "<option value='$key->substandar_id'>$key->substandar_kode - $key->namasub_standar</option>";
                            }
                          ?>
                        </select>
                      </div>


                      <div class="form-group">
                        <label for="exampleInputEmail1">Kode Sub Program</label>
                        <input type="text" name="kodesub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Sub Program" value="<?php echo $kode_mulai; ?>" readonly="readonly">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sub Program</label>
                        <input type="text" name="namasub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Sub Standar Nasional">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Komponen Pembiayaan</label>
                        <select class="form-control select2" name="biaya[]" multiple="multiple" data-placeholder="Komponen Pembiayaan" style="width: 100%;" required="required">
                          <?php
                            foreach ($biaya as $keys) {
                              echo "<option value='$keys->komponen_kode'>$keys->komponen_kode - $keys->nama_komponen</option>";
                            }
                          ?>
                        </select>
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                      $(".select2").select2();
                          
                  });
                </script>