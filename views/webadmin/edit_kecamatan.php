                <?php
                  foreach ($kec as $rows) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_kecamatan'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kabupaten</label>
                        <input type="hidden" name="idkec" required="required" class="form-control" placeholder="Nama Kecamatan" value="<?php echo $rows->kd_kec; ?>">
                        <select name="kab" class="form-control">
                          <?php
                            foreach ($kab as $row) {
                              if($rows->kd_kab == $row->kd_kab){
                                echo "<option value='$row->kd_kab' selected='selected'>$row->nama_kab</option>";
                              }else{
                                echo "<option value='$row->kd_kab'>$row->nama_kab</option>";  
                              }
                              
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kecamatan</label>
                        <input type="text" name="namakec" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kecamatan" value="<?php echo $rows->nama_kec; ?>">
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($rows->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>
