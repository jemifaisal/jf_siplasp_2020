<?php

  function rpbold($a){
    $h = "<b>".number_format($a,0,',','.')."</b>";
    return $h;
  }
  function rp($a){
    $h = number_format($a,0,',','.');
    return $h;
  }

  $this->db->where('soft_delete',0);
  $query = $this->db->get('standar_nasional');
  //$query = $this->db->query("SELECT * FROM rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id join sub_standar ss on sp.substandar_id=ss.substandar_id join standar_nasional sn on ss.standar_id=sn.standar_id");
  if($query){
    $data = $query->result();
  }

?>

<div class="panel-group" id="accordion">
  <?php foreach ($data as $row1) { ?>
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $row1->standar_id; ?>">
          <?php echo $row1->standar_kode." - ".$row1->nama_standar; ?>
        </a>
      </h4>
    </div>
    <div id="<?php echo $row1->standar_id; ?>" class="panel-collapse collapse">
      <div class="panel-body">

        <div class="panel-group" id="<?php echo $row1->standar_id.$row1->standar_kode; ?>">
          <?php
            $sql2 = $this->db->query("SELECT * FROM sub_standar where standar_id='$row1->standar_id' AND soft_delete='0'")->result();
            foreach ($sql2 as $row2) {
          ?>
          <div class="panel panel-success">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#<?php echo $row1->standar_id.$row1->standar_kode; ?>" href="#we<?php echo $row2->substandar_id; ?>">
                <?php echo $row2->substandar_kode." - ".$row2->namasub_standar; ?>
                </a>
              </h4>
            </div>
            <div id="we<?php echo $row2->substandar_id; ?>" class="panel-collapse collapse">
              <div class="panel-body">
                <a href="#" class="btn btn-success" id="rincian" data-id="<?php echo $row2->substandar_id; ?>"><i class="fa fa-plus"></i> Rincian</a>
                <br><br>
                <p>Data Rincian Belanja</p>
                <table border="0" class="table table-hover">
                  <tr>
                    <th>Kode Program</th>
                    <th>Detail Rincian</th>
                    <th>Triwulan I</th>
                    <th>Triwulan II</th>
                    <th>Triwulan III</th>
                    <th>Triwulan IV</th>
                    <th>Action</th>
                  </tr>
                  <?php
                    //$sql3 = $this->db->query("SELECT * FROM rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id join sub_standar ss on sp.substandar_id=ss.substandar_id WHERE ss.substandar_id='$row2->substandar_id'")->result();
                    $sql3 = $this->db->query("SELECT *, SUM(tw1) as twi,SUM(tw2) as twii,SUM(tw3) as twiii,SUM(tw4) as twiv FROM rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id WHERE sp.substandar_id='$row2->substandar_id'  AND rb.soft_delete='0' group by rb.program_id")->result();



                    foreach ($sql3 as $row3) {
                      $sql7 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id where rb.program_id='$row3->program_id' and rb.soft_delete='0'")->result();
                      foreach ($sql7 as $row7) {
                        $satu = $row7->total+$row7->total2;
                        $dua = $row7->total3+$row7->total4;
                        $tiga = $row7->total5+$row7->total6;
                        $empat = $row7->total7+$row7->total8;
                      }
                      echo "<tr>
                        <td><b>$row3->program_kode</b></td>
                        <td><b>$row3->nama_program</b></td>
                        <td>".rpbold($satu)."</td>
                        <td>".rpbold($dua)."</td>
                        <td>".rpbold($tiga)."</td>
                        <td>".rpbold($empat)."</td>
                        <td></td>
                      </tr>";

                      $sql4 = $this->db->query("SELECT * FROM rincian_belanja where program_id='$row3->program_id'  AND soft_delete='0'")->result();
                      foreach ($sql4 as $row4) {
                        if($row4->status_rincian ==1){
                          $dr = "<b>".$row4->detail_rincian."</b>";
                          $action = "<div class='input-group-btn'><a class='btn btn-success btn-xs dropdown-toggle' data-toggle='dropdown'>Action
                            <span class='fa fa-caret-down'></span></a>
                            <ul class='dropdown-menu' style='position:absolute;'>
                              <li><a href='#' id='rincianheader' data-id='$row4->rincian_id'>Tambah Rincian</a></li>
                              <li><a href='#' id='editrincian' data-id='$row4->rincian_id'>Ubah</a></li>
                              <li><a href='".base_url('webadmin/hapusrincian/'.$row4->rincian_id)."' onclick=\"return confirm('Yakin ?')\">Hapus</a></li>
                            </ul></div>";//<a href='#' class='btn btn-success btn-xs' id='rincianheader' data-id='$row4->rincian_id'><i class='fa fa-plus'></i> Rincian Header</a><br> <a href='#' class='btn btn-primary btn-xs'>Ubah</a> <a href='".base_url('webadmin/hapusrincian/'.$row4->rincian_id)."' class='btn btn-danger btn-xs' onclick=\"return confirm('Yakin ?')\">Hapus</a>";

                          $sql6 = $this->db->query("SELECT *, SUM(htw1) as sumhtw1,SUM(htw2) as sumhtw2,SUM(htw3) as sumhtw3,SUM(htw4) as sumhtw4 FROM rincian_header where rincian_id='$row4->rincian_id'  AND soft_delete='0'")->result();
                          foreach ($sql6 as $row6) {
                            $isitw1 = "<b>".rpbold($row6->sumhtw1)."</b>"; $isitw2="<b>".rpbold($row6->sumhtw2)."</b>"; $isitw3="<b>".rpbold($row6->sumhtw3)."</b>"; $isitw4="<b>".rpbold($row6->sumhtw4)."</b>";
                          }
                          

                        }else{
                          $dr = "- ".$row4->detail_rincian;
                          $action = "<a href='#' class='btn btn-primary btn-xs' id='editrincian' data-id='$row4->rincian_id'>Ubah</a> <a href='".base_url('webadmin/hapusrincian/'.$row4->rincian_id)."' class='btn btn-danger btn-xs' onclick=\"return confirm('Yakin ?')\">Hapus</a>";

                          $isitw1 = rp($row4->tw1); $isitw2=rp($row4->tw2); $isitw3=rp($row4->tw3); $isitw4=rp($row4->tw4);
                        }
                        echo "<tr>
                          <td></td>
                          <td>$dr</td>
                          <td>$isitw1</td>
                          <td>$isitw2</td>
                          <td>$isitw3</td>
                          <td>$isitw4</td>
                          <td>$action</td>
                        </tr>"; 

                        if($row4->status_rincian == 1){
                          $sql5 = $this->db->query("SELECT * FROM rincian_header where rincian_id='$row4->rincian_id'  AND soft_delete='0'")->result();
                          foreach ($sql5 as $row5) {
                            echo "<tr>
                              <td></td>
                              <td>-- $row5->header_rincian</td>
                              <td>".rp($row5->htw1)."</td>
                              <td>".rp($row5->htw2)."</td>
                              <td>".rp($row5->htw3)."</td>
                              <td>".rp($row5->htw4)."</td>
                              <td><a href='#' class='btn btn-primary btn-xs' id='edit2' data-id='$row5->rincian_id'>Ubah</a> <a href='".base_url('webadmin/hapusrincianheader/'.$row5->header_id)."' class='btn btn-danger btn-xs' onclick=\"return confirm('Yakin ?')\">Hapus</a></td>
                            </tr>"; 
                          }
                        }
                      }



                    }

                  ?>
                </table>
              </div>
            </div>
          </div>
          <?php } ?>

        </div>

      </div>
    </div>
  </div>
  <?php } ?>
</div>


      <div id="tambahrincian" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Rincian Belanja</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="tambahrincianheader" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Rincian Belanja Header</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="editrincianheader" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Rincian Belanja Header</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="edit22" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Rincian Belanja Header</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <script>
            $(document).ready(function () {
              $(document).on('click','#rincian',function(e){
                e.preventDefault();
                $("#tambahrincian").modal('show');
                $.post("<?php echo base_url('webadmin/tambahrincian'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#rincianheader',function(e){
                e.preventDefault();
                $("#tambahrincianheader").modal('show');
                $.post("<?php echo base_url('webadmin/tambahrincianheader'); ?>",
                    {id2:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#editrincian',function(e){
                e.preventDefault();
                $("#editrincianheader").modal('show');
                $.post("<?php echo base_url('webadmin/editrincian'); ?>",
                    {id3:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#edit2',function(e){
                e.preventDefault();
                $("#edit22").modal('show');
                $.post("<?php echo base_url('webadmin/editrincianheader'); ?>",
                    {id3:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
