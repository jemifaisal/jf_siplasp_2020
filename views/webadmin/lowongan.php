        <script type="text/javascript" src="<?php echo base_url(); ?>assets/webadmin/ckeditor/ckeditor.js"></script>
        <div id="kabupaten" class="modal fade" role="dialog">
          <div class="modal-dialog" style="width:90%;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Lowongan Kerja</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_lowongan'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Judul Lowongan Kerja</label>
                        <input type="text" name="judul" required="required" class="form-control" id="exampleInputEmail1" placeholder="Judul Lowongan">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Perusahaan</label>
                        <input type="text" name="namapt" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Perusahaan">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Isi Lowongan</label>
                        <textarea class="form-control ckeditor" id="ckeditor" name="isi"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kabupaten / Kota Lowongan Kerja</label>
                        <select class="form-control select2" name="kabupaten[]" multiple="multiple" data-placeholder="Kabupaten / Kota" style="width: 100%;">
                          <?php
                            foreach ($kabupaten as $keys) {
                              echo "<option value='$keys->kd_kab'>$keys->nama_kab</option>";
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Pendidikan yang Dibutuhkan</label>
                        <select class="form-control select2" name="pendidikan[]" multiple="multiple" data-placeholder="Pendidikan" style="width: 100%;">
                          <?php
                            foreach ($pendidikan as $key) {
                              echo "<option value='$key->pendidikan_id'>$key->jenjang</option>";
                            }
                          ?>
                        </select>

                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Terbit</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="tglterbit" class="form-control pull-right" id="datepicker">
                        </div>
                      </div>
                      <div class="bootstrap-timepicker">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jam Terbit</label>
                        <div class="input-group">
                          <input type="text" name="jamterbit" class="form-control" id="timepicker">

                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                        </div>
                      </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Akhir Lowongan</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="tglakhirlowongan" class="form-control pull-right" id="datepicker2">
                        </div>
                      </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="lowongan" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ubah Kabupaten</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          

          <p><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#kabupaten"><i class="fa fa-plus-circle"></i> Tambah Data</a></p>
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Kabupaten</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Lowongan</th>
                  <th>Nama Perusahaan</th>
                  <th>Pendidikan</th>
                  <th>Tgl. Terbit / Jam</th>
                  <th>Tgl. Akhir Lowongan</th>
                  <th>Tgl. Input</th>
                  <th>Status</th>
                  <th>Admin</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($lowongan as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->judul_lowongan; ?></td>
                  <td><?php echo $row->nama_pt; ?></td>
                  <td><?php echo $row->pendidikan_id; ?></td>
                  <td><?php echo $row->tgl_terbit." / ". $row->jam_terbit; ?></td>
                  <td><?php echo $row->tgl_akhir_lowongan; ?></td>
                  <td><?php echo $row->tgl_input; ?></td>
                  <td><?php if($row->soft_delete == 0){echo "Aktif";}else{echo "Tidak Aktif";} ?></td>
                  <td><?php echo $row->admin_id; ?></td>
                  <td><a href="<?php echo base_url('webadmin/editlowongan/'.$row->lowongan_id); ?>" class="btn btn-success btn-xs">Ubah</a> 
                      <a href="<?php echo base_url('webadmin/hapus_lowongan/'.$row->lowongan_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Lowongan</th>
                  <th>Nama Perusahaan</th>
                  <th>Pendidikan</th>
                  <th>Tgl. Terbit / Jam</th>
                  <th>Tgl. Akhir Lowongan</th>
                  <th>Tgl. Input</th>
                  <th>Status</th>
                  <th>Admin</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#edit',function(e){
                e.preventDefault();
                $("#lowongan").modal('show');
                $.post("<?php echo base_url('webadmin/edit_lowongan'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    } 

                );

              });



            });
          </script>