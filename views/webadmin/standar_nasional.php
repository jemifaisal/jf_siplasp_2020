        <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Standar Nasional</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          <p><a href="#" class="btn btn-primary" id="aksi" data-id="1"><i class="fa fa-plus-circle"></i> Tambah Data</a></p>
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Standar Nasional</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Standar Nasional</th>
                  <th>Nama Standar Nasional</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($standar as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->standar_kode; ?></td>
                  <td><?php echo $row->nama_standar; ?></td>
                  <td><?php if($row->soft_delete == 0){echo "Aktif";}else{echo "Tidak Aktif";} ?></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="aksi" data-id="2,<?php echo $row->standar_id; ?>">Ubah</a> <a href="<?php echo base_url('webadmin/hapus_standar/'.$row->standar_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('webadmin/aksi_standar'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
