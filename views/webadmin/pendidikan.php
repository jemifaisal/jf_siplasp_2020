        <div id="kabupaten" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Pendidikan</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_pendidikan'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jenjang Pendidikan</label>
                        <input type="text" name="jenjang" required="required" class="form-control" id="exampleInputEmail1" placeholder="Jenjang Pendidikan">
                      </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="editpddk" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ubah Pendidikan</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          

          <p><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#kabupaten"><i class="fa fa-plus-circle"></i> Tambah Data</a></p>
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Pendidikan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Jenjang Pendidikan</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($pendidikan as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->jenjang; ?></td>
                  <td><?php if($row->soft_delete == 0){echo "Aktif";}else{echo "Tidak Aktif";} ?></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="edit" data-id="<?php echo $row->pendidikan_id; ?>">Ubah</a> 
                      <a href="<?php echo base_url('webadmin/hapus_pendidikan/'.$row->pendidikan_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Jenjang Pendidikan</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#edit',function(e){
                e.preventDefault();
                $("#editpddk").modal('show');
                $.post("<?php echo base_url('webadmin/edit_pendidikan'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>