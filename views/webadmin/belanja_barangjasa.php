<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">RKAS BELANJA BARANG DAN JASA</h3>
  </div>
  <div class="box-body">
    	<div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">8 STANDAR NASIONAL</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="bj" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Standar</th>
                  <th>Nama Standar</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($standar_nasional as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->standar_kode; ?></td>
                  <td><?php echo $row->nama_standar; ?></td>
                  <td><a href="<?php echo base_url('webadmin/rincianbelanja/'.$row->standar_id); ?>" class="btn btn-success btn-xs" id="edit" data-id="<?php echo $row->standar_id; ?>">Tambah Rincian</a> </td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama Kabupaten</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>

</div>

		<script>
            $(function () {
              $("#bj").DataTable();

            });
          </script>
