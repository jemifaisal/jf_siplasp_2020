                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_sekolah'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">NPSN</label>
                        <input type="text" name="npsn" required="required" class="form-control" id="exampleInputEmail1" placeholder="NPSN">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sekolah</label>
                        <input type="text" name="namasp" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Sekolah">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Jenjang</label>
                        <select name="jenjang" class="form-control">
                          <option value="SDLB">SDLB</option>
                          <option value="SMPLB">SMPLB</option>
                          <option value="SMALB">SMALB</option>
                          <option value="SLB">SLB</option>
                          <option value="SMA">SMA</option>
                          <option value="SMK">SMK</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Status Sekolah</label>
                        <select name="statussekolah" class="form-control">
                          <option value="Negeri">Negeri</option>
                          <option value="Swasta">Swasta</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Akreditasi</label>
                        <select name="akreditasi" class="form-control">
                          <option value="A">A</option>
                          <option value="B">B</option>
                          <option value="C">C</option>
                          <option value="Belum Terakreditasi">Belum Terakreditasi</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kecamatan</label>
                        <select name="kec" class="show-tick form-control" data-live-search="true" id="e1">
                          <?php
                            foreach ($kec as $row) {
                              echo "<option value='$row->kd_kec' data-subtext='$row->nama_kab'>$row->nama_kec</option>";
                            }
                          ?>
                        </select>
                      </div>
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
            $(document).ready(function () {
                $("#e1").selectpicker();
                    
            });
          </script>
