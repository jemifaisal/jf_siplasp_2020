                <script type="text/javascript">
                  function rincianaksih(){
                    document.getElementById("tampil").style.display = "none";
                  }
                  function rincianaksid(){
                    document.getElementById("tampil").style.display = "block";
                  }
                </script>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_rincian'); ?>">
                    <div class="box-body">
                      <div class="form-group">


                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <label for="exampleInputEmail1">Sub Kegiatan</label>
                        <select class="form-control select2" name="program" id="e1" style="width: 100%;">
                          <?php
                            foreach ($sub_program as $rows) {
                              echo "<option value='$rows->program_id'>$rows->program_kode - $rows->nama_program</option>";
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Status Rincian</label><br>
                        <input class="flat-red" id="srh" type="radio" name="sr" value="1" onclick="rincianaksih()"> Header
                        <input class="flat-red" id="srd" type="radio" name="sr" value="0" onclick="rincianaksid()" checked="checked"> Detail
                      </div>


                      <div id="tampil" style="display:block;">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw1" class="form-control" id="exampleInputEmail1" placeholder="Triwulan I">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw2" class="form-control" id="exampleInputEmail1" placeholder="Triwulan II">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw3" class="form-control" id="exampleInputEmail1" placeholder="Triwulan III">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw4" class="form-control" id="exampleInputEmail1" placeholder="Triwulan IV">
                      </div>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <script>
            $(document).ready(function () {
                $("#e1").select2();
            });
          </script>

