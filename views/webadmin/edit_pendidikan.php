                <?php foreach ($pendidikan as $row) {
                  # code...
                } ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_pendidikan'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jenjang Pendidikan</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $row->pendidikan_id; ?>">
                        <input type="text" name="jenjang" required="required" class="form-control" id="exampleInputEmail1" placeholder="Jenjang Pendidikan" value="<?php echo $row->jenjang; ?>">
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($row->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                  </form>
                </div>
