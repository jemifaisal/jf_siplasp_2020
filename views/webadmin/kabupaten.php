        <div id="kabupaten" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Kabupaten</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_kabupaten'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kabupaten</label>
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kabupaten">
                      </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


        <div id="editkab" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ubah Kabupaten</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          

          <p><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#kabupaten"><i class="fa fa-plus-circle"></i> Tambah Data</a></p>
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Kabupaten</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kabupaten</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($kab as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->nama_kab; ?></td>
                  <td><?php if($row->soft_delete == 0){echo "Aktif";}else{echo "Tidak Aktif";} ?></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="edit" data-id="<?php echo $row->kd_kab; ?>">Ubah</a> <a href="<?php echo base_url('webadmin/hapus_kabupaten/'.$row->kd_kab); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama Kabupaten</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#edit',function(e){
                e.preventDefault();
                $("#editkab").modal('show');
                $.post("<?php echo base_url('webadmin/edit_kabupaten'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
