<!DOCTYPE html>
<html>
<head>
	<title>Download Excel</title>
</head>
<body>
	<!--<table border="1">-->
	<!--	<tr>-->
	<!--		<td colspan="17" align="center"><h3>LAPORAN ASET SEKOLAH <?php echo strtoupper($nama_kabupaten); ?> <br> KIB B<br>SUMBER DANA BOSDA<br>TAHUN <?php echo $tahun; ?></h3></td>-->
	<!--	</tr>-->
	<!--	<tr>-->
	<!--		<th>No.</th>-->
	<!--		<th>Kode Barang</th>-->
	<!--		<th>No. Register</th>-->
	<!--		<th>Nama Barang</th>-->
	<!--		<th>Merk</th>-->
	<!--		<th>CC</th>-->
	<!--		<th>Bahan</th>-->
	<!--		<th>Pabrik</th>-->
	<!--		<th>Rangka</th>-->
	<!--		<th>Mesin</th>-->
	<!--		<th>No. Polisi</th>-->
	<!--		<th>BPKB</th>-->
	<!--		<th>Jumlah Barang</th>-->
	<!--		<th>Harga Satuan</th>-->
	<!--		<th>Total Harga</th>-->
	<!--		<th>Keterangan</th>-->
	<!--		<th>Nama Sekolah</th>-->
	<!--	</tr>-->
	<!--	<ini?php-->
	<!--		$no = 0;-->
	<!--		foreach ($aset as $row1) {-->
	<!--			$no++;-->
	<!--			if($row1->ukuran_cc == 0){-->
	<!--				$cc = '-';-->
	<!--			}else{-->
	<!--				$cc = $row1->ukuran_cc;-->
	<!--			}-->
	<!--			echo "<tr>-->
	<!--				<td>$no</td>-->
	<!--				<td>$row1->kode_barang</td>-->
	<!--				<td>$row1->no_register</td>-->
	<!--				<td>$row1->nama_barang</td>-->
	<!--				<td>$row1->merk_type</td>-->
	<!--				<td>$cc</td>-->
	<!--				<td>$row1->bahan</td>-->
	<!--				<td>$row1->pabrik</td>-->
	<!--				<td>$row1->rangka</td>-->
	<!--				<td>$row1->mesin</td>-->
	<!--				<td>$row1->no_polisi</td>-->
	<!--				<td>$row1->bpkb</td>-->
	<!--				<td>$row1->jumlah_barang</td>-->
	<!--				<td>$row1->harga_perolehan</td>-->
	<!--				<td>".$row1->jumlah_barang * $row1->harga_perolehan."</td>-->
	<!--				<td>$row1->ket</td>-->
	<!--				<td>$row1->nama_sp</td>-->
	<!--			</tr>";-->
	<!--		}-->
	<!--	?>-->
	<!--	<tr>-->
	<!--		<th colspan="14">GRAND TOTAL</th>-->
			<th><?php //echo $total_aset_perkab; ?></th>
	<!--		<th>&nbsp;</th>-->
	<!--		<th>&nbsp;</th>-->
	<!--	</tr>-->
	<!--</table>-->
	
	<table border="1">
		<tr>
			<td colspan="14" align="center"><h3>LAPORAN ASET SEKOLAH <?php echo strtoupper($nama_kabupaten); ?><br> KIB E<br>SUMBER DANA BOSDA<br>TAHUN <?php echo $tahun; ?></h3></td>
		</tr>
		<tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Kode Barang</th>
			<th rowspan="2">No. Register</th>
			<th rowspan="2">Nama Barang</th>
			<th colspan="2">Buku / Perpustakaan</th>
			<th colspan="3">Barang Bercorak</th>
			<th rowspan="2">Kuantitas</th>
			<th rowspan="2">Harga Satuan</th>
			<th rowspan="2">Total Harga</th>
			<th rowspan="2">Keterangan</th>
			<th rowspan="2">Nama Sekolah</th>
		</tr>
		<tr>
			<th>Judul Buku</th>
			<th>Bahan</th>
			<th>Spesifikasi</th>
			<th>Asal Daerah</th>
			<th>Pencipta</th>
		</tr>
		<?php
			$no = 0;
			foreach($aset as $row3){
				$no++;
				echo "<tr>
					<td>$no</td>
					<td>$row3->kode_barang</td>
					<td>$row3->no_register</td>
					<td>$row3->nama_barang</td>
					<td>$row3->judul</td>
					<td>$row3->bahan</td>
					<td>$row3->spesifikasi</td>
					<td>$row3->asal_daerah</td>
					<td>$row3->pencipta</td>
					<td>$row3->jumlah</td>
					<td>$row3->harga_perolehan</td>
					<td>".$row3->harga_perolehan * $row3->jumlah."</td>
					<td>$row3->ket</td>
					<td>$row3->nama_sp</td>
				</tr>";
			}
		?>

		<tr>
			<th colspan="11">GRAND TOTAL</th>
			<th><?php //echo $total_kib_e_bosnas; ?></th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</table>

</body>
</html>