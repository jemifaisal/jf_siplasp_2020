<?php
  $id = $this->input->get('id');
  $tahun = $this->input->get('tahun');
  $url = $_SERVER['REQUEST_URI'];
?>
<div class="box">

  <div class="box-header with-border">
    <h3 class="box-title">Data Penerima BOS</h3>
  </div>
  <div class="box-body">
      <div class="box-body">
        <?php
          if(isset($tahun)){
            //tampilkan kabupaten berdasarkan tahun
            echo "<p><input type='submit' class='btn btn-warning' value='&laquo; Kembali' onclick=\"back()\"></p>";
            if(isset($id)){
              $bos = $this->webadmin_model->penerima_bos_per_kab($id,$tahun);
              echo "<h3 class='box-title'>Tahun: $tahun</h3>";
              echo "<table id='pres' class='table table-bordered table-striped'>
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Action</th>
                    <th>Input RKAS</th>
                    <th>NPSN</th>
                    <th>Nama Sekolah</th>
                    <th>Kabupaten</th>
                    <th>Kecamatan</th>
                    <th>Jenjang</th>
                    <th>Jumlah Siswa</th>
                    <th>Total BOS</th>
                    <th>Sisa Uang</th>
                    <th>Triwulan</th>
                    <th>Grand Total BOS</th>
                  </tr>
                </thead>
                <tbody>";
                $no=0;
                foreach ($bos as $row) {
                  $no++;
                  $jumlah_siswa = $row->kelas_i+$row->kelas_ii+$row->kelas_iii+$row->kelas_iv+$row->kelas_v+$row->kelas_vi+$row->kelas_vii+$row->kelas_viii+$row->kelas_ix+$row->kelas_x+$row->kelas_xi+$row->kelas_xii;
                  $cek_belanja = $this->webadmin_model->total_belanja_bos($row->sekolah_id,$tahun);
                  if($cek_belanja['total_belanja'] > 0){
                    if($cek_belanja['total_belanja'] == $cek_belanja['total_bos']){
                      $rkas = "<i class='btn btn-success btn-xs'>Balance</i>";
                    }else{
                      $rkas = "<i class='btn btn-warning btn-xs'>Proses</i>";
                    }
                  }else{
                    $rkas = "<i class='btn btn-danger btn-xs'>Belum</i>";
                  }
                  echo "<tr>
                    <td>$no</td>
                    <td><a href='#' id='add' data-id='$row->bos_id' class='btn btn-success btn-xs'>Sisa Uang</a></td>
                    <td>$rkas</td>
                    <td><a href='".base_url('webadmin/rkasekolah/'.$row->sekolah_id.'/'.$row->tahun)."'>$row->npsn</a></td>
                    <td>$row->nama_sp</td>
                    <td>$row->nama_kab</td>
                    <td>$row->nama_kec</td>
                    <td>$row->jenjang - $row->status_sekolah</td>
                    <td>".number_format($row->jumlah_siswa,0)."</td>
                    <td>".number_format($row->total_bos,0)."</td>
                    <td>".number_format($row->sisa_uang,0,',','.')."</td>
                    <td>$row->triwulan</td>
                    <td>".number_format($row->total_bos + $row->sisa_uang,0)."</td>
                  </tr>";
                }
                
              echo "</tbody>
              </table>";


            }else{
              echo "<h3 class='box-title'>Tahun: $tahun</h3>";
              echo "<table id='pres' class='table table-bordered table-striped'>
                    <thead>
                      <tr>
                        <th rowspan='2'>Nama Kabupaten</th>
                        <th colspan='6'>Jumlah Sekolah</th>
                      </tr>
                      <tr>
                        <td>SLB Negeri</td>
                        <td>SLB Swasta</td>
                        <td>SMA Negeri</td>
                        <td>SMA Swasta</td>
                        <td>SMK Negeri</td>
                        <td>SMK Swasta</td>
                      </tr>
              </thead><tbody>";
              foreach($get_kab as $get_kabs){
                echo "<tr>
                  <td><a href='$url&id=$get_kabs->kd_kab' class='btn btn-info btn-xs'><i class='fa fa-eye'></i> Detail</a> $get_kabs->nama_kab</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SLB','Negeri',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SLB','Swasta',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMA','Negeri',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMA','Swasta',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMK','Negeri',$tahun)."</td>
                  <td>".$this->webadmin_model->get_jumlah_sekolah_penerima_bos($get_kabs->kd_kab,'SMK','Swasta',$tahun)."</td>
                </tr>";
              }
              echo "</tbody></table>";
            }
            
          }else{
            //tampilkan pilihan tahun
            foreach($get_tahun as $get_tahuns){
              echo "<div class='col-md-4 col-sm-8 col-xs-12'>
                    <div class='info-box'>
                      <span class='info-box-icon bg-aqua'><i class='fa fa-money'></i></span>
        
                      <div class='info-box-content'>
                        <span class='info-box-text'>PENERIMA BOS</span>
                        <span class='info-box-number'>$get_tahuns->tahun</span>
                        <span class='info-box-text'><a href='$url?tahun=$get_tahuns->tahun' class='btn btn-success btn-xs'>Detail &raquo;</a></span>
                      </div>
                    </div>
              </div>";
            }

          }
        ?>
        
      </div>

  </div>

</div>

<div id="addsisauang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Form Sisa Uang</h4>
      </div>
      <div class="modal-body">
        <div class="box">
          
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  function back(){
		window.history.back();
	}     
  $(document).ready(function() {
    $("#pres").DataTable({
      responsive: true
    });    

    $(document).on('click','#add',function(e){
      e.preventDefault();
      $("#addsisauang").modal('show');
      $.post("<?php echo base_url('webadmin/edit_sisa_uang'); ?>",
          {id:$(this).attr('data-id')},
          function(html){
              $(".modal-body").html(html);
          }   
      );
    });

  });
</script>