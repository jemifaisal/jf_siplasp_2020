                <?php foreach ($bos as $row) { 
                  # code...
                }
                ?>
                <script type="text/javascript">
                  function convertToRupiah(objek) {
                      separator = ".";
                      a = objek.value;
                      b = a.replace(/[^\d]/g,"");
                      c = "";
                      panjang = b.length; 
                      j = 0; 
                      for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                          c = b.substr(i-1,1) + separator + c;
                        } else {
                          c = b.substr(i-1,1) + c;
                        }
                      }
                      objek.value = c;

                    }
                </script>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_sisa_uang'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sekolah</label>
                        <input type="text" readonly="readonly" name="namasekolah" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kecamatan" value="<?php echo $row->nama_sp; ?>">
                        <input type="hidden" readonly="readonly" name="bosid" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kecamatan" value="<?php echo $row->bos_id; ?>">
                        
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Sisa Uang Tahun Lalu</label>
                        <input type="text" name="sisauang" required="required" class="form-control" id="exampleInputEmail1" onkeyup="convertToRupiah(this);" placeholder="Sisa Uang Tahun Lalu" value="<?php echo $row->sisa_uang; ?>">
                        
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan</label>
                        <select name="triwulan" class="form-control">
                          <?php
                            if($row->triwulan == 1){
                              echo "<option value='1' selected='selected'>Triwulan I</option>
                                    <option value='2'>Triwulan II</option>
                                    <option value='3'>Triwulan III</option>
                                    <option value='4'>Triwulan IV</option>";

                            }elseif($row->triwulan == 2){
                              echo "<option value='1'>Triwulan I</option>
                                    <option value='2' selected='selected'>Triwulan II</option>
                                    <option value='3'>Triwulan III</option>
                                    <option value='4'>Triwulan IV</option>";  
                            }elseif($row->triwulan == 3){
                              echo "<option value='1'>Triwulan I</option>
                                    <option value='2'>Triwulan II</option>
                                    <option value='3' selected='selected'>Triwulan III</option>
                                    <option value='4'>Triwulan IV</option>"; 
                              
                            }elseif($row->triwulan == 4){
                              echo "<option value='1'>Triwulan I</option>
                                    <option value='2'>Triwulan II</option>
                                    <option value='3'>Triwulan III</option>
                                    <option value='4' selected='selected'>Triwulan IV</option>"; 
                              
                            }else{
                              echo "<option value='1'>Triwulan I</option>
                                    <option value='2'>Triwulan II</option>
                                    <option value='3'>Triwulan III</option>
                                    <option value='4'>Triwulan IV</option>"; 
                            }
                          ?>
                          
                        </select>
                      </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <?php
                  function convertToRupiah($isi){
                    return number_format($isi,0,',','.');
                  }
                ?>
