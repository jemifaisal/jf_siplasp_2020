                <?php
                  foreach ($standar as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_standar'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kode Standar Nasional</label>
                        <input type="text" name="kodestandar" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Standar Nasional" value="<?php echo $row->standar_kode; ?>">
                        <input type="hidden" name="standarid" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Standar Nasional" value="<?php echo $row->standar_id; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Standar Nasional</label>
                        <input type="text" name="namastandar" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Standar Nasional" value="<?php echo $row->nama_standar; ?>">
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($row->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>