                <?php
                  foreach ($katlomba as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_kategorilomba'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Lomba</label>
                        <select name="lomba" class="form-control">
                          <?php
                            foreach ($lomba as $key) {
                              if($row->lomba_id == $key->lomba_id){
                                echo "<option value='$key->lomba_id' selected='selected'>$key->nama_lomba</option>";
                              }else{
                                echo "<option value='$key->lomba_id'>$key->nama_lomba</option>";   
                              }
                              
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kategori Lomba</label>
                        <input type="text" name="namakatlomba" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kategori Lomba" value="<?php echo $row->nama_katlomba; ?>">
                        <input type="hidden" name="idkatlomba" required="required" class="form-control" placeholder="Nama Kategori Lomba" value="<?php echo $row->katlomba_id; ?>">
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($row->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>