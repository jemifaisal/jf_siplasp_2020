                <?php
                  //$this->db->select('MAX(right(substandar_kode,2)) as kode_akhir');
                  $sql = $this->db->query("SELECT MAX(right(substandar_kode,2)) as kode_akhir FROM sub_standar");
                  if($sql->num_rows() >0){
                    foreach ($sql->result() as $row) {
                      # code...
                      $a = $row->kode_akhir;
                      //$pecah = explode(".", $a);
                      $kode_mulai = $a + 1;
                    }  
                  }else{
                    $kode_mulai = "01";
                  }
                  
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/add_substandar'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Standar Nasional</label>
                        <select name="standar" class="form-control">
                          <?php
                            foreach ($standar as $key) {
                              echo "<option value='$key->standar_id'>$key->standar_kode - $key->nama_standar</option>";
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Kode Sub Standar Nasional</label>
                        <input type="text" name="kodesub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Sub Standar Nasional" value="<?php echo $kode_mulai; ?>" readonly="readonly">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sub Standar Nasional</label>
                        <input type="text" name="namasub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Sub Standar Nasional">
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>