                <?php
                  foreach ($program as $row) {
                    
                  }
                  
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_program'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Sub Standar Nasional</label>
                        <select name="substandar" class="form-control">
                          <?php
                            foreach ($substandar as $key) {
                              if($row->substandar_id == $key->substandar_id){
                                echo "<option value='$key->substandar_id' selected='selected'>$key->substandar_kode - $key->namasub_standar</option>";
                              }else{
                                echo "<option value='$key->substandar_id'>$key->substandar_kode - $key->namasub_standar</option>";  
                              }
                              
                            }
                          ?>
                        </select>
                      </div>


                      <div class="form-group">
                        <label for="exampleInputEmail1">Kode Sub Program</label>
                        <input type="text" name="kodesub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Sub Program" value="<?php echo $row->program_kode;  ?>" readonly="readonly">

                        <input type="hidden" name="programid" required="required" class="form-control" id="exampleInputEmail1" placeholder="Kode Sub Program" value="<?php echo $row->program_id;  ?>" readonly="readonly">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sub Program</label>
                        <input type="text" name="namasub" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Sub Standar Nasional" value="<?php echo $row->nama_program;  ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Komponen Pembiayaan</label>
                        <select class="form-control select2" name="biaya[]" multiple="multiple" data-placeholder="Komponen Pembiayaan" style="width: 100%;" required="required">
                          <?php
                            $k = $row->komponen_kode;
                            $pecah = explode(',',$k);
                            foreach ($biaya as $keys) {
                              if(in_array($keys->komponen_kode, $pecah)){$tempel= "selected='selected'";}else{$tempel='';}
                              echo "<option value='$keys->komponen_kode' $tempel>$keys->komponen_kode - $keys->nama_komponen</option>";
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($row->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                      $(".select2").select2();
                          
                  });
                </script>