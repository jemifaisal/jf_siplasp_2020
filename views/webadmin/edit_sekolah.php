                <?php
                  foreach ($sekolah as $key) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('webadmin/proses_edit_sekolah'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">NPSN</label>
                        <input type="text" name="npsn" required="required" class="form-control" id="exampleInputEmail1" placeholder="NPSN" value="<?php echo $key->npsn; ?>">
                        <input type="hidden" name="sid" required="required" class="form-control" id="exampleInputEmail1" placeholder="NPSN" value="<?php echo $key->sekolah_id; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sekolah</label>
                        <input type="text" name="namasp" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Sekolah" value="<?php echo $key->nama_sp; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Jenjang</label>
                        <select name="jenjang" class="form-control">
                          <?php
                            if($key->jenjang == "SDLB"){
                              echo "<option value='SDLB' selected='selected'>SDLB</option>
                                    <option value='SMPLB'>SMPLB</option>
                                    <option value='SMALB'>SMALB</option>
                                    <option value='SLB'>SLB</option>
                                    <option value='SMA'>SMA</option>
                                    <option value='SMK'>SMK</option>";

                            }elseif ($key->jenjang == "SMPLB") {
                              # code...
                              echo "<option value='SDLB' >SDLB</option>
                                    <option value='SMPLB' selected='selected'>SMPLB</option>
                                    <option value='SMALB'>SMALB</option>
                                    <option value='SLB'>SLB</option>
                                    <option value='SMA'>SMA</option>
                                    <option value='SMK'>SMK</option>";
                            }elseif ($key->jenjang == "SMALB") {
                              # code...
                              echo "<option value='SDLB' >SDLB</option>
                                    <option value='SMPLB'>SMPLB</option>
                                    <option value='SMALB' selected='selected'>SMALB</option>
                                    <option value='SLB'>SLB</option>
                                    <option value='SMA'>SMA</option>
                                    <option value='SMK'>SMK</option>";
                            }elseif ($key->jenjang == "SLB") {
                              # code...
                              echo "<option value='SDLB' >SDLB</option>
                                    <option value='SMPLB'>SMPLB</option>
                                    <option value='SMALB'>SMALB</option>
                                    <option value='SLB' selected='selected'>SLB</option>
                                    <option value='SMA'>SMA</option>
                                    <option value='SMK'>SMK</option>";
                            }elseif ($key->jenjang == "SMA") {
                              # code...
                              echo "<option value='SDLB' >SDLB</option>
                                    <option value='SMPLB'>SMPLB</option>
                                    <option value='SMALB'>SMALB</option>
                                    <option value='SLB'>SLB</option>
                                    <option value='SMA' selected='selected'>SMA</option>
                                    <option value='SMK'>SMK</option>";
                            }else{
                              echo "<option value='SDLB' >SDLB</option>
                                    <option value='SMPLB'>SMPLB</option>
                                    <option value='SMALB'>SMALB</option>
                                    <option value='SLB'>SLB</option>
                                    <option value='SMA'>SMA</option>
                                    <option value='SMK' selected='selected'>SMK</option>";
                            }
                          ?>
                          
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Status Sekolah</label>
                        <select name="statussekolah" class="form-control">
                          <?php
                            if($key->status_sekolah == "Negeri"){
                              echo "<option value='Negeri' selected='selected'>Negeri</option>
                                    <option value='Swasta'>Swasta</option>";
                            }else{
                              echo "<option value='Negeri'>Negeri</option>
                                    <option value='Swasta' selected='selected'>Swasta</option>";
                            }
                          ?>
                          
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Akreditasi</label>
                        <select name="akreditasi" class="form-control">
                          <?php
                            if($key->akreditasi == "A"){
                              echo "<option value='A' selected='selected'>A</option>
                                    <option value='B'>B</option>
                                    <option value='C'>C</option>
                                    <option value='Belum Terakreditasi'>Belum Terakreditasi</option>";
                            }elseif ($key->akreditasi =="B") {
                              # code...
                              echo "<option value='A'>A</option>
                                    <option value='B' selected='selected'>B</option>
                                    <option value='C'>C</option>
                                    <option value='Belum Terakreditasi'>Belum Terakreditasi</option>";
                            }elseif ($key->akreditasi == "C") {
                              # code...
                              echo "<option value='A'>A</option>
                                    <option value='B'>B</option>
                                    <option value='C' selected='selected'>C</option>
                                    <option value='Belum Terakreditasi'>Belum Terakreditasi</option>";
                            }else{
                              echo "<option value='A'>A</option>
                                    <option value='B'>B</option>
                                    <option value='C'>C</option>
                                    <option value='Belum Terakreditasi' selected='selected'>Belum Terakreditasi</option>";
                            }
                          ?>
                          
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kecamatan</label>
                        <select name="kec" class="show-tick form-control" data-live-search="true" id="e1">
                          <?php
                            foreach ($kec as $row) {
                              if($key->kd_kec == $row->kd_kec){
                                echo "<option value='$row->kd_kec' data-subtext='$row->nama_kab' selected='selected'>$row->nama_kec</option>";
                              }else{
                                echo "<option value='$row->kd_kec' data-subtext='$row->nama_kab'>$row->nama_kec</option>";  
                              }
                              
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="text" name="paswd" class="form-control" id="exampleInputEmail1" placeholder="Password" >
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($key->status == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
            $(document).ready(function () {
                $("#e1").selectpicker();
                    
            });
          </script>
