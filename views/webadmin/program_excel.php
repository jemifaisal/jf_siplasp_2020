<?php
	// Fungsi header dengan mengirimkan raw data excel
	header("Content-type: application/vnd-ms-excel");
	// Mendefinisikan nama file ekspor "hasil-export.xls"
	$nama = time();
	header("Content-Disposition: attachment; filename=".$nama.".xls");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Program Excel Download</title>
</head>
<body>
	<table border="1">
		<tr>
			<td rowspan="2">No.</td>
			<td colspan="2">Standar Nasional</td>
			<td colspan="2">Substandar</td>
			<td colspan="2">Program</td>
			<td rowspan="2">Komponen Pembiayaan</td>
		</tr>
		<tr>
			<td>Kode</td>
			<td>Nama</td>
			<td>Kode</td>
			<td>Nama</td>
			<td>Kode</td>
			<td>Nama</td>
		</tr>

		<?php
			$no=0;
			foreach ($program as $rows) {
				$no++;
				echo "<tr>
					<td>$no</td>
					<td>$rows->standar_kode</td>
					<td>$rows->nama_standar</td>
					<td>$rows->substandar_kode</td>
					<td>$rows->namasub_standar</td>
					<td>$rows->program_kode</td>
					<td>$rows->nama_program</td>
					<td>$rows->komponen_kode</td>
				</tr>";
			}
		?>
	</table>

</body>
</html>