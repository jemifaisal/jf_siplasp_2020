<!-- Header -->
<div class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>Selamat Datang</h1>
                        <h3>Sistem Pelayanan Satuan Pendidikan Provinsi Riau</h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons"> 
                            <li>
                                <a href="<?php echo base_url('sekolah'); ?>" class="btn btn-default btn-lg"><i class="fa fa-login"></i> <span class="network-name">Klik Untuk Login</span></a>
                            </li>
                        </ul>

                        

                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

    <div class="content-section-a">
        <div class="container">
            <div class="row">
              <div class="col-lg-12 text-center">
                <h2 class="section-heading">Fitur yang tersedia</h2>
                <hr class="my-4">
              </div>
            </div>
          </div>
        <div class="container">
            
            <div class="row">
                <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                      <i class="fa fa-4x fa-table text-primary mb-3 sr-icons"></i>
                      <h3 class="mb-3">RKA Sekolah</h3>
                      <p class="text-muted mb-0">Sekolah dapat menginput RKA Sekolah secara online.</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                      <i class="fa fa-4x fa-trophy text-primary mb-3 sr-icons"></i>
                      <h3 class="mb-3">Prestasi Sekolah</h3>
                      <p class="text-muted mb-0">Sekolah dapat menambahkan prestasi yang pernah di raih oleh siswa/i.</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                      <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
                      <h3 class="mb-3">Ruang Kelas dan Labor</h3>
                      <p class="text-muted mb-0">Sekolah dapat melaporkan kondidi Ruang Kelas dan Laboratorium yang ada disekolah.</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                      <i class="fa fa-4x fa-user text-primary mb-3 sr-icons"></i>
                      <h3 class="mb-3">Peserta Didik</h3>
                      <p class="text-muted mb-0">Sekolah dapat melihat jumlah siswa/i yang menerima BOS dan terkoneksi ke RKA Sekolah.</p>
                    </div>
                  </div>

                  <div class="col-lg-4 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                      <i class="fa fa-4x fa-users text-primary mb-3 sr-icons"></i>
                      <h3 class="mb-3">Guru Mata Pelajaran</h3>
                      <p class="text-muted mb-0">Sekolah dapat melakukan penginputan data Mata Pelajaran dari masing-masing guru PNS/Non PNS.</p>
                    </div>
                  </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->