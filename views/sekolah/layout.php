<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/img/favicon.png'); ?>">
  <title>Operator Sekolah</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/bootstrap/css/bootstrap.min.css">
  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url(); ?>assets/webadmin/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/sweetalert2/sweetalert2.min.css">
  <!-- SweetAlert2 -->
  <script src="<?php echo base_url(); ?>assets/webadmin/sweetalert2/sweetalert2.min.js"></script>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/datatables/dataTables.bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/bootstrap-select/dist/css/bootstrap-select.css">
  
  <link href="<?php echo base_url(); ?>assets/webadmin/easyui/themes/default/easyui.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/webadmin/easyui/themes/icon.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url(); ?>assets/webadmin/easyui/demo/demo.css" rel="stylesheet"> -->
  <script src="<?php echo base_url(); ?>assets/webadmin/easyui/jquery.easyui.min.js"></script>

  
<!-- Bootstrap 3.3.6 -->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('sekolah'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>O</b>PS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>OP</b>Sekolah</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>assets/webadmin/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('token_nama_sekolah'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url(); ?>assets/webadmin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Operator Sekolah
                </p>
              </li>
              <!-- Menu Body -->
              
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('sekolah/ubahpassword'); ?>" data-id='<?php echo $this->session->userdata('token_sekolah_id'); ?>' id="ubahpass" class="btn btn-default btn-flat">Ubah Password</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('sekolah/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/webadmin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('token_tahun'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <li class="active treeview">
          <a href="<?php echo base_url('sekolah'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li><a href="<?php echo base_url('sekolah/jumlahpd'); ?>"><i class="fa fa-user"></i> <span>Jumlah Peserta Didik</span></a></li>
        <li><a href="<?php echo base_url('sekolah/pejabat'); ?>"><i class="fa fa-users"></i> <span>Pejabat TTD RKAS</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i>
            <span>RKAS BOSNAS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('sekolah/barangjasa'); ?>"><i class="fa fa-circle-o"></i> Belanja Barang dan Jasa</a></li>
            <li><a href="<?php echo base_url('sekolah/modal'); ?>"><i class="fa fa-circle-o"></i> Belanja Modal</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i>
            <span>Laporan BOS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <span class="label label-danger">New</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('sekolah/bku'); ?>"><i class="fa fa-circle-o"></i> Input BKU</a></li>
            <li><a href="<?php echo base_url('sekolah/databku'); ?>"><i class="fa fa-circle-o"></i> Data BKU (Form K3)</a></li>
            <li><a href="<?php echo base_url('sekolah/bpk'); ?>"><i class="fa fa-circle-o"></i> Data BPK (Form K4)</a></li>
            <li><a href="<?php echo base_url('sekolah/bpb'); ?>"><i class="fa fa-circle-o"></i> Data BPB (Form K5)</a></li>
            <li><a href="<?php echo base_url('sekolah/bpp'); ?>"><i class="fa fa-circle-o"></i> Data BPP (Form K6)</a></li>
            <li><a href="<?php echo base_url('sekolah/realisasi'); ?>"><i class="fa fa-circle-o"></i> Realisasi Dana BOS</a></li>
            
          </ul>
        </li>
        <li><a href="<?php echo base_url('sekolah/cetakrkas'); ?>"><i class="fa fa-print"></i> <span>Cetak RKA Sekolah</span></a></li>
        <li><a href="<?php echo base_url('sekolah/prestasi'); ?>"><i class="fa fa-book"></i> <span>Prestasi</span></a></li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i>
            <span>ASET Sekolah</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('sekolah/kibb'); ?>"><i class="fa fa-circle-o"></i> KIB B</a></li>
            <li><a href="<?php echo base_url('sekolah/kibe'); ?>"><i class="fa fa-circle-o"></i> KIB E</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i>
            <span>Sarana Prasarana</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('sekolah/ruangkelas'); ?>"><i class="fa fa-circle-o"></i> Ruang Kelas</a></li>
            <li><a href="<?php echo base_url('sekolah/ruanglabor'); ?>"><i class="fa fa-circle-o"></i> Ruang Labor / Praktek</a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Guru</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('sekolah/gurumapel'); ?>"><i class="fa fa-circle-o"></i> Guru Mapel</a></li>
            <li><a href="<?php echo base_url('sekolah/gurutambahan'); ?>"><i class="fa fa-circle-o"></i> Guru Tambahan</a></li>
            
          </ul>
        </li>
        
        <li><a href="<?php echo base_url('sekolah/tamsil'); ?>"><i class="fa fa-book"></i> <span>TAMSIL Pegawai</span></a></li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $jh1; ?>
        <small><?php echo $jhsmall; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <?php if(isset($bc)){echo $bc;}; ?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!--<div class="callout callout-info">
        <p>Kualitas data atau informasi berdasarkan dari data yang di inputkan. Jadi telitilah dalam melakukan penginputan data. </p>
      </div>-->

      <?php $info = $this->session->flashdata('info'); if(isset($info)){echo $info;} ?>

      <!-- Content Dinamis -->
      <?php $this->load->view($conten); ?>
      <!-- end Content Dinamis -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017 <a href="#">Dinas Pendidikan Provinsi Riau</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->
        <div id="ubpass" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ubah Password</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/webadmin/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/webadmin/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/webadmin/dist/js/demo.js"></script>

<script src="<?php echo base_url(); ?>assets/webadmin/bootstrap-select/dist/js/bootstrap-select.js"></script>
<script>
  $(document).on('click','#ubahpass',function(e){
    e.preventDefault();
    $("#ubpass").modal('show');
    $.post("<?php echo base_url('sekolah/ubahpassword'); ?>",
        {id:$(this).attr('data-id')},
        function(html){
            $(".modal-body").html(html);
        }   
    );
  });
</script>
<?php
  if(isset($js)){
    $this->load->view($js);
  }
?>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      defaultViewDate: "today"
    });
    $('#datepicker2').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $("#timepicker").timepicker({
      showInputs: false,
      showMeridian: false
    });
  });
</script>
</body>
</html>
