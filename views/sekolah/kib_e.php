<!-- Default box -->
<div id="formaksi" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Form Manajemen KIB E</h4>
        </div>
        <div class="modal-body">
          

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    <!-- /.modal-content -->
   </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
  function doSearch(){
        $('#tt').datagrid('load',{
            kodebarang: $('#kode_barang').val(),
            namabarang: $('#nama_barang').val()
        });
    }
  
  function tombol(val,row){
        var idedit  = '2,'+val;
        var idhapus = '3,'+val;
        return '<a href="#" id="tombol" class="btn btn-warning btn-xs" data-id="'+idedit+'">Edit</a> <a href="#" class="btn btn-danger btn-xs" id="tombol" data-id="'+idhapus+'">Hapus</a>';
  }
</script>
<div class="box">

  <div class="box-header with-border">

    <h3 class="box-title">Data Aset Sekolah - KIB-E</h3>
  </div>
  <div class="box-body">
    <div class="alert alert-warning alert-dismissable" style="border:3px solid blue; border-style: dashed;"> 
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <strong><u>PENGUMUMAN: </u></strong> <br>
        Pada tanggal <b>22 Januari 2019 jam 01.00 WIB</b>, sistem aset sekolah dilakukan pembaharuan. Adapun pembaharuannya adalah pada <b>ASAL USUL</b> pada awalnya pengisiannya <b>DIKETIK MANUAL</b>, setelah pembaharuan sistem untuk <b>ASAL USUL</b> disediakan dalam bentuk pilihan. Atas perhatiannya kami ucapkan terima kasih.
    </div>
    <a href="#" id="tombol" data-id="1,0" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah Data</a>
            <div class="box-body">
                <p align="right">
                <?php
                  foreach ($totalaset as $key) {
                    if($key->nama_sumber_dana == "BOSNAS"){
                      $warna = "btn btn-danger btn-xs";
                    }else{
                      $warna = "btn btn-warning btn-xs";
                    }
                    echo "<p>Total Aset KIB-E tahun <span class='btn btn-success btn-xs'>$key->tahun_pembelian </span> <span class='$warna'>$key->nama_sumber_dana</span> <span class='btn btn-primary btn-xs'>Rp. ".number_format($key->total,0)."</span></p>"; 
                  }
                ?>
              </p>
              <table id="tt" class="easyui-datagrid" style="width:100% auto;height:550px auto;"
      url="<?php echo site_url("sekolah/datakibe") ?>"
      title="Data KIB E" iconCls="icon-user" toolbar="#tb" pageSize="10"
      rownumbers="true" pagination="true" method="post" singleSelect="true">
                <thead>
                  <tr>
                      <th field="id_new" formatter='tombol' rowspan="2">Action</th>
                      <th field="kode_barang" rowspan="2">Kode Barang</th>
                      <th field="no_register" rowspan="2">No. Register</th>
                      <th field="nama_barang" rowspan="2">Nama Barang</th>
                      <!--<th field="nama_lokasi" rowspan="2">Lokasi Barang</th>-->
                      <th data-options="field:'kondisi',align:'center'" rowspan="2">Kondisi <br>(B, RR, RB)</th>
                      <th field="judul" rowspan="2">Buku / Perpustakaan / Judul</th>
                      <th field="spesifikasi" rowspan="2">Spesifikasi</th>
                      <th colspan="3">Barang Bercorak<br>Kesenian / Kebudayaan</th>
                      <th data-options="field:'jumlah',align:'center'" rowspan="2">Jumlah Barang</th>
                      <th field="tahun_pembelian" rowspan="2">Tahun Cetak/<br>Pembelian</th>
                      <th field="nama_sumber_dana" rowspan="2">Asal / Usul</th>
                      <th field="harga_perolehan" rowspan="2">Harga Perolehan (Rp)</th>
                      <!--<th colspan="2">Harga</th>-->
                      <th field="total_harga" rowspan="2">Total Harga</th>
                      <th field="ket" rowspan="2">Keterangan</th>

                  </tr>
                  <tr>
                      <th field="asal_daerah" >Asal Daerah</th>
                      <th field="pencipta" >Pencipta</th>
                      <th field="bahan" >Bahan</th>
                      <th field="harga_perolehan" >Perolehan</th>
                      <th field="harga_estimasi" >Estimasi</th>
                  </tr>
                </thead>
            </table>
            <div id="tb" style="padding:3px">
                <span><b>Cari Berdasrkan:</b><br></span>
                <div class="divider-dashed"></div>
                <span>Kode Barang: </span>
                <input id="kode_barang" style="line-height:22px;border:1px solid #ccc" onkeyup="doSearch()">
                <span>Nama Barang: </span>
                <input id="nama_barang" style="line-height:22px;border:1px solid #ccc" onkeyup="doSearch()">
                <div class="divider-dashed"></div>
              </div>
            </div>
          </div>

  </div>


<script>

  $(document).on('click','#tombol',function(e){
      e.preventDefault();
      $("#formaksi").modal('show');
      $.post("<?php echo base_url('sekolah/aksi_kibe'); ?>",
          {id:$(this).attr('data-id')},
          function(html){
              $(".modal-body").html(html);
          }   
      );
    });
</script>


