<?php
	$sid = $this->session->userdata('token_sekolah_id');
	$tahun = $this->session->userdata('token_tahun');
	$data = $this->sekolah_model->total_bos_sekolah($sid,$tahun);
?>
<!-- Default box -->
<div class="box">
<div class="row">
	<div class="box-body">
		<div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-aqua"><i class="fa fa-calendar"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TAHUN</span>
	              <span class="info-box-number"><?php echo $tahun; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>
	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">JUMLAH SISWA</span>
	              <span class="info-box-number"><?php echo $data['jumlah_pd']; ?><small> Orang</small></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TOTAL DANA BOS</span>
	              <span class="info-box-number"><?php echo $data['total']; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">SISA UANG</span>
	              <span class="info-box-number"><?php echo $data['sisa_uang']; ?></span>
	              <span class="info-box-number"><?php echo "Triwulan: " .$data['triwulan']; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>
	</div>
    <!-- /.box-body -->
</div>

<div class="row">
	<div class="box-body">
		<div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN I</span>
	              <span class="info-box-number"><?php echo $data['tw1']; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN II</span>
	              <span class="info-box-number"><?php echo $data['tw2']; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN III</span>
	              <span class="info-box-number"><?php echo $data['tw3']; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN IV</span>
	              <span class="info-box-number"><?php echo $data['tw4']; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>
	</div>
    <!-- /.box-body -->
</div>
</div>
<!-- /.box -->