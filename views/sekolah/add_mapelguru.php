                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/add_mapelguru'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mata Pelajaran</label>
                        <select name="mapel" class="form-control select2" style="width: 100%;" data-placeholder="Pilih Data" required="required">
                          <?php
                            foreach ($mapel as $keys) {
                              echo "<option value='$keys->kd_mapel'>$keys->nama_mapel</option>";
                            }
                          ?>
                        </select>
                        <input type="hidden" name="gtkid" value="<?php echo $gtkid; ?>">
                        <input type="hidden" name="link" value="<?php echo $link; ?>">
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                    $('#tgllahir').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today"
                    }); 

                    $('.select2').select2();   
                          
                  });
                </script>
