<!-- Default box -->

<div class="box">

  <div class="box-header with-border">

    <h3 class="box-title">Data Prestasi Sekolah</h3>
  </div>
  <div class="box-body">
    <a href="#" class="btn btn-success" data-id='0' id="prestasi"><i class="fa fa-plus"></i> Prestasi</a>
            <div class="box-body">
              <table id="pres" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tahun</th>
                  <th>Nama Lomba</th>
                  <th>Cabang Lomba</th>
                  <th>Ket. Lomba</th>
                  <th>Jenis Lomba</th>
                  <th>Tingkat</th>
                  <th>Peringkat</th>
                  <th>Nama Siswa</th>
                  <th>Tempat Lomba</th>
                  <th>Penyelenggara</th>
                  <th>Tgl. Mulai</th>
                  <th>Tgl. Akhir</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Tahun</th>
                  <th>Nama Lomba</th>
                  <th>Cabang Lomba</th>
                  <th>Ket. Lomba</th>
                  <th>Jenis Lomba</th>
                  <th>Tingkat</th>
                  <th>Peringkat</th>
                  <th>Nama Siswa</th>
                  <th>Tempat Lomba</th>
                  <th>Penyelenggara</th>
                  <th>Tgl. Mulai</th>
                  <th>Tgl. Akhir</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>

  </div>

</div>

<div id="addprestasi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Prestasi Sekolah</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script>
  
            var save_method; //for save method string
            var table;

            $(document).ready(function() {
                //datatables
                table = $('#pres').DataTable({ 
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [[0,"desc"]], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('sekolah/jsonn/'); ?>",
                        "type": "POST"
                    },
                    //Set column definition initialisation properties.
                    "columns": [
                        {"data": "tahun"},
                        {"data": "namalomba"},
                        {"data": "nama_katlomba"},
                        {"data": "uraian"},
                        {"data": "jenis_lomba"},
                        {"data": "tingkat_lomba"},
                        {"data": "peringkat"},
                        {"data": "nama_siswa"},
                        {"data": "tempat_lomba"},
                        {"data": "penyelenggara"},
                        {"data": "tgl_mulai"},
                        {"data": "tgl_akhir"},
                        {"data": "prestasi_id","orderable":false,
                            "mRender": function (data) {
                                return '<a href="#" class="btn btn-warning btn-xs" data-id="'+data+'" id="prestasi">Edit</a> \n\
                                <a href="<?php echo base_url('sekolah/hapus_prestasi')."/" ?>'+ data +'" onclick="javascript:return confirm(\'Anda yakin?\');" class="btn btn-danger btn-xs">Delete</a>';
                            }
                        }
                    ],

                });

            });
</script>

<script>

              $(document).on('click','#prestasi',function(e){
                e.preventDefault();
                $("#addprestasi").modal('show');
                $.post("<?php echo base_url('sekolah/tambahprestasi'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });
</script>


