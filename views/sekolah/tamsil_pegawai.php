<script type="text/javascript">
  $(document).ready(function(){
    var ukuran = 0;

    $("#lampiran").bind('change',function(){ukuran = this.files[0].size/1024/600;if(eval(ukuran)>1){$("#lampiran").val('');$("#lampiran").focus;$("#n1").html('[ Maaf Ukuran File Melebihi batas yaitu 600 KB ]');$("#y1").html('');return false;
        }else{$("#y1").html('[ Lampiran Diterima, silahkan klik tombol Upload File ]');$("#n1").html('');return true;}});
  });
</script>
<?php
  $tahun = date('Y');
  $sid = $this->session->userdata('token_sekolah_id');
?>
         <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Tamsil Pegawai</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="alert alert-danger alert-dismissable" style="border:3px solid blue; border-style: dashed;">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong><u>PENGUMUMAN: </u></strong> <br>
            Penginputan TAMSIL Pegawai akan berakhir pada hari <B>JUM'AT</B> tanggal <b>19 Juni 2020</b> pukul <b>23.59 WIB.</b><BR>
            Informasi lebih lanjut mengenai TAMSIL Pegawai:<BR>
            <b>Wandi : 0812-6864-913</b>
        </div>

          <p><a href="#" class="btn btn-primary" id="aksi" data-id="1"><i class="fa fa-plus-circle"></i> Tambah Usulan Pegawai</a> 
          [ Klik <a href="<?php echo base_url('assets/lampiran/tahapan-input-tamsil.pdf'); ?>" target="_blank" class="btn btn-danger btn-xs">Disini</a> sebelum melakukan penginputan. ]</p>
          
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Upload File Tamsil</h3>
            </div>
            <div class="box-body">
              <?php 
                if($info_upload == 1){
              ?>
              <form enctype="multipart/form-data" method="post" action="<?php echo base_url('sekolah/upload_file_tamsil'); ?>">
                <div class="form-group">
                  <!-- <label for="exampleInputEmail1">Upload File Tamsil:</label> -->
                  <input class="form-control" type="file" accept="application/pdf" name="lampiran" id="lampiran" required="required">
                  <div class="required"><i>format file (.pdf), size maksimal 600 KB</i></div>
                  <p>
                  <span id="n1" style="color:red;font-weight:bold;"></span>
                  <span id="y1" style="color:green;font-weight:bold;"></span>
                  </p>
                  <button type="submit" class="btn btn-success btn-sm" id="btn_upload"><i class="fa fa-upload"></i> Upload File</button>
                  <?php
                    if($alert_tamsil == 1){
                      echo "<span style='color:green; font-size:15px;'>[ File Tamsil Sudah Diupload ], <a href='".base_url('assets/filetamsil/'.$file_tamsil)."' target='_blank'>Lihat</a></span>";
                    }else{
                      echo "<span style='color:red;font-size:15px;'>[ Anda Belum Mengupload File Tamsil ]</span>";
                    }
                  ?>
                </div>
              </form>

              <?php
                }else{
              ?>

                <p style="font-size: 20px; color:red;font-weight:bold;"><i>[ TOMBOL UPLOAD FILE TAMSIL TERSEDIA, APABILA ANDA SUDAH MENGENTRI DAFTAR USULAN PEGAWAI ]</i></p>

              <?php
                }
              ?>
              * <a href="<?php echo base_url("assets/lampiran/format-usulan-tamsil-pegawai-2020.docx"); ?>">Download Format Usulan TAMSIL Pegawai (.docx)</a><br>
              * <a href="<?php echo base_url("assets/lampiran/contoh-upload-file-format-usulan-tamsil-pegawai-2020.pdf"); ?>" target="_blank">Contoh Usulan TAMSIL Pegawai yang Diupload (.pdf)</a>
            </div>
          </div>

          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Usulan Tamsil Pegawai</h3><br>
              <a href="<?php echo base_url('sekolah/downloadtamsil/'.$sid.'/'.$tahun.'/'.$periode); ?>" class="btn btn-warning btn-xs"><span class="fa fa-download"></span> Download Usulan TAMSIL Pegawai</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th>Action</th>
                  <!-- <th>No</th> -->
                  <th>NIP</th>
                  <th>NUPTK</th>
                  <th>Nama Guru</th>
                  <th>Golongan</th>
                  <th>PDD. Terakhir</th>
                  <th>Jurusan</th>
                  <th>Jumlah Jam Mengajar</th>
                  <th>Mata Pelajaran</th>
                  <th>No. Rekening Bank Riau Kepri</th>
                  <th>Nama Pada Rekening</th>
                  <th>No. HP</th>
                  <th>Keterangan</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($pns as $row) { ?>
                <tr>
                  <td><a href="#" class="btn btn-success btn-xs" id="aksi" data-id="2,<?php echo $row->tamsil_id; ?>">Ubah</a> <a href="<?php echo base_url('sekolah/hapus_tamsil/'.$row->tamsil_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                  <!-- <td><?php echo $no++; ?></td> -->
                  <td><?php echo $row->nip; ?></td>
                  <td><?php echo $row->nuptk; ?></td>
                  <td><?php echo $row->gelar_depan.$row->nama_lengkap.", ".$row->gelar_belakang; ?></td>
                  <td><?php echo $row->golongan; ?></td>
                  <td><?php echo $row->nama_pdd; ?></td>
                  <td><?php echo $row->jurusan; ?></td>
                  <td><?php echo $row->jumlah_jam_mengajar; ?></td>
                  <td><?php echo $row->mapel; ?></td>
                  <td><?php echo $row->no_rekening; ?></td>
                  <td><?php echo $row->nama_rekening; ?></td>
                  <td><?php echo $row->no_hp; ?></td>
                  <td><?php echo $row->keterangan; ?></td>
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_tamsil'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
