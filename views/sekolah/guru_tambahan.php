        <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Guru Tambahan</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Guru Tambahan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Status Guru</th>
                  <th>NIP</th>
                  <th>NUPTK</th>
                  <th>Nama Guru</th>
                  <th>TTL</th>
                  <th>PDD. Terakhir</th>
                  <th>Pangkat / Golongan</th>
                  <th>Email</th>
                  <th>No. HP</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($guru as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->status_gtk; ?></td>
                  <td><?php echo $row->nip; ?></td>
                  <td><?php echo $row->nuptk; ?></td>
                  <td><?php echo $row->gelar_depan.$row->nama_lengkap.", ".$row->gelar_belakang; ?></td>
                  <td><?php echo $row->tempat_lahir." / ".$row->tgl_lahir; ?></td>
                  <td><?php echo $row->jurusan; ?></td>
                  <td><?php echo $row->kd_pangkat; ?></td>
                  <td><?php echo $row->email; ?></td>
                  <td><?php echo $row->no_hp; ?></td>

                  <td><a href="<?php echo base_url('sekolah/mapelgurutambahan/'.$row->gtk_id); ?>" class="btn btn-primary btn-xs">Mapel</a></td>
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          

          <script>
            $(function () {
              $("#example1").DataTable();
              $("#example2").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_gurumapel'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
