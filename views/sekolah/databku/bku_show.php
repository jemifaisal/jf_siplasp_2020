<a href="<?php echo base_url('sekolah/cetaklaporan/'.$bulan_id.'/bku'); ?>" class="btn btn-success"><i class='fa fa-download'></i> Excel</a>
<table border='0' width='100%'>
    <tr>
        <td colspan='3'><span class="lead"><center>BUKU KAS UMUM</center></span></td>
    </tr>
    <tr>
        <td colspan='3'><center>Bulan: <?php echo $bulan. ' '.$tahun; ?></center></td>
    </tr>
    <tr>
        <td width='150'>Nama Sekolah</td>
        <td width='2'>:</td>
        <td> <?php echo $profil['nama_sp']; ?> </td>
    </tr>
    <tr>
        <td>Desa/Kecamatan</td>
        <td>:</td>
        <td> <?php echo $profil['nama_kec']; ?></td>
    </tr>
    <tr>
        <td>Kabupaten/Kota</td>
        <td>:</td>
        <td> <?php echo $profil['nama_kab']; ?></td>
    </tr>
    <tr>
        <td>Provinsi</td>
        <td>:</td>
        <td> Riau</td>
    </tr>
</table>
<br>
<table class="table table-bordered table-hover table-responsive">
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Kode Rekening</th>
        <th>No. Kode</th>
        <th>No. Bukti</th>
        <th>Uraian</th>
        <th>Penerimaan<br> (Debit)</th>
        <th>Pengeluaran<br> (Kredit)</th>
        <th>Saldo</th>
    </tr>
    <?php
        $no = 0;
        $saldo = $saldo_awal;
        if($bulan_id != 1){
            echo "<tr style='font-weight:bold;color:blue;' bgcolor='#ddd'>
                <td colspan='7'>SALDO BULAN LALU</td>
                <td></td>
                <td>".number_format($saldo,0)."</td>
            </tr>";
        }
        foreach($bku as $bkus){
            $no ++;
            if($bkus->program_kode == NULL){
                $dot = "";
            }else{
                $dot = ".";
            }
            echo "<tr>
                <td>$no</td>
                <td>".date("d M Y",strtotime($bkus->tanggal_transaksi))."</td>
                <td>$bkus->rekening_belanja</td>
                <td>$bkus->program_kode$dot$bkus->kode_komponen</td>
                <td>$bkus->kode_bukti $bkus->no_urut_bukti</td>
                <td>$bkus->uraian_bku</td>";
            if($bkus->kelompok == "D"){
                $param = '+';
                echo "<td class='success'>$bkus->jumlah_transaksi_format</td>
                    <td class='warning'></td>";
                $saldo = $saldo + $bkus->jumlah_transaksi;
            }else{
                $param = '-';
                echo "<td class='success'></td>
                    <td class='warning'>$bkus->jumlah_transaksi_format</td>";
                $saldo = $saldo - $bkus->jumlah_transaksi;
            }

            
            echo "<td class='info'>".number_format($saldo,0)."</td></tr>";
        }
    ?>
</table>
<br>
<table border='0' width='100%'>
    <tr>
        <td width='230'>Saldo BKU Bulan <?php echo $bulan; ?> Sebesar</td>
        <td width='2'>:</td>
        <td> <?php echo number_format($saldo,0); ?> </td>
    </tr>
    <tr>
        <td>Terdiri Dari</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Saldo Tunai</td>
        <td>:</td>
        <td>
            <form method="post" id="simpan_sisa_uang">
                <div class="input-group col-md-4">
                    <input type="hidden" name="bulan_id" value="<?php echo $bulan_id; ?>">
                    <input type="hidden" name="saldo_bulan" value="<?php echo $saldo; ?>">
                    <input type="text" class="form-control" onkeyup="convertToRupiah(this)" placeholder="Saldo Tunai" name="saldo_tunai" value="<?php echo $saldo_tunai['jumlah_saldo']; ?>" required="required">
                    <div class="input-group-btn">
                        <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
        </td>
    </tr>
    <tr>
        <td>Saldo Bank</td>
        <td>: </td>
        <td><span id="txt_saldo"> <?php echo number_format($saldo - $saldo_tunai['jumlah_saldo'],0); ?></span></td>
    </tr>
</table>
<br>
<table border='0' width='100%'>
    <tr>
    <tr>
        <td align='center' width='45%'>
            Mengetahui<br>
            Kepala <?php echo $profil['nama_sp']; ?>
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['kepsek']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_kepsek']; ?>
        </td>
        <td width='10%'></td>
        <td align='center' width='45%'>
            ................., <?php $hari_ini = $tahun.'-'.$bulan_id; echo date('t', strtotime($hari_ini)); echo ' '. $bulan.' '. $tahun; ?> <br>
            Bendahara Dana BOS
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['nama_bendahara_bos']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_bendahara_bos']; ?>
        </td>
    </tr>
    </tr>
</table>

<script>
    $('#simpan_sisa_uang').on('submit', function(e){
        if(!e.isDefaultPrevented()){
            var url = "<?php echo base_url("sekolah/aksi_simpan_saldo"); ?>";
            var inputData = new FormData($(this)[0]);
            $.ajax({
                url: url,
                type: 'post',
                data: inputData,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,
                success: function(data){
                    swal({
                        title: 'Berhasil',
                        text: 'Data berhasil disimpan',
                        type: 'success',
                        timer: 1500
                    }).catch(swal.noop);
                    $('#txt_saldo').html(data.saldo_bank);
                },
                error: function(){
                    swal({
                        title: 'Error!',
                        text: 'Data gagal disimpan',
                        type: 'error',
                        timer: 1500
                    }).catch(swal.noop);
                }
            });
            return false;
        }
    });

    function convertToRupiah(objek) {
        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length; 
        j = 0; 
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i-1,1) + separator + c;
            } else {
                c = b.substr(i-1,1) + c;
            }
        }
        objek.value = c;
    }
</script>
