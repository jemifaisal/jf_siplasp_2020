<table border='0' width='100%'>
    <tr>
        <td colspan='9'><span class="lead"><center><h3>BUKU KAS UMUM</h3></center></span></td>
    </tr>
    <tr>
        <td colspan='9' align="center"><center>Bulan: <?php echo $bulan. ' '.$tahun; ?></center></td>
    </tr>
    <tr>
        <td colspan="2" width='150'>Nama Sekolah</td>
        <td colspan="7">: <?php echo $profil['nama_sp']; ?> </td>
    </tr>
    <tr>
        <td colspan="2">Desa/Kecamatan</td>
        <td colspan="7">: <?php echo $profil['nama_kec']; ?></td>
    </tr>
    <tr>
        <td colspan="2">Kabupaten/Kota</td>
        <td colspan="7">: <?php echo $profil['nama_kab']; ?></td>
    </tr>
    <tr>
        <td colspan="2">Provinsi</td>
        <td colspan="7">: Riau</td>
    </tr>
</table>
<br>
<table border="1" width="100%" class="table table-bordered table-hover table-responsive">
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Kode Rekening</th>
        <th>No. Kode</th>
        <th>No. Bukti</th>
        <th>Uraian</th>
        <th>Penerimaan<br> (Debit)</th>
        <th>Pengeluaran<br> (Kredit)</th>
        <th>Saldo</th>
    </tr>
    <?php
        $no = 0;
        $saldo = $saldo_awal;
        if($bulan_id != 1){
            echo "<tr style='font-weight:bold;color:blue;' bgcolor='#ddd'>
                <td colspan='7'>SALDO BULAN LALU</td>
                <td></td>
                <td>".$saldo."</td>
            </tr>";
        }
        foreach($bku as $bkus){
            $no ++;
            if($bkus->program_kode == NULL){
                $dot = "";
            }else{
                $dot = ".";
            }
            echo "<tr>
                <td>$no</td>
                <td>".date("d M Y",strtotime($bkus->tanggal_transaksi))."</td>
                <td>$bkus->rekening_belanja</td>
                <td>$bkus->program_kode$dot$bkus->kode_komponen</td>
                <td>$bkus->kode_bukti $bkus->no_urut_bukti</td>
                <td>$bkus->uraian_bku</td>";
            if($bkus->kelompok == "D"){
                $param = '+';
                echo "<td class='success'>$bkus->jumlah_transaksi</td>
                    <td class='warning'></td>";
                $saldo = $saldo + $bkus->jumlah_transaksi;
            }else{
                $param = '-';
                echo "<td class='success'></td>
                    <td class='warning'>$bkus->jumlah_transaksi</td>";
                $saldo = $saldo - $bkus->jumlah_transaksi;
            }

            
            echo "<td class='info'>".$saldo."</td></tr>";
        }
    ?>
</table>
<br>
<table border='0' width='100%'>
    <tr>
        <td colspan="3" width='230'>Saldo BKU Bulan <?php echo $bulan; ?> Sebesar</td>
        <td>: <?php echo $saldo; ?> </td>
    </tr>
    <tr>
        <td colspan="3">Terdiri Dari</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">Saldo Tunai</td>
        <td>:
            <?php echo $saldo_tunai['jumlah_saldo']; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3">Saldo Bank</td>
        <td>: <span id="txt_saldo"> <?php echo $saldo - $saldo_tunai['jumlah_saldo']; ?></span></td>
    </tr>
</table>
<br>
<table border='0' width='100%'>
    <tr>
    <tr>
        <td align='center' colspan="5">
            Mengetahui<br>
            Kepala <?php echo $profil['nama_sp']; ?>
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['kepsek']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_kepsek']; ?>
        </td>
        <td align='center' colspan="4">
            ................., <?php $hari_ini = $tahun.'-'.$bulan_id; echo date('t', strtotime($hari_ini)); echo ' '. $bulan.' '. $tahun; ?> <br>
            Bendahara Dana BOS
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['nama_bendahara_bos']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_bendahara_bos']; ?>
        </td>
    </tr>
    </tr>
</table>