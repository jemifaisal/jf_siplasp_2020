                <?php
                  foreach ($riwayat as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_mapelguru'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mata Pelajaran</label>
                        <select name="mapel" class="form-control select2" style="width: 100%;" data-placeholder="Pilih Data" required="required">
                          <?php
                            foreach ($mapel as $keys) {
                              if($row->kd_mapel == $keys->kd_mapel){
                                echo "<option value='$keys->kd_mapel' selected='selected'>$keys->nama_mapel</option>";
                              }else{
                                echo "<option value='$keys->kd_mapel'>$keys->nama_mapel</option>";  
                              }
                              
                            }
                          ?>
                        </select>
                        <input type="hidden" name="link" value="<?php echo $link; ?>">
                        <input type="hidden" name="kdmapel" value="<?php echo $row->kd_mapel; ?>">
                        <input type="hidden" name="gtkid" value="<?php echo $row->gtk_id; ?>">
                        <input type="hidden" name="riwayat" value="<?php echo $row->riwayatmapel_id; ?>">
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                    $('#tgllahir').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today"
                    }); 

                    $('.select2').select2();   
                          
                  });
                </script>
