<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Rincian Belanja</h3>
  </div>
  <div class="box-body">

      <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">SUB PROGRAM</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="bj" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Sub Standar</th>
                  <th>Nama Sub Standar</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($sub_standar as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->substandar_kode; ?></td>
                  <td><?php echo $row->namasub_standar; ?></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="rincian" data-id="<?php echo $row->substandar_id; ?>">Tambah Rincian</a> </td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama Kabupaten</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>

</div>

        <div id="tambahrincian" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Rincian Belanja</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>



    <script>
            $(document).ready(function () {
              $("#bj").DataTable();

              $(document).on('click','#rincian',function(e){
                e.preventDefault();
                $("#tambahrincian").modal('show');
                $.post("<?php echo base_url('webadmin/tambahrincian'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>

  </div>

</div>
