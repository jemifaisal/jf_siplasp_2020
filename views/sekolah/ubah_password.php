                <?php
                  foreach ($ubahp as $row) {
                    # code...
                  }
                ?>
                <script type="text/javascript">
                  function cekpw(){
                    var pbaru = document.getElementById('pbaru').value; 
                    var pbaruulang = document.getElementById('pbaruulang').value;
                    
                    if(pbaru != ""){
                      if(pbaru != pbaruulang){
                        document.getElementById('warning').innerHTML = "<font color='red'><i>Password Belum Sama</i></font>";
                        document.getElementById('simpan').style.display = "none";
                      }else{
                        document.getElementById('warning').innerHTML = "<font color='green'><i>Password Sudah Sama, Silahkan Disimpan.</i></font>";
                        document.getElementById('simpan').style.display = "block";
                      } 
                    }else{
                      document.getElementById('warning').innerHTML = "";
                      document.getElementById('simpan').style.display = "none";
                    }
                    
                  }
                </script>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_ubah_password'); ?>" enctype="multipart/form-data">
                    <div class="box-body">

                      <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
              
                        <input type="text" name="uname" required="required" class="form-control" readonly="readonly" placeholder="Username" value="<?php echo $row->npsn; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Password Lama</label>
                        <input type="password" name="plama" id="plama" required="required" class="form-control" placeholder="Password Lama" onkeyup="cekpw();">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Password Baru</label>
                        <input type="password" name="pbaru" id="pbaru" required="required" class="form-control" placeholder="Password Baru" onkeyup="cekpw();">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Ketik Ulang Password Baru</label>
                        <input type="password" name="pbaruulang" required="required" id="pbaruulang" class="form-control" placeholder="Password Baru" onkeyup="cekpw();">
                      </div>

                      <span id="warning"></span>                     
                      
                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" id="simpan" class="btn btn-primary" style="display:none">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php
                  function convertToRupiah($isi){
                    return number_format($isi,0,',','.');
                  }
                ?>

                <script>
                $(document).ready(function () {
                    $("#e1").select2();
                    $("#basic").selectpicker();

                    $('#tglmulai').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today"
                    });

                    $('#tglakhir').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today"
                    });
                        
                });
          </script>
          

