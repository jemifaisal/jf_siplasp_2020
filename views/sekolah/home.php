<!-- Default box -->

<div class="box">

  <div class="box-header with-border">

    <h3 class="box-title">Selamat Datang Operator Sekolah.</h3>

  </div>

  <div class="box-body">

    <h3>Silahkan gunakan sistem ini dengan sebaik-baiknya, <a href="https://drive.google.com/file/d/1xcjDvFdoXr5DUklS5NKEjlwjt_OIWoaw/view?usp=sharing" target="_blank">download</a> petunjuk penggunaan aplikasi.</h3>


    <div class="alert alert-danger alert-dismissable" style="border:3px solid blue; border-style: dashed;">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong><u>PENGUMUMAN: </u></strong> <br>
			Jika jumlah <b><u>SISWA</u></b> yang menerima dana BOS pada sistem ini tidak sesuai dengan jumlah SISWA yang ada disekolah bapak/ibu, silahkan WA ke no hp: <b>0853 5775 6617</b> dengan mengirimkan <u>nama sekolah</u> dan <u>NPSN Sekolah</u>. Terima Kasih.
	</div>


    <div class="alert alert-warning alert-dismissable">

		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

			Jika ada yang ditanya mengenai aplikasi, silahkan hubungi ke: <br>

			<strong><u>Contact Person:</u></strong> <br>

			<b>- Jemi Faisal</b> (0853-5775-6617) <br>

			email: dinaspendidikan@riau.go.id <br><BR>

			<p>Dinas Pendidikan Provinsi Riau</p>

			Jl. Cut Nyak Dien No. 3



			<p><u><i>NB: Pelayanan dibuka pada jam Kantor (09:00 - 16:00 WIB) </i></u></p>

	</div>

  </div>

    <!-- /.box-body -->

</div>

<!-- /.box -->