                <script type="text/javascript">
                    function convertToRupiah(objek) {
                      separator = ".";
                      a = objek.value;
                      b = a.replace(/[^\d]/g,"");
                      c = "";
                      panjang = b.length; 
                      j = 0; 
                      for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                          c = b.substr(i-1,1) + separator + c;
                        } else {
                          c = b.substr(i-1,1) + c;
                        }
                      }
                      objek.value = c;

                    }

                    function jum(){

                      var a = document.getElementById("vol").value;
                      var c = a.replace(/[^\d]/g,"");
                      //alert(c);
                      var b = document.getElementById("hs").value;
                      var d = b.replace(/[^\d]/g,"");

                      var jumlah = parseInt(c) * parseInt(d);

                      if(!isNaN(jumlah)){
                        document.getElementById("t").value = jumlah;  
                      }else{
                        document.getElementById("t").value = 0; 
                      }

                      
                    }

                </script>
                <?php
                  foreach ($rincian as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_rincian'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="hidden" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian" value="<?php echo $row->detail_rincian; ?>">
                      </div>

                      <div id="tampil" style="display:block;">
                      <div class="row">
                        <div class="form-group">
                          
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Volume</label>
                            <input type="text" id="vol" name="volume" class="form-control" placeholder="Volume" onkeyup="convertToRupiah(this);jum();" value="<?php echo $row->volume; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Satuan</label>
                            <input type="text" id="s" required="required" name="satuan" class="form-control" placeholder="Satuan" value="<?php echo $row->satuan; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Harga Satuan</label>
                            <input type="text" id="hs" name="hargasatuan" class="form-control" placeholder="Harga Satuan" onkeyup="convertToRupiah(this);jum()" value="<?php echo $row->harga_satuan; ?>">
                          </div>
                        </div>

                        <div class="form-group" style="display:none;">
                          <div class="col-xs-3">
                            <label for="exampleInputEmail1">Total</label>
                            <input type="text" readonly="readonly" id="t" name="total" class="form-control" onclick="jum();" placeholder="Total" value="0">
                          </div>
                        </div>
                      </div><br>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw1" class="form-control" id="exampleInputEmail1" placeholder="Triwulan I" value="<?php echo $row->tw1; ?>" onkeyup="convertToRupiah(this);">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw2" class="form-control" id="exampleInputEmail1" placeholder="Triwulan II" value="<?php echo $row->tw2; ?>" onkeyup="convertToRupiah(this);">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw3" class="form-control" id="exampleInputEmail1" placeholder="Triwulan III" value="<?php echo $row->tw3; ?>" onkeyup="convertToRupiah(this);">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw4" class="form-control" id="exampleInputEmail1" placeholder="Triwulan IV" value="<?php echo $row->tw4; ?>" onkeyup="convertToRupiah(this);">
                      </div>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php
                  function convertToRupiah($isi){
                    return number_format($isi,0,',','.');
                  }
                ?>

                <script>
                  $(document).ready(function () {
                      $("#e1").select2();
                  });

                </script>