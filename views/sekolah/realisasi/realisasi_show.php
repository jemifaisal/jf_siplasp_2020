<h2 class="lead">
    <span class="label label-warning"><a href="<?php echo base_url('sekolah/cetakrealisasi/'.$tw.'/realisasi'); ?>" class="btn" style="color:white;font-weight:bold;"><i class="fa fa-download"></i> Excel</a></span>
    <span class="label label-success">Laporan Realisasi Dana BOS</span>
</h2>
<table class="table table-bordered table-responsive">
    <tr>
        <th>Uraian</th>
        <th>Jumlah Anggaran (Rp.)</th>
        <th>Realisasi s/d <br>Triwulan Lalu (Rp.)</th>
        <th>Realisasi Triwulan ini (Rp.)</th>
        <th>Jumlah Realisasi s/d <br>Triwulan ini</th>
        <th>Selisih (Rp.)</th>
    </tr>
    <tr>
        <td>Penerimaan</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'],0); ?></span></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'] + $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw'] ,0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'] - ($jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'] + $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw']),0); ?></span></td>
    </tr>
    <tr>
        <td>Pengeluaran:</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'],0); ?></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right">
            <?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($jasa['penerimaan'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>a) Belanja Pegawai</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>b) Belanja Barang dan Jasa</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_jasa['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_jasa['total'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>c) Belanja Modal:</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_mesin['total'] + $total_belanja_aset['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format(($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'])) + ($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'])),0); ?></span></td>
    </tr>
    <tr>
        <td>1) Belanja Modal Peralatan dan Mesin</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_mesin['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>2) Belanja Modal Aset Tetap Lainnya</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_aset['total'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']),0); ?></span></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_belanja_jasa['total'] + $total_belanja_mesin['total'] + $total_belanja_aset['total'],0);  ?></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
        <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format(($total_belanja_jasa['total'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'])) + (($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'])) + ($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']))),0); ?></span></td>
    </tr>
</table>

<h2 class="lead">
    <span class="label label-warning"><a href="<?php echo base_url('sekolah/cetakrealisasi/'.$tw.'/penggunaan'); ?>" class="btn" style="color:white;font-weight:bold;"><i class="fa fa-download"></i> Excel</a></span>
    <span class="label label-success">Penggunaan Dana</span>
</h2>
<div class="table-responsive">
<table class="table table-bordered table-responsive">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Program/Kegiatan</th>
        <?php
            foreach($komponen_biaya as $komponen_biayas){
                echo "<th>$komponen_biayas->nama_komponen</th>";
            }
        ?>
        <th rowspan="2">Jumlah</th>
    </tr>
    <tr>
        <?php
            foreach($komponen_biaya as $komponen_biayas){
                echo "<th>$komponen_biayas->komponen_kode</th>";
            }
        ?>
    </tr>
    <?php
        $no=0;
        $jumlah = array();
        foreach($standar_nasional as $standar_nasionals){
            $no++;
            echo "<tr>
                <td>1.$no</td>
                <td>$standar_nasionals->nama_standar</td>";
                $total_per_standar = array();
                $i=0;
                foreach($komponen_biaya as $komponen_biayas){
                    $i++;        
                    echo "<td>".number_format($this->rkas_model->get_total_per_standar_komponen($tw,$standar_nasionals->standar_id,$komponen_biayas->komponen_kode,$tahun,$sekolahid),0)."</td>";
                    $total_per_standar[] = $this->rkas_model->get_total_per_standar_komponen($tw,$standar_nasionals->standar_id,$komponen_biayas->komponen_kode,$tahun,$sekolahid);
                }  

                
            echo "<td>".number_format(array_sum($total_per_standar),0)."</td>
            </tr>";   
        }
        
    ?>
    <tr>
        <th></th>
        <th>Total</th>
        <?php
            $komponen_total = array();
            foreach($komponen_biaya as $komponen_biayas){
                echo "<th>".number_format($total_komponen[$komponen_biayas->komponen_kode],0)."</th>";
                $komponen_total[] = $total_komponen[$komponen_biayas->komponen_kode];
            }
        ?>
        <th><?php echo number_format(array_sum($total_komponen),0); ?></th>
    </tr>
</table>
</div>

<h2 class="lead">
    <span class="label label-warning"><a href="<?php echo base_url('sekolah/cetakrealisasi/'.$tw.'/sptjm'); ?>" class="btn" style="color:white;font-weight:bold;"><i class="fa fa-download"></i> Excel</a></span>    
    <span class="label label-success">SPTJM</span>
</h2>
<div class="col-md-8" style="border: 0px solid #ccc;">
    <div style="border:0px solid;">
        <table border='0' width="800px">
            <tr>
                <td colspan="4">
                    <p align="center">
                    SURAT PERNYATAAN TANGGUNG JAWAB MUTLAK (SPTJM)<br>
                    SURAT PERNYATAAN TANGGUNG JAWAB <br>
                    Nomor :
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" width="50%">1. Nama Satuan Pendidikan</td>
                <td colspan="2">: <?php echo $profil['nama_sp']; ?></td>
            </tr>
            <tr>
                <td colspan="2">2. Kode Organisasi</td>
                <td colspan="2">:: 1.01.01.01 Dinas Pendidikan</td>
            </tr>
            <tr>
                <td colspan="2">3. Nomor/tanggal DPA-SKPD</td>
                <td colspan="2">: 1.01.01.1.01.01.01.33.001 / tanggal ......
        </td>
            </tr>
            <tr>
                <td colspan="2">4. Kegiatan Dana BOS</td>
                <td colspan="2">: Penyelenggaraan Bantuan Operasional Sekolah (BOS)</td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    <p align="justify">
                        Saya yang bertanda tangan dibawah ini menyatakan bahwa bertanggung jawab secara formal dan material atas kebenaran realisasi penerimaan dan pengeluaran Dana BOS serta kebenaran perhitungan dan setoran pajak yang telah dipungut atas penggunaan Dana BOS pada triwulan III tahun anggaran 2019 dengan rincian sebagai berikut:
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="4">A. Penerimaan Dana BOS (8)</td>
            </tr>
            <tr>
                <td width="50px">&nbsp;&nbsp;</td>
                <td>1. Triwulan I</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw1,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Triwulan II</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw2,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>3. Triwulan III</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw3,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>4. Triwulan IV</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($sptjm_tw4,0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Jumlah</td>
                <td></td>
                <td><span class="pull-left"><u><b>Rp</span><span class="pull-right"><u><?php echo number_format($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4,0); ?></b></u></span></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">B. Pengeluaran Dana BOS</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>1. Jenis Belanja Pegawai</td>
                <td>Rp</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Jenis Belanja Barang dan Jasa</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>3. Jenis Belanja Modal</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;a) Peralatan dan Mesin</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;b) Aset Tetap Lainnya</td>
                <td><span class="pull-left">Rp</span><span class="pull-right"><?php echo number_format($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Jumlah</td>
                <td></td>
                <td><span class="pull-left"><u><b>Rp</span><span class="pull-right"><u><?php echo number_format($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'],0); ?></u></b></span></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td>C. Sisa Dana BOS (A-B)</td>
                <td></td>
                <td></td>
                <td>
                    <span class="pull-left"><b><u>Rp</u></b></span>
                    <span class="pull-right"><b><u>
                    <?php
                        $sisa_dana = ($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4) - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']);
                        echo number_format(($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4) - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']),0);   
                    ?>
                    </u></b></span>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>Terdiri Atas:</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>1. Sisa Kas Tunai</td>
                <td>
                    <span class="pull-left"><b>Rp</b></span>
                    <span class="pull-right"><b><?php echo number_format($get_total_saldo_tunai['total_saldo'],0); ?></b></span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Sisa di Bank</td>
                <td>
                    <span class="pull-left"><b>Rp</b></span>
                    <span class="pull-right"><b><?php echo number_format($sisa_dana - $get_total_saldo_tunai['total_saldo'],0); ?></b></span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    <p align="justify">
                    Bukti-bukti atas belanja tersebut pada huruf B disimpan pada Satdikmen Negeri/Satdiksus Negeri untuk kelengkapan administrasi dan keperluan pemeriksaan sesuai peraturan perundang-undangan.
                    Apabila bukti-bukti tersebut tidak benar yang mengakibatkan kerugian daerah, saya bertanggung jawab sepenuhnya atas kerugian daerah dimaksud sesuai kewenangan saya berdasarkan ketentuan peraturan perundang-undangan.
                    Demikian surat Pernyataan ini dibuat dengan sebenarnya.
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;<br><br><br><br></td>
                <td colspan="2">
                    Pekanbaru, ..... <br>
                    Kepala Sekolah <?php echo $profil['nama_sp']; ?>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?php
                        echo "<u><b>".$profil['kepsek']."</b></u><br>NIP. ".$profil['nip_kepsek'];
                    ?>
                </td>
            </tr>

        </table>
    </div>
</div>
