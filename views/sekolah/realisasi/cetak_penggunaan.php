<table width="100%">
    <tr>
        <td colspan="6" align="center"><center>
        REKAPITULASI REALISASI PENGGUNAAN DANA BOS <?php echo $profil['nama_sp']; ?>
        <br>Periode: <?php echo $periode; ?> (Triwulan <?php echo $tw_romawi; ?>)
        <br>Tahun <?php echo $tahun; ?>
        </center></td>
    </tr>
</table>
<br>
<table border='0' width='100%'>
    <tr>
        <td colspan="2" width='150'>Nama Sekolah</td>
        <td colspan="7">: <?php echo $profil['nama_sp']; ?> </td>
    </tr>
    <tr>
        <td colspan="2">Desa/Kecamatan</td>
        <td colspan="7">: <?php echo $profil['nama_kec']; ?></td>
    </tr>
    <tr>
        <td colspan="2">Kabupaten/Kota</td>
        <td colspan="7">: <?php echo $profil['nama_kab']; ?></td>
    </tr>
    <tr>
        <td colspan="2">Provinsi</td>
        <td colspan="7">: Riau</td>
    </tr>
</table>
<br>
<div class="table-responsive">
<table border="1" class="table table-bordered table-responsive">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Program/Kegiatan</th>
        <?php
            foreach($komponen_biaya as $komponen_biayas){
                echo "<th>$komponen_biayas->nama_komponen</th>";
            }
        ?>
        <th rowspan="2">Jumlah</th>
    </tr>
    <tr>
        <?php
            foreach($komponen_biaya as $komponen_biayas){
                echo "<th>$komponen_biayas->komponen_kode</th>";
            }
        ?>
    </tr>
    <?php
        $no=0;
        $jumlah = array();
        foreach($standar_nasional as $standar_nasionals){
            $no++;
            echo "<tr>
                <td>1.$no</td>
                <td>$standar_nasionals->nama_standar</td>";
                $total_per_standar = array();
                $i=0;
                foreach($komponen_biaya as $komponen_biayas){
                    $i++;        
                    echo "<td>".number_format($this->rkas_model->get_total_per_standar_komponen($tw,$standar_nasionals->standar_id,$komponen_biayas->komponen_kode,$tahun,$sekolahid),0)."</td>";
                    $total_per_standar[] = $this->rkas_model->get_total_per_standar_komponen($tw,$standar_nasionals->standar_id,$komponen_biayas->komponen_kode,$tahun,$sekolahid);
                }  

                
            echo "<td>".number_format(array_sum($total_per_standar),0)."</td>
            </tr>";   
        }
        
    ?>
    <tr>
        <th></th>
        <th>Total</th>
        <?php
            $komponen_total = array();
            foreach($komponen_biaya as $komponen_biayas){
                echo "<th>".number_format($total_komponen[$komponen_biayas->komponen_kode],0)."</th>";
                $komponen_total[] = $total_komponen[$komponen_biayas->komponen_kode];
            }
        ?>
        <th><?php echo number_format(array_sum($total_komponen),0); ?></th>
    </tr>
</table>
</div>
<br>
<table width="100%" border="0">
    <tr>
        <td>Saldo periode sebelumnya 5)</td>
        <td>: <?php echo $get_total_saldo_tunai_before['total_saldo']; ?></td>
    </tr>
    <tr>
        <td>Total dana BOS periode ini</td>
        <td>: <?php echo $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw']; ?> </td>
    </tr>
    <tr>
        <td>Saldo BOS periode ini </td>
        <td>: <?php echo $get_total_saldo_tunai['total_saldo']; ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="5">
            Pekanbaru, ..... <br>
            Kepala Sekolah <?php echo $profil['nama_sp']; ?>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <?php
                echo "<u><b>".$profil['kepsek']."</b></u><br>NIP. ".$profil['nip_kepsek'];
            ?>
        </td>
    </tr>
</table>