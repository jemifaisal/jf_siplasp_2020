<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data Realisasi Dana BOS</h3>
    </div>
    <div class="box-body">
        <form class="form-inline" method="post" id="form_lihat">
            <div class="form-group">
                <label for="email">Tahun:</label>
                <b class="form-control" ><?php echo $tahun; ?></b>
                <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
            </div>
            <div class="form-group">
                <?php
                    if($regulasi == 3){
                ?>
                    <label for="pwd">Tahap:</label>
                    <select name="tw" id="tw" class="form-control">
                        <option value="1">Tahap I</option>
                        <option value="2">Tahap II</option>
                        <option value="3">Tahap III</option>
                    </select>

                <?php
                    }else{
                ?>
                    <label for="pwd">Triwulan:</label>
                    <select name="tw" id="tw" class="form-control">
                        <option value="1">Triwulan I</option>
                        <option value="2">Triwulan II</option>
                        <option value="3">Triwulan III</option>
                        <option value="4">Triwulan IV</option>
                    </select>
                <?php
                    }
                ?>
                
            </div>
            <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
            <button type="submit" class="btn btn-warning"><span id="btn_caption"><i class='fa fa-send'></i> Proses</span></button>
            <br><span class="required"><i>* tahun berdasarkan yang dipilih ketika login</i></span>
          
        </form>
        <br>
        <div id="bku_show"></div>
    </div>
</div>