<div class="col-md-8" style="border: 0px solid #ccc;">
    <div style="border:0px solid;">
        <table border='0' width="100%">
            <tr>
                <td colspan="4">
                    <p align="center">
                    SURAT PERNYATAAN TANGGUNG JAWAB MUTLAK (SPTJM)<br>
                    SURAT PERNYATAAN TANGGUNG JAWAB <br>
                    Nomor :
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" width="50%" align="left">1. Nama Satuan Pendidikan</td>
                <td colspan="2" align="left">: <?php echo $profil['nama_sp']; ?></td>
            </tr>
            <tr>
                <td colspan="2" align="left">2. Kode Organisasi</td>
                <td colspan="2" align="left">: 1.01.01.01 Dinas Pendidikan</td>
            </tr>
            <tr>
                <td colspan="2" align="left">3. Nomor/tanggal DPA-SKPD</td>
                <td colspan="2" align="left">: 1.01.01.1.01.01.01.33.001 / tanggal ......
        </td>
            </tr>
            <tr>
                <td colspan="2" align="left">4. Kegiatan Dana BOS</td>
                <td colspan="2" align="left">: Penyelenggaraan Bantuan Operasional Sekolah (BOS)</td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    <p align="justify">
                        Saya yang bertanda tangan dibawah ini menyatakan bahwa bertanggung jawab secara formal dan material atas kebenaran realisasi penerimaan dan pengeluaran Dana BOS serta kebenaran perhitungan dan setoran pajak yang telah dipungut atas penggunaan Dana BOS pada triwulan III tahun anggaran 2019 dengan rincian sebagai berikut:
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="4">A. Penerimaan Dana BOS (8)</td>
            </tr>
            <tr>
                <td width="50px">&nbsp;&nbsp;</td>
                <td>1. Triwulan I</td>
                <td><span class="pull-right"><?php echo $sptjm_tw1; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Triwulan II</td>
                <td><span class="pull-right"><?php echo $sptjm_tw2; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>3. Triwulan III</td>
                <td><span class="pull-right"><?php echo $sptjm_tw3; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>4. Triwulan IV</td>
                <td><span class="pull-right"><?php echo $sptjm_tw4; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Jumlah</td>
                <td></td>
                <td><span class="pull-right"><u><?php echo $sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4; ?></b></u></span></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">B. Pengeluaran Dana BOS</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>1. Jenis Belanja Pegawai</td>
                <td>Rp</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Jenis Belanja Barang dan Jasa</td>
                <td><span class="pull-right"><?php echo $total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi']; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>3. Jenis Belanja Modal</td>
                <td><span class="pull-right"><?php echo $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;a) Peralatan dan Mesin</td>
                <td><span class="pull-right"><?php echo $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi']; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;b) Aset Tetap Lainnya</td>
                <td><span class="pull-right"><?php echo $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Jumlah</td>
                <td></td>
                <td><span class="pull-right"><u><?php echo $total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></u></b></span></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td>C. Sisa Dana BOS (A-B)</td>
                <td></td>
                <td></td>
                <td>
                    <span class="pull-right"><b><u>
                    <?php
                        $sisa_dana = ($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4) - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']);
                        echo ($sptjm_tw1+$sptjm_tw2+$sptjm_tw3+$sptjm_tw4) - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']);   
                    ?>
                    </u></b></span>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>Terdiri Atas:</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>1. Sisa Kas Tunai</td>
                <td>
                    <b><?php echo $get_total_saldo_tunai['total_saldo']; ?></b>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>2. Sisa di Bank</td>
                <td>
                    <span class="pull-right"><b><?php echo $sisa_dana - $get_total_saldo_tunai['total_saldo']; ?></b></span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                    <p align="justify">
                    Bukti-bukti atas belanja tersebut pada huruf B disimpan pada Satdikmen Negeri/Satdiksus Negeri untuk kelengkapan administrasi dan keperluan pemeriksaan sesuai peraturan perundang-undangan.
                    Apabila bukti-bukti tersebut tidak benar yang mengakibatkan kerugian daerah, saya bertanggung jawab sepenuhnya atas kerugian daerah dimaksud sesuai kewenangan saya berdasarkan ketentuan peraturan perundang-undangan.
                    Demikian surat Pernyataan ini dibuat dengan sebenarnya.
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;<br><br><br><br></td>
                <td colspan="2">
                    Pekanbaru, ..... <br>
                    Kepala Sekolah <?php echo $profil['nama_sp']; ?>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?php
                        echo "<u><b>".$profil['kepsek']."</b></u><br>NIP. ".$profil['nip_kepsek'];
                    ?>
                </td>
            </tr>

        </table>
    </div>
</div>
