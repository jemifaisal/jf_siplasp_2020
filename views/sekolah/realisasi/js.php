<script>
  $(document).ready(function(){
    $('#tw').on('change', function(){
      $("#bku_show").html("");
    });

    $('#form_lihat').on('submit', function(e){
      $("#bku_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('sekolah/realisasi_show'); ?>";
        $.ajax({
          url: url,
          type: "post",
          data: inputData,
          processData: false,
          contentType: false,
          cache: false,
          success: function(data){
            $("#bku_show").html(data);
            $("#btn_caption").html("<i class='fa fa-send'></i> Proses");
          }
        });
        
        return false;
      }
    });   

  });
</script>