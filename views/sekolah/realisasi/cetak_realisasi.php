<table width="100%">
    <tr>
        <td colspan="6" align="center"><center>LAPORAN REALISASI DANA BANTUAN OPERASIONAL SEKOLAH <?php echo $profil['nama_sp']; ?>  PROVINSI RIAU.</center></td>
    </tr>
    <tr>
        <td colspan="6">Bersama ini kami laporkan realisasi atas penerimaan dan pengeluaran Dana BOS untuk triwulan <?php echo $tw_romawi; ?> sebagai berikut :</td>
    </tr>
</table>
<br>
<table width="100%" border="1" class="table table-bordered table-responsive">
    <tr>
        <th>Uraian</th>
        <th>Jumlah Anggaran (Rp.)</th>
        <th>Realisasi s/d <br>Triwulan Lalu (Rp.)</th>
        <th>Realisasi Triwulan ini (Rp.)</th>
        <th>Jumlah Realisasi s/d <br>Triwulan ini</th>
        <th>Selisih (Rp.)</th>
    </tr>
    <tr>
        <td>Penerimaan</td>
        <td><?php echo $jasa['penerimaan']; ?></span></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td><?php echo $jasa['penerimaan']; ?></span></td>
        <td><?php echo $jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw']; ?></span></td>
        <td><?php echo $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw']; ?></span></td>
        <td><?php echo $jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'] + $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw']; ?></span></td>
        <td><?php echo $jasa['penerimaan'] - ($jasa_before['total_tw'] + $mesin_before['total_tw'] + $aset_before['total_tw'] + $jasa['total_tw'] + $mesin['total_tw'] + $aset['total_tw']); ?></span></td>
    </tr>
    <tr>
        <td>Pengeluaran:</td>
        <td><?php echo $jasa['penerimaan']; ?></td>
        <td><?php echo $total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td>
            <?php echo $total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td><?php echo $jasa['penerimaan'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']); ?></span></td>
    </tr>
    <tr>
        <td>a) Belanja Pegawai</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>b) Belanja Barang dan Jasa</td>
        <td><?php echo $total_belanja_jasa['total']; ?></span></td>
        <td><?php echo $total_realisasi_jasa_before['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_jasa['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi']; ?></span></td>
        <td><?php echo $total_belanja_jasa['total'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi']); ?></span></td>
    </tr>
    <tr>
        <td>c) Belanja Modal:</td>
        <td><?php echo $total_belanja_mesin['total'] + $total_belanja_aset['total']; ?></span></td>
        <td><?php echo $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td><?php echo ($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'])) + ($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi'])); ?></span></td>
    </tr>
    <tr>
        <td>1) Belanja Modal Peralatan dan Mesin</td>
        <td><?php echo $total_belanja_mesin['total']; ?></span></td>
        <td><?php echo $total_realisasi_mesin_before['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_mesin['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi']; ?></span></td>
        <td><?php echo $total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi']); ?></span></td>
    </tr>
    <tr>
        <td>2) Belanja Modal Aset Tetap Lainnya</td>
        <td><?php echo $total_belanja_aset['total']; ?></span></td>
        <td><?php echo $total_realisasi_aset_before['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td><?php echo $total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']); ?></span></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td><?php echo $total_belanja_jasa['total'] + $total_belanja_mesin['total'] + $total_belanja_aset['total'];  ?></td>
        <td><?php echo $total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td><?php echo $total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_aset_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'] + $total_realisasi_mesin['total_realisasi'] + $total_realisasi_aset['total_realisasi']; ?></span></td>
        <td><?php echo ($total_belanja_jasa['total'] - ($total_realisasi_jasa_before['total_realisasi'] + $total_realisasi_jasa['total_realisasi'])) + (($total_belanja_mesin['total'] - ($total_realisasi_mesin_before['total_realisasi'] + $total_realisasi_mesin['total_realisasi'])) + ($total_belanja_aset['total'] - ($total_realisasi_aset_before['total_realisasi'] + $total_realisasi_aset['total_realisasi']))); ?></span></td>
    </tr>
</table>
<br>
<table width="100%" border="0">
    <tr>
        <td colspan="6">Demikian laporan realisasi ini dibuat untuk digunakan sebagaimana mestinya.</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="5">
            Pekanbaru, ..... <br>
            Kepala Sekolah <?php echo $profil['nama_sp']; ?>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <?php
                echo "<u><b>".$profil['kepsek']."</b></u><br>NIP. ".$profil['nip_kepsek'];
            ?>
        </td>
    </tr>
</table>
