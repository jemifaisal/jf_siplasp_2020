<a href="<?php echo base_url('sekolah/cetaklaporan/'.$bulan_id.'/bpb'); ?>" class="btn btn-success"><i class='fa fa-download'></i> Excel</a>
<table border='0' width='100%'>
    <tr>
        <td colspan='3'><span class="lead"><center>BUKU PEMBANTU BANK</center></span></td>
    </tr>
    <tr>
        <td colspan='3'><center>Bulan: <?php echo $bulan. ' '.$tahun; ?></center></td>
    </tr>
    <tr>
        <td width='150'>Nama Sekolah</td>
        <td width='2'>:</td>
        <td> <?php echo $profil['nama_sp']; ?> </td>
    </tr>
    <tr>
        <td>Desa/Kecamatan</td>
        <td>:</td>
        <td> <?php echo $profil['nama_kec']; ?></td>
    </tr>
    <tr>
        <td>Kabupaten/Kota</td>
        <td>:</td>
        <td> <?php echo $profil['nama_kab']; ?></td>
    </tr>
    <tr>
        <td>Provinsi</td>
        <td>:</td>
        <td> Riau</td>
    </tr>
</table>
<br>
<table class="table table-bordered table-responsive">
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Kode Rekening</th>
        <th>No. Kode</th>
        <th>No. Bukti</th>
        <th>Uraian</th>
        <th>Penerimaan<br> (Debit)</th>
        <th>Pengeluaran<br> (Kredit)</th>
        <th>Saldo</th>
    </tr>
    <?php
        $no = 0;
        $saldo = $saldo_awal;
        foreach($bku as $bkus){
            $no ++;
            if($bkus->program_kode == NULL){
                $dot = "";
            }else{
                $dot = ".";
            }
            echo "<tr>
                <td>$no</td>
                <td>".date("d M Y",strtotime($bkus->tanggal_transaksi))."</td>
                <td>$bkus->rekening_belanja</td>
                <td>$bkus->program_kode$dot$bkus->kode_komponen</td>
                <td>$bkus->kode_bukti $bkus->no_urut_bukti</td>
                <td>$bkus->uraian_bku</td>";
            if($bkus->kelompok == "D"){
                $param = '+';
                echo "<td class='success'>$bkus->jumlah_transaksi_format</td>
                    <td class='warning'></td>";
                $saldo = $saldo + $bkus->jumlah_transaksi;
            }else{
                $param = '-';
                echo "<td class='success'></td>
                    <td class='warning'>$bkus->jumlah_transaksi_format</td>";
                $saldo = $saldo - $bkus->jumlah_transaksi;
            }

            
            echo "<td class='info'>".number_format($saldo,0)."</td></tr>";
        }
    ?>
</table>
<br>
<table border='0' width='100%'>
    <tr>
    <tr>
        <td align='center' width='45%'>
            Mengetahui<br>
            Kepala <?php echo $profil['nama_sp']; ?>
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['kepsek']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_kepsek']; ?>
        </td>
        <td width='10%'></td>
        <td align='center' width='45%'>
            ................., <?php $hari_ini = $tahun.'-'.$bulan_id; echo date('t', strtotime($hari_ini)); echo ' '. $bulan.' '. $tahun; ?> <br>
            Bendahara Dana BOS
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['nama_bendahara_bos']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_bendahara_bos']; ?>
        </td>
    </tr>
    </tr>
</table>
