<table border="0">
<thead>
<tr>
    <td colspan="2">Lampiran 1</td>
    <td colspan="9">: Usulan nama guru PNS-D penerima TAMSIL Semester I periode Januari s.d Juni 2020</td>
</tr>
<tr>
    <td colspan="2">Nomor</td>
    <td colspan="9">:</td>
</tr>
<tr>
    <td colspan="2">Tanggal</td>
    <td colspan="9">:</td>
</tr>
<tr>
    <td colspan="2">Sekolah Induk</td>
    <td colspan="9">: <?php echo $nama_sekolah; ?></td>
</tr>
<tr>
    <td colspan="2">Kab / Kota</td>
    <td colspan="9">: <?php echo $nama_kab; ?></td>
</tr>
<tr>
    <td colspan="11">&nbsp;</td>
</tr>
<tr>
    <th>No</th>
    <th>Nama Guru</th>
    <th>NIP</th>
    <th>Gol</th>
    <th>Pendidikan Terakhir</th>
    <th>Jurusan</th>
    <th>NUPTK</th>
    <th>JJM/ minggu dan Mapel semester I (Jan s.d Jun 2020) sesuai Dapodik </th>
    <th>Nomor rekening Guru (Bank Riau)</th>
    <th>Nama pada rekening Bank Riau </th>
    <th>No HP Guru</th>
    <th>Keterangan</th>
    
</tr>
</thead>
<tbody>
<?php $no=1; foreach ($tamsil as $row) { ?>
<tr>
    <td><?php echo $no++; ?></td>
    <td><?php echo $row->gelar_depan.$row->nama_lengkap.", ".$row->gelar_belakang; ?></td>
    <td><?php echo "'".$row->nip; ?></td>
    <td><?php echo $row->golongan; ?></td>
    <td><?php echo $row->nama_pdd; ?></td>
    <td><?php echo $row->jurusan; ?></td>
    <td><?php echo "'".$row->nuptk; ?></td>
    <td><?php echo $row->jumlah_jam_mengajar; ?></td>
    <td><?php echo "'".$row->no_rekening; ?></td>
    <td><?php echo $row->nama_rekening; ?></td>
    <td><?php echo "'".$row->no_hp; ?></td>
    <td><?php echo $row->keterangan; ?></td>
</tr>
<?php } ?>
</tbody>
<tr>
    <td colspan="12">&nbsp;</td>
</tr>
<tr>
    <td colspan="6">
        Operator DAPODIK<BR>
        <?php echo $nama_sekolah; ?>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        Nama<br>
        NIP
    </td>
    <td colspan="6">
        Kepala Sekolah<BR>
        <?php echo $nama_sekolah; ?>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        Nama<br>
        NIP
    </td>
</tr>

<tr>
    <td colspan="12">
        Catatan:<br>
        - Pastikan NIP PNS-D tercatat di sistem BKN sebagai jabatan Guru dan jenis jabatan Fungsional tertentu.<br>
        - Pastikan Nomor Rekening Bank Riau penerima masih aktif dan benar sesuai nama Guru yang bersangkutan.<br>
        <u><i>Kesalahan nomor rekening Bank menjadi tanggung jawab masing-masing guru dan sekolah apabila terjadi salah penyaluran ataupun tidak bisa disalurkan</i></u>
    </td>
</tr>


</table>