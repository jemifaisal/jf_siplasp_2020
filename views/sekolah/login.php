<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/img/favicon.png'); ?>">
  <title>SIPLASP - Login OP Sekolah</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/webadmin/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    SIPLASP<BR>
    <b>OP</b>Sekolah
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Silahkan Login terlebih dahulu.</p>
    <?php
      $info = $this->session->flashdata('info');
      if(isset($info)){
        echo $info;
      }
    ?>
    <span id="msg"></span>
    <form>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" id="username" placeholder="Username" required="required">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="required">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <select name="tahun" id="tahun" class="form-control">
          <option value="2020">2020</option>
          <option value="2019">2019</option>
          <option value="2018">2018</option>
          <!--<option value="2018">2018</option> -->
        </select>
        
      </div>
      </form>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" id="btn_login"><span id="loading"></span> Login</button>
        </div>
        <!-- /.col -->
      </div>
    

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/webadmin/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/webadmin/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  $(document).ready(function(){
    $('#btn_login').click(function(){
      var username = $('#username').val();
      var password = $('#password').val();
      var tahun = $('#tahun option:selected').val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('sekolah/getlogin'); ?>",
        data: {'username':username, 'password':password, 'tahun':tahun},
        dataType: "JSON",
        cache: false,
        beforeSend: function(){
          $("#btn_login").prop("disabled", true);
				  // $("#btn_login").val('Connecting...');
          $("#loading").html("<i class='fa fa-spinner fa-spin' style='font-size:20px'></i>");
        },
        success: function(data){
          if(data.status == true){
            // $("#jform").style.display("none");
            $("#msg").html(data.msg);
            setTimeout(function() {
              location.href="<?php base_url('sekolah'); ?>";
            },2000);    
          }else{
            $("#btn_login").prop("disabled", false);
            $("#msg").html(data.msg);
          }
          $("#loading").html("");
          $("#btn_login").val('Login');
        },error:function(){

        }
      });
      return false;

    });

  });
</script>
</body>
</html>
