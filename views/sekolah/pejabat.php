        <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Pejabat Penandatangan</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          <p><a href="#" class="btn btn-primary" id="aksi" data-id="1"><i class="fa fa-plus-circle"></i> Tambah Data</a></p>
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Pejabat Penandatangan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Komite Sekolah</th>
                  <th>NIP Komite Sekolah</th>
                  <th>Nama Kepsek</th>
                  <th>NIP Kepsek</th>
                  <th>Nama Bendahara BOS</th>
                  <th>NIP Bendahara BOS</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($pejabat as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->nama_komite_sekolah; ?></td>
                  <td><?php echo $row->nip_komite_sekolah; ?></td>
                  <td><?php echo $row->nama_kepsek; ?></td>
                  <td><?php echo $row->nip_kepsek; ?></td>
                  <td><?php echo $row->nama_bendahara_bos; ?></td>
                  <td><?php echo $row->nip_bendahara_bos; ?></td>
                  <td><?php if($row->soft_delete == 0){echo "Aktif";}else{echo "Tidak Aktif";} ?></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="aksi" data-id="2,<?php echo $row->ttd_id; ?>">Ubah</a> <a href="<?php echo base_url('sekolah/hapus_pejabat/'.$row->ttd_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_pejabat'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
