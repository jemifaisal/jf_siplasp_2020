<?php
	function rp($a){
	    $h = number_format($a,0,',','.');
	    return $h;
	}
	$sid = $this->session->userdata('token_sekolah_id');
	$tahun = $this->session->userdata('token_tahun');
	$sql = $this->db->query("SELECT s.npsn,pb.jumlah_siswa,s.jenjang FROM sekolah s left join penerima_bos pb on s.npsn=pb.npsn where s.sekolah_id='$sid' and pb.tahun='$tahun' LIMIT 1");
	if($sql->num_rows() >0){
		foreach ($sql->result() as $row) {
			if($row->jenjang == "SMA" OR $row->jenjang == "SMK"){
				$tot = $row->jumlah_siswa * 1400000;
				$total = number_format($tot,0,',','.');
				$jumlahpd = $row->jumlah_siswa;
				$tw1 = 20 * $tot / 100;
				$tw2 = 40 * $tot / 100;
				$tw3 = 20 * $tot / 100;
				$tw4 = 20 * $tot / 100;
			}
			
		}
	}else{
		$jumlahpd = "0"; $total="0";
		$tw1 = 0;$tw2 = 0;$tw3 =0;$tw4 = 0;
	}
?>
<!-- Default box -->
<div class="box">
<div class="row">
	<div class="box-body">
		<div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-aqua"><i class="fa fa-calendar"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TAHUN</span>
	              <span class="info-box-number"><?php echo $tahun; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">JUMLAH SISWA</span>
	              <span class="info-box-number"><?php echo $jumlahpd; ?><small> Orang</small></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TOTAL DANA BOS</span>
	              <span class="info-box-number"><?php echo $total; ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>
	</div>
    <!-- /.box-body -->
</div>

<div class="row">
	<div class="box-body">
		<div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN I</span>
	              <span class="info-box-number"><?php echo rp($tw1); ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN II</span>
	              <span class="info-box-number"><?php echo rp($tw2); ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN III</span>
	              <span class="info-box-number"><?php echo rp($tw3); ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>

	    <div class="col-md-3 col-sm-6 col-xs-12">
	          <div class="info-box">
	            <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>

	            <div class="info-box-content">
	              <span class="info-box-text">TRIWULAN IV</span>
	              <span class="info-box-number"><?php echo rp($tw4); ?></span>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
	          <!-- /.info-box -->
	    </div>
	</div>
    <!-- /.box-body -->
</div>
</div>
<!-- /.box -->