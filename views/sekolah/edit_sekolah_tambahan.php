                <?php
                  foreach ($riwayat as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_sekolah_tambahan'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sekolah Tambahan Guru</label>
                        <select name="sekolah" class="form-control select2" style="width: 100%;" data-placeholder="Pilih Sekolah">
                          <?php
                            foreach ($sekolah as $baris) {
                              if($row->sekolah_id == $baris->sekolah_id){
                                echo "<option value='$baris->sekolah_id' selected='selected'>$baris->nama_sp</option>";
                              }else{
                                echo "<option value='$baris->sekolah_id'>$baris->nama_sp</option>";   
                              }
                              
                            }
                          ?>
                        </select>
                        <input type="hidden" name="sekolahid" value="<?php echo $row->sekolah_id; ?>">
                        <input type="hidden" name="gtkid" value="<?php echo $row->gtk_id; ?>">
                        <input type="hidden" name="riwayatid" value="<?php echo $row->riwayatsekolah_id; ?>">
                        <span class=""><i>*jika tidak ada mengajar disekolah lain, dikosongkan saja (tidak perlu dipilih).</i></span>
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                    $('#tgllahir').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today"
                    }); 

                    $('.select2').select2();   
                          
                  });
                </script>
