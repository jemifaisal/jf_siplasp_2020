<script>
  $(document).ready(function(){
    $(document).on('click','#tombol',function(e){
      e.preventDefault();
      $("#formaksi").modal('show');
      $.post("<?php echo base_url('sekolah/form_bku'); ?>",
          {id:$(this).attr('data-id')},
          function(html){
              $(".modal-body").html(html);
          }   
      );
    });

    $('#bulan').on('change', function(){
      $("#bku_show").html("");
    });

    $('#form_lihat').on('submit', function(e){
      $("#bku_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('sekolah/bku_show'); ?>";
        $.ajax({
          url: url,
          type: "post",
          data: inputData,
          processData: false,
          contentType: false,
          cache: false,
          success: function(data){
            $("#bku_show").html(data);
            $("#btn_caption").html("<i class='fa fa-send'></i> Proses");
          }
        });
        
        return false;
      }
    });   

  });

  function show_bku(bulan)
  {
    var url = "<?php echo base_url('sekolah/bku_show_ajax'); ?>"+"/"+bulan;
    $.ajax({
      url: url,
      processData: false,
      contentType: false,
      cache: false,
      success: function(data){
        $("#bku_show").html(data);
        $("#btn_caption").html("<i class='fa fa-send'></i> Proses");
      }
    }); 
  }

  function hapus_bku(id,bulan)
  {
    // alert(id+'-'+bulan);
    var url = "<?php echo base_url('sekolah/hapus_bku'); ?>";
    swal({
        title: 'Apakah anda yakin ?',
        text: "Data yang dihapus tidak bisa dikembalikan lagi",
        type: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
    }).then(function() {
      $.ajax({
        url: url,
        type: 'post',
        data: {id:id, bulan:bulan},
        dataType: 'JSON',
        success: function(data){
          swal({
              title: 'Berhasil',
              text: 'Data berhasil diubah',
              type: 'success',
              timer: 1500
          }).catch(swal.noop);
          show_bku(data.bulan);
        },
        error: function(){
          swal({
              title: 'Error!',
              text: 'Data gagal dihapus',
              type: 'error',
              timer: 1500
          }).catch(swal.noop);
        }
      });
      return false;
    }).catch(swal.noop);
  }
</script>