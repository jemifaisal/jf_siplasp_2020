<div id="program_pajak">
    <div class="form-group" id="form_nama_program">
        <label>Nama Program:</label><br>
        <select name="program" id="program" class="form-control select2" style="width:100%;" required="required">
            <option></option>
            <?php
                foreach($program as $programs){
                    echo "<option value='$programs->rincian_id'>$programs->program_kode.$programs->kode_komponen - $programs->nama_program</option>";
                }
            ?>
        </select>
        <div id="total_belanja"></div>
    </div>
</div>


<script>
    $(document).ready(function(){
        $('#program').select2({
            placeholder: "--Pilih Nama Program--",
            allowClear: true
        });

        $('#program').on('change', function(){
            var id = $('#program').val();
            var bulan = "<?php echo $bulan_id; ?>";
            var url = "<?php echo base_url('sekolah/data_belanja_rkas'); ?>";
            $.ajax({
                url: url,
                type: 'post',
                data: {rincian_id:id, bulan_id:bulan},
                dataType: 'JSON',
                success: function(data){
                    $("#total_belanja").html("<br><span class='form-control'> Total RKAS "+data.ket+" : <b>"+data.total_tw_format+"</b> ::: Total BKU : <b>"+data.total_bku_format+"</b></span>");
                },
                error: function(){

                }
            });
            return false;
            
        });
    });
</script>