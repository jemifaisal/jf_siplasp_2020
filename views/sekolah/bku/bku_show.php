<p><a href="#" class="btn btn-success" id="tombol" data-id="0,<?php echo $bulan.','.$bulan_id;?>"><i class="fa fa-plus"></i> BKU <?php echo $bulan; ?></a></p>
<table class="table table-bordered table-responsive">
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Kode Rekening</th>
        <th>No. Kode</th>
        <th>No. Bukti</th>
        <th>Uraian</th>
        <th>Penerimaan<br> (Debit)</th>
        <th>Pengeluaran<br> (Kredit)</th>
        <th>Saldo</th>
        <th>Aksi</th>
    </tr>
    <?php
        $no = 0;
        $saldo = $saldo_awal;
        if($bulan_id != 1){
            echo "<tr style='font-weight:bold;color:blue;' bgcolor='#ddd'>
                <td colspan='8'>SALDO BULAN LALU</td>
                <td>".number_format($saldo,0)."</td>
                <td></td>
            </tr>";
        }
        foreach($bku as $bkus){
            $no ++;
            if($bkus->program_kode == NULL){
                $dot = "";
            }else{
                $dot = ".";
            }
            echo "<tr>
                <td>$no</td>
                <td>".date("d M Y",strtotime($bkus->tanggal_transaksi))."</td>
                <td>$bkus->rekening_belanja</td>
                <td>$bkus->program_kode$dot$bkus->kode_komponen</td>
                <td>$bkus->kode_bukti $bkus->no_urut_bukti</td>
                <td>$bkus->uraian_bku</td>";
            if($bkus->kelompok == "D"){
                $param = '+';
                echo "<td class='success'>$bkus->jumlah_transaksi_format</td>
                    <td class='warning'></td>";
                $saldo = $saldo + $bkus->jumlah_transaksi;
            }else{
                $param = '-';
                echo "<td class='success'></td>
                    <td class='warning'>$bkus->jumlah_transaksi_format</td>";
                $saldo = $saldo - $bkus->jumlah_transaksi;
            }

            
            echo "<td class='info'>".number_format($saldo,0)."</td>
                <td>
                    <a href='#' class='btn btn-warning btn-xs' id='tombol' data-id='1,$bulan_id,$bkus->bku_id'><i class='fa fa-edit'></i> Ubah</a>
                    <a href='#' class='btn btn-danger btn-xs' onclick='hapus_bku($bkus->bku_id,$bulan_id)'><i class='fa fa-remove'></i> Hapus</a>
                </td>
            </tr>";
            //$bkus->uraian_bku;
        }
    ?>
</table>
