<?php
    if($aksi == 0){
?>
<!-- aksi simpan -->
<form action="" method="post" id="form_simpan" name="form_simpan">
    <div class="form-group">
        <label>No. Bukti:</label><br>
        <input type="hidden" id="id_bukti">
        <div style='border:1px solid #ccc;font-weight:bold;padding:5px;width:100%;'>
            <?php 
                foreach($bukti as $buktis){
                    echo "<label class='radio-inline'><input type='radio' onclick='showresult(this.value)' name='bukti' id='bukti' value='$buktis->bukti_id' required='required'><b>$buktis->kode_bukti ($buktis->deskripsi)</b></label>";
                }
            ?>
        </div>
    </div>

    <div id="form_jenis_pajak" style="display:none;">
        <div class="form-group">
            <label>Jenis Transaksi:</label><br>
            <div style='border:1px solid #ccc;font-weight:bold;padding:5px;width:100%;'>
                <label class='radio-inline'>
                    <input type="radio" name="jenis_transaksi" id="jenis_transaksi1" value='nonpajak' onclick="showjenispajak(this.value)" required="required" checked="checked"> Non Pajak
                </label>
                <label class='radio-inline'>
                    <input type="radio" name="jenis_transaksi" id="jenis_transaksi2" value='pajak' onclick="showjenispajak(this.value)" required="required"> Pajak
                </label>
                <label class='radio-inline'>
                    <span id="bunga_bank"></span>
                </label>
                
            </div>
        </div>
    </div>

    <div id="form_daftar_pajak" style="display:none;">
        <div class="form-group">
            <label>Daftar Pajak:</label>
            <select name="daftar_pajak" id="daftar_pajak" class="form-control select2" style="width:100%;">
                <option></option>
                <?php
                    foreach($daftar_pajak as $daftar_pajaks){
                        echo "<option value='$daftar_pajaks->pajak_id,$daftar_pajaks->rincian_id,$daftar_pajaks->bku_id'>$daftar_pajaks->uraian_bku | ".number_format($daftar_pajaks->jumlah_transaksi,0)."</option>";
                    }
                ?>
            </select>
        </div>
    </div>
    
    <div id="form_pajak" style="display:none;">
        <div class="form-group">
            <label>Nama Pajak:</label><br>
            <div style='border:1px solid #ccc;font-weight:bold;padding:5px;width:100%;'>
                <?php 
                    foreach($pajak as $pajaks){
                        echo "<label class='radio-inline'><input type='radio' name='pajak' id='pajak$pajaks->pajak_id' value='$pajaks->pajak_id' required='required' checked='checked'>$pajaks->nama_pajak</label>";
                    }
                ?>
            </div>
        </div>
    </div>

    <div class="form-group" id="kode_rekening" style="display:none;width:100%;">
        <label>Kode Rekening:</label>
        <select name="rekening" id="rekening" class="form-control select2" style="width:100%;">
            <option></option>
            <?php
                foreach($rekening as $rekenings){
                    echo "<option value='$rekenings->belanja_id'>$rekenings->subrekening_belanja : $rekenings->nama_subrekening</option>";
                }
            ?>
        </select>
    </div>
    <div id="loading"></div>

    <div id="nama_program">
    
    </div>

    <div id="uraian_harga" style="display:none">
        <div class="form-group">
            <label>Tanggal Transaksi:</label>
            <input type="text" name="tanggal_transaksi" class="form-control" id="tanggal_transaksi" placeholder="contoh: <?php echo date('Y-m-d'); ?>" required="required">
            <input type="hidden" name="bulan_id" class="form-control" id="bulan_id" value="<?php echo $bulan_id; ?>">
        </div>

        <div class="form-group">
            <label>Uraian:</label>
            <textarea name="uraian" id="uraian" cols="30" rows="2" class="form-control" required="required"></textarea>
        </div>

        <div class="form-group">
            <label>Jumlah Transaksi:</label>
            <input type="text" name="jumlah_transaksi" id="jumlah_transaksi" onkeyup="convertToRupiah(this)" class="form-control" required="required" placeholder="Jumlah Transaksi">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary"><span id="loading"></span> <i class="fa fa-save"></i> SIMPAN</button>
        </div>
    </div>

</form>

<?php
    }else{
?>
<!-- aksi update -->
<form action="" method="post" id="form_edit">
    <div class="form-group">
        <label>No. Bukti:</label><br>
        <div class="form-control">
            <?php echo $data_bku['kode_bukti']; echo " (".$data_bku['deskripsi'].") - "; echo $data_bku['no_urut_bukti']; ?>
        </div>
        <input type="hidden" name="bukti" class="form-control" value="<?php echo $data_bku['bukti_id']; ?>">
        <input type="hidden" name="bku_id" class="form-control" value="<?php echo $data_bku['bku_id']; ?>">
    </div>
    <?php
        if($data_bku['bukti_id'] == 1 or $data_bku['bukti_id'] == 2){ ?>
        <!-- jika bukti BK OR BM -->
            <div class="form-group">
                <label>Tanggal Transaksi:</label>
                <div class="form-control">
                    <?php echo $data_bku['tanggal_transaksi']; ?>
                </div>
                <input type="hidden" name="bulan_id" class="form-control" id="bulan_id" value="<?php echo $bulan_id; ?>">
            </div>

            <div class="form-group">
                <label>Uraian:</label>
                <textarea name="uraian" id="uraian" cols="30" rows="2" class="form-control" required="required"><?php echo $data_bku['uraian_bku']; ?></textarea>
            </div>

            <div class="form-group">
                <label>Jumlah Transaksi:</label>
                <input type="text" name="jumlah_transaksi" id="jumlah_transaksi" onkeyup="convertToRupiah(this)" class="form-control" required="required" placeholder="Jumlah Transaksi" value="<?php echo $data_bku['jumlah_transaksi']; ?>">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary"><span id="loading"></span> <i class="fa fa-save"></i> SIMPAN</button>
            </div>


    <?php
        }else{ ?>
        <!-- jika bukti bukan KM or KK -->
            <?php
                if($data_bku['bukti_id'] == 3){
                    //KM
                    if($data_bku['pajak_id'] != 0){ ?>
                        <!-- //pajak (kode rek, nama program, jenis pajak, nama pajak) -->
                        <div class="form-group" id="kode_rekening">
                            <label>Kode Rekening:</label>
                            <div class="form-control">
                                <?php
                                    echo $data_bku['subrekening_belanja']. " : ". $data_bku['nama_subrekening'];
                                ?>
                            </div>
                        </div>

                        <div class="form-group" id="form_nama_program">
                            <label>Nama Program:</label><br>
                            <select name="program" id="program" class="form-control select2" style="width:100%;">
                                <?php
                                    foreach($program as $programs){
                                        if($data_bku['rincian_id'] == $programs->rincian_id){
                                            echo "<option value='$programs->rincian_id' selected='selected'>$programs->program_kode.$programs->kode_komponen - $programs->nama_program</option>";
                                        }else{
                                            echo "<option value='$programs->rincian_id'>$programs->program_kode.$programs->kode_komponen - $programs->nama_program</option>";                            
                                        }
                                    }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Jenis Transaksi:</label><br>
                            <div style='border:1px solid #ccc;font-weight:bold;padding:5px;width:100%;'>
                                <label class='radio-inline'>
                                    <input type="radio" name="jenis_transaksi" id="jenis_transaksi2" value='pajak' onclick="showjenispajak(this.value)" required="required" checked="checked"> Pajak
                                </label>
                            </div>
                        </div>
                        <?php
                            if($data_bku['pajak_id'] != 0){ ?>
                                <!-- bukan pajak -->
                                <div id="form_pajak">
                                    <div class="form-group">
                                        <label>Nama Pajak:</label><br>
                                        <div style='border:1px solid #ccc;font-weight:bold;padding:5px;width:100%;'>
                                            <?php 
                                                foreach($pajak as $pajaks){
                                                    if($data_bku['pajak_id'] == $pajaks->pajak_id){
                                                        echo "<label class='radio-inline'><input type='radio' name='pajak' id='pajak$pajaks->pajak_id' value='$pajaks->pajak_id' required='required' checked='checked'>$pajaks->nama_pajak</label>";
                                                    }else{
                                                        echo "<label class='radio-inline'><input type='radio' name='pajak' id='pajak$pajaks->pajak_id' value='$pajaks->pajak_id' required='required'>$pajaks->nama_pajak</label>";
                                                    }
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                        <?php
                            }else{ ?>
                                <!-- pajak -->
                                <div id="form_pajak" style="display:none;">
                                    <div class="form-group">
                                        <label>Nama Pajak:</label><br>
                                        <div style='border:1px solid #ccc;font-weight:bold;padding:5px;width:100%;'>
                                            <?php 
                                                foreach($pajak as $pajaks){
                                                    echo "<label class='radio-inline'><input type='radio' name='pajak' id='pajak$pajaks->pajak_id' value='$pajaks->pajak_id' required='required' checked='checked'>$pajaks->nama_pajak</label>";
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                
                        <?php
                            } ?>
                        <!-- tutup KM pajak -->
            <?php
                    }
                }else{
                    //KK
                    if($data_bku['jenis_transaksi'] != 1){ ?>
                        <!-- //kk pajak (kode rek, nama program) -->
                        <div class="form-group" id="kode_rekening">
                            <label>Kode Rekening:</label>
                            <div class="form-control">
                                <?php
                                    echo $data_bku['subrekening_belanja']. " : ". $data_bku['nama_subrekening'];
                                ?>
                            </div>
                        </div>

                    <?php
                        if($data_bku['pajak_id'] != 0){ ?>
                            <!-- //tampilkan jenis pajak dan nama pajak(tidak bisa diedit) -->
                            <div class="form-group" id="form_nama_program">
                                <label>Nama Program:</label>
                                <div class="form-control">
                                    <?php
                                        foreach($program as $programs){
                                            if($data_bku['rincian_id'] == $programs->rincian_id){
                                                echo $programs->program_kode.".".$programs->kode_komponen." - ".$programs->nama_program;
                                            }
                                        }    
                                    ?>
                                </div>
                            </div> 
                            <div id="form_pajak">
                                <div class="form-group">
                                    <label>Nama Pajak:</label><br>
                                    <div style='border:1px solid #ccc;font-weight:bold;padding:5px;width:100%;'>
                                        <?php 
                                            foreach($pajak as $pajaks){
                                                if($pajaks->pajak_id == $data_bku['pajak_id']){
                                                    echo "<label class='radio-inline'><input type='radio' name='pajak' id='pajak$pajaks->pajak_id' value='$pajaks->pajak_id' required='required' checked='checked'>$pajaks->nama_pajak</label>";
                                                }
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }else{ ?>
                            <div class="form-group" id="form_nama_program">
                                <label>Nama Program:</label><br>
                                <select name="program" id="program" class="form-control select2" style="width:100%;">
                                    <?php
                                        foreach($program as $programs){
                                            if($data_bku['rincian_id'] == $programs->rincian_id){
                                                echo "<option value='$programs->rincian_id' selected='selected'>$programs->program_kode.$programs->kode_komponen - $programs->nama_program</option>";
                                            }else{
                                                echo "<option value='$programs->rincian_id'>$programs->program_kode.$programs->kode_komponen - $programs->nama_program</option>";                            
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                    <?php
                        }

                    }else{
                        
                    }
                }
            ?>
            

            <div class="form-group">
                <label>Tanggal Transaksi:</label>
                <div class="form-control">
                    <?php echo $data_bku['tanggal_transaksi']; ?>
                </div>
                <input type="hidden" name="bulan_id" class="form-control" id="bulan_id" value="<?php echo $bulan_id; ?>">
            </div>

            <div class="form-group">
                <label>Uraian:</label>
                <textarea name="uraian" id="uraian" cols="30" rows="2" class="form-control" required="required"><?php echo $data_bku['uraian_bku']; ?></textarea>
            </div>

            <div class="form-group">
                <label>Jumlah Transaksi:</label>
                <input type="text" name="jumlah_transaksi" id="jumlah_transaksi" onkeyup="convertToRupiah(this)" class="form-control" required="required" placeholder="Jumlah Transaksi" value="<?php echo $data_bku['jumlah_transaksi']; ?>">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary"><span id="loading"></span> <i class="fa fa-save"></i> UBAH</button>
            </div>

    <?php
        }
    ?>
    <?php
        if($data_bku['pajak_id'] == NULL){
            $nilai_pajak = 0;
        }else{
            $nilai_pajak = $data_bku['pajak_id'];
        }
    ?>
    <input type="text" name="pajak_id" value="<?php echo $nilai_pajak; ?>">
    <input type="text" name="jenis_id" value="<?php echo $data_bku['jenis_transaksi']; ?>">
</form>

<?php
    }
?>

<script>
    $(document).ready(function(){
        var bulan = "<?php echo $bulan_id; ?>";
        var tahun = "<?php echo $this->session->userdata('token_tahun'); ?>";
        var bln = parseInt(bulan)-1;
        var today = new Date(tahun,bln,1);
        var akhir = new Date(tahun,parseInt(bln)+1,0);

        $('#tanggal_transaksi').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            startDate: today,
            endDate: akhir,
            // defaultViewDate: today,
            orientation: "auto top"
        });

        $('#rekening').select2({
            placeholder: "--Pilih Kode Rekening--",
            allowClear: true
        });

        $('#program').select2({
            placeholder: "--Pilih Nama Program--",
            allowClear: true
        });
        $('#daftar_pajak').select2({
            placeholder: "--Pilih Daftar Pajak--",
            allowClear: true
        });

        // $("#tanggal_transaksi").datepicker("setDate", tahun+"-"+bulan+"-01");
    });

    $('#daftar_pajak').on('change', function(){
        var nilai = $('#daftar_pajak').val();
        var pecah = nilai.split(',');
        var id = pecah[2];
        var url = "<?php echo base_url('sekolah/get_data_pajak'); ?>";
        $.ajax({
            url: url,
            method:'post',
            data: {id:id},
            async: false,
            dataType: 'JSON',
            success: function(data){
                $('#uraian').val(data.uraian);
                $('#jumlah_transaksi').val(data.jumlah_transaksi);
            },
            error: function(){
                
            }
        });
    });

    $('#rekening').on('change', function(){
        $("#loading").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> <i>Loading...</i>");
        var id = $(this).val();
        var bulan_id = "<?php echo $bulan_id; ?>";
        var url = "<?php echo base_url('sekolah/bku_ajax') ?>";
        $.ajax({
            url: url,
            method: 'post',
            data: {id:id, bulan_id:bulan_id},
            async: false,
            success: function(data){
                $("#nama_program").html(data);
                $("#loading").html("");
                document.getElementById("uraian_harga").style.display = "block";
            }
        });
    });

    $('#form_simpan').on('submit', function(e){
        if(!e.isDefaultPrevented()){
            var url = "<?php echo base_url('sekolah/aksi_simpan_bku'); ?>";
            var input_data = new FormData($(this)[0]);
            $.ajax({
                url: url,
                type: 'post',
                dataType: "JSON",
                data: input_data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data){
                    swal({
                        title: 'Berhasil',
                        text: 'Data berhasil disimpan',
                        type: 'success',
                        timer: 1500
                    }).catch(swal.noop);
                    show_bku(data.bulan);
                    $('#formaksi').modal('hide');
                },
                error: function(){
                    swal({
                        title: 'Error!',
                        text: 'Data gagal disimpan',
                        type: 'error',
                        timer: 1500
                    }).catch(swal.noop);
                }
            });
            
            return false;
        }
    });

    $('#form_edit').on('submit', function(e){
        if(!e.isDefaultPrevented()){
            var url = "<?php echo base_url('sekolah/aksi_edit_bku'); ?>";
            var input_data = new FormData($(this)[0]);
            $.ajax({
                url: url,
                type: 'post',
                dataType: "JSON",
                data: input_data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data){
                    swal({
                        title: 'Berhasil',
                        text: 'Data berhasil diubah',
                        type: 'success',
                        timer: 1500
                    }).catch(swal.noop);
                    show_bku(data.bulan);
                    $('#formaksi').modal('hide');
                },
                error: function(){
                    swal({
                        title: 'Error!',
                        text: 'Data gagal disimpan',
                        type: 'error',
                        timer: 1500
                    }).catch(swal.noop);
                }
            });
            return false;
        }
    });

    function showresult(data)
    {
        document.getElementById("id_bukti").value=data;
        if(data == 3){
            document.getElementById("form_jenis_pajak").style.display = "block";
            document.getElementById("uraian_harga").style.display = "block";
            document.getElementById("form_pajak").style.display = "none";
            document.getElementById("kode_rekening").style.display = "none";
            document.getElementById("form_daftar_pajak").style.display = "none";
            document.getElementById("bunga_bank").innerHTML = "";
            $("#nama_program").html("");
            document.getElementById("rekening").selectedIndex = "0";
            document.getElementById("jenis_transaksi1").checked = true;
        }else if(data == 4){
            document.getElementById("form_jenis_pajak").style.display = "block";
            document.getElementById("uraian_harga").style.display = "block";
            document.getElementById("kode_rekening").style.display = "block";
            document.getElementById("form_pajak").style.display = "none";
            document.getElementById("form_daftar_pajak").style.display = "none";
            document.getElementById("bunga_bank").innerHTML = "<input type='radio' name='jenis_transaksi' id='jenis_transaksi3' value='bungabank' onclick='showjenispajak(this.value)' required='required'> Bunga Bank";
            $("#nama_program").html("");
            document.getElementById("jenis_transaksi1").checked = true;
            document.getElementById("rekening").selectedIndex = "0";
        }else{
            document.getElementById("form_jenis_pajak").style.display = "none";
            document.getElementById("uraian_harga").style.display = "block";
            document.getElementById("kode_rekening").style.display = "none";
            document.getElementById("form_pajak").style.display = "none";
            document.getElementById("form_daftar_pajak").style.display = "none";
            document.getElementById("bunga_bank").innerHTML = "";
            $("#nama_program").html("");
            document.getElementById("rekening").selectedIndex = "0";
        }
    }

    function showjenispajak(data)
    {
        var id_bukti = document.getElementById("id_bukti").value;
        if(data == "pajak"){
            if(id_bukti == 3){
                document.getElementById("form_pajak").style.display = "block";
                document.getElementById("kode_rekening").style.display = "block";
                document.getElementById("form_daftar_pajak").style.display = "none";
                document.getElementById("pajak1").checked = false;
                $("#rekening").val('').trigger('change');
            }else if(id_bukti == 4){
                document.getElementById("form_pajak").style.display = "none";
                document.getElementById("kode_rekening").style.display = "none";
                document.getElementById("form_daftar_pajak").style.display = "block";
                $("#daftar_pajak").val('').trigger('change');
                document.getElementById("program_pajak").style.display = "none";
            }
            
        }else if(data == "nonpajak"){
            if(id_bukti == 3){
                document.getElementById("form_pajak").style.display = "none";
                document.getElementById("kode_rekening").style.display = "none";
                document.getElementById("program_pajak").style.display = "none";
                document.getElementById("pajak1").checked = true;
            }else if(id_bukti == 4){
                document.getElementById("form_pajak").style.display = "none";
                document.getElementById("kode_rekening").style.display = "block";
                $("#rekening").val('').trigger('change');
                document.getElementById("form_daftar_pajak").style.display = "none";
            }
        }else{
            document.getElementById("form_pajak").style.display = "none";
            document.getElementById("kode_rekening").style.display = "none";
            document.getElementById("form_daftar_pajak").style.display = "none";
            document.getElementById("program_pajak").style.display = "none";
        }
        document.getElementById("pajak2").checked = false;
        document.getElementById("pajak3").checked = false;
        document.getElementById("pajak4").checked = false;
    } 
    function convertToRupiah(objek) {
        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length; 
        j = 0; 
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i-1,1) + separator + c;
            } else {
                c = b.substr(i-1,1) + c;
            }
        }
        objek.value = c;
    }
</script>