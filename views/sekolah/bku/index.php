<!-- Default box -->
<div id="formaksi" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Form BKU</h4>
        </div>
        <div class="modal-body">
          

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    <!-- /.modal-content -->
   </div>
  <!-- /.modal-dialog -->
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data Buku Kas Umum</h3>
    </div>
    <div class="box-body">
        <form class="form-inline" method="post" id="form_lihat">
            <div class="form-group">
                <label for="email">Tahun:</label>
                <b class="form-control" ><?php echo $tahun; ?></b>
                <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
            </div>
            <div class="form-group">
                <label for="pwd">Bulan:</label>
                <select name="bulan" id="bulan" class="form-control">
                    <!-- <option value="Januari">Januari</option> -->
                    <?php
                      foreach($bulan as $bulans){
                        echo "<option value='$bulans->bulan_id'>$bulans->nama_bulan</option>";
                      }
                    ?>
                </select>
            </div>
            <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
            <button type="submit" class="btn btn-warning"><span id="btn_caption"><i class='fa fa-send'></i> Proses</span></button>
            <br><span class="required"><i>* tahun berdasarkan yang dipilih ketika login</i></span>
          
        </form>
        <br>
        <div id="bku_show"></div>
    </div>
</div>