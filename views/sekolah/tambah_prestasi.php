                <div class="box">

                  <form role="form" method="post" action="<?php echo base_url('sekolah/upload_image'); ?>" enctype="multipart/form-data">

                    <div class="box-body">

                      <div class="form-group">

                        <label for="exampleInputEmail1">Lomba Yang Diikuti</label>

                        <select name="lomba" id="basic" class="show-tick form-control" data-live-search="true">

                          <?php

                            foreach ($lomba as $row) {

                              echo "<option value='$row->katlomba_id' data-subtext='$row->nama_katlomba'>$row->nama_lomba</option>";

                            }

                          ?>

                        </select>

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Keterangan Lomba</label>

                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Keterangan Lomba">

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Jenis Lomba</label><br>

                        <input class="flat-red" required="required" type="radio" name="jl" value="Perorangan" > Perorangan

                        <input class="flat-red" required="required" type="radio" name="jl" value="Group"> Group

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Tingkat Lomba</label>

                        <select name="tingkat" class="form-control">

                          <option value="Kabupaten">Kabupaten</option>

                          <option value="Provinsi">Provinsi</option>

                          <option value="Nasional">Nasional</option>

                          <option value="Internasional">Internasional</option>

                        </select>

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Peringkat</label>

                        <select name="peringkat" class="form-control">

                          <option value="1">1</option>

                          <option value="2">2</option>

                          <option value="3">3</option>

                          <option value="4">4</option>

                          <option value="5">5</option>

                        </select>

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Nama Siswa/i</label>

                        <textarea name="namasiswa" required="required" class="form-control"></textarea>

                        

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Tempat Lomba</label>

                        <input type="text" id="tw1" name="tempat" class="form-control" placeholder="Tempat Lomba" required="required">

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Penyelenggara</label>

                        <input type="text" id="tw1" name="penyelenggara" class="form-control" placeholder="Penyelenggara" required="required">

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Tanggal Mulai Lomba</label>

                        <input type="text" id="tglmulai" name="tglmulai" class="form-control" placeholder="Tanggal Mulai Lomba" required="required">

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Tanggal Akhir Lomba</label>

                        <input type="text" id="tglakhir" name="tglakhir" class="form-control" placeholder="Tanggal Akhir Lomba" required="required">

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Sertifikat</label>

                        <input type="file" name="sertifikat" class="form-control">

                      </div>



                      

                      

                    </div>

                    <!-- /.box-body -->



                    <div class="box-footer">

                      <button type="submit" class="btn btn-primary">SIMPAN</button>

                    </div>

                  </form>

                </div>



                <?php

                  function convertToRupiah($isi){

                    return number_format($isi,0,',','.');

                  }

                ?>



                <script>

                $(document).ready(function () {

                    $("#e1").select2();

                    $("#basic").selectpicker();



                    $('#tglmulai').datepicker({

                      format: "yyyy-mm-dd",

                      autoclose: true,

                      defaultViewDate: "today"

                    });



                    $('#tglakhir').datepicker({

                      format: "yyyy-mm-dd",

                      autoclose: true,

                      defaultViewDate: "today"

                    });

                        

                });

          </script>

          



