<!-- Default box -->
<div id="formaksi" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Form Manajemen KIB B</h4>
        </div>
        <div class="modal-body">
          

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    <!-- /.modal-content -->
   </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
  function doSearch(){
        $('#tt').datagrid('load',{
            kodebarang: $('#kode_barang').val(),
            namabarang: $('#nama_barang').val()
        });
    }
  
  function tombol(val,row){
        var idedit  = '2,'+val;
        var idhapus = '3,'+val;
        return '<a href="#" id="tombol" class="btn btn-warning btn-xs" data-id="'+idedit+'">Edit</a> <a href="#" class="btn btn-danger btn-xs" id="tombol" data-id="'+idhapus+'">Hapus</a>';
  }
</script>
<div class="box">

  <div class="box-header with-border">

    <h3 class="box-title">Data Aset Sekolah - KIB-B</h3>
  </div>
  <div class="box-body">
    <div class="alert alert-warning alert-dismissable" style="border:3px solid blue; border-style: dashed;"> 
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <strong><u>PENGUMUMAN: </u></strong> <br>
        Pada tanggal <b>22 Januari 2019 jam 01.00 WIB</b>, sistem aset sekolah dilakukan pembaharuan. Adapun pembaharuannya adalah pada <b>ASAL USUL</b> pada awalnya pengisiannya <b>DIKETIK MANUAL</b>, setelah pembaharuan sistem untuk <b>ASAL USUL</b> disediakan dalam bentuk pilihan. Atas perhatiannya kami ucapkan terima kasih.
    </div>
    <a href="#" id="tombol" data-id="1,0" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah Data</a>
            <div class="box-body">
                <p align="right">
                <?php
                  foreach ($totalaset as $key) {
                    if($key->nama_sumber_dana == "BOSNAS"){
                      $warna = "btn btn-danger btn-xs";
                    }else{
                      $warna = "btn btn-warning btn-xs";
                    }
                    echo "<p>Total Aset KIB-B tahun <span class='btn btn-success btn-xs'>$key->tahun_pembelian </span> <span class='$warna'>$key->nama_sumber_dana</span> <span class='btn btn-primary btn-xs'>Rp. ".number_format($key->total,0)."</span></p>"; 
                  }
                ?>
              </p>
              <table id="tt" class="easyui-datagrid" style="width:100% auto;height:550px auto;"
      url="<?php echo site_url("sekolah/datakibb") ?>"
      title="Cari Berdasarkan" iconCls="icon-user" toolbar="#tb" pageSize="10"
      rownumbers="true" pagination="true" method="post" singleSelect="true">
                <thead>
                  <tr>
                      <th field="id_new" formatter='tombol' rowspan="2">Action</th>
                      <th field="kode_barang" rowspan="2">Kode Barang</th>
                      <th field="no_register" rowspan="2">No. Register</th>
                      <th field="nama_barang" rowspan="2">Nama Barang</th>
                      <th data-options="field:'jumlah_barang',align:'center'" rowspan="2">Jumlah Barang</th>
                      <th data-options="field:'satuan',align:'center'" rowspan="2">Satuan</th>
                      <th data-options="field:'kondisi',align:'center'" rowspan="2">Kondisi <br>(B, RR, RB)</th>
                      <th field="merk_type" rowspan="2">Merk / Type</th>
                      <th field="ukuran_cc" rowspan="2">Ukuran / CC</th>
                      <th field="bahan" rowspan="2">Bahan</th>
                      <th field="pabrik" rowspan="2">Pabrik</th>
                      <th field="rangka" rowspan="2">Rangka</th>
                      <th field="mesin" rowspan="2">Mesin</th>
                      <th field="no_polisi" rowspan="2">No. Polisi</th>
                      <th field="bpkb" rowspan="2">BPKB</th>
                      <th data-options="field:'tahun_pembelian',align:'center'" rowspan="2">Tahun <br>Pembelian</th>
                      <th field="nama_sumber_dana" rowspan="2">Asal / Usul</th>
                      <th field="hargaperolehan_new" rowspan="2">Harga Perolehan</th>
                      <!-- <th colspan="2">Harga (Rp) / Unit</th> -->
                      <th field="total_harga" rowspan="2">Total Harga</th>
                      <th field="ket" rowspan="2">Keterangan</th>

                  </tr>
                  <tr>
                      <th field="hargaperolehan_new" >Perolehan</th>
                      <th field="jumlah_perolehan" >Jumlah</th>
                      <th field="hargaestimasi_new" >Estimasi</th>
                      <th field="jumlah_estimasi" >Jumlah</th>
                  </tr>
                </thead>
            </table>
            <div id="tb" style="padding:3px">
                <!-- <span><b>Cari Berdasrkan:</b><br></span> -->
                <div class="divider-dashed"></div>
                <span>Kode Barang: </span>
                <input id="kode_barang" style="line-height:22px;border:1px solid #ccc" onkeyup="doSearch()">
                <span>Nama Barang: </span>
                <input id="nama_barang" style="line-height:22px;border:1px solid #ccc" onkeyup="doSearch()">
                <div class="divider-dashed"></div>
              </div>
            </div>
          </div>

  </div>

<script>

  $(document).on('click','#tombol',function(e){
      e.preventDefault();
      $("#formaksi").modal('show');
      $.post("<?php echo base_url('sekolah/aksi_kibb'); ?>",
          {id:$(this).attr('data-id')},
          function(html){
              $(".modal-body").html(html);
          }   
      );
    });
</script>


