                <script type="text/javascript">
                  function rincianaksih(){
                    document.getElementById("tampil").style.display = "none";
                  }
                  function rincianaksid(){
                    document.getElementById("tampil").style.display = "block";
                  }

                  function convertToRupiah(objek) {
                    separator = ".";
                    a = objek.value;
                    b = a.replace(/[^\d]/g,"");
                    c = "";
                    panjang = b.length; 
                    j = 0; 
                    for (i = panjang; i > 0; i--) {
                      j = j + 1;
                      if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + separator + c;
                      } else {
                        c = b.substr(i-1,1) + c;
                      }
                    }
                    objek.value = c;

                  }  

                  function sum(){
                    var a = document.getElementById("volume").value;
                    var c = a.replace(/[^\d]/g,"");
                    var b = document.getElementById("harga").value;
                    var d = b.replace(/[^\d]/g,"");

                    var jumlah = parseInt(c) * parseInt(d);

                    if(!isNaN(jumlah)){
                      document.getElementById("total").value = jumlah;  
                    }else{
                      document.getElementById("total").value = 0; 
                    }

                    
                  }

                </script>

                <script type="text/javascript">
                  $(document).ready(function(){

                    //document.getElementById("sub").style.display="none";
                    $('#e1').change(function(){
                      var id=$("#e1").val();
                      if(id == "00-00"){
                        document.getElementById("sub").style.display="none"; 
                      }else{
                        document.getElementById("sub").style.display="block"; 
                        $.ajax({
                          url : "<?php echo base_url();?>sekolah/json",
                          method : "POST",
                          data : {id: id},
                          async : false,
                          dataType : 'json',
                          success: function(data){
                            var html = '';
                            
                            if(data.length >1){
                              document.getElementById("sub").style.display="block";
                              var i;
                              for(i=0; i<data.length; i++){
                                html += "<input type='radio' id='skb' name='skb' required='required' value='"+data[i].komponen_kode+"'>"+data[i].nama_komponen+"&nbsp;&nbsp;&nbsp;";
                              }
                              $('#kabupaten').html(html);
                            }else{
                              document.getElementById("sub").style.display="block";
                              var i;
                              for(i=0; i<data.length; i++){
                                html += "<input type='radio' checked='checked' id='skb' name='skb' required='required' value='"+data[i].komponen_kode+"'>"+data[i].nama_komponen+"&nbsp;&nbsp;&nbsp;";
                              }
                              $('#kabupaten').html(html); 
                            }    
                          }
                        });
                      }
                      
                    });
                  });
                </script>

                <?php
                  if($save_method == "tr"){

                ?>

                <p id="hasil"></p>
                <div class="box">
                  <!-- <form role="form" method="post" action="<?php echo base_url('sekolah/add_rincian'); ?>"> -->
                  <form>
                    <div class="box-body">
                      <div class="form-group">


                        <input type="text" id="id" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" id="kode" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <label for="exampleInputEmail1">Sub Kegiatan</label>
                        <select class="form-control select2" name="program" id="e1" style="width: 100%;">
                          <option value="00-00">--Pilih Program--</option>
                          <?php
                            foreach ($sub_program as $rows) {
                              echo "<option value='$rows->program_id-$rows->komponen_kode'>$rows->program_kode - $rows->nama_program</option>";
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group" id="sub" style="display:none;border:1px solid red;padding:5px;">
                        <label for="exampleInputEmail1">Komponen Pembiayaan</label>

                        <p id="kabupaten"></p>
                        
                      </div>


                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="namarincian" placeholder="Nama Rincian">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Status Rincian</label><br>
                        <input class="flat-red" id="srh" type="radio" name="sr" value="1" onclick="rincianaksih()"> Header
                        <input class="flat-red" id="srh" type="radio" name="sr" value="0" onclick="rincianaksid()" checked="checked"> Detail
                      </div>


                      <div id="tampil" style="display:block;">
                      <div class="row">
                        <div class="form-group">
                          
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Volume</label>
                            <input type="text" id="volume" name="volume" class="form-control" placeholder="Volume" onkeyup="convertToRupiah(this);sum();" value="0">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Satuan</label>
                            <input type="text" id="satuan" name="satuan" required="required" class="form-control" placeholder="Satuan">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Harga Satuan</label>
                            <input type="text" id="harga" name="hargasatuan" class="form-control" placeholder="Harga Satuan" onkeyup="convertToRupiah(this); sum();" value="0">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1"></label>
                        <input type="text" style="text-align: center;font-size: 20px;font-weight: bold;" readonly="readonly" id="total" name="total" class="form-control" placeholder="Triwulan I" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                      <br>
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" id="tw1" name="tw1" class="form-control" placeholder="Triwulan I" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw2" class="form-control" id="tw2" placeholder="Triwulan II" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw3" class="form-control" id="tw3" placeholder="Triwulan III" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw4" class="form-control" id="tw4" placeholder="Triwulan IV" onkeyup="convertToRupiah(this);" value="0">
                      </div>
                      </div>
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <!-- <button type="submit" class="btn btn-primary" id="btn_simpan">SIMPAN</button> -->
                      <button class="btn btn-primary" id="btn_simpan">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php 
                  }elseif ($save_method == "er") {
                ?>

                <?php
                  foreach ($rincian as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_rincian'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="hidden" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian" value="<?php echo $row->detail_rincian; ?>">
                      </div>

                      <div id="tampil" style="display:block;">
                      <div class="row">
                        <div class="form-group">
                          
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Volume</label>
                            <input type="text" id="volume" name="volume" class="form-control" placeholder="Volume" onkeyup="convertToRupiah(this);sum();" value="<?php echo $row->volume; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Satuan</label>
                            <input type="text" id="s" required="required" name="satuan" class="form-control" placeholder="Satuan" value="<?php echo $row->satuan; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Harga Satuan</label>
                            <input type="text" id="harga" name="hargasatuan" class="form-control" placeholder="Harga Satuan" onkeyup="convertToRupiah(this);sum()" value="<?php echo $row->harga_satuan; ?>">
                          </div>
                        </div>

                        

                        <div class="form-group" style="display:none;">
                          <div class="col-xs-3">
                            <label for="exampleInputEmail1">Total</label>
                            <input type="text" readonly="readonly" id="t" name="total" class="form-control" onclick="jum();" placeholder="Total" value="0">
                          </div>
                        </div>
                      </div>
                      <?php $totall = $row->volume * $row->harga_satuan; ?>
                      <div class="form-group">
                          <label for="exampleInputEmail1"></label>
                          <input type="text" style="text-align: center;font-size: 20px;font-weight: bold;" readonly="readonly" id="total" name="total" class="form-control" placeholder="Triwulan I" onkeyup="convertToRupiah(this);" value="<?php echo $totall; ?>">
                        </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw1" class="form-control" id="exampleInputEmail1" placeholder="Triwulan I" value="<?php echo $row->tw1; ?>" onkeyup="convertToRupiah(this);">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw2" class="form-control" id="exampleInputEmail1" placeholder="Triwulan II" value="<?php echo $row->tw2; ?>" onkeyup="convertToRupiah(this);">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw3" class="form-control" id="exampleInputEmail1" placeholder="Triwulan III" value="<?php echo $row->tw3; ?>" onkeyup="convertToRupiah(this);">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw4" class="form-control" id="exampleInputEmail1" placeholder="Triwulan IV" value="<?php echo $row->tw4; ?>" onkeyup="convertToRupiah(this);">
                      </div>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php }elseif ($save_method == "eh") { ?>

                <?php
                  foreach ($rincian as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_header'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="hidden" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian" value="<?php echo $row->detail_rincian; ?>">
                      </div>

                      

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>


                <?php }elseif ($save_method == "trh") {?>

                <?php
                  foreach ($head as $keys) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/add_rincian_header'); ?>">
                    <div class="box-body">
                      <div class="form-group">


                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="hidden" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <label for="exampleInputEmail1">Nama Header</label>
                        <input type="text" readonly="readonly" name="namaheader" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Header" value="<?php echo $keys->detail_rincian; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian">
                      </div>

                      <div id="tampil" style="display:block;">
                      <div class="row">
                        <div class="form-group"></div>
                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Volume</label>
                            <input type="text" id="volume" name="volume" class="form-control" placeholder="Volume" onkeyup="convertToRupiah(this);sum();" value="0">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Satuan</label>
                            <input type="text" id="satuan" required="required" name="satuan" class="form-control" placeholder="Satuan">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Harga Satuan</label>
                            <input type="text" id="harga" name="hargasatuan" class="form-control" placeholder="Harga Satuan" onkeyup="convertToRupiah(this);sum();" value="0">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                          <label for="exampleInputEmail1"></label>
                          <input type="text" style="text-align: center;font-size: 20px;font-weight: bold;" readonly="readonly" id="total" name="total" class="form-control" placeholder="Triwulan I" onkeyup="convertToRupiah(this);" value="0">
                        </div>

                      <div class="form-group">
                      <br>
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="text" name="tw1" class="form-control" id="exampleInputEmail1" placeholder="Triwulan I" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="text" name="tw2" class="form-control" id="exampleInputEmail1" placeholder="Triwulan II" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="text" name="tw3" class="form-control" id="exampleInputEmail1" placeholder="Triwulan III" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="text" name="tw4" class="form-control" id="exampleInputEmail1" placeholder="Triwulan IV" onkeyup="convertToRupiah(this);" value="0">
                      </div>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php }elseif ($save_method == "erh") {?>

                <?php
                  foreach ($rincian as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_rincian_header'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="hidden" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian" value="<?php echo $row->header_rincian; ?>">
                      </div>

                      <div id="tampil" style="display:block;">
                      <div class="row">
                        <div class="form-group"></div>
                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Volume</label>
                            <input type="text" id="volume" name="volume" class="form-control" placeholder="Volume" onkeyup="convertToRupiah(this);sum();" value="<?php echo $row->volume_hr; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Satuan</label>
                            <input type="text" id="satuan" required="required" name="satuan" class="form-control" placeholder="Satuan" value="<?php echo $row->satuan_hr; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Harga Satuan</label>
                            <input type="text" id="harga" name="hargasatuan" class="form-control" placeholder="Harga Satuan" onkeyup="convertToRupiah(this);sum();" value="<?php echo $row->harga_satuan_hr; ?>">
                          </div>
                        </div>
                      </div>
                      <?php $totall = $row->volume_hr * $row->harga_satuan_hr; ?>
                      <div class="form-group">
                          <label for="exampleInputEmail1"></label>
                          <input type="text" style="text-align: center;font-size: 20px;font-weight: bold;" readonly="readonly" id="total" name="total" class="form-control" placeholder="Triwulan I" onkeyup="convertToRupiah(this);" value="<?php echo $totall; ?>">
                        </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw1" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan I" value="<?php echo $row->htw1; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw2" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan II" value="<?php echo $row->htw2; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw3" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan III" value="<?php echo $row->htw3; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw4" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan IV" value="<?php echo $row->htw4; ?>">
                      </div>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php } ?>




                <!-- convert rupiah -->

                <?php
                  function convertToRupiah($isi){
                    return number_format($isi,0,',','.');
                  }
                ?>

          <script type="text/javascript">
              $(document).ready(function(){
                  $("#e1").select2();

                  //Save product
                  $('#btn_simpan').on('click',function(){
                    
                    // var kode = $('#kode').val();
                    // var program = $('#e1').val();
                    // var skb = $('#skb').val();
                    var nama = $('#namarincian').val();
                    // var sr = $('#srh').val();
                    // var volume = $('#volume').val();
                    // var satuan = $('#satuan').val();
                    // var hargasatuan = $('#harga').val();
                    // var tw1 = $('#tw1').val();
                    // var tw2 = $('#tw2').val();
                    // var tw3 = $('#tw3').val();
                    // var tw4 = $('#tw4').val();
                    $.ajax({
                        type : "POST",
                        url  : "<?php echo base_url('sekolah/add_tes')?>",
                        dataType : "JSON",
                        data : {sr:sr , kode:kode, harga:harga, program:program, nama:nama, skb:skb, volume:volume, tw1:tw1, tw2:tw2, tw3:tw3, tw4:tw4 },
                        success: function(data){

                        }
                    });
                      return false;
                  });
              });
          
          </script>
          

