<?php
  $tahun = $this->session->userdata('token_tahun');
  $sekolahid = $this->session->userdata('token_sekolah_id');

  function rpbold($a){
    $h = "<b>".number_format($a,0,',','.')."</b>";
    return $h;
  }
  function rp($a){
    $h = number_format($a,0,',','.');
    return $h;
  }

  $this->db->where('soft_delete',0);
  $query = $this->db->get('standar_nasional');
  //$query = $this->db->query("SELECT * FROM rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id join sub_standar ss on sp.substandar_id=ss.substandar_id join standar_nasional sn on ss.standar_id=sn.standar_id");
  if($query){
    $data = $query->result();
  }

  $sql10 = $this->db->query("SELECT pb.jumlah_siswa, s.jenjang FROM penerima_bos pb left join sekolah s on pb.npsn=s.npsn where s.sekolah_id='$sekolahid' and pb.tahun='$tahun'");
  if($sql10->num_rows() > 0){
    foreach ($sql10->result() as $row10) {
      if($row10->jenjang == "SMA" OR $row10->jenjang == "SMK"){
        $jumlahterima = $row10->jumlah_siswa * 1400000;
      }else{
        $jumlahterima = 0;
      }
    }
  }else{
    $jumlahterima = 0;   
  }

  $sql12 = $this->db->query("select sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join rincian_header rh on rb.rincian_id=rh.rincian_id where rb.soft_delete='0' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql12 as $row12) {
                        $ttw1 = $row12->total+$row12->total2;
                        $ttw2 = $row12->total3+$row12->total4;
                        $ttw3 = $row12->total5+$row12->total6;
                        $ttw4 = $row12->total7+$row12->total8;
                      }

                      $sball = $ttw1+$ttw2+$ttw3+$ttw4;

  

  if($sball >= $jumlahterima){
    $show = "disabled='disabled'";
    $idi = "#";
    $pesan = "<font color='red'><i> Anda Tidak Bisa Menambahkan Rincian Belanja Lagi, Karena Total Belanja Sudah Melebihi atau sudah sama dengan Pendapatan, Silahkan mengubah atau menghapus rincian belanja.</i></font>";
  }else{
    $show = " ";
    $idi = "rincian";
    $pesan ="";
  }


?>

<div class="box box-primary">
  <div class="box-header with-border">
  <?php echo "<span class='btn btn-success btn-lg'> Pendapatan: ".rpbold($jumlahterima)."</span> 
              <span class='btn btn-primary btn-lg'>Total Belanja: ".rpbold($sball)."</span>
              <span class='btn btn-warning btn-lg'>Sisa / Lebih: ".rpbold($jumlahterima -$sball)."</span>";
              //<span class='btn btn-danger btn-lg'>Lebih: ".rpbold($sball-$jumlahterima )."</span>"; ?>
  <H4>8 STANDAR NASIONAL</H4>
<input type="hidden" id="kode" value="<?php echo $belanja_id; ?>">
<div class="panel-group" id="accordion">
  <?php foreach ($data as $row1) { ?>
  <div class="panel panel-success">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $row1->standar_id; ?>">
          <?php 
            $sql8 = $this->db->query("select sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join rincian_header rh on rb.rincian_id=rh.rincian_id left join sub_program sp on rb.program_id=sp.program_id left join sub_standar ss on sp.substandar_id=ss.substandar_id left join standar_nasional sn on ss.standar_id=sn.standar_id where rb.soft_delete='0' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid' and sn.standar_id='$row1->standar_id' and rb.belanja_id='$belanja_id'");

            foreach ($sql8->result() as $row8) {
              $totalstandar = $row8->total + $row8->total2 + $row8->total3 + $row8->total4 + $row8->total5 + $row8->total6 + $row8->total7 + $row8->total8; 
            }
          ?>
          <?php echo $row1->standar_kode." - ".$row1->nama_standar. " - "; echo "Total: Rp. " .rp($totalstandar); ?>  
          
        </a>
      </h4>
    </div>
    <div id="<?php echo $row1->standar_id; ?>" class="panel-collapse collapse">
      <div class="panel-body">

        <div class="panel-group" id="<?php echo $row1->standar_id.$row1->standar_kode; ?>">
          <?php
            $sql2 = $this->db->query("SELECT * FROM sub_standar where standar_id='$row1->standar_id' AND soft_delete='0'")->result();
            foreach ($sql2 as $row2) {
          ?>
          <div class="panel panel-warning">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#<?php echo $row1->standar_id.$row1->standar_kode; ?>" href="#we<?php echo $row2->substandar_id; ?>">
                <?php 
                  $sql9 = $this->db->query("select sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join rincian_header rh on rb.rincian_id=rh.rincian_id left join sub_program sp on rb.program_id=sp.program_id left join sub_standar ss on sp.substandar_id=ss.substandar_id left join standar_nasional sn on ss.standar_id=sn.standar_id where rb.soft_delete='0' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid' and ss.substandar_id='$row2->substandar_id' and rb.belanja_id='$belanja_id'");

                  foreach ($sql9->result() as $row9) {
                    $totalsubstandar = $row9->total + $row9->total2 + $row9->total3 + $row9->total4 + $row9->total5 + $row9->total6 + $row9->total7 + $row9->total8; 
                  }
                ?>
                <?php echo $row2->substandar_kode." - ".$row2->namasub_standar. " - "; echo "Total: Rp. " .rp($totalsubstandar);  ?>
                </a>
              </h4>
            </div>
            <div id="we<?php echo $row2->substandar_id; ?>" class="panel-collapse collapse">
              <div class="panel-body">
                <a href="#" class="btn btn-success" <?php echo $show; ?> id="<?php echo $idi; ?>" data-id="<?php echo $row2->substandar_id; ?>" ><i class="fa fa-plus"></i> Rincian</a> 
                <span><?php echo $pesan; ?></span>
                
                <br><br>
                <p>Data Rincian Belanja</p>
                <table border="0" class="table table-hover">
                  <tr>
                    <th>Kode Program</th>
                    <th>Detail Rincian</th>
                    <th>Volume</th>
                    <th>Satuan</th>
                    <th>Harga Satuan</th>
                    <th>Triwulan I</th>
                    <th>Triwulan II</th>
                    <th>Triwulan III</th>
                    <th>Triwulan IV</th>
                    <th>Action</th>
                  </tr>
                  <?php
                    //$sql3 = $this->db->query("SELECT * FROM rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id join sub_standar ss on sp.substandar_id=ss.substandar_id WHERE ss.substandar_id='$row2->substandar_id'")->result();
                    $sql3 = $this->db->query("SELECT *, rb.komponen_kode as kom_kode FROM rincian_belanja rb join sub_program sp on rb.program_id=sp.program_id WHERE sp.substandar_id='$row2->substandar_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid' and rb.belanja_id='$belanja_id'  AND rb.soft_delete='0' group by rb.program_id,rb.komponen_kode")->result();



                    foreach ($sql3 as $row3) {
                      $x = $row3->komponen_kode;
                      $sql7 = $this->db->query("select *, sum(tw1) as total, sum(htw1) as total2, sum(tw2) as total3, sum(htw2) as total4, sum(tw3) as total5,sum(htw3) as total6, sum(tw4) as total7,sum(htw4) as total8 from rincian_belanja rb left join sub_program sp on rb.program_id=sp.program_id left join rincian_header rh on rb.rincian_id=rh.rincian_id where rb.program_id='$row3->program_id' and rb.komponen_kode='$row3->kom_kode' and rb.soft_delete='0' and rb.belanja_id='$belanja_id' and rb.tahun='$tahun' and rb.sekolah_id='$sekolahid'")->result();
                      foreach ($sql7 as $row7) {
                        $satu = $row7->total+$row7->total2;
                        $dua = $row7->total3+$row7->total4;
                        $tiga = $row7->total5+$row7->total6;
                        $empat = $row7->total7+$row7->total8;
                      }
                      echo "<tr>
                        <td><b>$row3->program_kode.$row3->kom_kode</b></td>
                        <td><b>$row3->nama_program</b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>".rpbold($satu)."</td>
                        <td>".rpbold($dua)."</td>
                        <td>".rpbold($tiga)."</td>
                        <td>".rpbold($empat)."</td>
                        <td></td>
                      </tr>";

                      $sql4 = $this->db->query("SELECT * FROM rincian_belanja where program_id='$row3->program_id' and komponen_kode='$row3->kom_kode' AND soft_delete='0' and tahun='$tahun' and belanja_id='$belanja_id' and sekolah_id='$sekolahid' ")->result();
                      foreach ($sql4 as $row4) {
                        if($row4->status_rincian ==1){
                          $dr = "<b>".$row4->detail_rincian."</b>";
                          $action = "<div class='input-group-btn'><a class='btn btn-success btn-xs dropdown-toggle' data-toggle='dropdown'>Action
                            <span class='fa fa-caret-down'></span></a>
                            <ul class='dropdown-menu' style='position:absolute;'>
                              <li><a href='#' id='rincianheader' data-id='$row4->rincian_id'>Tambah Rincian</a></li>
                              <li><a href='#' id='editheader' data-id='$row4->rincian_id'>Ubah</a></li>
                              <li><a href='".base_url('sekolah/hapusrincian/'.$row4->rincian_id."/".$belanja_id)."' onclick=\"return confirm('Anda Yakin Menghapus Data ?')\">Hapus</a></li>
                            </ul></div>";//<a href='#' class='btn btn-success btn-xs' id='rincianheader' data-id='$row4->rincian_id'><i class='fa fa-plus'></i> Rincian Header</a><br> <a href='#' class='btn btn-primary btn-xs'>Ubah</a> <a href='".base_url('sekolah/hapusrincian/'.$row4->rincian_id)."' class='btn btn-danger btn-xs' onclick=\"return confirm('Yakin ?')\">Hapus</a>";

                          $sql6 = $this->db->query("SELECT *, SUM(htw1) as sumhtw1,SUM(htw2) as sumhtw2,SUM(htw3) as sumhtw3,SUM(htw4) as sumhtw4 FROM rincian_header where rincian_id='$row4->rincian_id'  AND soft_delete='0'")->result();
                          foreach ($sql6 as $row6) {
                            $isitw1 = "<b>".rpbold($row6->sumhtw1)."</b>"; $isitw2="<b>".rpbold($row6->sumhtw2)."</b>"; $isitw3="<b>".rpbold($row6->sumhtw3)."</b>"; $isitw4="<b>".rpbold($row6->sumhtw4)."</b>";

                            $vol = ""; $sat =""; $hargasat="";
                          }
                          
                        //rincian belanja non header
                        }else{
                          $dr = "- ".$row4->detail_rincian;
                          $action = "<a href='#' class='btn btn-primary btn-xs' id='editrincian' data-id='$row4->rincian_id'>Ubah</a> <a href='".base_url('sekolah/hapusrincian/'.$row4->rincian_id."/".$belanja_id)."' class='btn btn-danger btn-xs' onclick=\"return confirm('Anda Yakin Menghapus Data ?')\">Hapus</a>";

                          $isitw1 = rp($row4->tw1); $isitw2=rp($row4->tw2); $isitw3=rp($row4->tw3); $isitw4=rp($row4->tw4);

                          $vol = $row4->volume; $sat =$row4->satuan; $hargasat=rp($row4->harga_satuan);
                        }
                        echo "<tr>
                          <td></td>
                          <td>$dr</td>
                          <td>$vol</td>
                          <td>$sat</td>
                          <td>$hargasat</td>
                          <td>$isitw1</td>
                          <td>$isitw2</td>
                          <td>$isitw3</td>
                          <td>$isitw4</td>
                          <td>$action</td>
                        </tr>"; 

                        if($row4->status_rincian == 1){
                          $sql5 = $this->db->query("SELECT * FROM rincian_header where rincian_id='$row4->rincian_id'  AND soft_delete='0'")->result();
                          foreach ($sql5 as $row5) {
                            echo "<tr>
                              <td></td>
                              <td>-- $row5->header_rincian</td>
                              <td>$row5->volume_hr</td>
                              <td>$row5->satuan_hr</td>
                              <td>".rp($row5->harga_satuan_hr)."</td>
                              <td>".rp($row5->htw1)."</td>
                              <td>".rp($row5->htw2)."</td>
                              <td>".rp($row5->htw3)."</td>
                              <td>".rp($row5->htw4)."</td>
                              <td><a href='#' class='btn btn-primary btn-xs' id='editrincianheader' data-id='$row5->header_id'>Ubah</a> <a href='".base_url('sekolah/hapusrincianheader/'.$row5->header_id."/".$belanja_id)."' class='btn btn-danger btn-xs' onclick=\"return confirm('Anda Yakin Menghapus Data ?')\">Hapus</a></td>
                            </tr>"; 
                          }
                        }
                      }



                    }

                  ?>
                </table>
              </div>
            </div>
          </div>
          <?php } ?>

        </div>

      </div>
    </div>
  </div>
  <?php } ?>
</div>


      <div id="tambahrincian" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Rincian Belanja</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="tambahrincianheader" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Rincian Belanja Header</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


        <div id="editrincianbelanja" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Edit Rincian Belanja</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="editdataheader" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Data Header</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="editdatarincianheader" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Data Rincian Header</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  </div>
</div>

        <script>
            $(document).ready(function () {
              $(document).on('click','#editrincian',function(e){
                e.preventDefault();
                $("#editrincianbelanja").modal('show');
                $.post("<?php echo base_url('sekolah/editrincian'); ?>",
                    {id3:$(this).attr('data-id'),kode:$("#kode").val()},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });
              
              $(document).on('click','#rincian',function(e){
                e.preventDefault();
                $("#tambahrincian").modal('show');
                $.post("<?php echo base_url('sekolah/tambahrincian'); ?>",
                    {id:$(this).attr('data-id'),kode:$("#kode").val()},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#rincianheader',function(e){
                e.preventDefault();
                $("#tambahrincianheader").modal('show');
                $.post("<?php echo base_url('sekolah/tambahrincianheader'); ?>",
                    {id2:$(this).attr('data-id'),kode:$("#kode").val()},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              

              $(document).on('click','#editheader',function(e){
                e.preventDefault();
                $("#editdataheader").modal('show');
                $.post("<?php echo base_url('sekolah/editheader'); ?>",
                    {id3:$(this).attr('data-id'),kode:$("#kode").val()},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#editrincianheader',function(e){
                e.preventDefault();
                $("#editdatarincianheader").modal('show');
                $.post("<?php echo base_url('sekolah/editrincianheader'); ?>",
                    {id3:$(this).attr('data-id'),kode:$("#kode").val()},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
