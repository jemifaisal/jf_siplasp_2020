                <script type="text/javascript">
                    function convertToRupiah(objek) {
                      separator = ".";
                      a = objek.value;
                      b = a.replace(/[^\d]/g,"");
                      c = "";
                      panjang = b.length; 
                      j = 0; 
                      for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                          c = b.substr(i-1,1) + separator + c;
                        } else {
                          c = b.substr(i-1,1) + c;
                        }
                      }
                      objek.value = c;

                    } 
                  
                </script>

                <?php
                  foreach ($head as $keys) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/add_rincian_header'); ?>">
                    <div class="box-body">
                      <div class="form-group">


                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="hidden" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <label for="exampleInputEmail1">Nama Header</label>
                        <input type="text" readonly="readonly" name="namaheader" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Header" value="<?php echo $keys->detail_rincian; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian">
                      </div>

                      <div id="tampil" style="display:block;">
                      <div class="row">
                        <div class="form-group"></div>
                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Volume</label>
                            <input type="text" id="volume" name="volume" class="form-control" placeholder="Volume" onkeyup="convertToRupiah(this);" value="0">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Satuan</label>
                            <input type="text" id="satuan" required="required" name="satuan" class="form-control" placeholder="Satuan">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Harga Satuan</label>
                            <input type="text" id="harga" name="hargasatuan" class="form-control" placeholder="Harga Satuan" onkeyup="convertToRupiah(this);" value="0">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                      <br>
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="text" name="tw1" class="form-control" id="exampleInputEmail1" placeholder="Triwulan I" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="text" name="tw2" class="form-control" id="exampleInputEmail1" placeholder="Triwulan II" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="text" name="tw3" class="form-control" id="exampleInputEmail1" placeholder="Triwulan III" onkeyup="convertToRupiah(this);" value="0">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="text" name="tw4" class="form-control" id="exampleInputEmail1" placeholder="Triwulan IV" onkeyup="convertToRupiah(this);" value="0">
                      </div>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php
                  function convertToRupiah($isi){
                    return number_format($isi,0,',','.');
                  }
                ?>

                <script>
            $(document).ready(function () {
                $("#e1").select2();
            });
          </script>

