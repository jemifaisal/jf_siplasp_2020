<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" id="nama" class="form-control">
            </div>
            <div class="form-group">
                <button id="btn_save" class="btn btn-primary">Simpan</button>
            </div>
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<script type="text/javascript">
    $(document).ready(function(){
        //Save product
        $('#btn_save').on('click',function(){
            var nama = $('#nama').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('sekolah/add_tes')?>",
                dataType : "JSON",
                data : {nama:nama},
                success: function(data){
                    //$("#myModal").modal('hide');
                    $("#myModal").modal('hide');
                }
            });
            return false;
        });
    });
 
</script>