<script type="text/javascript">
  $(document).ready(function(){
    var ukuran = 0;

    $("#lampiran").bind('change',function(){ukuran = this.files[0].size/1024/600;if(eval(ukuran)>1){$("#lampiran").val('');$("#lampiran").focus;$("#n1").html('[ Maaf Ukuran File Melebihi batas yaitu 600 KB ]');$("#y1").html('');return false;
        }else{$("#y1").html('[ Lampiran Diterima, silahkan klik tombol Upload File ]');$("#n1").html('');return true;}});
  });
</script>
<?php
  $tahun = date('Y');
  $periode = 1;
  $sid = $this->session->userdata('token_sekolah_id');
?>
         <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Tamsil Pegawai</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <div class="alert alert-danger alert-dismissable" style="border:3px solid blue; border-style: dashed;">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong><u>PENGUMUMAN: </u></strong> <br>
            Penginputan TAMSIL Pegawai akan berakhir pada hari <B>SENIN</B> tanggal <b>24 Juni 2019</b> pukul <b>23.59 WIB.</b><BR>
            Informasi lebih lanjut mengenai TAMSIL Pegawai:<BR>
            <b>Wandi : 0812-6864-913</b>
        </div>

        <div class="alert alert-warning alert-dismissable" style="border:3px solid blue; border-style: dashed;">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <h2>PENGUSULAN DATA TAMSIL PEGAWAI SUDAH DITUTUP</h2>
            <h2>Terima Kasih atas Kerja Sama Anda.</h2>
        </div>

          

          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_tamsil'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
