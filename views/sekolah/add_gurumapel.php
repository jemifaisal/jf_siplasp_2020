                <script type="text/javascript">

                  function pns(){

                    //alert('woi');
                    document.getElementById("nnip").value = "";
                    document.getElementById("form").style.display ="block";

                    document.getElementById("nip").style.display ="block";

                    document.getElementById("pangkat").style.display ="block";

                  }



                  function nonpns(){

                    //alert('woi');
                    document.getElementById("nnip").value = "-";
                    document.getElementById("form").style.display ="block";

                    document.getElementById("nip").style.display ="none";

                    document.getElementById("pangkat").style.display ="none";

                  }  

                </script>

                <div class="box">

                  <form role="form" method="post" action="<?php echo base_url('sekolah/add_gurumapel'); ?>">

                    <div class="box-body">

                      <div class="form-group" style="border:1px solid red;padding: 10px;">

                        <div class="row container">

                          <label for="exampleInputEmail1">Status Guru</label>

                        </div>

                        <input type="radio" name="statusguru" required="required" class="minimal" id="statusguru" onclick="pns();" value="PNS"> PNS

                        <input type="radio" name="statusguru" required="required" class="minimal" id="statusguru" onclick="nonpns();" value="NON PNS"> NON PNS

                      </div>

                    <div id="form" style="display: none;">

                      <div class="form-group" id="nip" style="display: none;">

                        <label for="exampleInputEmail1">NIP</label>

                        <input type="text" name="nip" required="required" class="form-control" id="nnip" placeholder="NIP">

                        <span class=""><i>*penulisan NIP tidak perlu menggunakan spasi.</i></span>

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">NUPTK</label>

                        <input type="text" name="nuptk" required="required" class="form-control" id="exampleInputEmail1" placeholder="NUPTK">

                        <span class=""><i>*jika tidak memiliki NUPTK, diisi dengan tanda strip (-).</i></span>

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Gelar Depan</label>

                        <input type="text" name="gelardepan" class="form-control" id="exampleInputEmail1" placeholder="Gelar Depan">

                        <span class=""><i>*jika tidak memiliki Gelar Depan, dikosongkan saja.</i></span>

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Nama Lengkap</label>

                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Lengkap">

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Gelar Belakang</label>

                        <input type="text" name="gelarbelakang" class="form-control" id="exampleInputEmail1" placeholder="Gelar Belakang">

                        <span class=""><i>*jika tidak memiliki Gelar Belakang, dikosongkan saja.</i></span>

                      </div>

                      <div class="form-group">

                        <div class="row container">

                          <label for="exampleInputEmail1">Jenis Kelamin</label>

                        </div>

                        <input type="radio" name="jk" required="required" class="minimal" value="L"> Laki-laki

                        <input type="radio" name="jk" required="required" class="minimal" value="P"> Perempuan

                      </div>



                      <div class="form-group">

                        <label for="exampleInputEmail1">Agama</label>

                        <select name="agama" class="form-control">

                          <?php

                            foreach ($agama as $key2) {

                              echo "<option value='$key2->agama_id'>$key2->nama_agama</option>";

                            }

                          ?>

                        </select>

                      </div>

                      <div class="form-group">

                        <label for="exampleInputEmail1">Tempat Lahir</label>

                        <input type="text" name="tempatlahir" required="required" class="form-control" id="exampleInputEmail1" placeholder="Tempat Lahir">

                      </div>

                      <div class="form-group">

                        <label for="exampleInputEmail1">Tanggal Lahir</label>

                        <input type="text" id="tgllahir" name="tgllahir" class="form-control" placeholder="Tanggal Lahir" required="required">

                      </div>

                      <div class="form-group">

                        <label for="exampleInputEmail1">Email</label>

                        <input type="email" name="email" required="required" class="form-control" id="exampleInputEmail1" placeholder="Email">

                      </div>

                      <div class="form-group">

                        <label for="exampleInputEmail1">No. HP</label>

                        <input type="text" name="nohp" required="required" class="form-control" id="exampleInputEmail1" placeholder="No. HP">

                      </div>

                      <div class="form-group">

                        <label for="exampleInputEmail1">Pendidikan Terakhir</label>

                        <select name="pddk" class="form-control">

                          <?php

                            foreach ($pddk as $key3) {

                              echo "<option value='$key3->pdd_terakhir_id'>$key3->nama_pdd</option>";

                            }

                          ?>

                        </select>

                      </div>

                      <div class="form-group">

                        <label for="exampleInputEmail1">Jurusan</label>

                        <input type="text" name="jurusan" required="required" class="form-control" id="exampleInputEmail1" placeholder="Jurusan">

                      </div>

                      <div class="form-group" style="display: none;" id="pangkat">

                        <label for="exampleInputEmail1">Pangkat / Golongan</label>

                        <select name="pangkat" class="form-control select2" style="width: 100%;" data-placeholder="Pilih Data" required="required">

                          <?php

                            foreach ($pangkat as $key) {

                              echo "<option value='$key->kd_pangkat'>$key->nama_pangkat - $key->golongan - $key->jabatan</option>";

                            }

                          ?>

                        </select>

                      </div>



                    </div>



                      

                    </div>

                    <!-- /.box-body -->



                    <div class="box-footer">

                      <button type="submit" class="btn btn-primary">Simpan</button>

                    </div>

                  </form>

                </div>



                <script>

                  $(document).ready(function () {

                    $('#tgllahir').datepicker({

                      format: "yyyy-mm-dd",

                      autoclose: true,

                      defaultViewDate: "today"

                    }); 



                    $('.select2').select2();   

                          

                  });

                </script>

