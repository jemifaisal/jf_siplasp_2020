<?php if($tombol == 1){ ?>
  <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="<?php echo base_url('sekolah/add_kibe'); ?>">
    
    <div class="divider-dashed"></div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="kodebarang" placeholder="Kode Barang" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No. Register Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="noregister" placeholder="No. Register Barang" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="namabarang" placeholder="Nama Barang" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kondisi </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <select class="form-control col-md-7 col-xs-12" name="kondisi">
          <option value="B">Baik</option>
          <option value="RR">Rusak Ringan</option>
          <option value="RB">Rusak Berat</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Judul Buku <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="judul" placeholder="Judul Buku" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Spesifikasi <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="spesifikasi" placeholder="Spesifikasi" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Barang Bercorak Kesenian/Kebudayaan Asal Daerah <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="asaldaerah" placeholder="Barang Bercorak Kesenian/Kebudayaan Asal Daerah" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Barang Bercorak Kesenian/Kebudayaan Pencipta <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="pencipta" placeholder="Barang Bercorak Kesenian/Kebudayaan Pencipta" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Barang Bercorak Kesenian/Kebudayaan Bahan <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="bahan" placeholder="Barang Bercorak Kesenian/Kebudayaan Bahan" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jumlah Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="jumlah" placeholder="Jumlah Barang" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tahun Cetak / Pembelian </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="tahuncetak" id="tahuncetak" required="required" placeholder="Tahun Cetak / Pembelian" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <!--<div class="form-group">-->
    <!--  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Asal Usul </label>-->
    <!--  <div class="col-md-9 col-sm-9 col-xs-12">-->
    <!--    <input type="text" name="asal" required="required" placeholder="Asal Usul" class="form-control col-md-7 col-xs-12">-->
    <!--  </div>-->
    <!--</div>-->
    
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Asal Usul </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <select class="form-control col-md-7 col-xs-12" name="asal">
          <?php
            foreach ($sumberdana as $value) {
              echo "<option value='$value->dana_id'>$value->nama_sumber_dana</option>";
            }
          ?>
        </select>
      </div>
    </div>
    
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Perolehan (Rp)</label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="hargaperolehan" required="required" placeholder="Harga Perolehan (Rp)" class="form-control col-md-7 col-xs-12">
      </div>
    </div>
    <!--<div class="form-group">-->
    <!--  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Estimasi (Rp)</label>-->
    <!--  <div class="col-md-9 col-sm-9 col-xs-12">-->
    <!--    <input type="text" name="hargaestimasi" required="required" placeholder="Harga Estimasi (Rp)" class="form-control col-md-7 col-xs-12">-->
    <!--  </div>-->
    <!--</div>-->

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <textarea name="ket" class="form-control"></textarea>
      </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="submit" class="btn btn-success">Simpan</button>
      </div>
    </div>
  </form>

<?php }else if($tombol == 2){ foreach ($datakibe as $rows) {
  # code...
} ?>

<form id="demo-form2" class="form-horizontal form-label-left" method="post" action="<?php echo base_url('sekolah/edit_kibe/'.$rows->id); ?>">
    
    <div class="divider-dashed"></div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="hidden" name="idkibe" placeholder="Kode Barang" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->id; ?>">
        <input type="text" name="kodebarang" placeholder="Kode Barang" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->kode_barang; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No. Register Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="noregister" placeholder="No. Register Barang" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->no_register; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="namabarang" placeholder="Nama Barang" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->nama_barang; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kondisi </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <select class="form-control col-md-7 col-xs-12" name="kondisi">
          <?php 
            if($rows->kondisi == "B"){
              echo "<option value='B' selected='selected'>Baik</option>
                    <option value='RR'>Rusak Ringan</option>
                    <option value='RB'>Rusak Berat</option>";
            }else if($rows->kondisi == "RR"){
              echo "<option value='B'>Baik</option>
                    <option value='RR' selected='selected'>Rusak Ringan</option>
                    <option value='RB'>Rusak Berat</option>";
            }else{
              echo "<option value='B'>Baik</option>
                    <option value='RR'>Rusak Ringan</option>
                    <option value='RB' selected='selected'>Rusak Berat</option>";
            }
          ?>
          
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Judul Buku <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="judul" placeholder="Judul Buku" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->judul; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Spesifikasi <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="spesifikasi" placeholder="Spesifikasi" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->spesifikasi; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Barang Bercorak Kesenian/Kebudayaan Asal Daerah <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="asaldaerah" placeholder="Barang Bercorak Kesenian/Kebudayaan Asal Daerah" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->asal_daerah; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Barang Bercorak Kesenian/Kebudayaan Pencipta <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="pencipta" placeholder="Barang Bercorak Kesenian/Kebudayaan Pencipta" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->pencipta; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Barang Bercorak Kesenian/Kebudayaan Bahan <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="bahan" placeholder="Barang Bercorak Kesenian/Kebudayaan Bahan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->bahan; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jumlah Barang <span class="required">*</span>
      </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="jumlah" placeholder="Jumlah Barang" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->jumlah; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tahun Cetak / Pembelian </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="tahuncetak" id="tahuncetak" required="required" placeholder="Tahun Cetak / Pembelian" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->tahun_pembelian; ?>">
      </div>
    </div>
    <!--<div class="form-group">-->
    <!--  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Asal Usul </label>-->
    <!--  <div class="col-md-9 col-sm-9 col-xs-12">-->
    <!--    <input type="text" name="asal" required="required" placeholder="Asal Usul" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->asal_usul; ?>">-->
    <!--  </div>-->
    <!--</div>-->
    
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Asal Usul </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <select class="form-control col-md-7 col-xs-12" name="asal">
          <?php
            foreach ($sumberdana as $value) {
              if($rows->asal_usul == $value->dana_id){
                echo "<option value='$value->dana_id' selected='selected'>$value->nama_sumber_dana</option>";
              }else{
                echo "<option value='$value->dana_id'>$value->nama_sumber_dana</option>"; 
              }
              
            }
          ?>
        </select>
      </div>
    </div>
    
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Perolehan (Rp)</label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input type="text" name="hargaperolehan" required="required" placeholder="Harga Perolehan (Rp)" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->harga_perolehan; ?>">
      </div>
    </div>
    <!--<div class="form-group">-->
    <!--  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Harga Estimasi (Rp)</label>-->
    <!--  <div class="col-md-9 col-sm-9 col-xs-12">-->
    <!--    <input type="text" name="hargaestimasi" required="required" placeholder="Harga Estimasi (Rp)" class="form-control col-md-7 col-xs-12" value="<?php echo $rows->harga_estimasi; ?>">-->
    <!--  </div>-->
    <!--</div>-->

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan </label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <textarea name="ket" class="form-control"><?php echo $rows->ket; ?></textarea>
      </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="submit" class="btn btn-success">Ubah</button>
      </div>
    </div>
  </form>



<?php }else{ foreach ($datakibe as $rows) {
  # code...
}?>
  <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="<?php echo base_url('sekolah/hapus_kibe/'.$rows->id); ?>">
    <h2>Anda Yakin Menghapsu Data dengan Nama Barang: <i><b><u><?php echo $rows->nama_barang; ?></u></b></i> ?</h2>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <button type="submit" class="btn btn-success">Yakin</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
      </div>
    </div> 
  </form> 
<?php } ?>

<script>
$(document).ready(function() {
  $('#datatable').DataTable();
  //$('#tglsertifikat').daterangepicker();
  $('#tgldokumen').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      defaultViewDate: "today",
      orientation: "auto top"
  });
  $('#tahuncetak').datepicker({
      format: "yyyy",
      autoclose: true,
      defaultViewDate: "today",
      viewMode: "years",
      minViewMode: "years",
      orientation: "auto top"
  });
});
</script>