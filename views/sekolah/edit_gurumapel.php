                <?php
                  foreach ($gtk as $row) {
                    if($row->status_gtk == "PNS"){
                      $tempel = "style='display:block'";
                      $required = "required='required'";
                    }else{
                      $tempel = "style='display:none'";
                      $required = "";
                    }
                  }

                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_gurumapel'); ?>">
                    <div class="box-body">
                      
                    <div id="form" style="display: block;">
                      <div class="form-group" id="nip" <?php echo $tempel; ?>>
                        <label for="exampleInputEmail1">NIP</label>
                        <input type="text" name="nip" <?php echo $required; ?> class="form-control" id="exampleInputEmail1" placeholder="NIP" value="<?php echo $row->nip; ?>">
                        <span class=""><i>*penulisan NIP tidak perlu menggunakan spasi.</i></span>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">NUPTK</label>
                        <input type="text" name="nuptk" required="required" class="form-control" id="exampleInputEmail1" placeholder="NUPTK" value="<?php echo $row->nuptk; ?>">
                        <span class=""><i>*jika tidak memiliki NUPTK, diisi dengan tanda strip (-).</i></span>
                        <input type="hidden" name="gtkid" required="required" class="form-control" id="exampleInputEmail1" placeholder="NUPTK" value="<?php echo $row->gtk_id; ?>">
                        <input type="hidden" name="status" required="required" class="form-control" id="exampleInputEmail1" placeholder="NUPTK" value="<?php echo $row->status_gtk; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Gelar Depan</label>
                        <input type="text" name="gelardepan" class="form-control" id="exampleInputEmail1" placeholder="Gelar Depan" value="<?php echo $row->gelar_depan; ?>">
                        <span class=""><i>*jika tidak memiliki Gelar Depan, dikosongkan saja.</i></span>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Lengkap</label>
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Lengkap" value="<?php echo $row->nama_lengkap; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Gelar Belakang</label>
                        <input type="text" name="gelarbelakang" class="form-control" id="exampleInputEmail1" placeholder="Gelar Belakang" value="<?php echo $row->gelar_belakang; ?>">
                        <span class=""><i>*jika tidak memiliki Gelar Belakang, dikosongkan saja.</i></span>
                      </div>
                      <div class="form-group">
                        <div class="row container">
                          <label for="exampleInputEmail1">Jenis Kelamin</label>
                        </div>
                        <?php
                          if($row->jk == "L"){
                            echo "<input type='radio' name='jk' required='required' class='minimal' value='L' checked='checked'> Laki-laki
                              <input type='radio' name='jk' required='required' class='minimal' value='P'> Perempuan";
                          }else{
                            echo "<input type='radio' name='jk' required='required' class='minimal' value='L'> Laki-laki
                              <input type='radio' name='jk' required='required' class='minimal' value='P' checked='checked'> Perempuan";
                          }
                        ?>
                        
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Agama</label>
                        <select name="agama" class="form-control">
                          <?php
                            foreach ($agama as $key2) {
                              if($row->agama_id == $key2->agama_id){
                                echo "<option value='$key2->agama_id' selected='selected'>$key2->nama_agama</option>";
                              }else{
                                echo "<option value='$key2->agama_id'>$key2->nama_agama</option>";
                              }
                              
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tempat Lahir</label>
                        <input type="text" name="tempatlahir" required="required" class="form-control" id="exampleInputEmail1" placeholder="Tempat Lahir" value="<?php echo $row->tempat_lahir; ?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                        <input type="text" id="tgllahir" name="tgllahir" class="form-control" placeholder="Tanggal Lahir" required="required" value="<?php echo $row->tgl_lahir; ?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" name="email" required="required" class="form-control" id="exampleInputEmail1" placeholder="Email" value="<?php echo $row->email; ?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">No. HP</label>
                        <input type="text" name="nohp" required="required" class="form-control" id="exampleInputEmail1" placeholder="No. HP" value="<?php echo $row->no_hp; ?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Pendidikan Terakhir</label>
                        <select name="pddk" class="form-control">
                          <?php
                            foreach ($pddk as $key3) {
                              if($row->pdd_terakhir_id == $key3->pdd_terakhir_id){
                                echo "<option value='$key3->pdd_terakhir_id' selected='selected'>$key3->nama_pdd</option>";
                              }else{
                                echo "<option value='$key3->pdd_terakhir_id'>$key3->nama_pdd</option>";  
                              }
                              
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jurusan</label>
                        <input type="text" name="jurusan" required="required" class="form-control" id="exampleInputEmail1" placeholder="Jurusan" value="<?php echo $row->jurusan; ?>">
                      </div>
                      <div class="form-group" <?php echo $tempel; ?> id="pangkat">
                        <label for="exampleInputEmail1">Pangkat / Golongan</label>
                        <select name="pangkat" class="form-control select2" style="width: 100%;" data-placeholder="Pilih Data" <?php echo $required; ?>>
                          <?php
                            foreach ($pangkat as $key) {
                              if($row->kd_pangkat == $key->kd_pangkat){
                                echo "<option value='$key->kd_pangkat' selected='selected'>$key->nama_pangkat - $key->golongan - $key->jabatan</option>";
                              }else{
                                echo "<option value='$key->kd_pangkat'>$key->nama_pangkat - $key->golongan - $key->jabatan</option>";  
                              }
                              
                            }
                          ?>
                        </select>
                      </div>

                    </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                    $('#tgllahir').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today"
                    }); 

                    $('.select2').select2();   
                          
                  });
                </script>
