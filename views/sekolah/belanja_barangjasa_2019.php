<?php 
  $url = $_SERVER['REQUEST_URI']; 
  $getid = $this->input->get('id');
  $getsubid = $this->input->get('subid');
  $sekolahid = $this->session->userdata('token_sekolah_id');
  $tahun = $this->session->userdata('token_tahun');
  $cek_total_bos = $this->sekolah_model->total_belanja_bos($sekolahid,$tahun);
  if(isset($getid)){
    $id= str_replace(array('%', '_',"'",'or','OR','='), array('\\%', '\\_'), $getid); 
  }
  
  if(isset($getsubid)){
    $subid= str_replace(array('%', '_',"'",'or','OR','='), array('\\%', '\\_'), $getsubid); 
  }
  
?>

<div class="box box-success">
  <div class="box-header with-border">
  <input type="hidden" id="kode" value="<?php echo $belanja_id; ?>">
    <?php
      if(isset($subid)){
        //if get subid
    ?>
        <p><button onclick="back()" class="btn btn-info btn-xs">&laquo; Kembali</button></p>

        <ul class="list-group"> 
            <?php
              echo "<li class='list-group-item list-group-item-success'><b>".$this->sekolah_model->get_standar_nasional($id)['standar_kode']. " " .$this->sekolah_model->get_standar_nasional($id)['nama_standar']. ' : Rp '. $this->sekolah_model->get_total_per_standar($id,$belanja_id)."</b></li>";
              $sub_standar = $this->sekolah_model->get_sub_standar($id,$subid);
              echo "<ul><li class='list-group-item'><b>".$sub_standar['substandar_kode'].' ' .$sub_standar['namasub_standar']." : Rp ".$this->sekolah_model->get_total_per_standar($sub_standar['standar_id'],$belanja_id,$sub_standar['substandar_id'])."</b></li></ul>";
            ?>
        </ul>
        <!-- <span id='load'></span> -->
        <div id="cek_belanja"></div>
        <div id="data-belanja"></div>

    <?php

      }else{
    ?>
      <!-- if not get subid -->
      <?php 
        if(isset($id)){
          //jika ada isset id
      ?>
          <p><button onclick="back()" class="btn btn-info btn-xs">&laquo; Kembali</button></p>

          <ul class="list-group"> 
            <?php
              echo "<li class='list-group-item  list-group-item-success'><b>".$this->sekolah_model->get_standar_nasional($id)['standar_kode']. " " .$this->sekolah_model->get_standar_nasional($id)['nama_standar']. ' : Rp '. $this->sekolah_model->get_total_per_standar($id,$belanja_id)."</b></li>";
              $sub_standar = $this->sekolah_model->get_sub_standar($id);
              echo "<ul>";
              foreach ($sub_standar as $sub_standars) {
                echo "<li class='list-group-item'><a href='$url&subid=$sub_standars->substandar_id' class='btn btn-success btn-xs'>$sub_standars->substandar_kode</a> $sub_standars->namasub_standar : Rp ".$this->sekolah_model->get_total_per_standar($sub_standars->standar_id,$belanja_id,$sub_standars->substandar_id)."</li>";
              }
              echo "</ul>";
            ?>
          </ul>

      <?php
        }else{
          //jika tidak ada isset id
      ?>  <p>
            <span class="btn btn-success">Pendapatan: <?php echo $this->sekolah_model->total_belanja_bos($sekolahid,$tahun)['total_bos']; ?></span>
            <span class="btn btn-primary">Total Belanja: <?php echo $this->sekolah_model->total_belanja_bos($sekolahid,$tahun)['total_belanja']; ?></span>
            <span class="btn btn-warning">Sisa / Lebih: <?php echo $this->sekolah_model->total_belanja_bos($sekolahid,$tahun)['sisa_lebih']; ?></span>
          </p>
          <div class="form-group">
            <label for="exampleInputEmail1">CEK KODE PROGRAM:</label>
            <select name="cekprogram" class="form-control select2" id="kp">
              <?php
                foreach ($cp as $nilaicp) {
                  echo "<option value=''>$nilaicp->program_kode - $nilaicp->nama_program</option>";
                }
              ?>
            </select>
          </div>

          <H4>8 STANDAR NASIONAL</H4>
          <table class="table table-responsive table-hover">
            <tr>
              <th>Kode</th>
              <th>Nama Standar</th>
              <th>Total Belanja</th>
            </tr>
            <?php
              foreach ($standar_nasional as $standar_nasionals) {
                echo "<tr>
                  <td><a href='$url?id=$standar_nasionals->standar_id' class='btn btn-success btn-xs'>$standar_nasionals->standar_kode</a></td>
                  <td>$standar_nasionals->nama_standar</td>
                  <td>".$this->sekolah_model->get_total_per_standar($standar_nasionals->standar_id,$belanja_id)."</td>
                </tr>";
              }
            ?>
          </table>

        <?php } //tutup if isset id ?>



    <?php

      }
    ?>

    
    <div id="tambahrincian" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Form Tambah Rincian Belanja</h4>
          </div>
          <div class="modal-body">
            <div class="box">
              
              
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div id="tambahrincianheader" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Form Tambah Rincian Belanja Header</h4>
          </div>
          <div class="modal-body">
            <div class="box">
              
              
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>


    <div id="editrincianbelanja" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Form Edit Rincian Belanja</h4>
          </div>
          <div class="modal-body">
            <div class="box">
              
              
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div id="editdataheader" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Data Header</h4>
          </div>
          <div class="modal-body">
            <div class="box">
              
              
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div id="editdatarincianheader" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Data Rincian Header</h4>
          </div>
          <div class="modal-body">
            <div class="box">
              
              
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>


  </div>
</div>

<script>
    function show_data(){
      var d = new Date();
      var tgl = d.getFullYear();
      $.ajax({
          url  : "<?php echo base_url('sekolah/data_belanja/'.$subid.'/'.$belanja_id)?>",
          success: function(html){
            $.ajax({
              url: "<?php echo base_url('sekolah/cek_belanja_bos/'.$sekolahid.'/'.$tahun); ?>",
              dataType: "JSON",
              cache: false,
              success: function(data){
                if(data.tahun == tgl){
                  if(data.tombol == false){
                    $("#cek_belanja").html("<a href='#' class='btn btn-success' id='rincian' data-id='tr,<?php echo $sub_standar['substandar_id']; ?>'><i class='fa fa-plus-circle'></i> Rincian</a> <span id='load'></span>");
                  }else{
                    $("#cek_belanja").html(data.tombol);
                  }
                }else{
                  $("#cek_belanja").html("<span id='load'></span>");  
                }
                $('#load').html("<span class='btn btn-default'><i class='fa fa-spinner fa-spin' style='font-size:20px'></i></span>");
                setTimeout(function() {
                  $('#load').html("");
                  $("#data-belanja").html(html);
                },1000);
                
              }
            });
          }
      });
    }

    $(document).ready(function () {
      show_data();

      $(document).on('click','#editrincian',function(e){
        e.preventDefault();
        $("#editrincianbelanja").modal('show');
        $.post("<?php echo base_url('sekolah/editrincian'); ?>",
            {id3:$(this).attr('data-id'),kode:$("#kode").val()},
            function(html){
                $(".modal-body").html(html);
            }   
        );
      });
      
      $(document).on('click','#rincian',function(e){
        e.preventDefault();
        $("#tambahrincian").modal('show');
        $.post("<?php echo base_url('sekolah/tambahrincian'); ?>",
            {id:$(this).attr('data-id'),kode:$("#kode").val()},
            function(html){
                $(".modal-body").html(html);
            }   
        );
      });

      $(document).on('click','#rincianheader',function(e){
        e.preventDefault();
        $("#tambahrincianheader").modal('show');
        $.post("<?php echo base_url('sekolah/tambahrincianheader'); ?>",
            {id2:$(this).attr('data-id'),kode:$("#kode").val()},
            function(html){
                $(".modal-body").html(html);
            }   
        );
      });

      

      $(document).on('click','#editheader',function(e){
        e.preventDefault();
        $("#editdataheader").modal('show');
        $.post("<?php echo base_url('sekolah/editheader'); ?>",
            {id3:$(this).attr('data-id'),kode:$("#kode").val()},
            function(html){
                $(".modal-body").html(html);
            }   
        );
      });

      $(document).on('click','#editrincianheader',function(e){
        e.preventDefault();
        $("#editdatarincianheader").modal('show');
        $.post("<?php echo base_url('sekolah/editrincianheader'); ?>",
            {id3:$(this).attr('data-id'),kode:$("#kode").val()},
            function(html){
                $(".modal-body").html(html);
            }   
        );
      });

    });
</script>

<script>
    $(document).ready(function () {
        $("#kp").select2();
            
    });

    function back(){
      window.history.back();
    }
</script>