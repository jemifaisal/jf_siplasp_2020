<table border="0" class="table table-hover table-responsive">
    <tr class="info">
        <th>Kode Program</th>
        <th>Detail Rincian</th>
        <th>Volume</th>
        <th>Satuan</th>
        <th>Harga Satuan</th>
        <th>Total</th>
        <th>Triwulan I</th>
        <th>Triwulan II</th>
        <th>Triwulan III</th>
        <th>Triwulan IV</th>
        <th>Action</th>
    </tr>
    <?php
        $tgl = date('Y');
        $tanggal = $this->session->userdata('token_tahun');
        foreach($program as $programs){
            echo "<tr style='font-weight:bold;'>
                <td>$programs->program_kode.$programs->komponenkode</td>
                <td>$programs->nama_program</td>
                <td></td>
                <td></td>
                <td></td>
                <td align='right'>".number_format($this->sekolah_model->total_program_per_id($programs->program_id,$belanja_id,$programs->komponenkode),0)."</td>
                <td align='right'>".number_format($this->sekolah_model->total_perogram_per_tw($programs->program_id,'tw1',$belanja_id,$programs->komponenkode),0)."</td>
                <td align='right'>".number_format($this->sekolah_model->total_perogram_per_tw($programs->program_id,'tw2',$belanja_id,$programs->komponenkode),0)."</td>
                <td align='right'>".number_format($this->sekolah_model->total_perogram_per_tw($programs->program_id,'tw3',$belanja_id,$programs->komponenkode),0)."</td>
                <td align='right'>".number_format($this->sekolah_model->total_perogram_per_tw($programs->program_id,'tw4',$belanja_id,$programs->komponenkode),0)."</td>
                <td></td>
            </tr>";

            $detail = $this->sekolah_model->detail_program_per_id($programs->program_id,$belanja_id,$programs->komponenkode);
            foreach($detail as $details){
                if($details->status_rincian == 1){
                    // $rincian = $details->detail_rincian;
                    echo "<tr style='font-weight:bold;'>
                        <td></td>
                        <td><i class='fa fa-folder-o'></i> $details->detail_rincian</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align='right'>".number_format($this->sekolah_model->total_per_header($details->rincian_id,'all',0),0)."</td>
                        <td align='right'>".number_format($this->sekolah_model->total_per_header($details->rincian_id,'htw1',1),0)."</td>
                        <td align='right'>".number_format($this->sekolah_model->total_per_header($details->rincian_id,'htw2',1),0)."</td>
                        <td align='right'>".number_format($this->sekolah_model->total_per_header($details->rincian_id,'htw3',1),0)."</td>
                        <td align='right'>".number_format($this->sekolah_model->total_per_header($details->rincian_id,'htw4',1),0)."</td>
                        <td>";
                        if($tgl == $tanggal){
                            echo "<div class='input-group-btn'>
                                <a class='btn btn-success btn-xs dropdown-toggle' data-toggle='dropdown'>Action
                                <span class='fa fa-caret-down'></span></a>
                                <ul class='dropdown-menu' style='position:absolute;'>
                                    <li><a href='#' id='rincian' data-id='trh,$details->rincian_id'>Tambah Rincian</a></li>
                                    <li><a href='#' id='rincian' data-id='eh,$details->rincian_id'>Ubah</a></li>
                                    <li><a href='#' onclick='aksi_hapus($details->rincian_id)'>Hapus</a></li>
                                </ul>
                            </div>";
                        }
                       
                    echo "</td></tr>";
                    $header = $this->sekolah_model->detail_program_header_per_id($details->rincian_id);
                    foreach($header as $headers){
                        echo "<tr>
                            <td></td>
                            <td><i class='fa fa-angle-double-right'></i> $headers->header_rincian</td>
                            <td align='center'>".number_format($headers->volume_hr,0)."</td>
                            <td align='center'>$headers->satuan_hr</td>
                            <td align='right'>".number_format($headers->harga_satuan_hr,0)."</td>
                            <td align='right'>".number_format($headers->volume_hr * $headers->harga_satuan_hr,0)."</td>
                            <td align='right'>".number_format($headers->htw1,0)."</td>
                            <td align='right'>".number_format($headers->htw2,0)."</td>
                            <td align='right'>".number_format($headers->htw3,0)."</td>
                            <td align='right'>".number_format($headers->htw4,0)."</td>
                            <td>";
                            if($tgl == $tanggal){
                                echo "<a href='#' class='btn btn-primary btn-xs' id='rincian' data-id='erh,$headers->header_id'>Ubah</a>
                                <a href='#' class='btn btn-danger btn-xs' onclick='aksi_hapus_header($headers->header_id)'>Hapus</a>";
                            }
                                
                        echo "</td></tr>";
                    }
                }else{
                    // $rincian = '- '.$details->detail_rincian;
                    echo "<tr>
                        <td></td>
                        <td><i class='fa fa-angle-right'></i> $details->detail_rincian</td>
                        <td align='center'>".number_format($details->volume,0)."</td>
                        <td align='center'>$details->satuan</td>
                        <td align='right'>".number_format($details->harga_satuan,0)."</td>
                        <td align='right'>".number_format($details->volume * $details->harga_satuan,0)."</td>
                        <td align='right'>".number_format($details->tw1,0)."</td>
                        <td align='right'>".number_format($details->tw2,0)."</td>
                        <td align='right'>".number_format($details->tw3,0)."</td>
                        <td align='right'>".number_format($details->tw4,0)."</td>
                        <td>";
                        if($tgl == $tanggal){
                            echo "<a href='#' class='btn btn-primary btn-xs' id='rincian' data-id='er,$details->rincian_id'>Ubah</a>
                            <a href='#' class='btn btn-danger btn-xs' onclick='aksi_hapus($details->rincian_id)'>Hapus</a>";
                        }
                        
                    echo "</td></tr>";
                }
                
            }
        }
    ?>
</table>

<script>
    function aksi_hapus($id){
        var id = $id;
        swal({
            title: 'Apakah anda yakin ?',
            text: "Untuk menghapus rincian belanja",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function() {
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('sekolah/hapusrincian')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    //$("#tambahrincian").modal('hide');
                    show_data();
                    swal({
                      title: 'Berhasil!',
                      text: 'Data berhasil dihapus',
                      type: 'success',
                      timer: 1500
                    }).catch(swal.noop);
                },
                error:function(){
                  //$("#tambahrincian").modal('hide');
                  swal({
                    title: 'Gagal!',
                    text: 'Data gagal dihapus',
                    type: 'error',
                    timer: 1500
                  }).catch(swal.noop);
                }
            });
            return false;
        }).catch(swal.noop);
    }

    function aksi_hapus_header($id){
        var id = $id;
        swal({
            title: 'Apakah anda yakin ?',
            text: "Untuk menghapus rincian belanja",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function() {
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('sekolah/hapusrincianheader')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    //$("#tambahrincian").modal('hide');
                    show_data();
                    swal({
                      title: 'Berhasil!',
                      text: 'Data berhasil dihapus',
                      type: 'success',
                      timer: 1500
                    }).catch(swal.noop);
                },
                error:function(){
                  //$("#tambahrincian").modal('hide');
                  swal({
                    title: 'Gagal!',
                    text: 'Data gagal dihapus',
                    type: 'error',
                    timer: 1500
                  }).catch(swal.noop);
                }
            });
            return false;
        }).catch(swal.noop);
    }

    
</script>