<?php
    foreach($tamsil as $rows){

    }
?>
<div class="box">
    <form>
    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Guru</label>
            <input class="form-control" type="text" name="namaguru" value="<?php echo $rows->nama_lengkap; ?>" id="namaguru" placeholder="Nama Guru" readonly="readonly">
            <input class="form-control" type="hidden" name="tamsilid" value="<?php echo $rows->tamsil_id; ?>" id="tamsilid" placeholder="Nama Guru" required="required">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Jumlah Jam Mengajar</label>
            <input class="form-control" onkeyup="convertToRupiah(this);" type="text" name="jumlahjam" id="jumlahjam" value="<?php echo $rows->jumlah_jam_mengajar; ?>" placeholder="Jumlah Jam Mengajar" required="required">
            <div class="required"><i>jumlah jam mengajar/minggu semester I (januari sd juni) sesuai DAPODIK</i></div>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Mata Pelajaran</label>
            <input class="form-control" type="text" name="mapel" id="mapel" value="<?php echo $rows->mapel; ?>" placeholder="Mata Pelajaran" required="required">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">No. Rekening <u>Bank Riau Kepri</u></label>
            <input class="form-control" onkeyup="convertToRupiah(this);" type="text" name="norek" id="norek" value="<?php echo $rows->no_rekening; ?>" placeholder="No. Rekening Bank Riau Kepri" required="required">
            <div class="required"><i>no rekening diisi dengan angka, tidak menggunakan <b>spasi</b>, <b>titik</b> dan <b>tanda strip (-)</b></i></div>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Nama Pada Rekening</label>
            <input class="form-control" type="text" name="namarek" id="namarek" value="<?php echo $rows->nama_rekening; ?>" placeholder="Nama Pada Rekening" required="required">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Keterangan</label>
            <textarea name="keterangan" id="keterangan" cols="30" rows="3" class="form-control"><?php echo $rows->keterangan; ?></textarea>  
            <div class="required"><i>jika tidak ada keterangan dikosongkan saja</i></div>                          
        </div>

        
    </div>
    <!-- /.box-body -->
    </form>
    <div class="box-footer">
        <button type="submit" id="btn_simpan" class="btn btn-primary"><span id="loading"></span> Simpan</button>
    </div>
    
</div>

<script>
  $(document).ready(function () {
    $('#tgllahir').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      defaultViewDate: "today"
    }); 
    $('.select2').select2();   

    $('#btn_simpan').on('click',function(){
      var tamsilid = $('#tamsilid').val();
      var jam = $('#jumlahjam').val();
      var norek = $('#norek').val();
      var namarek = $('#namarek').val();
      var mapel = $('#mapel').val();
      var keterangan = $('#keterangan').val();
      $('#loading').html("<i class='fa fa-spinner fa-spin' style='font-size:20px'></i>");
    //simpan
    $.ajax({
        type : "POST",
        url : "<?php echo base_url('sekolah/proses_edit_tamsil'); ?>",
        dataType : "JSON",
        data : { tamsilid:tamsilid, jam:jam, norek:norek, namarek:namarek, mapel:mapel, keterangan:keterangan },
        success: function(data){
            $("#formaksi").modal('hide');
            swal({
                title: 'Berhasil!',
                text: 'Data berhasil disimpan',
                type: 'success',
                timer: 1500

            }).catch(swal.noop);
            // show_data();
            setTimeout("location.reload(true);", 1500);
        },
        error:function(){
            swal({
                title: 'Gagal!',
                text: 'Data gagal disimpan',
                type: 'error',
                timer: 1500
            }).catch(swal.noop);
        }

    });
      return false;
    });
          
  });

  function convertToRupiah(objek) {
    separator = ".";
    a = objek.value;
    b = a.replace(/[^\d]/g,"");
    c = "";
    panjang = b.length; 
    j = 0; 
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i-1,1) + separator + c;
        } else {
            c = b.substr(i-1,1) + c;
        }
    }
    objek.value = b;
  }
</script>
