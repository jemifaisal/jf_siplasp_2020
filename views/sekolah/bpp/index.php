<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Data Buku Pembantu Pajak</h3>
  </div>

  <div class="box-body">
    <form class="form-inline" method="post" id="form_lihat">
        <div class="form-group">
            <label for="email">Tahun:</label>
            <b class="form-control" ><?php echo $tahun; ?></b>
            <input type="hidden" readonly="readonly" name="tahun" id="tahun" class="form-control" value="<?php echo $tahun; ?>">
        </div>
        <div class="form-group">
            <label for="pwd">Bulan:</label>
            <select name="bulan" id="bulan" class="form-control">
                <!-- <option value="Januari">Januari</option> -->
                <?php
                foreach($bulan as $bulans){
                    echo "<option value='$bulans->bulan_id'>$bulans->nama_bulan</option>";
                }
                ?>
            </select>
        </div>
        <!-- <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span>Proses</button> -->
        <button type="submit" class="btn btn-warning"><span id="btn_caption"><i class='fa fa-send'></i> Proses</span></button>
        <br><span class="required"><i>* tahun berdasarkan yang dipilih ketika login</i></span>
    
    </form>
    <br>
    <div id="bku_show"></div>
    
  </div>
</div>
<script>
    $('#form_lihat').on('submit', function(e){
      $("#bku_show").html("");
      if(!e.isDefaultPrevented()){
        $("#btn_caption").html("<i class='fa fa-spinner fa-spin' style='font-size:18px;'></i> Loading...");
        var inputData = new FormData($(this)[0]);
        var bulan = $("#bulan").val();
        var url = "<?php echo base_url('sekolah/data_bpp_show'); ?>";
        $.ajax({
          url: url,
          type: "post",
          data: inputData,
          processData: false,
          contentType: false,
          cache: false,
          success: function(data){
            $("#bku_show").html(data);
            $("#btn_caption").html("<i class='fa fa-send'></i> Proses");
          }
        });
        
        return false;
      }
    });

    $('#bulan').on('change', function(){
      $("#bku_show").html("");
    });
</script>