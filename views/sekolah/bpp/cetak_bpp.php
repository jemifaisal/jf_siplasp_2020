<table border='0' width='100%'>
    <tr>
        <td colspan='9'><span class="lead"><center><h3>BUKU PEMBANTU PAJAK</h3></center></span></td>
    </tr>
    <tr>
        <td colspan='9' align="center"><center>Bulan: <?php echo $bulan. ' '.$tahun; ?></center></td>
    </tr>
    <tr>
        <td colspan="2" width='150'>Nama Sekolah</td>
        <td colspan="7">: <?php echo $profil['nama_sp']; ?> </td>
    </tr>
    <tr>
        <td colspan="2">Desa/Kecamatan</td>
        <td colspan="7">: <?php echo $profil['nama_kec']; ?></td>
    </tr>
    <tr>
        <td colspan="2">Kabupaten/Kota</td>
        <td colspan="7">: <?php echo $profil['nama_kab']; ?></td>
    </tr>
    <tr>
        <td colspan="2">Provinsi</td>
        <td colspan="7">: Riau</td>
    </tr>
</table>
<br>
<table border="1" class="table table-bordered table-responsive">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Tanggal</th>
        <th rowspan="2">Kode Rekening</th>
        <th rowspan="2">No. Kode</th>
        <th rowspan="2">No. Bukti</th>
        <th rowspan="2">Uraian</th>
        <th colspan="4">Penerimaan (Debit)</th>
        <th rowspan="2">Pengeluaran<br> (Kredit)</th>
        <th rowspan="2">Saldo</th>
    </tr>
    <tr>
        <th>PPN</th>
        <th>PPh 21</th>
        <th>PPh 22</th>
        <th>PPh 23</th>
    </tr>
    <?php
        $no = 0;
        $saldo = $saldo_awal;
        foreach($bku as $bkus){
            $no ++;
            if($bkus->program_kode == NULL){
                $dot = "";
            }else{
                $dot = ".";
            }
            echo "<tr>
                <td>$no</td>
                <td>".date("d M Y",strtotime($bkus->tanggal_transaksi))."</td>
                <td>$bkus->rekening_belanja</td>
                <td>$bkus->program_kode$dot$bkus->kode_komponen</td>
                <td>$bkus->kode_bukti $bkus->no_urut_bukti</td>
                <td>$bkus->uraian_bku</td>";
            if($bkus->kelompok == "D"){
                $param = '+';
                if($bkus->pajak_id == 1){
                    $ppn = $bkus->jumlah_transaksi;
                    $pph21 = "";
                    $pph22 = "";
                    $pph23 = "";
                }elseif($bkus->pajak_id == 2){
                    $ppn = "";
                    $pph21 = $bkus->jumlah_transaksi;
                    $pph22 = "";
                    $pph23 = "";
                }elseif($bkus->pajak_id == 3){
                    $ppn = "";
                    $pph21 = "";
                    $pph22 = $bkus->jumlah_transaksi;
                    $pph23 = "";
                }elseif($bkus->pajak_id == 4){
                    $ppn = "";
                    $pph21 = "";
                    $pph22 = "";
                    $pph23 = $bkus->jumlah_transaksi;
                }
                echo "<td class='success'>$ppn</td>
                    <td class='success'>$pph21</td>
                    <td class='success'>$pph22</td>
                    <td class='success'>$pph23</td>
                    <td class='warning'></td>";
                $saldo = $saldo + $bkus->jumlah_transaksi;
            }else{
                $param = '-';
                echo "<td class='success'></td>
                    <td class='success'></td>
                    <td class='success'></td>
                    <td class='success'></td>
                    <td class='warning'>$bkus->jumlah_transaksi</td>";
                $saldo = $saldo - $bkus->jumlah_transaksi;
            }

            
            echo "<td class='info'>".$saldo."</td></tr>";
        }
    ?>
</table>
<br>
<table border='0' width='100%'>
    <tr>
    <tr>
        <td align='center' colspan="5">
            Mengetahui<br>
            Kepala <?php echo $profil['nama_sp']; ?>
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['kepsek']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_kepsek']; ?>
        </td>
        <td align='center' colspan="4">
            ................., <?php $hari_ini = $tahun.'-'.$bulan_id; echo date('t', strtotime($hari_ini)); echo ' '. $bulan.' '. $tahun; ?> <br>
            Bendahara Dana BOS
            <br>
            <br>
            <br>
            <br>
            <br>
            <b><u><?php echo $profil['nama_bendahara_bos']; ?></u></b><br>
            <?php echo 'NIP. '.$profil['nip_bendahara_bos']; ?>
        </td>
    </tr>
    </tr>
</table>