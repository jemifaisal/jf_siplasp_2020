        <?php foreach ($guru as $rows) {
          # code...
        } ?>
        <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Mapel Guru</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          <p><a href="#" class="btn btn-primary" id="aksi" data-id="1,<?php echo $rows->gtk_id; ?>,mapelgurutambahan"><i class="fa fa-plus-circle"></i> Tambah Mapel Guru</a></p>
          <div class="box box-warning">
            <div class="box-header">
              <h3>Nama Guru: <?php echo $rows->nama_lengkap; ?></h3>
              <hr>
              <h3 class="box-title">Data Mapel Guru</h3><br>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Mata Pelajaran</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                  $sekolahid = $this->session->userdata('token_sekolah_id');
                  $this->db->where('rm.gtk_id',$rows->gtk_id);
                  $this->db->where('rm.sekolah_id',$sekolahid);
                  $this->db->where('rm.soft_delete',0);
                  $this->db->join('mapel m','m.kd_mapel=rm.kd_mapel');
                  $sql = $this->db->get('riwayat_mapel rm');
                  $no=1; foreach ($sql->result() as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->nama_mapel; ?></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="aksi" data-id="2,<?php echo $row->riwayatmapel_id; ?>,mapelgurutambahan">Ubah</a> <a href="<?php echo base_url('sekolah/hapus_mapelgurutambahan/'.$row->riwayatmapel_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <script>
            $(function () {
              $("#example1").DataTable();
              $("#example2").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_mapelguru'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#aksi2',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_sekolah_tambahan'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
