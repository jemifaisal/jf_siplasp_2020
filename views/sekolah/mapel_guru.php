        <?php foreach ($guru as $rows) {
          # code...
        } ?>
        <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Mapel Guru</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


          <p><a href="#" class="btn btn-primary" id="aksi" data-id="1,<?php echo $rows->gtk_id; ?>,mapelguru"><i class="fa fa-plus-circle"></i> Tambah Mapel Guru</a>
            <a href="#" class="btn btn-warning" id="aksi" data-id="3,<?php echo $rows->gtk_id; ?>"><i class="fa fa-plus-circle"></i> Sekolah Tambahan Guru</a></p>
          <div class="box box-warning">
            <div class="box-header">
              <h3>Nama Guru: <?php echo $rows->nama_lengkap; ?></h3>
              <hr>
              <h3 class="box-title">Data Mapel Guru</h3><br>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Mata Pelajaran</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                  $sekolahid = $this->session->userdata('token_sekolah_id');
                  $this->db->where('rm.gtk_id',$rows->gtk_id);
                  $this->db->where('rm.sekolah_id',$sekolahid);
                  $this->db->where('rm.soft_delete',0);
                  $this->db->join('mapel m','m.kd_mapel=rm.kd_mapel');
                  $sql = $this->db->get('riwayat_mapel rm');
                  $no=1; foreach ($sql->result() as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $row->nama_mapel; ?></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="aksi" data-id="2,<?php echo $row->riwayatmapel_id; ?>,mapelguru">Ubah</a> <a href="<?php echo base_url('sekolah/hapus_mapelguru/'.$row->riwayatmapel_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Data Mapel Guru Disekolah Lain</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th colspan="2">Nama Sekolah</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                  $this->db->where('rs.gtk_id',$rows->gtk_id);
                  $this->db->where('rs.soft_delete',0);
                  //$this->db->where('rm.soft_delete',0);
                  //$this->db->where('m.soft_delete',0);
                  $this->db->where('s.status',0);
                  $this->db->join('riwayat_sekolah rs','rs.gtk_id=g.gtk_id','left');
                  $this->db->join('sekolah s','s.sekolah_id=rs.sekolah_id','left');
                  //$this->db->join('riwayat_mapel rm','rs.gtk_id=rm.gtk_id and rs.sekolah_id=rm.sekolah_id','left');
                  //$this->db->join('riwayat_mapel rm','rs.gtk_id=rm.gtk_id');
                  //$this->db->join('mapel m','m.kd_mapel=rm.kd_mapel','left');

                  $sql2 = $this->db->get('gtk g');
                  $no=1; foreach ($sql2->result() as $row2) { ?>
                <tr>
                  <td><b><?php echo $no++; ?></b></td>
                  <td colspan="2"><b><?php echo $row2->nama_sp; ?></b></td>
                  <td><a href="#" class="btn btn-success btn-xs" id="aksi2" data-id="<?php echo $row2->riwayatsekolah_id; ?>">Ubah</a> <a href="<?php echo base_url('sekolah/hapus_sekolah_tambahan/'.$row2->riwayatsekolah_id); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Anda Yakin Menghapus Data ?')">Hapus</a></td>
                </tr>
                <?php
                  $this->db->where('rm.gtk_id',$row2->gtk_id);
                  $this->db->where('rm.sekolah_id',$row2->sekolah_id);
                  $this->db->where('rm.soft_delete',0);
                  $this->db->where('m.soft_delete',0);
                  //$this->db->where('s.status',0);
                  //$this->db->join('riwayat_sekolah rs','rs.gtk_id=g.gtk_id','left');
                  //$this->db->join('sekolah s','s.sekolah_id=rs.sekolah_id','left');
                  //$this->db->join('riwayat_mapel rm','rs.gtk_id=rm.gtk_id and rs.sekolah_id=rm.sekolah_id','left');
                  //$this->db->join('riwayat_mapel rm','rs.gtk_id=rm.gtk_id');
                  $this->db->join('mapel m','m.kd_mapel=rm.kd_mapel','left');
                  $sql3 = $this->db->get('riwayat_mapel rm');
                  foreach ($sql3->result() as $row3) {
                    echo "<tr>
                      <td></td>
                      <td>- $row3->nama_mapel</td>
                      <td></td>
                      <td></td>
                    </tr>";
                  }

                ?>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <script>
            $(function () {
              $("#example1").DataTable();
              $("#example3").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_mapelguru'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#aksi2',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/aksi_sekolah_tambahan'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
