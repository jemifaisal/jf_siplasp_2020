                <?php
                  foreach ($pejabat as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_pejabat'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Ketua Yayasan</label>
                        <input type="text" name="ketuayayasan" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Ketua Yayasan" value="<?php echo $row->nama_komite_sekolah; ?>">
                        <input type="hidden" name="idpejabat" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Ketua Yayasan" value="<?php echo $row->ttd_id; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">NIP Ketua Yayasan</label>
                        <input type="text" name="nipketua" required="required" class="form-control" id="exampleInputEmail1" placeholder="NIP Ketua Yayasan" value="<?php echo $row->nip_komite_sekolah; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kepala Sekolah</label>
                        <input type="text" name="kepsek" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kepala Sekolah" value="<?php echo $row->nama_kepsek; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">NIP Kepala Sekolah</label>
                        <input type="text" name="nipkepsek" required="required" class="form-control" id="exampleInputEmail1" placeholder="NIP Kepala Sekolah" value="<?php echo $row->nip_kepsek; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Bendahara BOS</label>
                        <input type="text" name="namabendahara" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Bendahara BOS" value="<?php echo $row->nama_bendahara_bos; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">NIP Bendahara BOS</label>
                        <input type="text" name="nipbendahara" required="required" class="form-control" id="exampleInputEmail1" placeholder="NIP Bendahara BOS" value="<?php echo $row->nip_bendahara_bos; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tgl. Mulai Aktif</label>
                        <input type="text" id="tglaktif" name="tglaktif" class="form-control" placeholder="Tgl. Mulai Aktif" required="required" value="<?php echo $row->periode_aktif; ?>">
                      </div>

                      <div class="form-group">
                        <label>Status: </label>
                        <label>
                        <?php if($row->soft_delete == 0){ ?>
                          <input type="radio" name="status" class="minimal" checked="checked" value="0"> Aktif &nbsp;&nbsp;
                          <input type="radio" name="status" class="minimal" value="1"> Tidak Aktif
                        <?php }else{ ?>
                          <input type="radio" name="status" class="minimal" value="0"> Aktif
                          <input type="radio" name="status" class="minimal" checked="checked" value="1"> Tidak Aktif
                        <?php } ?>
                        </label>
                      </div>


                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                    $('#tglaktif').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today",
                      todayHighlight: true
                    }); 
                      $("#e1").selectpicker();
                          
                  });
                </script>
