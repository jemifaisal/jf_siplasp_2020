                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/add_sekolah_tambahan'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Sekolah Tambahan Guru</label>
                        <select name="sekolah" class="form-control select2" style="width: 100%;" data-placeholder="Pilih Sekolah">
                          <?php
                            foreach ($sekolah as $baris) {
                              echo "<option value='$baris->sekolah_id'>$baris->nama_sp</option>";
                            }
                          ?>
                        </select>
                        <input type="hidden" name="gtkid" value="<?php echo $gtkid; ?>">
                        <span class=""><i>*jika tidak ada mengajar disekolah lain, dikosongkan saja (tidak perlu dipilih).</i></span>
                      </div>

                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                    $('#tgllahir').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today"
                    }); 

                    $('.select2').select2();   
                          
                  });
                </script>
