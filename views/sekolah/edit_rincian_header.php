                <script type="text/javascript">
                    function convertToRupiah(objek) {
                      separator = ".";
                      a = objek.value;
                      b = a.replace(/[^\d]/g,"");
                      c = "";
                      panjang = b.length; 
                      j = 0; 
                      for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                          c = b.substr(i-1,1) + separator + c;
                        } else {
                          c = b.substr(i-1,1) + c;
                        }
                      }
                      objek.value = c;

                    } 
                  
                </script>
                <?php
                  foreach ($rincian as $row) {
                    # code...
                  }
                ?>
                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/proses_edit_rincian_header'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Rincian</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="hidden" name="kode" class="form-control" value="<?php echo $kode; ?>">
                        <input type="text" name="nama" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Rincian" value="<?php echo $row->header_rincian; ?>">
                      </div>

                      <div id="tampil" style="display:block;">
                      <div class="row">
                        <div class="form-group"></div>
                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Volume</label>
                            <input type="text" id="volume" name="volume" class="form-control" placeholder="Volume" onkeyup="convertToRupiah(this);" value="<?php echo $row->volume_hr; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Satuan</label>
                            <input type="text" id="satuan" required="required" name="satuan" class="form-control" placeholder="Satuan" value="<?php echo $row->satuan_hr; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-4">
                            <label for="exampleInputEmail1">Harga Satuan</label>
                            <input type="text" id="harga" name="hargasatuan" class="form-control" placeholder="Harga Satuan" onkeyup="convertToRupiah(this);" value="<?php echo $row->harga_satuan_hr; ?>">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan I</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw1" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan I" value="<?php echo $row->htw1; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan II</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw2" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan II" value="<?php echo $row->htw2; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan III</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw3" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan III" value="<?php echo $row->htw3; ?>">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Triwulan IV</label>
                        <input type="hidden" name="id" class="form-control" value="<?php echo $key; ?>">
                        <input type="text" name="tw4" onkeyup="convertToRupiah(this);" class="form-control" id="exampleInputEmail1" placeholder="Triwulan IV" value="<?php echo $row->htw4; ?>">
                      </div>
                      </div>

                      
                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>

                <?php
                  function convertToRupiah($isi){
                    return number_format($isi,0,',','.');
                  }
                ?>

                <script>
            $(document).ready(function () {
                $("#e1").select2();
            });
          </script>

