<script type="text/javascript">
                  function angka(objek) {
                    separator = ".";
                    a = objek.value;
                    b = a.replace(/[^\d]/g,"");
                    c = "";
                    panjang = b.length; 
                    j = 0; 
                    for (i = panjang; i > 0; i--) {
                      j = j + 1;
                      if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + separator + c;
                      } else {
                        c = b.substr(i-1,1) + c;
                      }
                    }
                    objek.value = c;

                  } 

                  function cek(){
                    var kelas = document.getElementById("labor").value;
                    var baik = document.getElementById("baik").value;
                    var ringan = document.getElementById("ringan").value;
                    var sedang = document.getElementById("sedang").value;
                    var berat = document.getElementById("berat").value;

                    var total = parseInt(baik) + parseInt(ringan) + parseInt(sedang) + parseInt(berat);

                    if(total !=kelas){
                      document.getElementById("simpan").style.display = "none";
                      //document.getElementById("warning").innerHTML = "Total Penjumlahan Ruang Kelas Tidak Sama.!";
                    }else{
                      document.getElementById("simpan").style.display = "block";
                      //document.getElementById("warning").innerHTML = "Jumlah Data Sama";
                    }
                  }

                  function ubah(){
                    document.getElementById("update").style.display = "none";
                    document.getElementById("edit").style.display = "block"; 
                  }
                  function batal(){
                    document.getElementById("update").style.display = "block";
                    document.getElementById("edit").style.display = "none"; 
                  }


</script>

<div class="box box-primary">
  <div class="box-header with-border">
  <H4>Data Ruang Labor / Praktek Sekolah</H4>


    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="ruangkelas">

        <div class="box">  
          <div class="box-body">
          <?php
            $sid = $this->session->userdata('token_sekolah_id');
            $tahun = $this->session->userdata('token_tahun');
            $this->db->where('sekolah_id',$sid);
            $this->db->where('tahun',$tahun);
            $q = $this->db->get('sarpras_labor');
            //if($q->num_rows() > 0){
              foreach ($q->result() as $rowq) {
                $rk = $rowq->jumlah_labor; $kb = $rowq->jumlah_baik; $rr = $rowq->rusak_ringan;
                $rs = $rowq->rusak_sedang;
                $rb = $rowq->rusak_berat;
              }
          ?>

          <div id="update">
            <form role="form" method="post" action="<?php echo base_url('sekolah/add_sarpras_labor'); ?>">
              <div class="row">
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Jenis Labor</label>
                    <?php
                      $this->db->where('soft_delete',0);
                      $qlabor = $this->db->get('labor');
                    ?>
                    <select name="jenislab" class="form-control">
                      <?php
                        foreach ($qlabor->result() as $baris) {
                          echo "<option value='$baris->labor_id'>$baris->nama_labor</option>";  
                        }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Jumlah Ruang Labor</label>
                    <input type="text" name="labor" required="required" class="form-control" id="labor" placeholder="Jumlah Ruang Labor" onkeyup="angka(this);cek();">
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kondisi Baik</label>
                    <input type="text" name="baik" required="required" class="form-control" id="baik" placeholder="Kondisi Baik" onkeyup="angka(this);cek();">
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Rusak Ringan</label>
                    <input type="text" name="ringan" required="required" class="form-control" id="ringan" placeholder="Rusak Ringan" onkeyup="angka(this);cek();">
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Rusak Sedang</label>
                    <input type="text" name="sedang" required="required" class="form-control" id="sedang" placeholder="Rusak Sedang" onkeyup="angka(this);cek();">
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Rusak Berat</label>
                    <input type="text" name="berat" required="required" class="form-control" id="berat" placeholder="Rusak Berat" onkeyup="angka(this);cek();">
                  </div>
                </div>

                <div class="col-sm-2">
                  
                </div>

              </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-primary" id="simpan" style="display:none;">SIMPAN</button>
                </div>  
            </form>
            </div>

            </div>
          </div>

          <?php
            $sid = $this->session->userdata('token_sekolah_id');
            $thfoto = $this->session->userdata('token_tahun');
            $this->db->where('sekolah_id',$sid);
            $this->db->where('tahun',$thfoto);
            $this->db->join('labor','labor.labor_id=sarpras_labor.labor_id');
            $this->db->group_by('sarpras_labor.labor_id');
            $sql = $this->db->get('sarpras_labor');
            if($sql){
              foreach ($sql->result() as $row) {
          ?>
                <table border="0" class='table'>
                  <tr>
                    <td colspan="3"><h3>Labor: <?php echo $row->nama_labor; ?></h3></td>
                  </tr>
                  <tr>
                    <td >Jumlah Ruang Labor / Praktek yang Rusak Ringan</td>
                    <td colspan="2">: <?php echo $row->rusak_ringan; ?></td>
                  </tr>
                  <?php
                      $this->db->where('sarpraslabor_id',$row->sarpraslabor_id);
                      $this->db->where('kategori_foto',"rr");
                      //$this->db->where('tahun_foto',$thfoto);
                      $sql2 = $this->db->get('foto_labor');
                      $arr3="";
                      if($sql2->num_rows() > 0){


                      foreach ($sql2->result() as $row2) {
                        $arr3= array('foto_1' => $row2->foto_1,'foto_2' => $row2->foto_2,'foto_3' => $row2->foto_3,'foto_4' => $row2->foto_4,'foto_5' => $row2->foto_5,'foto_6' => $row2->foto_6,'foto_7' => $row2->foto_7,'foto_8' => $row2->foto_8,'foto_9' => $row2->foto_9,'foto_10' => $row2->foto_10); 
                      }
                    }else{
                        $arr3= array('foto_1' => '','foto_2' => '','foto_3' => '','foto_4' => '','foto_5' => '','foto_6' => '','foto_7' => '','foto_8' => '','foto_9' => '','foto_10' => ''); 
                    }

                      
                      //var_dump($arr= array(1=>'$row2->foto_1',2=>'sds'));
                      

                      for($c=1;$c<=$row->rusak_ringan;$c++){
                        $x3 = "foto_".$c;
                        if(!empty($row2->fotolabor_id)){
                          $id = $row2->fotolabor_id;
                        }else{
                          $id = 0;
                        }
                        echo "<tr>
                          <td><a href='#' id='aksi' data-id='$id,$x3,labor'><img src='".base_url('assets/foto_labor/'.$arr3[$x3])."' width='100'></a></td>
                          <td>Foto $c</td>
                          <td><a href='#' id='fotokelas' class='btn btn-success btn-xs' data-id='foto_$c,rr,$row->sarpraslabor_id'>Upload Foto</a></td>
                        </tr>";
                        
                      }
                    
                  ?>

                  <tr>
                    <td >Jumlah Ruang Labor / Praktek yang Rusak Sedang</td>
                    <td colspan="2">: <?php echo $row->rusak_sedang; ?></td>
                  </tr>
                  <?php
                      $this->db->where('sarpraslabor_id',$row->sarpraslabor_id);
                      $this->db->where('kategori_foto',"rs");
                      //$this->db->where('tahun_foto',$thfoto);
                      $sql2 = $this->db->get('foto_labor');
                      $arr="";
                      if($sql2->num_rows() > 0){


                      foreach ($sql2->result() as $row2) {
                        $arr= array('foto_1' => $row2->foto_1,'foto_2' => $row2->foto_2,'foto_3' => $row2->foto_3,'foto_4' => $row2->foto_4,'foto_5' => $row2->foto_5,'foto_6' => $row2->foto_6,'foto_7' => $row2->foto_7,'foto_8' => $row2->foto_8,'foto_9' => $row2->foto_9,'foto_10' => $row2->foto_10);   
                      }
                    }else{
                        $arr= array('foto_1' => '','foto_2' => '','foto_3' => '','foto_4' => '','foto_5' => '','foto_6' => '','foto_7' => '','foto_8' => '','foto_9' => '','foto_10' => ''); 
                    }

                      
                      //var_dump($arr= array(1=>'$row2->foto_1',2=>'sds'));
                      

                      for($i=1;$i<=$row->rusak_sedang;$i++){
                        $x = "foto_".$i;
                        if(!empty($row2->fotolabor_id)){
                          $id = $row2->fotolabor_id;
                        }else{
                          $id = 0;
                        }
                        echo "<tr>
                          <td><a href='#' id='aksi' data-id='$id,$x,labor'><img src='".base_url('assets/foto_labor/'.$arr[$x])."' width='100'></a></td>
                          <td>Foto $i</td>
                          <td><a href='#' id='fotokelas' class='btn btn-success btn-xs' data-id='foto_$i,rs,$row->sarpraslabor_id'>Upload Foto</a></td>
                        </tr>";
                        
                      }
                    
                  ?>

                  <tr>
                    <td>Jumlah Ruang Labor / Praktek yang Rusak Berat</td>
                    <td colspan="2">: <?php echo $row->rusak_berat; ?></td>
                  </tr>
                  <?php
                      $this->db->where('sarpraslabor_id',$row->sarpraslabor_id);
                      $this->db->where('kategori_foto',"rb");
                      //$this->db->where('tahun_foto',$thfoto);
                      $sql3 = $this->db->get('foto_labor');
                      $arr2="";
                      if($sql3->num_rows() >0){


                      foreach ($sql3->result() as $row3) {
                        $arr2= array('foto_1' => $row3->foto_1,'foto_2' => $row3->foto_2,'foto_3' => $row3->foto_3,'foto_4' => $row3->foto_4,'foto_5' => $row3->foto_5,'foto_6' => $row3->foto_6,'foto_7' => $row3->foto_7,'foto_8' => $row3->foto_8,'foto_9' => $row3->foto_9,'foto_10' => $row3->foto_10);    
                      }
                    }else{
                        $arr2= array('foto_1' => '','foto_2' => '','foto_3' => '','foto_4' => '','foto_5' => '','foto_6' => '','foto_7' => '','foto_8' => '','foto_9' => '','foto_10' => '');  
                    }
                    for($a=1;$a<=$row->rusak_berat;$a++){
                      $x2 = "foto_".$a;
                      if(!empty($row3->fotolabor_id)){
                          $id = $row3->fotolabor_id;
                        }else{
                          $id = 0;
                        }
                      echo "<tr>
                        <td><a href='#' id='aksi' data-id='$id,$x2,labor'><img src='".base_url('assets/foto_labor/'.$arr2[$x2])."' width='100'></a></td>
                        <td>Foto $a</td>
                        <td><a href='#' id='fotokelas' class='btn btn-success btn-xs' data-id='foto_$a,rb,$row->sarpraslabor_id'>Upload Foto</a></td>
                      </tr>";
                    }
                  ?>
                </table>


          <?php           
              }
            }
          ?>

        </div>

        <div id="kelasform" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Upload Foto Ruang Labor / Praktek</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div id="formaksi" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Foto</h4>
              </div>
              <div class="modal-body">
                <div class="box">
                  
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<script>

              $(document).on('click','#fotokelas',function(e){
                e.preventDefault();
                $("#kelasform").modal('show');
                $.post("<?php echo base_url('sekolah/tambahsarpraslabor'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

              $(document).on('click','#aksi',function(e){
                e.preventDefault(); 
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('sekolah/lihatfoto'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });
</script>
