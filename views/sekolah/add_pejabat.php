                <div class="box">
                  <form role="form" method="post" action="<?php echo base_url('sekolah/add_pejabat'); ?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Ketua Yayasan</label>
                        <input type="text" name="ketuayayasan" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Ketua Yayasan">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">NIP Ketua Yayasan</label>
                        <input type="text" name="nipketua" required="required" class="form-control" id="exampleInputEmail1" placeholder="NIP Ketua Yayasan">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kepala Sekolah</label>
                        <input type="text" name="kepsek" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Kepala Sekolah">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">NIP Kepala Sekolah</label>
                        <input type="text" name="nipkepsek" required="required" class="form-control" id="exampleInputEmail1" placeholder="NIP Kepala Sekolah">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Bendahara BOS</label>
                        <input type="text" name="namabendahara" required="required" class="form-control" id="exampleInputEmail1" placeholder="Nama Bendahara BOS">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">NIP Bendahara BOS</label>
                        <input type="text" name="nipbendahara" required="required" class="form-control" id="exampleInputEmail1" placeholder="NIP Bendahara BOS">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tgl. Mulai Aktif</label>
                        <input type="text" id="tglaktif" name="tglaktif" class="form-control" placeholder="Tgl. Mulai Aktif" required="required">
                      </div>


                      
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>

                <script>
                  $(document).ready(function () {
                    $('#tglaktif').datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true,
                      defaultViewDate: "today",
                      todayHighlight: true
                    }); 
                      $("#e1").selectpicker();
                          
                  });
                </script>
