<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Maaf, Sistem sedang dalam perbaikan.</title>
</head>
<body style="background-color:green;color:white;">
    <center>
        <h1>
            Mohon maaf atas ketidaknyamanan anda.<br>
            Untuk sementara waktu sistem sedang dalam perbaikan.<br>
            Atas pengertiannya, kami ucapkan terima kasih.

        </h1>
    </center>
    
</body>
</html>