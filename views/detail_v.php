<?php foreach ($lowongan as $row) {
    # code...
} $link = set_permalink($row->lowongan_id,$row->judul_lowongan);?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta content="<?php echo strip_tags($row->judul_lowongan); ?>" name="description"/>
    <meta content="<?php echo base_url('home/'.$row->lowongan_id."-").$link.".html"; ?>" property="og:url"/>
    <meta content="<?php echo strip_tags($row->judul_lowongan); ?>" property="og:title"/>
    <meta content="<?php echo strip_tags($row->judul_lowongan); ?>" property="og:description"/>

    <title>Lapak Kerja Riau </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .content-section-a{margin-top: 68px;}
    .list li{list-style-type: square;}
    </style>

</head>

<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation" style="padding:8px;">
        <div class="container topnav">
            <!--<div class="col-md-11" style="margin:0px auto;border:0px solid;float:none;">-->
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="<?php echo base_url(); ?>"><h3 style="padding:0px;margin:0px;"><i class="fa fa-home"></i> Lapak Kerja Riau</h3></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="font-size:17px;">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url(); ?>">Home</a>
                    </li>
                    <li>
                        <a href="#services">Tentang Kami</a>
                    </li>
                    <li>
                        <a href="#contact">Kontak</a>
                    </li>
                </ul>
            </div>
            <!--</div>-->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->

    <div class="content-section-a">
        <div class="container" style="background:#fff;">
            <!--<div class="col-md-11" style="margin:0px auto;border:0px solid;float:none;background:#fff;">-->
            <div class="row">
                <div class="col-md-8" style="border-right:1px solid #F5F5F5;">
                    <br>
                    <p style="font-size:13px;"><a href="">Home</a> <i class="fa fa-angle-double-right"></i> <a href="">Lowongan Kerja Oktober 2017</a> <i class="fa fa-angle-double-right"></i>
                        <a href="">Lowongan Kerja Pekanbaru</a> <i class="fa fa-angle-double-right"></i> <a href="">Lowongan Kerja Riau</a> <i class="fa fa-angle-double-right"></i>
                        <?php echo $row->judul_lowongan; ?>
                    </p>
                    <hr>
                    <h3 class="section-heading"><?php echo strip_tags($row->judul_lowongan); ?></h3>
                    <p class="lead">
                        <?php
                            echo strip_tags($row->isi_lowongan,'<p><br><img><blockquote><a><b><i><u><strong>&nbsp;');
                        ?>
                    </p>

                    
                    <div class="panel panel-warning" style="border:0px;">
                        <div class="panel-heading" style="font-size:18px;"><I>Lowongan Lainnya:</I></div>
                        <div class="panel-body">
                            <ul class="list">
                                <li style="font-size:15px;">LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017
                                    <p style="font-size:12px;color:#777777;">LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017 - PT Bank Riau sedang membutuhkan tenaga kerja dengan kualifikasi sebagai berikut...</p>
                                </li style="font-size:15px;"><hr>
                                <li>LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017
                                    <p style="font-size:12px;color:#777777;">LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017 - PT Bank Riau sedang membutuhkan tenaga kerja..</p>
                                </li><hr>
                                <li style="font-size:15px;">LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017
                                    <p style="font-size:12px;color:#777777;">LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017 - PT Bank Riau sedang membutuhkan tenaga kerja..</p>
                                </li><hr>
                                <li style="font-size:15px;">LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017
                                    <p style="font-size:12px;color:#777777;">LOWONGAN PT BANK RIAU PEKANBARU RIAU 2017 - PT Bank Riau sedang membutuhkan tenaga kerja..</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <hr>
                    <h5 class="section-heading">KOMENTAR UNTUK <?php echo strip_tags($row->judul_lowongan); ?></h5>
                    <div class="fb-comments" data-href="http://disdik.riau.go.id/home/berita/1751-riau-berhasil-bawa-4-emas-dalam-ajang-o2sn-" data-numposts="5" data-width="100%"></div>
                </div>


                <div class="col-md-4">
                    
                    <p class="lead">
                    <h4>Lowongan Minggu ini:</h4> 
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#terkini"><i class="fa fa-bell"></i> Terkini</a></li>
                            <li><a data-toggle="tab" href="#populer"><i class="fa fa-star"></i> Populer</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="terkini" class="tab-pane fade in active">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <ul class="list-unstyled mb-0">
                                            <?php
                                                //foreach ($lowongan as $row) {
                                                    //$link = set_permalink($row->lowongan_id,$row->judul_lowongan);
                                                    //echo "<a href='".base_url('home/'.$link.".html")."'><li>$row->judul_lowongan</li></a><hr>";
                                               // }
                                            ?>
                                            <li class="list-group-item"><a href="">PT ABC MAJU JAYA PRATAMA TERKINI </a></li>
                                            <li class="list-group-item">PT ABC MAJU JAYA PRATAMA</li>
                                            <li class="list-group-item">PT ABC MAJU JAYA PRATAMA</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div id="populer" class="tab-pane fade">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <ul class="list-unstyled mb-0">
                                            <li class="list-group-item">PT ABC MAJU JAYA PRATAMA TERKINI </li>
                                            <li class="list-group-item">PT ABC MAJU JAYA PRATAMA</li>
                                            <li class="list-group-item">PT ABC MAJU JAYA PRATAMA</li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading" style="font-size:18px;">Kategori Lowongan:</div>
                                    <div class="panel-body">
                                        <?php
                                            foreach ($pendidikan as $baris) {
                                                echo "<a href='#' class='btn btn-default'>Lowongan Kerja ".$baris->jenjang."</a> ";
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-success">
                                <div class="panel-heading" style="font-size:18px;">Lowongan Kabupaten:</div>
                                    <div class="panel-body">
                                        <?php
                                            foreach ($kabupaten as $rows) {
                                                echo "<a href='#' class='btn btn-default'>Lowongan Kerja ".$rows->nama_kab."</a> ";
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>

                        


                        
                    </p>     
                </div>
            </div>
            <!--</div>-->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <!-- Footer -->
    <footer style="background-color:#2C3E50;">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#about">Tentang Kami</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#services">Kontak</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; Lapak Kerja Riau 2017. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

</body>

</html>
