<div class="container">
<br><br><br><br>

<style type="text/css">
  .tabel{text-align: left;font-size: 11px;}
  .tabel th{text-align: center;}
  .tabel td{padding: 5px;}
  .kanan{text-align: right;}
  .tengah{text-align: center;}
  table{border-collapse: collapse; border:1px solid #fbfbfb;}

</style>
<div class="box box-primary">
  <div class="box-header with-border">
  <H4>Lihat RKA Sekolah</H4><hr>
  <!-- FORM -->
  <form class="form-inline" method="post" id="form_lihat">
    <div class="form-group">
      <label for="email">Tahun:</label>
      <select name="tahun" id="tahun" class="form-control">
        <?php
          foreach($data_tahun as $data_tahuns){
            echo "<option value='$data_tahuns->tahun'>$data_tahuns->tahun</option>";
          }
        ?>
      </select>
    </div>
    <div class="form-group">
      <label for="pwd">Nama Sekolah:</label>
      <select name="sekolah" id="sekolah" class="form-control">
          <?php
            foreach($data_sekolah as $data_sekolahs){
              echo "<option value='$data_sekolahs->npsn'>$data_sekolahs->nama_sp</option>";
            }
        ?>  
      </select>
    </div>
    <button type="submit" class="btn btn-success"><span id="eye"></span><span id="spin"></span> <span id="tombol"></span></button>
  </form>
  <br>

  <div id="show_data">
  
  </div>


<BR>
</div>
</div>

</div>

<script>
  $(document).ready(function(){
    $("#sekolah").select2();
    $("#spin").html("");
    $("#tombol").html("Tampilkan");
    $("#eye").html("<i class='fa fa-eye'></i>");
    $('#form_lihat').on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var inputData = new FormData($(this)[0]);
        var tahun = $('#tahun').val();
        var npsn = $('#sekolah').val();
        var url = "<?php echo base_url('rkas/show/'); ?>";
        // alert(tahun);
        // return false;
        $("#spin").html("<i class='fa fa-spinner fa-spin' style='font-size:20px;'></i>");
        $("#eye").html("");
        $("#tombol").html("Loading...");
        $('#show_data').html("");
        $.ajax({
          url: url,
          type: "post",
          data: inputData,
          processData: false,
          contentType: false,
          cache: false,
          success: function(data){
            $('#show_data').html(data);
            $("#spin").html("");
            $("#eye").html("<i class='fa fa-eye'></i>");
            $("#tombol").html("Tampilkan");
          }
        });

        return false;
      }
    });
  });
</script>