<!-- Default box -->

<div class="box">

  <div class="box-header with-border">

    <h3 class="box-title">Data Prestasi Sekolah</h3>
  </div>
  <div class="box-body">
            <div class="box-body">

              <table id="pres" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tahun</th>
                  <th>Nama Sekolah</th>
                  <th>Nama Lomba</th>
                  <th>Cabang Lomba</th>
                  <th>Ket. Lomba</th>
                  <th>Jenis Lomba</th>
                  <th>Tingkat</th>
                  <th>Peringkat</th>
                  <th>Nama Siswa</th>
                  <th>Tempat Lomba</th>
                  <th>Penyelenggara</th>
                  <th>Tgl. Mulai</th>
                  <th>Tgl. Akhir</th>

                </tr>
                </thead>
                <tbody>
                
                </tbody>
                
              </table>
            </div>

  </div>

</div>

<script>
            var save_method; //for save method string
            var table;

            $(document).ready(function() {
                //datatables
                table = $('#pres').DataTable({ 
                    "responsive": true,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [[0,"desc"]], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('supervisor/json_prestasi/'); ?>",
                        "type": "POST"
                    },
                    //Set column definition initialisation properties.
                    "columns": [
                        {"data": "tahun"},
                        {"data": "nama_sp"},
                        {"data": "namalomba"},
                        {"data": "nama_katlomba"},
                        {"data": "uraian"},
                        {"data": "jenis_lomba"},
                        {"data": "tingkat_lomba"},
                        {"data": "peringkat"},
                        {"data": "nama_siswa"},
                        {"data": "tempat_lomba"},
                        {"data": "penyelenggara"},
                        {"data": "tgl_mulai"},
                        {"data": "tgl_akhir"}
                        
                    ],

                });

            });
</script>