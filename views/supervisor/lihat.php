<div class="box">
  	<div class="box-header with-border">
    	<h3 class="box-title">Data Aset Sekolah</h3>
  	</div>
  	<div class="box-body">
  		
<?php
	$gettahun = $this->input->get('tahun');
	$getkab = $this->input->get('kab');
	$getsekolah= $this->input->get('sekolah');
	if(!isset($gettahun)){

		echo "<div class='col-md-4 col-sm-8 col-xs-12'>
	          <div class='info-box'>
	            <span class='info-box-icon bg-aqua'><i class='fa fa-calendar'></i></span>

	            <div class='info-box-content'>
	              <span class='info-box-text'>ASET SEKOLAH</span>
	              <span class='info-box-number'>2018</span>
	              <span class='info-box-text'><a href='".base_url('supervisor/aset?tahun=2018')."' class='btn btn-success btn-xs'>Detail &raquo;</a></span>
	            </div>
	          </div>
	    </div>";
	}else{
		if(!isset($getkab)){
		//jika ada get kab
?>
			<p><input type="submit" class="btn btn-warning" value="&laquo; Kembali" onclick="back()"></p>

			<h2>Aset Tahun: <?php echo $this->input->get('tahun'); ?></h2>
			<table border="1" class="table table-bordered table-striped">
				<thead>
					<tr> 
						<th>Nama Kabupaten</th>
						<th>Total Aset KIB-B</th>
						<th>Total Aset KIB-E</th>
						<th>Total (KIB-B + KIB-E)</th>
						<th>Action</th>
					</tr>
				</thead>
			<?php
				echo $this->input->get('sekolah');
				$kab = $this->webadmin_model->kabupaten();
				foreach ($kab as $kabs) {
					echo $this->webadmin_model->aset_perkab('kib_b',$kabs->kd_kab);
				}
			?>
			</table>

		<?php }else{ 
			// jika ada get kab
				if(!isset($getsekolah)){
					echo "<p><input type='submit' class='btn btn-warning' value='&laquo; Kembali' onclick='back()'></p>";
					echo "<table border='1' class='table table-bordered table-striped'>";
					echo $this->webadmin_model->sekolah($getkab);
					echo "</table>";
				}else{
					//tampilkan data aset sekolah
					// echo "<p><a href='' class='btn btn-primary'>Kembali</a></p>";
					echo "<p><input type='submit' class='btn btn-warning' value='&laquo; Kembali' onclick='back()'></p>";
					$sekolah = $this->input->get('sekolah');
					echo $this->webadmin_model->header_aset($sekolah);
					echo "<br><p><a href='".base_url('supervisor/downloadaset/'.$gettahun.'/'.$getsekolah)."' class='btn btn-success'><span class='fa fa-file-excel-o'> Download</span></a></p>";
					echo "<h2>Data KIB-B</h2>";
					echo "<table border='1' class='table table-bordered table-striped'>";
					echo "<thead>
						<tr>
							<th>Kode Barang</th>
							<th>No. Register</th>
							<th>Nama Barang</th>
							<th>Bahan</th>
							<th>Jumlah Barang</th>
							<th>Satuan</th>
							<th>Harga Perolehan (Rp)</th>
							<th>Tahun Pembelian</th>
							<th>Sumber Dana</th>
							<th>Ket</th>

						</tr>
					</thead>";
					$detail = $this->webadmin_model->detail_aset('kib_b',$sekolah);
					foreach ($detail as $details) {
						echo "<tr>
							<td>$details->kode_barang</td>
							<td>$details->no_register</td>
							<td>$details->nama_barang</td>
							<td>$details->bahan</td>
							<td>$details->jumlah_barang</td>
							<td>$details->satuan</td>
							<td>$details->harga_satuan</td>
							<td>$details->tahun_pembelian</td>
							<td>$details->nama_sumber_dana</td>
							<td>$details->ket</td>

						</tr>";
					}
					echo "</table>";

					//kib e
					echo "<BR><h2>Data KIB-E</h2>";
					echo "<table border='1' class='table table-bordered table-striped'>";
					echo "<thead>
						<tr>
							<th>Kode Barang</th>
							<th>No. Register</th>
							<th>Nama Barang</th>
							<th>Judul Buku</th>
							<th>Spesifikasi</th>
							<th>Jumlah Barang</th>
							<th>Harga Perolehan (Rp)</th>
							<th>Tahun Pembelian</th>
							<th>Sumber Dana</th>
							<th>Ket</th>

						</tr>
					</thead>";
					$detail = $this->webadmin_model->detail_aset('kib_e',$sekolah);
					foreach ($detail as $details) {
						echo "<tr>
							<td>$details->kode_barang</td>
							<td>$details->no_register</td>
							<td>$details->nama_barang</td>
							<td>$details->judul</td>
							<td>$details->spesifikasi</td>
							<td>$details->jumlah</td>
							<td>$details->harga_satuan</td>
							<td>$details->tahun_pembelian</td>
							<td>$details->nama_sumber_dana</td>
							<td>$details->ket</td>

						</tr>";
					}
					echo "</table>";

				}
			
		?>

<?php 	}
	} 
?>


	</div>
</div>
<script>
	function back(){
		window.history.back();
	}
	$(function () {
		$(".table").DataTable();
	});
</script>