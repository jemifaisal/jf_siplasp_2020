<div class="box">
  	<div class="box-header with-border">
    	<h3 class="box-title">Data TAMSIL Pegawai</h3>
  	</div>
  	<div class="box-body">
  		
<?php
	$gettahun = $this->input->get('tahun');
	$getkab = $this->input->get('kab');
    $getsekolah= $this->input->get('sekolah');
    $periode= $this->input->get('periode');
    $url = $_SERVER['REQUEST_URI'];
	if(!isset($gettahun)){
        foreach($tamsil_tahun as $tamsil_tahuns){
            echo "<div class='col-md-4 col-sm-8 col-xs-12'>
                <div class='info-box'>
                    <span class='info-box-icon bg-aqua'><i class='fa fa-calendar'></i></span>

                    <div class='info-box-content'>
                    <span class='info-box-text'>TAMSIL Pegawai</span>
                    <span class='info-box-number'>$tamsil_tahuns->tahun</span>
                    <span class='info-box-text'>Periode: $tamsil_tahuns->periode</span>
                    <span class='info-box-text'><a href='".base_url('supervisor/tamsil?tahun='.$tamsil_tahuns->tahun.'&periode='.$tamsil_tahuns->periode)."' class='btn btn-success btn-xs'>Detail &raquo;</a></span>
                    </div>
                </div>
            </div>";
        }
	}else{
		if(!isset($getkab)){
		//jika ada get kab
?>
			<p><input type="submit" class="btn btn-warning" value="&laquo; Kembali" onclick="back()"></p>

			<h3 class="info-box-title">Tahun: <?php echo $this->input->get('tahun'); ?> | Periode: <?php echo $this->input->get('periode'); ?></h3>
			<table border="1" class="table table-bordered table-striped">
				<thead>
					<tr> 
						<th>Nama Kabupaten</th>
						<th>Action</th>
					</tr>
				</thead>
			<?php
				echo $this->input->get('sekolah');
				$kab = $this->webadmin_model->kabupaten();
				foreach ($kab as $kabs) {
                    echo "<tr>
                        <td>$kabs->nama_kab</td>
                        <td><a href='$url&kab=$kabs->kd_kab' class='btn btn-success btn-xs'><span class='fa fa-eye'></span> Lihat</a>
                        <a href='".base_url('supervisor/downloadtamsil/'.$kabs->kd_kab.'/'.$gettahun.'/'.$periode.'/'.'kab')."' class='btn btn-success btn-xs'><span class='fa fa-download'></span> Download Data</a>
                        </td>
                    </tr>";
				}
			?>
			</table>

		<?php }else{ 
			// jika ada get kab
				if(!isset($getsekolah)){
					echo "<p><input type='submit' class='btn btn-warning' value='&laquo; Kembali' onclick='back()'></p>";
                    echo "<table border='1' class='table table-bordered table-striped'>";
                    echo "<thead>
                        <tr>
                            <th>Kabupaten / Kota</th>
                            <th>Nama Sekolah</th>
                            <th>Jenjang</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>";
                    $this->db->join('kec','kec.kd_kec=sekolah.kd_kec');
                    $this->db->join('kab','kab.kd_kab=kec.kd_kab');
                    $this->db->where('kec.kd_kab',$getkab);
                    $sql = $this->db->get('sekolah');
                    if($sql){
                        foreach($sql->result() as $sqls){
                            echo "<tr>
                                <td>$sqls->nama_kab</td>
                                <td>$sqls->nama_sp</td>
                                <td>$sqls->jenjang</td>
                                <td>$sqls->status_sekolah</td>
                                <td><a href='".$url."&sekolah=$sqls->sekolah_id' class='btn btn-success btn-xs'>Detail</a></td>
                            </tr>";
                        }
                    }
					//echo $this->webadmin_model->sekolah($getkab);
					echo "</table>";
				}else{
					//tampilkan data aset sekolah
					// echo "<p><a href='' class='btn btn-primary'>Kembali</a></p>";
					$this->db->where('tamsil_file.tahun',$gettahun);
                    $this->db->where('tamsil_file.periode',$periode);
                    $this->db->where('tamsil_file.sekolah_id',$getsekolah);
                    $qfile = $this->db->get('tamsil_file');
                    if($qfile){
                        $qfiles = $qfile->row_array();
                        $file = $qfiles['file_tamsil'];
                    }
					echo "<p><input type='submit' class='btn btn-warning' value='&laquo; Kembali' onclick='back()'></p>";
					$sekolah = $this->input->get('sekolah');
					echo $this->webadmin_model->header_aset_tamsil($sekolah);
					echo "<br><p><a href='".base_url('supervisor/downloadtamsil/'.$getsekolah.'/'.$gettahun.'/'.$periode.'/'.'sekolah')."' class='btn btn-success'><span class='fa fa-file-excel-o'> Download</span></a>
					    <a target='_blank' href='".base_url('assets/filetamsil/')."".$file."' class='btn btn-warning'><span class='fa fa-file-pdf-o'> Lihat File</span></a>
					</p>";
					// echo "<h2>Data KIB-B</h2>";
					echo "<table border='1' class='table table-bordered table-striped'>";
					echo "<thead>
						<tr>
                            <th>NIP</th>
                            <th>NUPTK</th>
                            <th>Nama Guru</th>
                            <th>Golongan</th>
                            <th>PDD. Terakhir</th>
                            <th>Jurusan</th>
                            <th>Jumlah Jam Mengajar</th>
                            <th>Mata Pelajaran</th>
                            <th>No. Rekening Bank Riau Kepri</th>
                            <th>Nama Pada Rekening</th>
                            <th>No. HP</th>
						</tr>
					</thead>";
					$this->db->join('gtk','gtk.gtk_id=tamsil.gtk_id');
                    $this->db->join('pangkat','pangkat.kd_pangkat=gtk.kd_pangkat');
                    $this->db->join('pdd_terakhir','pdd_terakhir.pdd_terakhir_id=gtk.pdd_terakhir_id');
                    $this->db->where('tamsil.sekolah_id',$getsekolah);
                    $this->db->where('tamsil.tahun',$gettahun);
                    $this->db->where('tamsil.periode',$periode);
                    $this->db->where('tamsil.deleted_at',NULL);
                    $pns = $this->db->get('tamsil');
                    if($pns){
                        foreach($pns->result() as $row) {
                            echo "<tr>
                                <td>$row->nip</td>
                                <td>$row->nuptk</td>
                                <td>$row->gelar_depan $row->nama_lengkap, $row->gelar_belakang</td>
                                <td>$row->golongan</td>
                                <td>$row->nama_pdd</td>
                                <td>$row->jurusan</td>
                                <td>$row->jumlah_jam_mengajar</td>
                                <td>$row->mapel</td>
                                <td>$row->no_rekening</td>
                                <td>$row->nama_rekening</td>
                                <td>$row->no_hp</td>

                            </tr>";
                        }
                    }

                    echo "</table>";

				}
			
		?>

<?php 	}
	} 
?>


	</div>
</div>
<script>
	function back(){
		window.history.back();
	}
	$(function () {
		$(".table").DataTable();
	});
</script>