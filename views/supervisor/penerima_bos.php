<!-- Default box -->

<div class="box">

  <div class="box-header with-border">

    <h3 class="box-title">Data Prestasi Sekolah</h3>
  </div>
  <div class="box-body">
            <div class="box-body">

              <table id="pres" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Input RKAS</th>
                  <th>Tahun</th>
                  <th>NPSN</th>
                  <th>Nama Sekolah</th>
                  <th>Kabupaten</th>
                  <th>Kecamatan</th>
                  <th>Jenjang</th>
                  <th>Jumlah Siswa</th>
                  <th>Total BOS</th>
                  <th>Sisa Uang</th>
                  <th>Grand Total BOS</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0;
                    foreach ($bos as $row) {
                      $no++;
                      $jumlah_siswa = $row->kelas_i+$row->kelas_ii+$row->kelas_iii+$row->kelas_iv+$row->kelas_v+$row->kelas_vi+$row->kelas_vii+$row->kelas_viii+$row->kelas_ix+$row->kelas_x+$row->kelas_xi+$row->kelas_xii;

                      $sid = $row->sekolah_id;
                      if($sid){
                        $this->db->where('sekolah_id',$sid);
                        $sql = $this->db->get('rincian_belanja');
                        if($sql->num_rows($sql) > 0){
                          $rkas = "<i class='btn btn-primary btn-xs'>Sudah</i>";
                        }else{
                          $rkas = "<i class='btn btn-danger btn-xs'>Belum</i>";
                        }
                      }
                      

                      if($row->jenjang == "SMA" or $row->jenjang =="SMK"){
                        $total_bos = $jumlah_siswa * 1400000;  
                      }else{
                        if($jumlah_siswa < 60){
                          $total_bos = 120000000; 
                        }else{
                            $total_bos = $jumlah_siswa * 2000000;  
                        }
                        
                      }
                      
                      echo "<tr>
                        <td>$no</td>
                        <td>$rkas</td>
                        <td>$row->tahun</td>
                        <td><a href='".base_url('supervisor/rkasekolah/'.$row->sekolah_id.'/'.$row->tahun)."'>$row->npsn</a></td>
                        <td>$row->nama_sp</td>
                        <td>$row->nama_kab</td>
                        <td>$row->nama_kec</td>
                        <td>$row->jenjang - $row->status_sekolah</td>
                        <td>$jumlah_siswa</td>
                        <td>".number_format($total_bos,0,',','.')."</td>
                        <td>".number_format($row->sisa_uang,0,',','.')."</td>
                        <td>".number_format($row->sisa_uang+$total_bos,0,',','.')."</td>
                      </tr>";
                    }
                  ?>
                
                </tbody>
                
              </table>
            </div>

  </div>

</div>

<div id="addsisauang" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Form Sisa Uang</h4>
      </div>
      <div class="modal-body">
        <div class="box">
          
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
          
  $(document).ready(function() {
    $("#pres").DataTable({
      responsive: true
    });    

    $(document).on('click','#add',function(e){
      e.preventDefault();
      $("#addsisauang").modal('show');
      $.post("<?php echo base_url('webadmin/edit_sisa_uang'); ?>",
          {id:$(this).attr('data-id')},
          function(html){
              $(".modal-body").html(html);
          }   
      );
    });

  });
</script>