<!DOCTYPE html>
<html>
<head>
	<title>Download Excel</title>
</head>
<body>
	<table border="1">
		<tr>
			<td colspan="16" align="center"><h3>LAPORAN ASET SEKOLAH <?php echo strtoupper($nama_sekolah); ?><br> KIB B<br>SUMBER DANA BOSNAS<br>TAHUN <?php echo $tahun; ?></h3></td>
		</tr>
		<tr>
			<th>No.</th>
			<th>Kode Barang</th>
			<th>No. Register</th>
			<th>Nama Barang</th>
			<th>Merk</th>
			<th>CC</th>
			<th>Bahan</th>
			<th>Pabrik</th>
			<th>Rangka</th>
			<th>Mesin</th>
			<th>No. Polisi</th>
			<th>BPKB</th>
			<th>Jumlah Barang</th>
			<th>Harga Satuan</th>
			<th>Total Harga</th>
			<th>Keterangan</th>
		</tr>
		<?php
			$no = 0;
			foreach ($kib_b_bosnas as $row1) {
				$no++;
				if($row1->ukuran_cc == 0){
					$cc = '-';
				}else{
					$cc = $row1->ukuran_cc;
				}
				echo "<tr>
					<td>$no</td>
					<td>$row1->kode_barang</td>
					<td>$row1->no_register</td>
					<td>$row1->nama_barang</td>
					<td>$row1->merk_type</td>
					<td>$cc</td>
					<td>$row1->bahan</td>
					<td>$row1->pabrik</td>
					<td>$row1->rangka</td>
					<td>$row1->mesin</td>
					<td>$row1->no_polisi</td>
					<td>$row1->bpkb</td>
					<td>$row1->jumlah_barang</td>
					<td>$row1->harga_perolehan</td>
					<td>".$row1->jumlah_barang * $row1->harga_perolehan."</td>
					<td>$row1->ket</td>
				</tr>";
			}
		?>
		<tr>
			<th colspan="14">GRAND TOTAL</th>
			<th><?php echo $total_kib_b_bosnas; ?></th>
			<th>&nbsp;</th>
		</tr>
	</table>
	<BR><BR>
	<table border="1">
		<tr>
			<td colspan="16" align="center"><h3>LAPORAN ASET SEKOLAH <?php echo strtoupper($nama_sekolah); ?><br> KIB B<br>SUMBER DANA BOSDA<br>TAHUN <?php echo $tahun; ?></h3></td>
		</tr>
		<tr>
			<th>No.</th>
			<th>Kode Barang</th>
			<th>No. Register</th>
			<th>Nama Barang</th>
			<th>Merk</th>
			<th>CC</th>
			<th>Bahan</th>
			<th>Pabrik</th>
			<th>Rangka</th>
			<th>Mesin</th>
			<th>No. Polisi</th>
			<th>BPKB</th>
			<th>Jumlah Barang</th>
			<th>Harga Satuan</th>
			<th>Total Harga</th>
			<th>Keterangan</th>
		</tr>
		<?php
			$no = 0;
			foreach ($kib_b_bosda as $row2) {
				$no++;
				if($row2->ukuran_cc == 0){
					$cc = '-';
				}else{
					$cc = $row2->ukuran_cc;
				}
				echo "<tr>
					<td>$no</td>
					<td>$row2->kode_barang</td>
					<td>$row2->no_register</td>
					<td>$row2->nama_barang</td>
					<td>$row2->merk_type</td>
					<td>$cc</td>
					<td>$row2->bahan</td>
					<td>$row2->pabrik</td>
					<td>$row2->rangka</td>
					<td>$row2->mesin</td>
					<td>$row2->no_polisi</td>
					<td>$row2->bpkb</td>
					<td>$row2->jumlah_barang</td>
					<td>$row2->harga_perolehan</td>
					<td>".$row2->jumlah_barang * $row2->harga_perolehan."</td>
					<td>$row2->ket</td>
				</tr>";
			}
		?>
		<tr>
			<th colspan="14">GRAND TOTAL</th>
			<th><?php echo $total_kib_b_bosda; ?></th>
			<th>&nbsp;</th>
		</tr>
	</table>
	<BR><BR>
	<table border="1">
		<tr>
			<td colspan="13" align="center"><h3>LAPORAN ASET SEKOLAH <?php echo strtoupper($nama_sekolah); ?><br> KIB E<br>SUMBER DANA BOSNAS<br>TAHUN <?php echo $tahun; ?></h3></td>
		</tr>
		<tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Kode Barang</th>
			<th rowspan="2">No. Register</th>
			<th rowspan="2">Nama Barang</th>
			<th colspan="2">Buku / Perpustakaan</th>
			<th colspan="3">Barang Bercorak</th>
			<th rowspan="2">Kuantitas</th>
			<th rowspan="2">Harga Satuan</th>
			<th rowspan="2">Total Harga</th>
			<th rowspan="2">Keterangan</th>
		</tr>
		<tr>
			<th>Judul Buku</th>
			<th>Bahan</th>
			<th>Spesifikasi</th>
			<th>Asal Daerah</th>
			<th>Pencipta</th>
		</tr>
		
		<?php
			$no = 0;
			foreach($kib_e_bosnas as $row3){
				$no++;
				echo "<tr>
					<td>$no</td>
					<td>$row3->kode_barang</td>
					<td>$row3->no_register</td>
					<td>$row3->nama_barang</td>
					<td>$row3->judul</td>
					<td>$row3->bahan</td>
					<td>$row3->spesifikasi</td>
					<td>$row3->asal_daerah</td>
					<td>$row3->pencipta</td>
					<td>$row3->jumlah</td>
					<td>$row3->harga_perolehan</td>
					<td>".$row3->harga_perolehan * $row3->jumlah."</td>
					<td>$row3->ket</td>
				</tr>";
			}
		?>

		<tr>
			<th colspan="11">GRAND TOTAL</th>
			<th><?php echo $total_kib_e_bosnas; ?></th>
			<th>&nbsp;</th>
		</tr>
	</table>

	<BR><BR>
	<table border="1">
		<tr>
			<td colspan="13" align="center"><h3>LAPORAN ASET SEKOLAH <?php echo strtoupper($nama_sekolah); ?><br> KIB E<br>SUMBER DANA BOSDA<br>TAHUN <?php echo $tahun; ?></h3></td>
		</tr>
		<tr>
			<th rowspan="2">No.</th>
			<th rowspan="2">Kode Barang</th>
			<th rowspan="2">No. Register</th>
			<th rowspan="2">Nama Barang</th>
			<th colspan="2">Buku / Perpustakaan</th>
			<th colspan="3">Barang Bercorak</th>
			<th rowspan="2">Kuantitas</th>
			<th rowspan="2">Harga Satuan</th>
			<th rowspan="2">Total Harga</th>
			<th rowspan="2">Keterangan</th>
		</tr>
		<tr>
			<th>Judul Buku</th>
			<th>Bahan</th>
			<th>Spesifikasi</th>
			<th>Asal Daerah</th>
			<th>Pencipta</th>
		</tr>
		
		<?php
			$no = 0;
			foreach($kib_e_bosda as $row4){
				$no++;
				echo "<tr>
					<td>$no</td>
					<td>$row4->kode_barang</td>
					<td>$row4->no_register</td>
					<td>$row4->nama_barang</td>
					<td>$row4->judul</td>
					<td>$row4->bahan</td>
					<td>$row4->spesifikasi</td>
					<td>$row4->asal_daerah</td>
					<td>$row4->pencipta</td>
					<td>$row4->jumlah</td>
					<td>$row4->harga_perolehan</td>
					<td>".$row4->harga_perolehan * $row4->jumlah."</td>
					<td>$row4->ket</td>
				</tr>";
			}
		?>

		<tr>
			<th colspan="11">GRAND TOTAL</th>
			<th><?php echo $total_kib_e_bosda; ?></th>
			<th>&nbsp;</th>
		</tr>
	</table>

</body>
</html>