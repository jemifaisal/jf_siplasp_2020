          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Sarpras Sekolah</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th rowspan="2">No</th>
                  <th rowspan="2">NPSN</th>
                  <th rowspan="2">Nama Sekolah</th>
                  <th colspan="5">Ruang Kelas</th>
                  <th colspan="5">Ruang Labor</th>
                </tr>
                <tr>
                  <td>Jumlah Kelas</td>
                  <td>Kondisi Baik</td>
                  <td>Rusak Ringan</td>
                  <td>Rusak Sedang</td>
                  <td>Rusak Berat</td>
                  <td>Jumlah Labor</td>
                  <td>Kondisi Baik</td>
                  <td>Rusak Ringan</td>
                  <td>Rusak Sedang</td>
                  <td>Rusak Berat</td>
                </tr>
                </thead>
                <tbody>
                <?php $no=1; foreach ($sarpras as $row) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><a href='<?php echo base_url('supervisor/detailsarpras/'.$row->sekolahid); ?>'><?php echo $row->npsn; ?></a></td>
                  <td><a href='<?php echo base_url('supervisor/detailsarpras/'.$row->sekolahid); ?>'><?php echo $row->nama_sp; ?></a></td>
                  <td><?php echo $row->jumlah_kelas; ?></td>
                  <td><?php echo $row->k_baik; ?></td>
                  <td><?php echo $row->k_rr; ?></td>
                  <td><?php echo $row->k_rs; ?></td>
                  <td><?php echo $row->k_rb; ?></td>
                  <td><?php echo $row->j_labor; ?></td>
                  <td><?php echo $row->l_baik; ?></td>
                  <td><?php echo $row->l_rr; ?></td>
                  <td><?php echo $row->l_rs; ?></td>
                  <td><?php echo $row->l_rb; ?></td>
                  
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <script>
            $(function () {
              $("#example1").DataTable();

              $(document).on('click','#aksi',function(e){
                e.preventDefault();
                $("#formaksi").modal('show');
                $.post("<?php echo base_url('webadmin/aksi_sekolah'); ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
              });

            });
          </script>
